import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt/dist/jwt.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import keycloakConfig from '@suite5/core/authentication/authentication.module.config';
import starterKitDemoConfig from '@suite5/core/authentication/demo.config';
import { Blacklist, Occupation, Skill, User } from '@suite5/core/user/entities';
import { VaultService } from '@suite5/vault';
import {
  BlacklistController,
  BlacklistService,
  FileController,
  OccupationController,
  OccupationService,
  SkillController,
  SkillService,
  UserController,
  UserModule,
  UserService,
} from './user';
import { AuthenticationModule } from '@suite5/core/authentication/authentication.module';
import {
  AUTHENTICATION_STRATEGY_TOKEN,
  KeycloakAuthenticationStrategy,
} from '@suite5/core/authentication/strategies';
import {
  AuthenticationMailService,
  AuthenticationService,
} from '@suite5/core/authentication/services';
import { AuthenticationController } from '@suite5/core/authentication/controllers';
import { CommonModule } from '@suite5/common/common.module';
import {
  CLOUD_USER_TOKEN,
  FakeCloudUserStrategy,
  RealCloudUserStrategy,
} from './user/strategies';

@Module({
  imports: [
    UserModule,
    CommonModule,
    AuthenticationModule,
    TypeOrmModule.forFeature([User, Occupation, Skill, Blacklist]),
    HttpModule,
    ConfigModule.forFeature(keycloakConfig),
    ConfigModule.forFeature(starterKitDemoConfig),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secret: config.get<string>('keycloak.module.secret'),
        signOptions: {
          expiresIn: config.get<string>('keycloak.module.expiration_time'),
        },
      }),
    }),
  ],
  providers: [
    UserService,
    OccupationService,
    SkillService,
    VaultService,
    BlacklistService,
    AuthenticationService,
    {
      provide: AUTHENTICATION_STRATEGY_TOKEN,
      useClass: KeycloakAuthenticationStrategy,
    },
    AuthenticationMailService,
    {
      provide: CLOUD_USER_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeCloudUserStrategy
          : RealCloudUserStrategy,
    },
  ],
  exports: [
    UserService,
    OccupationService,
    SkillService,
    VaultService,
    BlacklistService,
    AuthenticationService,
  ],
  controllers: [
    AuthenticationController,
    UserController,
    FileController,
    OccupationController,
    SkillController,
    BlacklistController,
  ],
})
export class CoreModule {}
