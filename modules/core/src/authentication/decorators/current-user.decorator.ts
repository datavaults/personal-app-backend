import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { KeycloakUser } from '@suite5/core/authentication/entities';

export type UserData = KeycloakUser;

/* istanbul ignore next */
export const CurrentUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    if (ctx.getType() === 'http') {
      const [req] = ctx.getArgs(); // eslint-disable-line @typescript-eslint/no-unused-vars
      return req.user;
    }

    throw new UnauthorizedException();
  },
);
