import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import * as R from 'ramda';
import { Connection } from 'mongoose';

/* istanbul ignore next */
export const MongoConnection = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): Connection => {
    if (ctx.getType() === 'http') {
      const [req] = ctx.getArgs(); // eslint-disable-line @typescript-eslint/no-unused-vars
      return R.has('mongoConnection', req) && !R.isNil(req.mongoConnection)
        ? req.mongoConnection
        : null;
    }

    throw new UnauthorizedException();
  },
);
