export class PasswordStrength {
  static readonly TOO_WEAK = new PasswordStrength(0, 'Too weak', 0, 0);
  static readonly WEAK = new PasswordStrength(1, 'Weak', 2, 6);
  static readonly MEDIUM = new PasswordStrength(2, 'Medium', 4, 8);
  static readonly STRONG = new PasswordStrength(3, 'Strong', 4, 10);

  private id: number;
  private value: string;
  private minDiversity: number;
  private minLength: number;

  constructor(
    id: number,
    value: string,
    minDiversity: number,
    minLength: number,
  ) {
    this.id = id;
    this.value = value;
    this.minDiversity = minDiversity;
    this.minLength = minLength;
  }

  public static all() {
    return [this.TOO_WEAK, this.WEAK, this.MEDIUM, this.STRONG];
  }

  public static rules(): {
    id: number;
    value: string;
    minDiversity: number;
    minLength: number;
  }[] {
    return PasswordStrength.all().map((option: PasswordStrength) => {
      return {
        id: option.id,
        value: option.value,
        minDiversity: option.minDiversity,
        minLength: option.minLength,
      };
    });
  }
}
