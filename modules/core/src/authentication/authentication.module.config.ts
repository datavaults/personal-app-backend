import { registerAs } from '@nestjs/config';
import * as R from 'ramda';
import { v4 as uuidv4 } from 'uuid';
import * as fs from 'fs';

export default registerAs('keycloak.module', () => ({
  baseURL: process.env.KEYCLOAK_BASE_URL,
  realm: process.env.KEYCLOAK_REALM,
  username: process.env.KEYCLOAK_USERNAME,
  password: process.env.KEYCLOAK_PASSWORD,
  keycloakSecret: process.env.KEYCLOAK_CLIENT_SECRET,
  clientId: process.env.KEYCLOAK_CLIENT_ID,
  secret: process.env.JWT_SECRET,
  expiration_time: process.env.JWT_EXPIRATION_TIME,
  verifyEmailUrl: process.env.VERIFY_EMAIL_URL,
  verifyResetPasswordUrl: process.env.VERIFY_RECOVER_PASSWORD_URL,
  get publicKey(): string {
    const prefix = '-----BEGIN PUBLIC KEY-----';
    const suffix = '-----END PUBLIC KEY-----';

    // If no public key provided return random
    if (
      R.isNil(process.env.KEYCLOAK_PUBLIC_KEY) ||
      process.env.KEYCLOAK_PUBLIC_KEY.trim() == ''
    ) {
      return uuidv4();
    }

    // If it's a valid path
    if (fs.existsSync(process.env.KEYCLOAK_PUBLIC_KEY)) {
      const key: string = fs.readFileSync(
        process.env.KEYCLOAK_PUBLIC_KEY,
        'utf8',
      );
      if (!R.isEmpty(key) && key.trim() != '') {
        return key;
      }

      return uuidv4();
    }

    // Otherwise use configured string as key and prefix/suffix public key
    return `${prefix}
${process.env.KEYCLOAK_PUBLIC_KEY}
${suffix}`;
  },
}));
