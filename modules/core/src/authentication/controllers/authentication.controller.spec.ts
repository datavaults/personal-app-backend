import { HttpException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { MailService } from '@suite5/common';
import { UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { UserService } from '@suite5/core/user';
import { VaultService } from '@suite5/vault';
import { Request, Response } from 'express';
import * as mocks from 'node-mocks-http';
import { AuthenticationController } from '.';
import { ChangePasswordDTO, CredentialsDTO } from '../dto';

describe('AuthController', () => {
  let controller: AuthenticationController;
  let service: any;
  let configService: any;
  let req: Request;
  let res: Response;
  let credentialData: CredentialsDTO;
  let token: any;
  let userService: any;
  let user;
  let mongoConnection: any;
  let mailService: any;

  beforeEach(async () => {
    mongoConnection = jest.fn();
    req = mocks.createRequest();
    req.res = mocks.createResponse();
    res = req.res;

    user = {
      username: 'test@test.org',
    } as UserData;

    credentialData = {
      username: 'test@test.org',
      password: '1',
    };
    token = {
      access_token: 'sdjfsdfjhs',
      refresh_token: 'ksdflksjdfl',
      expires_in: '1223',
    };

    service = {
      login: () => jest.fn(),
      logout: () => jest.fn(),
      authenticate: () => jest.fn(),
      register: () => jest.fn(),
      verify: () => jest.fn(),
      resetPassword: () => jest.fn(),
      checkPasswordStrength: () => jest.fn(),
      changePassword: () => jest.fn(),
      sendVerificationEmail: () => jest.fn(),
      sendResetPasswordEmail: () => jest.fn(),
      setUserEmailToVerified: () => jest.fn(),
      verifyToken: () => jest.fn(),
    };

    userService = {
      isExistByEmail: () => jest.fn(),
      createUser: () => jest.fn(),
      retrieveByEmail: () => jest.fn(),
    };

    mailService = {
      send: () => jest.fn(),
      sendToSupport: () => jest.fn(),
    };

    configService = jest.fn();

    controller = new AuthenticationController(
      service,
      userService,
      configService,
      mailService,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should change password 1', async () => {
    const userData = {
      username: user.email,
    } as UserData;
    const changePasswordData = {
      newPassword: 'test',
      oldPassword: 'test',
    } as ChangePasswordDTO;
    // jest.spyOn(userService, 'isExistByEmail').mockResolvedValue(false);
    jest.spyOn(service, 'checkPasswordStrength').mockImplementation();
    jest.spyOn(service, 'login').mockImplementation();
    jest.spyOn(service, 'changePassword').mockImplementation();
    await controller.changePassword(userData, changePasswordData);
  });

  it('should change password 2', async () => {
    const userData = {
      username: user.email,
    } as UserData;
    const changePasswordData = {
      newPassword: 'test',
      oldPassword: 'test',
    } as ChangePasswordDTO;
    // jest.spyOn(userService, 'isExistByEmail').mockResolvedValue(false);
    jest.spyOn(service, 'checkPasswordStrength').mockImplementation();
    jest.spyOn(service, 'login').mockImplementation(() => {
      throw new Error();
    });
    jest.spyOn(service, 'changePassword').mockImplementation();
    try {
      await controller.changePassword(userData, changePasswordData);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toEqual(
        'The old password you have entered is incorrect',
      );
    }
  });
  it('should change password 3', async () => {
    const userData = {
      username: user.email,
    } as UserData;
    const changePasswordData = {
      newPassword: 'test',
      oldPassword: 'test',
    } as ChangePasswordDTO;
    // jest.spyOn(userService, 'isExistByEmail').mockResolvedValue(false);
    jest.spyOn(service, 'checkPasswordStrength').mockImplementation();
    jest.spyOn(service, 'login').mockImplementation();
    jest.spyOn(service, 'changePassword').mockImplementation(() => {
      throw new Error();
    });
    try {
      await controller.changePassword(userData, changePasswordData);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toEqual(
        'Something went wrong, please try again later,the password was not changed',
      );
    }
  });
});
