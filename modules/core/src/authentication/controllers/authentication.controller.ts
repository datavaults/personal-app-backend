import {
  Body,
  Controller,
  HttpCode,
  HttpException,
  Inject,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { MailService } from '@suite5/common';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { MongoConnection } from '@suite5/core/authentication/decorators/mongo-connection.decorator';
import { KeycloakUser } from '@suite5/core/authentication/entities';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { UserService } from '@suite5/core/user/services/user.service';
import { Request, Response } from 'express';
import { Connection } from 'mongoose';
import ms from 'ms';
import { MongoInterceptor } from '../../../../../src/interceptors';
import { Cookies } from '../constants';
import { ChangePasswordDTO } from '../dto';
import { AuthenticationGuard } from '../guards';

@Controller('auth')
@ApiTags('auth')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
export class AuthenticationController {
  constructor(
    @Inject(AuthenticationService)
    private readonly service: AuthenticationService,
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(ConfigService)
    private readonly configService: ConfigService,
    @Inject(MailService)
    private readonly mailService: MailService,
  ) {}

  @Post('keycloakLogin')
  @ApiUnauthorizedResponse()
  @ApiOperation({ summary: 'Login with Keycloak' })
  @UseInterceptors(MongoInterceptor)
  @UseGuards(AuthenticationGuard)
  async keycloakLogin(
    @MongoConnection() mongoConnection: Connection,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const keycloakUser = req.user as KeycloakUser;
    await this.service.keycloakLogin(mongoConnection, keycloakUser);

    res.cookie(Cookies.Token, req.body.accessToken, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
      expires: new Date(new Date(Date.now() + ms(keycloakUser.expiry))),
    });
    res.cookie(Cookies.RefreshToken, req.body.refreshToken, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
      expires: new Date(new Date(Date.now() + ms(keycloakUser.expiry))),
    });

    res.send({
      access_token: req.body.accessToken,
      refresh_token: req.body.refreshToken,
    });
  }

  @Post('logout')
  @UseGuards(AuthenticationGuard)
  @HttpCode(205)
  @ApiUnauthorizedResponse()
  @ApiHeader({
    name: 'X-Refresh-Token',
    description: 'Refresh Token from KEYCLOAK',
  })
  @ApiBearerAuth()
  @ApiResponse({ status: 205, description: 'Reset Content' })
  @ApiOperation({ summary: 'Logout' })
  async logout(@Req() req: Request, @Res() res: Response): Promise<any> {
    const refreshFromCookie =
      req?.cookies[Cookies.RefreshToken] || req.header('X-Refresh-Token') || '';
    res.clearCookie(Cookies.Token, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
    });
    res.clearCookie(Cookies.RefreshToken, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
    });
    // const logoutResponse = await this.service.logout(refreshFromCookie);
    // if (logoutResponse === true) {
    //   res.send({
    //     statusCode: 205,
    //     message: 'Success',
    //   });
    // } else {
    //   res.sendStatus(401);
    // }

    res.send();
  }

  @Put('changepassword')
  @UseGuards(AuthenticationGuard)
  @ApiCreatedResponse({ description: 'Change Password Email' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @ApiOperation({ summary: 'changepassword' })
  @ApiBody({ type: ChangePasswordDTO })
  async changePassword(
    @CurrentUser() user: UserData,
    @Body() data: ChangePasswordDTO,
  ) {
    await this.service.checkPasswordStrength(data.newPassword);
    try {
      await this.service.login(user.username, data.oldPassword);
    } catch (error) {
      throw new HttpException(
        'The old password you have entered is incorrect',
        401,
      );
    }
    try {
      await this.service.changePassword(user.sub, data.newPassword);
    } catch (error) {
      throw new HttpException(
        'Something went wrong, please try again later,the password was not changed',
        401,
      );
    }
  }
}
