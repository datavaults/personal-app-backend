import { Inject, Injectable } from '@nestjs/common';
import { MailService } from '@suite5/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthenticationMailService {
  constructor(
    @Inject(MailService)
    private readonly mailService: MailService,
    @Inject(ConfigService)
    private readonly configService: ConfigService,
  ) {}

  async sendEmailVerification(email: string, token: string): Promise<void> {
    await this.mailService.send(email, 'Email Verification', './verify-email', {
      'verify-link': `${this.configService.get(
        'keycloak.module.verifyEmailUrl',
      )}${token}`,
    });
  }

  async sendResetPasswordEmail(email: string, token: string): Promise<void> {
    await this.mailService.send(
      email,
      'Password Reset Request 🔒',
      './reset-password-email',
      {
        'verify-link': `${this.configService.get(
          'keycloak.module.verifyResetPasswordUrl',
        )}${token}`,
      },
    );
  }
}
