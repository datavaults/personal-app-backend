import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationMailService } from './authentication-mail.service';
import { ConfigService } from '@nestjs/config';
import { MailService } from '@suite5/common';

describe('AuthenticationService', () => {
  let service: AuthenticationMailService;
  let mailService: MailService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
        {
          provide: MailService,
          useClass: jest.fn(() => ({
            send: () => jest.fn(),
            sendToSupport: () => jest.fn(),
          })),
        },
        AuthenticationMailService,
      ],
    }).compile();

    service = await module.get<AuthenticationMailService>(
      AuthenticationMailService,
    );
    mailService = await module.get<MailService>(MailService);
    configService = await module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(mailService).toBeDefined();
    expect(configService).toBeDefined();
  });

  it(`should sendEmailVerification`, () => {
    const email = 'test@test.org';
    const token = 'abcdefg';
    jest.spyOn(configService, 'get').mockReturnValue('http://foo/');
    jest.spyOn(mailService, 'send').mockImplementation();
    service.sendEmailVerification(email, token);
    expect(mailService.send).toBeCalledWith(
      email,
      'Email Verification',
      './verify-email',
      {
        'verify-link': `http://foo/${token}`,
      },
    );
  });

  it(`should send reset password email`, () => {
    const email = 'test@test.org';
    const token = 'abcdefg';
    jest.spyOn(configService, 'get').mockReturnValue('http://foo/');
    jest.spyOn(mailService, 'send').mockImplementation();
    service.sendResetPasswordEmail(email, token);
    expect(mailService.send).toBeCalledWith(
      email,
      'Password Reset Request 🔒',
      './reset-password-email',
      {
        'verify-link': `${configService.get(
          'keycloak.module.verifyResetPasswordUrl',
        )}${token}`,
      },
    );
  });
});
