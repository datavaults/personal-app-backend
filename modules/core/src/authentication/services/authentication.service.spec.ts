import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '@suite5/core/user/entities';
import { UserService } from '@suite5/core/user/services';
import { CredentialsDTO } from '../dto';
import {
  AUTHENTICATION_STRATEGY_TOKEN,
  KeycloakUserInfoResponse,
} from '../strategies/authentication.strategy';
import { KeycloakAuthenticationStrategy } from '../strategies/keycloak.strategy';
import {
  AuthenticationError,
  AuthenticationService,
} from './authentication.service';
import { ConfigService } from '@nestjs/config';
import { MailService } from '@suite5/common';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let mockedUserService: any;
  let authenticationStrategy: KeycloakAuthenticationStrategy;
  let userInfo: KeycloakUserInfoResponse;
  let jwtService: JwtService;
  let mongoConnection: any;
  let mockedMailService: any;
  let mockConfigService: any;
  let mockedAuthenticationStrategy: any;

  beforeEach(async () => {
    mongoConnection = jest.fn();
    userInfo = {
      sub: 'hkfjsdhkfjhsdkfj-s928dajfksdj-hkdjfhskd',
      email_verified: true,
      name: 'test',
      preferred_username: 'test',
      given_name: '',
      family_name: '',
      email: 'test@test.com',
    } as KeycloakUserInfoResponse;
    mockedUserService = jest.fn(() => ({
      login: () => jest.fn(),
      retrieveBySub: () => jest.fn(),
      retrieveByEmail: () => jest.fn(),
      setEmailVerificationToken: () => jest.fn(),
      createUser: () => jest.fn(),
    }));
    mockedAuthenticationStrategy = {
      login: () => jest.fn(),
      logout: () => jest.fn(),
      authenticate: () => jest.fn(),
      register: () => jest.fn(),
      verify: () => jest.fn(),
      resetPassword: () => jest.fn(),
      changePassword: () => jest.fn(),
      changeEmail: () => jest.fn(),
      setUserEmailToVerified: () => jest.fn(),
      sendResetPasswordEmail: () => jest.fn(),
      deleteUser: () => jest.fn(),
    };

    mockedMailService = {
      send: () => jest.fn(),
      sendToSupport: () => jest.fn(),
    };

    mockConfigService = {
      get: () => jest.fn(),
    };
    

    service = new AuthenticationService(
      mockedAuthenticationStrategy,
      mockedUserService,
      jwtService,
      mockedMailService,
      mockConfigService,
    );

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

//   it('should login valid user', async () => {
//     const credential = {
//       username: 'test@test.com',
//       password: 'test',
//     } as CredentialsDTO;
//     jest.spyOn(authenticationStrategy, 'login').mockImplementation();
//     await service.login(credential.username, credential.password);
//     expect(authenticationStrategy.login).toBeCalledWith(
//       credential.username,
//       credential.password,
//     );
//   });

//   it('should change password', async () => {
//     const credential = {
//       id: 'fksjldkfjsl-sdjfhskdf-sdkjfhksd',
//       password: 'ksdjflksdjf',
//     };
//     jest.spyOn(authenticationStrategy, 'changePassword').mockImplementation();
//     await service.changePassword(credential.id, credential.password);
//     expect(authenticationStrategy.changePassword).toBeCalledWith(
//       credential.id,
//       credential.password,
//     );
//   });

//   it('should change email', async () => {
//     const credential = {
//       id: 'fksjldkfjsl-sdjfhskdf-sdkjfhksd',
//       email: 'test@test.com',
//     };
//     jest.spyOn(authenticationStrategy, 'changeEmail').mockImplementation();
//     jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(new User());
//     await service.changeEmail(credential.id, credential.email);
//     expect(authenticationStrategy.changeEmail).toBeCalledWith(
//       expect.any(User),
//       credential.email,
//     );
//   });

//   it('should logout valid user', async () => {
//     const refreshToken = 'sdfnsdjfnksjdhk';
//     jest.spyOn(authenticationStrategy, 'logout').mockImplementation();
//     await service.logout(refreshToken);
//     expect(authenticationStrategy.logout).toBeCalledWith(refreshToken);
//   });

//   it('should checkPasswordStrength Weak', async () => {
//     expect((await service.checkPasswordStrength('fsjhfksjdf')).id).toBeLessThan(
//       2,
//     );
//   });
//   it('should checkPasswordStrength medium to strong', async () => {
//     expect(
//       (await service.checkPasswordStrength('JH1234SJKSDsdfsd!@')).id,
//     ).toBeGreaterThanOrEqual(2);
//   });
// });
})