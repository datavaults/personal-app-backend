import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
} from '@nestjs/common';
import * as R from 'ramda';
import { JwtService } from '@nestjs/jwt/dist/jwt.service';
import { CreateUserDTO } from '@suite5/core/user/dto/create-user.dto';
import { User } from '@suite5/core/user/entities';
import { UserService } from '@suite5/core/user/services/user.service';
import { passwordStrength } from 'check-password-strength';
import { PasswordStrength } from '../constants';
import {
  AUTHENTICATION_STRATEGY_TOKEN,
  AuthenticationStrategy,
} from '../strategies/authentication.strategy';
import { Connection } from 'mongoose';
import { MailService } from '@suite5/common';
import { ConfigService } from '@nestjs/config';
import { KeycloakUser } from '@suite5/core/authentication/entities';

export class AuthenticationError extends Error {}

@Injectable()
export class AuthenticationService {
  constructor(
    @Inject(AUTHENTICATION_STRATEGY_TOKEN)
    private readonly strategy: AuthenticationStrategy,
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(JwtService)
    private readonly jwtService: JwtService,
    @Inject(MailService)
    private readonly mailService: MailService,
    @Inject(ConfigService)
    private readonly configService: ConfigService,
  ) {}

  async logout(refreshToken: string): Promise<boolean> {
    return this.strategy.logout(refreshToken);
  }

  async changePassword(id: string, password: string): Promise<boolean> {
    return await this.strategy.changePassword(id, password);
  }

  async changeEmail(sub: string, email: string): Promise<boolean> {
    const user: User = await this.userService.retrieveBySub(sub);
    return await this.strategy.changeEmail(user, email);
  }

  async login(
    username: string,
    password: string,
  ): Promise<{
    refresh_token: string;
    access_token: string;
    expires_in: number;
  }> {
    return this.strategy.login(username, password);
  }

  async keycloakLogin(
    mongoConnection,
    keycloakUser: KeycloakUser,
  ): Promise<User> {
    if (!keycloakUser.emailVerified) {
      throw new ForbiddenException(
        `User with email ${keycloakUser.email} has not verified their email`,
      );
    }
    const user = await this.userService.retrieveBySub(keycloakUser.sub, true);
    if (user) {
      return user;
    }

    if (await this.userService.retrieveByEmail(keycloakUser.email, true)) {
      throw new BadRequestException(
        `User with email ${keycloakUser.email} already exists`,
      );
    }

    return await this.userService.createUser(mongoConnection, {
      email: keycloakUser.email,
      sub: keycloakUser.sub,
    } as CreateUserDTO);
  }

  checkPasswordStrength(password: string): { id: number; value: string } {
    return passwordStrength(password, PasswordStrength.rules() as any);
  }

  async keycloakDeleteUser(id:any){
    return await this.strategy.deleteUser(id);
  }
}
