export interface KeycloakUser {
  id: number;
  sub: string;
  email: string;
  username: string;
  emailVerified: boolean;
  expiry: number;
}
