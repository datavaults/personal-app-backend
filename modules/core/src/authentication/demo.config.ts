import { registerAs } from '@nestjs/config';

export default registerAs('starterkitdemo', () => ({
  enabled:
    process.env.STARTER_KIT_DEMO &&
    process.env.STARTER_KIT_DEMO.trim() === 'true',
  user: process.env.STARTER_KIT_DEMO_USER,
}));
