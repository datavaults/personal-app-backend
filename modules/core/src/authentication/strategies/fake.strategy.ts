import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import {
  AuthenticationStrategy,
  KeycloakUserInfoResponse,
} from './authentication.strategy';
import { User } from '@suite5/core/user/entities';

@Injectable()
export class FakeAuthenticationStrategy implements AuthenticationStrategy {
  setUserEmailToVerified(token: string): boolean | PromiseLike<boolean> {
    throw new Error('Method not implemented.');
  }
  changeEmail(user: User, email: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  changePassword(id: string, password: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  verify(user: User): Promise<any> {
    throw new Error('Method not implemented.');
  }
  sendResetPasswordEmail(id: string): Promise<any> {
    throw new Error('Method not implemented.');
  }
  register(username: string, password: string): Promise<string> {
    throw new Error('Method not implemented.');
  }
  async logout(refreshToken: string): Promise<boolean> {
    return false;
  }

  async login(
    username: string,
    password: string,
  ): Promise<{
    refresh_token: string;
    access_token: string;
    expires_in: number;
  }> {
    return Promise.resolve({
      access_token: '',
      refresh_token: '',
      expires_in: 0,
    });
  }
  /**
   * Blindly trust the JWT, assume it has the Keycloak structure and return the decoded payload
   */
  public authenticate(accessToken: string): Promise<KeycloakUserInfoResponse> {
    return Promise.resolve(jwt.decode(accessToken) as KeycloakUserInfoResponse);
  }

  async deleteUser(userId: string) {
    return true;
  }
}
