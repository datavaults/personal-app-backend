import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Cookies } from '@suite5/core/authentication/constants';
import { KeycloakUser } from '@suite5/core/authentication/entities';
import { UserService } from '@suite5/core/user';
import { ExtractJwt, Strategy } from 'passport-jwt';
import * as R from 'ramda';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  private userService: UserService;

  constructor(
    @Inject(ConfigService) configService: ConfigService,
    @Inject(UserService) userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req) => {
          if (!req) return null;
          const authorisationHeaderKey: string | undefined = Object.keys(
            req.headers,
          ).find(
            (headerKey: string) =>
              headerKey.toLowerCase().trim() === 'authorization',
          );
          if (authorisationHeaderKey && req?.headers[authorisationHeaderKey]) {
            if (R.is(Array, req.headers[authorisationHeaderKey])) {
              return req.headers[authorisationHeaderKey][0]
                .replace('Bearer', '')
                .trim();
            }
            return (req.headers[authorisationHeaderKey] as string)
              .replace('Bearer', '')
              .trim();
          }

          return null;
        },
        (req) => {
          let token = null;
          if (req && req.body && R.has('accessToken', req.body)) {
            token = req.body.accessToken;
          }
          return token;
        },
        (req) => {
          return req?.cookies[Cookies.Token];
        },
      ]),
      secretOrKey: configService.get<string>('keycloak.module.publicKey'),
    });
    this.userService = userService;
  }

  async validate(payload: any): Promise<KeycloakUser> {
    const user = await this.userService.retrieveBySub(payload.sub, true);

    return {
      id: !!user ? user.id : null,
      sub: payload.sub,
      email: payload.email,
      username: payload.preferred_username,
      emailVerified: payload.email_verified,
      expiry: payload.exp,
    };
  }
}
