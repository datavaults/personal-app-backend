import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Cookies } from '@suite5/core/authentication/constants';
import { KeycloakUser } from '@suite5/core/authentication/entities';
import { UserService } from '@suite5/core/user';
import { Strategy } from 'passport-custom';
import * as R from 'ramda';

@Injectable()
export class DemoStrategy extends PassportStrategy(Strategy, 'demo') {
  private userService: UserService;
  private configService: ConfigService;

  constructor(
    @Inject(ConfigService) configService: ConfigService,
    @Inject(UserService) userService: UserService,
  ) {
    super();
    this.userService = userService;
    this.configService = configService;
  }

  async validate(): Promise<KeycloakUser> {
    if (this.configService.get('starterkitdemo.enabled')) {
      const user = await this.userService.retrieveBySub(
        this.configService.get('starterkitdemo.user'),
        true,
      );

      return {
        id: !!user ? user.id : null,
        sub: user.sub,
        email: user.email,
        username: user.email,
        emailVerified: true,
        expiry: null,
      };
    }
    return null;
  }
}
