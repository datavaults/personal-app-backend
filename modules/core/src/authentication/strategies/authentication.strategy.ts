import { User } from '@suite5/core/user/entities';

export const AUTHENTICATION_STRATEGY_TOKEN = 'AuthenticationStrategy';

export interface KeycloakUserInfoResponse {
  sub: string;
  email_verified: boolean;
  name: string;
  preferred_username: string;
  given_name: string;
  family_name: string;
  email: string;
}

export interface AuthenticationStrategy {
  setUserEmailToVerified(token: string): boolean | PromiseLike<boolean>;
  authenticate(accessToken: string): Promise<KeycloakUserInfoResponse>;
  logout(refreshToken: string): Promise<boolean>;
  login(
    username: string,
    password: string,
  ): Promise<{
    access_token: string;
    refresh_token: string;
    expires_in: number;
  }>;
  register(username: string, password: string): Promise<string>;
  verify(id: User): Promise<any>;
  sendResetPasswordEmail(id: string): Promise<any>;
  changePassword(id: string, password: string): Promise<boolean>;
  changeEmail(user: User, email: string): Promise<boolean>;
  deleteUser(id: string): Promise<any>;
}
