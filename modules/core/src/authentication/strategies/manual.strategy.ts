import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as jwt from 'jsonwebtoken';
import { map } from 'rxjs/operators';
import {
  AuthenticationStrategy,
  KeycloakUserInfoResponse,
} from './authentication.strategy';
import { User } from '@suite5/core/user/entities';

export class InvalidTokenPublicKeyId extends Error {
  constructor(keyId: string) {
    super(`Invalid public key ID ${keyId}`);
  }
}

/**
 * Format of the keys returned in the JSON response from Keycloak for the list of public keys
 */
interface KeycloakCertsResponse {
  keys: KeycloakKey[];
}
interface KeycloakKey {
  kid: KeyId;
  x5c: PublicKey;
}
type KeyId = string;
type PublicKey = string;

@Injectable()
export class ManualAuthenticationStrategy implements AuthenticationStrategy {
  /**
   * Keep an in-memory map of the known public keys to avoid calling Keycloak every time
   */
  private readonly keysMap: Map<KeyId, PublicKey> = new Map<KeyId, PublicKey>();

  private readonly KEYCLOAK_URL = this.configService.get(
    'keycloak.module.baseURL',
  );
  private readonly KEYCLOAK_REALM = this.configService.get(
    'keycloak.module.realm',
  );

  constructor(
    private httpService: HttpService,

    private readonly configService: ConfigService,
  ) {}
  sendResetPasswordEmail(id: string): Promise<void> {
    throw new Error('Method not implemented.');
  }
  setUserEmailToVerified(token: string): boolean | PromiseLike<boolean> {
    throw new Error('Method not implemented.');
  }
  changeEmail(user: User, email: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  changePassword(id: string, password: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  verify(user: User): Promise<void> {
    throw new Error('Method not implemented.');
  }
  resetPassword(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  register(username: string, password: string): Promise<string> {
    throw new Error('Method not implemented.');
  }

  async logout(refreshToken: string): Promise<boolean> {
    const params = new URLSearchParams();
    params.append('refresh_token', refreshToken);
    params.append('client_id', 'admin-cli');
    return await this.httpService
      .post(
        `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/logout`,
        params,
        {
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .toPromise()
      .then((response) => {
        return response.status - 200 > 0 && response.status - 200 < 100;
      })
      .catch(() => {
        return false;
      });
  }

  async login(
    username: string,
    password: string,
  ): Promise<{
    refresh_token: string;
    access_token: string;
    expires_in: number;
  }> {
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    params.append('grant_type', 'password');
    params.append('client_id', 'admin-cli');
    return await this.httpService
      .post(
        `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/token`,
        params,
        {
          headers: {
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
        },
      )
      .toPromise()
      .then((resp) => resp.data)
      .catch(() => ({ refresh_token: '', access_token: '', expires_in: 0 }));
  }

  public async authenticate(
    accessToken: string,
  ): Promise<KeycloakUserInfoResponse> {
    const token = jwt.decode(accessToken, { complete: true }); // For once, we'd like to have the header and not just the payload

    const keyId = token.header.kid;

    const publicKey = await this.getPublicKey(keyId);
    return jwt.verify(accessToken, publicKey) as KeycloakUserInfoResponse;
  }

  private async getPublicKey(keyId: KeyId): Promise<PublicKey> {
    if (this.keysMap.has(keyId)) {
      return this.keysMap.get(keyId);
    } else {
      const keys = await this.httpService
        .get<KeycloakCertsResponse>(
          `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/certs`,
        )
        .pipe(map((response) => response.data.keys))
        .toPromise();

      const key = keys.find((k) => k.kid === keyId);

      if (key) {
        const publicKey = `
-----BEGIN CERTIFICATE-----
${key.x5c}
-----END CERTIFICATE-----
`;
        this.keysMap.set(keyId, publicKey);

        return publicKey;
      } else {
        // Token is probably so old, Keycloak doesn't even advertise the corresponding public key anymore
        throw new InvalidTokenPublicKeyId(keyId);
      }
    }
  }

  async deleteUser(userId: string) {
    return true;
  }
}
