import { MailerService } from '@nestjs-modules/mailer/dist/mailer.service';
import {
  ForbiddenException,
  forwardRef,
  HttpService,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '@suite5/core/user';
import KcAdminClient from 'keycloak-admin';
import { AuthenticationService } from '../services/authentication.service';
import {
  AuthenticationStrategy,
  KeycloakUserInfoResponse,
} from './authentication.strategy';
import { AuthenticationMailService } from '@suite5/core/authentication/services';
import { User } from '@suite5/core/user/entities';
@Injectable()
export class KeycloakAuthenticationStrategy implements AuthenticationStrategy {
  private readonly KEYCLOAK_URL = this.configService.get(
    'keycloak.module.baseURL',
  );
  private readonly KEYCLOAK_REALM = this.configService.get(
    'keycloak.module.realm',
  );

  private readonly KEYCLOAK_CLIENT_ID = this.configService.get(
    'keycloak.module.clientId',
  );

  private readonly kcAdminClient = new KcAdminClient({
    baseUrl: this.KEYCLOAK_URL,
    realmName: this.KEYCLOAK_REALM,
  });

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    private readonly authService: AuthenticationService,

    @Inject(forwardRef(() => JwtService))
    private readonly jwtService: JwtService,
    @Inject(forwardRef(() => MailerService))
    private readonly mailerService: MailerService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private readonly authenticationMailService: AuthenticationMailService,
  ) {}

  async setUserEmailToVerified(email: string): Promise<boolean> {
    try {
      await this.keycloakLogin();
      const user = await this.userService.retrieveByEmail(email);
      await this.kcAdminClient.users.update(
        { id: user.sub },
        {
          emailVerified: true,
        },
      );
      return true;
    } catch (error) {
      return false;
    }
  }

  async changePassword(id: string, password: string): Promise<boolean> {
    try {
      await this.keycloakLogin();
      await this.kcAdminClient.users.resetPassword({
        id,
        credential: {
          temporary: false,
          type: 'password',
          value: password,
        },
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  async changeEmail(user: User, email: string): Promise<boolean> {
    try {
      await this.keycloakLogin();
      await this.kcAdminClient.users.update(
        { id: user.sub },
        {
          email: email,
          username: email,
        },
      );
      await this.verify(user);
      return true;
    } catch {
      return false;
    }
  }

  async verify(user: User): Promise<void> {
    const token = this.jwtService.sign({
      email: user.email,
    });
    await this.authenticationMailService.sendEmailVerification(
      user.email,
      token,
    );
    await this.userService.setEmailVerificationToken(user.email, token);

    if (!!token) {
      await this.kcAdminClient.users.update(
        { id: user.sub },
        {
          emailVerified: false,
        },
      );
    }
  }

  async sendResetPasswordEmail(id: string): Promise<any> {
    await this.keycloakLogin();
    const user = await this.kcAdminClient.users.findOne({
      id,
    });
    const token = this.jwtService.sign({
      email: user.email,
    });
    await this.authenticationMailService.sendResetPasswordEmail(
      user.email,
      token,
    );

    await this.userService.setPasswordVerificationToken(user.email, token);
    return token;
  }

  private async keycloakLogin() {
    try {
      await this.kcAdminClient.auth({
        username: this.configService.get('keycloak.module.username'),
        password: this.configService.get('keycloak.module.password'),
        grantType: 'password',
        clientId: 'admin-cli',
        clientSecret: this.configService.get('keycloak.module.keycloakSecret'),
      });
    } catch (error) {
      console.error(error);
    }
  }

  async register(username: string, password: string): Promise<string> {
    await this.keycloakLogin();
    const savedId = (
      (await this.kcAdminClient.users.create({
        realm: this.KEYCLOAK_REALM,
        username: username,
        email: username,
        emailVerified: false,
        enabled: true,
      })) || { id: null }
    ).id;

    await this.kcAdminClient.users.resetPassword({
      id: savedId,
      credential: {
        temporary: false,
        type: 'password',
        value: password,
      },
    });
    return savedId;
  }

  async logout(refreshToken: string): Promise<boolean> {
    const params = new URLSearchParams();
    params.append('refresh_token', refreshToken);
    params.append('client_id', this.KEYCLOAK_CLIENT_ID);
    return await this.httpService
      .post(
        `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/logout`,
        params,
        {
          headers: {
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
        },
      )
      .toPromise()
      .then((response) => {
        return response.status - 200 > 0 && response.status - 200 < 100;
      })
      .catch((e) => {
        return false;
      });
  }

  async login(
    username: string,
    password: string,
  ): Promise<{
    refresh_token: string;
    access_token: string;
    expires_in: number;
  }> {
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    params.append('grant_type', 'password');
    params.append('client_id', 'admin-cli');
    params.append(
      'client_secret',
      this.configService.get('keycloak.module.keycloakSecret'),
    );

    return await this.httpService
      .post(
        `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/token`,
        params,
        {
          headers: {
            accept: 'application/x-www-form-urlencoded',
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        const userInfo: KeycloakUserInfoResponse = await this.authenticate(
          resp.data.access_token,
        );

        if (!userInfo)
          throw new NotFoundException(
            'Email and password do not match an existing user',
          );
        if (!userInfo.email_verified) {
          const user = await this.userService.retrieveByEmail(userInfo.email);

          await this.verify(user);
          throw new ForbiddenException(
            `User with email ${userInfo.email} has not verified their email`,
          );
        }
        return resp.data;
      });
  }

  /**
   * Call the OpenId Connect UserInfo endpoint on Keycloak: https://openid.net/specs/openid-connect-core-1_0.html#UserInfo
   *
   * If it succeeds, the token is valid and we get the user infos in the response
   * If it fails, the token is invalid or expired
   */
  async authenticate(accessToken: string): Promise<KeycloakUserInfoResponse> {
    const url = `${this.KEYCLOAK_URL}/realms/${this.KEYCLOAK_REALM}/protocol/openid-connect/userinfo`;
    return await this.httpService
      .get<KeycloakUserInfoResponse>(url, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .toPromise()
      .then((resp) => resp.data)
      .then((data) => {
        return data;
      });
  }

  async deleteUser(userId: string) {
    await this.keycloakLogin();
    return await this.kcAdminClient.users.del({ id: userId });
  }
}
