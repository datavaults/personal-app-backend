import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt/dist/jwt.module';
import { UserModule } from '../user';
import keycloakConfig from './authentication.module.config';
import { AuthenticationController } from './controllers';
import { AuthenticationGuard } from './guards/authentication.guard';
import { AuthenticationService } from './services/authentication.service';
import { AUTHENTICATION_STRATEGY_TOKEN } from './strategies/authentication.strategy';
import { KeycloakAuthenticationStrategy } from './strategies/keycloak.strategy';
import { CommonModule } from '@suite5/common/common.module';
import { AuthenticationMailService } from '@suite5/core/authentication/services';
import { JwtStrategy } from '@suite5/core/authentication/strategies/jwt.strategy';
import { DemoStrategy } from '@suite5/core/authentication/strategies/demo.strategy';

@Module({
  imports: [
    forwardRef(() => UserModule),
    CommonModule,
    HttpModule,
    ConfigModule.forFeature(keycloakConfig),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secret: config.get<string>('keycloak.module.secret'),
        signOptions: {
          expiresIn: config.get<string>('keycloak.module.expiration_time'),
        },
      }),
    }),
  ],
  providers: [
    AuthenticationMailService,
    AuthenticationGuard,
    AuthenticationService,
    {
      provide: AUTHENTICATION_STRATEGY_TOKEN,
      useClass: KeycloakAuthenticationStrategy,
    },
    JwtStrategy,
    DemoStrategy,
  ],
  controllers: [AuthenticationController],
  exports: [AuthenticationGuard, AuthenticationService],
})
export class AuthenticationModule {}
