import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ResetPasswordDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly token: any;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly password: string;
}
