import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ChangePasswordDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly oldPassword: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly newPassword: string;
}
