import { ApiProperty } from '@nestjs/swagger';
import { IsValidEmail } from '@suite5/common';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class EmailDTO {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty()
  @IsValidEmail()
  readonly email: string;
}
