import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SkillService } from '@suite5/core/user/services/skill.service';
import { Repository } from 'typeorm';
import { Skill } from '../entities';

describe('SkillService', () => {
  let service: SkillService;

  let repository: Repository<Skill>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Skill),
          useClass: Repository,
        },
        SkillService,
      ],
    }).compile();
    repository = module.get<Repository<Skill>>(getRepositoryToken(Skill));

    service = module.get<SkillService>(SkillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve all Skills', async () => {
    jest.spyOn(repository, 'find').mockImplementation();

    await service.getSkills();
    expect(repository.find).toBeCalled();
  });

  it('should retrieve an Skill', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    await service.retrieve(1);
    expect(repository.findOneOrFail).toBeCalled();
    expect(repository.findOneOrFail).toBeCalledWith({ id: 1 });
  });
});
