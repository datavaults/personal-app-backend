import { HttpModule, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { OccupationService } from '@suite5/core/user/services/occupation.service';
import { SkillService } from '@suite5/core/user/services/skill.service';
import { VaultService } from '@suite5/vault';
import mongoose from 'mongoose';
import { Repository } from 'typeorm';
import { UpdateEmailDTO, UpdateUserDTO } from '../dto';
import { Occupation, Skill } from '../entities';
import { User } from '../entities/user.entity';
import { CLOUD_USER_TOKEN } from '../strategies';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let repository: Repository<User>;
  let user: User;
  let mockLogger: any;
  let eventEmitter: any;
  let mockOccupationService;
  let mockSkillService;
  let mockRepository;
  let mockStrategy: any;

  beforeEach(async () => {
    user = new User();
    user.email = 'test@test.org';
    user.qualificationsList = [];
    user.occupationsList = [];

    mockLogger = jest.fn(() => ({
      debug: () => jest.fn(),
      log: () => jest.fn(),
    }));

    mockOccupationService = jest.fn(() => ({
      retrieve: () => new Occupation(),
    }));
    mockSkillService = jest.fn(() => ({
      retrieve: () => new Skill(),
    }));

    eventEmitter = {
      emit: () => jest.fn(),
    };

    const createQueryBuilderImplementation: any = {
      where: jest.fn(() => {
        return {
          getOneOrFail: jest.fn(() => user),
          leftJoinAndSelect: jest.fn(() => {
            return {
              getOneOrFail: jest.fn(() => user),
              leftJoinAndSelect: jest.fn(() => {
                return {
                  getOneOrFail: jest.fn(() => user),
                  leftJoinAndSelect: jest.fn(() => {
                    return {
                      getOneOrFail: jest.fn(() => user),
                    };
                  }),
                };
              }),
            };
          }),
        };
      }),
    };
    mockRepository = jest.fn(() => ({
      createQueryBuilder: () => createQueryBuilderImplementation,
      save: jest.fn(),
      merge: jest.fn(),
      find: jest.fn(() => [user]),
      findOneOrFail: jest.fn(() => user),
      delete: jest.fn(),
    }));

    mockStrategy = {
      updateDataOwnerProfile: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        {
          provide: getRepositoryToken(User),
          useClass: mockRepository,
        },
        {
          provide: Logger,
          useClass: mockLogger,
        },
        {
          provide: OccupationService,
          useClass: mockOccupationService,
        },
        {
          provide: SkillService,
          useClass: mockSkillService,
        },
        UserService,
        ConfigService,
        VaultService,

        {
          provide: 'MONGO_CONNECTION',
          useClass: mongoose.Connection,
        },
        {
          provide: EventEmitter2,
          useValue: eventEmitter,
        },
        {
          provide: CLOUD_USER_TOKEN,
          useValue: mockStrategy,
        },
      ],
    }).compile();
    service = module.get<UserService>(UserService);
    repository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve all users', async () => {
    expect(await service.getUsers()).toStrictEqual([user]);
  });

  it('should retrieve a user by id', async () => {
    expect(await service.retrieve(1)).toBe(user);
  });

  it('should retrieve a user by sub', async () => {
    await service.retrieveBySub('fdkjdshfksjd-dfjkshkdjfh-djfksjdfh');
  });

  it('should delete user', async () => {
    jest.spyOn(repository, 'find').mockReturnValue(user as any);
    await service.delete(user.email);

    expect(repository.delete).toBeCalledWith({ email: user.email });
  });

  it('should update user', async () => {
    const userUpdate: UpdateUserDTO = <UpdateUserDTO>{
      email: 'kjfsdkfjsdlk@jsdhfkjsdh.com',
      occupationsList: [1],
      qualificationsList: [1],
    };
    await service.update(1, userUpdate);
    expect(repository.merge).toBeCalledWith(user, userUpdate as any);
    expect(repository.merge).toBeCalled();
    expect(mockStrategy.updateDataOwnerProfile).toBeTruthy();
    expect(repository.save).toBeCalled();
  });

  it('should update email', async () => {
    const userUpdateEmail: UpdateEmailDTO = <UpdateEmailDTO>{
      email: 'kjfsdkfjsdlk@jsdhfewewe11111kjsdh.com',
    };
    await service.updateEmail(1, userUpdateEmail);

    expect(repository.merge).toBeCalled();
    expect(repository.merge).toBeCalledWith(user, userUpdateEmail);
    expect(repository.save).toBeCalled();
  });

  it('should update profilePicture', async () => {
    const image = Buffer.from('hello world');

    await service.updateProfilePicture(1, image, 'image/png');

    expect(repository.merge).toBeCalled();
    expect(repository.merge).toBeCalledWith(user, {
      profilePicture: image.toJSON(),
      profilePictureType: 'image/png',
    });
    expect(repository.save).toBeCalled();
  });

  it('should update shareProfileEnabled', async () => {
    await service.shareProfileEnabled(1, true);

    expect(repository.merge).toBeCalled();
    expect(repository.merge).toBeCalledWith(user, { shareProfile: true });
    expect(repository.save).toBeCalled();
  });

  it('should update requestResolverSettings', async () => {
    await service.requestResolverSettings(1, true);

    expect(repository.merge).toBeCalled();
    expect(repository.merge).toBeCalledWith(user, {
      requestResolverSettings: true,
    });
    expect(repository.save).toBeCalled();
  });

  it('should update user language', async () => {
    await service.updateLanguage(1, 'en');

    expect(repository.merge).toBeCalled();
    expect(repository.merge).toBeCalledWith(user, {
      language: 'en',
    });
    expect(repository.save).toBeCalled();
  });
});
