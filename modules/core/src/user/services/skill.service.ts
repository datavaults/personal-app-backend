import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Skill } from '../entities';

@Injectable()
export class SkillService {
  constructor(
    @InjectRepository(Skill)
    private readonly skillRepository: Repository<Skill>,
  ) {}

  async getSkills(): Promise<Skill[]> {
    return await this.skillRepository.find();
  }

  async retrieve(id: number): Promise<Skill> {
    return await this.skillRepository.findOneOrFail({ id });
  }
}
