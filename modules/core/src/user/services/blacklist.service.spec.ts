import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { BlacklistService } from '@suite5/core/user/services/blacklist.service';
import { Repository } from 'typeorm';
import { Blacklist } from '../entities';

describe('BlacklistService', () => {
  let service: BlacklistService;
  let repository: Repository<Blacklist>;
  let blacklist: Blacklist;

  beforeEach(async () => {
    // blacklist = new Blacklist();
    blacklist = { blacklistedById: 1, dataSeekerUUID: 'test' } as Blacklist;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Blacklist),
          useClass: Repository,
        },
        BlacklistService,
      ],
    }).compile();
    repository = module.get<Repository<Blacklist>>(
      getRepositoryToken(Blacklist),
    );

    service = module.get<BlacklistService>(BlacklistService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should add data seeker to blacklist', async () => {
    jest.spyOn(repository, 'findOne').mockResolvedValue(null);
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(blacklist);
    expect(await service.addToBlacklist(blacklist)).toEqual(blacklist);
    expect(repository.save).toBeCalled();
  });

  it('should not add data seeker to blacklist if already exists', async () => {
    jest
      .spyOn(repository, 'findOne')
      .mockResolvedValue({ ...blacklist, id: 2 });
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(blacklist);
    expect(await service.addToBlacklist(blacklist)).toEqual({
      ...blacklist,
      id: 2,
    });
    expect(repository.save).not.toBeCalled();
  });

  it('should retrieve blacklist of specific user', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([blacklist]);
    expect(await service.retrieveByUserId(1)).toEqual([blacklist]);
  });

  it('should remove data seeker from blacklist', async () => {
    jest.spyOn(repository, 'findOne').mockResolvedValue(blacklist);
    jest.spyOn(repository, 'delete').mockImplementation();
    await service.removeFromBlacklist(1, 'test');
    expect(repository.delete).toBeCalledWith(blacklist.id);

    jest.spyOn(repository, 'findOne').mockResolvedValue(null);
    await expect(
      service.removeFromBlacklist(2, 'test2'),
    ).rejects.toBeInstanceOf(NotFoundException);
  });
});
