import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDTO, DataOwnerProfileDTO } from '@suite5/core/user/dto';
import { UpdateEmailDTO } from '@suite5/core/user/dto/update-email.dto';
import { UpdateUserDTO } from '@suite5/core/user/dto/update-user.dto';
import { OccupationService } from '@suite5/core/user/services/occupation.service';
import { SkillService } from '@suite5/core/user/services/skill.service';
import { VaultService } from '@suite5/vault/vault.service';
import { plainToClass } from 'class-transformer';
import { Connection } from 'mongoose';
import SRP from 'secure-random-password';
import { Repository } from 'typeorm';
import { Events } from '@suite5/constants/events';
import { v4 as uuidv4 } from 'uuid';
import { UserFields } from '../constants/user-fields.constants';
import { ViewUserDTO } from '../dto/view-user.dto';
import { User } from '../entities/user.entity';
import { CloudUserStrategy, CLOUD_USER_TOKEN } from '../strategies';

@Injectable()
export class UserService {
  constructor(
    @Inject(CLOUD_USER_TOKEN)
    private readonly strategy: CloudUserStrategy,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @Inject(OccupationService)
    private occupationService: OccupationService,
    @Inject(SkillService)
    private skillService: SkillService,
    private readonly configService: ConfigService,
    private readonly vaultService: VaultService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async update(id: number, data: UpdateUserDTO): Promise<User> {
    const user = await this.retrieve(id);
    this.userRepository.merge(user, data as any);
    const updatedUser = await this.userRepository.save(user);
    await this.strategy.updateDataOwnerProfile(updatedUser);
    return updatedUser;
  }

  async updateEmail(id: number, data: UpdateEmailDTO): Promise<User> {
    const user = await this.userRepository.findOneOrFail({ id });
    this.userRepository.merge(user, data);
    return await this.userRepository.save(user);
  }

  async updateLanguage(id: number, language: string): Promise<User> {
    const user = await this.userRepository.findOneOrFail({ id });
    this.userRepository.merge(user, { language });
    return await this.userRepository.save(user);
  }

  async updateTerms(id: number, demonstratorStr: any): Promise<User> {
    const user = await this.userRepository.findOneOrFail({ id });
    const lastTermsAcceptance = new Date();
    const demonstrator =
      demonstratorStr.charAt(0).toLowerCase() + demonstratorStr.slice(1);
    this.userRepository.merge(user, { demonstrator, lastTermsAcceptance });
    return await this.userRepository.save(user);
  }

  async updateProfilePicture(
    id: number,
    data: Buffer,
    mimeType: string,
  ): Promise<User> {
    const user = await this.userRepository.findOneOrFail({ id });
    this.userRepository.merge(user, {
      profilePicture: data.toJSON(),
      profilePictureType: mimeType,
    });
    return await this.userRepository.save(user);
  }

  async shareProfileEnabled(id: number, enabled: boolean): Promise<boolean> {
    const user = await this.userRepository.findOneOrFail({ id });
    this.userRepository.merge(user, { shareProfile: enabled });
    return !!(await this.userRepository.save(user));
  }

  async requestResolverSettings(
    id: number,
    enabled: boolean,
  ): Promise<boolean> {
    const user = await this.userRepository.findOneOrFail({ id });
    this.userRepository.merge(user, { requestResolverSettings: enabled });
    return !!(await this.userRepository.save(user));
  }

  async getUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  private async retrieveUser(id: number) {
    return await this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id })
      .leftJoinAndSelect('user.messages', 'message')
      .getOneOrFail();
  }

  async retrieve(id: number): Promise<User> {
    return await this.retrieveUser(id);
  }

  async getUserProfile(id: number): Promise<ViewUserDTO> {
    const user = await this.retrieveUser(id);
    let completeness = 0;
    for (const [key, value] of Object.entries(user)) {
      if (Object.values(UserFields).includes(key as any)) {
        if (value === null || Object.keys(value).length === 0) {
          if (value instanceof Date) {
            completeness;
          } else {
            completeness = completeness + 1;
          }
        }
      }
    }

    const completenessPercentage = parseFloat(
      ((20 - completeness) / 20).toFixed(2),
    );
    const returnedUser: ViewUserDTO = {
      ...user,
      completenessPercentage,
      sub: user.sub,
      isVerified: true,
    };
    return returnedUser;
  }

  async retrieveBySub(sub: string, graceful = false): Promise<User> {
    const query = await this.userRepository
      .createQueryBuilder('user')
      .where('user.sub = :sub', { sub })
      .leftJoinAndSelect('user.messages', 'message');
    return graceful ? query.getOne() : query.getOneOrFail();
  }

  async isExistByEmail(email: string): Promise<boolean> {
    return (await this.userRepository.count({ email })) != 0;
  }

  async retrieveByEmail(email: string, graceful = true): Promise<User> {
    return graceful
      ? await this.userRepository.findOne({ email })
      : await this.userRepository.findOneOrFail({ email });
  }

  async delete(email: string): Promise<void> {
    const user = await this.userRepository.find({ email });
    await this.eventEmitter.emit(Events.DeleteUser, user[0]);
    await this.userRepository.delete({ email });
  }

  private async createUserDB(mongoConnection: Connection, userId: number) {
    const adminCredentials = await this.vaultService.getSecret(
      'user',
      'admin',
      'mongo',
      this.configService.get('vault.vaultRootToken'),
    );

    try {
      console.log('Creating mongo db for user: #' + userId);

      const password: string = SRP.randomPassword({
        length: 12,
        characters: [SRP.lower, SRP.upper, SRP.digits],
      });

      const username = `user${userId}`;
      const database: string = username;

      const credentials: any = {
        authMechanism: 'SCRAM-SHA-1',
        host: `${process.env.MONGO_HOST}:${process.env.MONGO_PORT}`,
        name: database,
        username: username,
        password: password,
      };

      if (
        !(await this.vaultService.secretExists('user', String(userId), 'mongo'))
      ) {
        await this.vaultService.createSecret(
          'user',
          String(userId),
          'mongo',
          credentials,
        );

        // create database user
        const userDB = await mongoConnection.useDb(database);
        try {
          const userCreated = await userDB.db.command({
            createUser: username,
            pwd: password,
            roles: [{ db: database, role: 'readWrite' }],
          });

          if (userCreated.ok === 1) {
            console.log('Database created for user ' + userId);
          }
        } catch (err) {
          console.log(err);
          console.log(`Database for user ${userId} already exists`);
          return false;
        }
      } else {
        console.log(
          `Mongo db credentials for user #${userId} already exist, so no database was created`,
        );
      }
    } catch (err) {
      console.error(
        `Error while creating mongo credentials for user #${userId}`,
      );
    }
  }

  private async setVerificationToken(
    email: string,
    token: string,
    field: string,
  ): Promise<void> {
    const updateQuery = JSON.parse(`{"${field}": "${token}"}`);
    const user = await this.userRepository.findOneOrFail({ email: email });
    this.userRepository.merge(user, updateQuery);
    await this.userRepository.save(user);
  }

  async setEmailVerificationToken(email: string, token: string): Promise<void> {
    await this.setVerificationToken(email, token, 'emailVerificationToken');
  }

  async setPasswordVerificationToken(
    email: string,
    token: string,
  ): Promise<void> {
    await this.setVerificationToken(
      email,
      token,
      'resetPasswordVerificationToken',
    );
  }

  async createUser(
    mongoConnection: Connection,
    data: CreateUserDTO,
  ): Promise<User> {
    const user = new User();
    const currentUser = await this.userRepository.merge(user, data);
    const createdUser = await this.userRepository.save(currentUser);

    const mobileSources = [
      {
        name: 'Location',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Location'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: createdUser.id,
        credentials: { subId: createdUser.sub, sourceType: 'Location' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Route',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Route'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: createdUser.id,
        credentials: { subId: createdUser.sub, sourceType: 'Route' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Google Fit',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Google Fit'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: createdUser.id,
        credentials: { subId: createdUser.sub, sourceType: 'Google Fit' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
    ];
    await this.createUserDB(mongoConnection, createdUser.id);
    this.eventEmitter.emit(
      Events.UserCreated,
      createdUser,
      mobileSources,
      'userCreation',
    );
    return createdUser;
  }

  async getProfileById(id: number): Promise<any> {
    const userFullDetails = await this.retrieve(id);
    const profile = plainToClass(DataOwnerProfileDTO, userFullDetails);

    return userFullDetails.shareProfile
      ? {
          ...profile,
        }
      : {};
  }

  async getUsersWithRequestReceivingEnabled(): Promise<User[]> {
    return await this.userRepository.find({ requestResolverSettings: true });
  }
}
