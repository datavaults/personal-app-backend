import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Occupation } from '../entities';

@Injectable()
export class OccupationService {
  constructor(
    @InjectRepository(Occupation)
    private readonly occupationRepository: Repository<Occupation>,
  ) {}

  async getOccupations(): Promise<Occupation[]> {
    return await this.occupationRepository.find();
  }

  async retrieve(id: number): Promise<Occupation> {
    return await this.occupationRepository.findOneOrFail({ id });
  }
}
