import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { OccupationService } from '@suite5/core/user/services/occupation.service';
import { Repository } from 'typeorm';
import { Occupation } from '../entities';

describe('OccupationService', () => {
  let service: OccupationService;

  let repository: Repository<Occupation>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Occupation),
          useClass: Repository,
        },
        OccupationService,
      ],
    }).compile();
    repository = module.get<Repository<Occupation>>(
      getRepositoryToken(Occupation),
    );

    service = module.get<OccupationService>(OccupationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve all occupations', async () => {
    jest.spyOn(repository, 'find').mockImplementation();

    await service.getOccupations();
    expect(repository.find).toBeCalled();
  });

  it('should retrieve an occupation', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    await service.retrieve(1);
    expect(repository.findOneOrFail).toBeCalled();
    expect(repository.findOneOrFail).toBeCalledWith({ id: 1 });
  });
});
