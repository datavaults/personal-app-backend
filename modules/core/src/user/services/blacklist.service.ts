import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddToBlacklistDTO } from '../dto';
import { Blacklist } from '../entities';

@Injectable()
export class BlacklistService {
  constructor(
    @InjectRepository(Blacklist)
    private readonly blacklistRepository: Repository<Blacklist>,
  ) {}

  async addToBlacklist(blacklistData: AddToBlacklistDTO): Promise<Blacklist> {
    const existingBlacklist = await this.blacklistRepository.findOne({
      blacklistedById: blacklistData.blacklistedById,
      dataSeekerUUID: blacklistData.dataSeekerUUID,
    });
    if (existingBlacklist) {
      return existingBlacklist;
    }
    const blacklist = this.blacklistRepository.create(blacklistData);
    return await this.blacklistRepository.save(blacklist);
  }

  async retrieveByUserId(userId: number): Promise<Blacklist[]> {
    return await this.blacklistRepository.find({
      blacklistedById: userId,
    });
  }

  async removeFromBlacklist(
    userId: number,
    dataSeekerUUID: string,
  ): Promise<void> {
    const blacklist = await this.blacklistRepository.findOne({
      blacklistedById: userId,
      dataSeekerUUID,
    });
    if (!blacklist) {
      throw new NotFoundException(`${Blacklist.name} not found!`);
    }
    await this.blacklistRepository.delete(blacklist.id);
  }
}
