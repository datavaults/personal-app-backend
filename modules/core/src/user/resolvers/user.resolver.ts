import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { Args, Int, Query, Resolver } from '@nestjs/graphql';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { User } from '../entities/user.entity';
import { UserService } from '../services';

@Resolver(() => User)
@UseGuards(AuthenticationGuard)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(
    /* istanbul ignore next */
    () => User,
  )
  async user(
    @Args({
      name: 'id',
      type:
        /* istanbul ignore next */
        () => Int,
    })
    id: number,
  ): Promise<User> {
    return await this.userService.retrieve(id);
  }
}
