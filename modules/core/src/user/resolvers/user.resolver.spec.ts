import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { User } from '../entities/user.entity';
import { UserService } from '../services';
import { UserResolver } from './user.resolver';
import { ConfigService } from '@nestjs/config';
import { Controller } from '@nestjs/common';

describe('UserResolver', () => {
  const user: User = new User();
  let service: any;
  // let occupationService: OccupationService;
  // let skillService: SkillService;
  // let repository: Repository<User>;
  let resolver: UserResolver;

  beforeEach(async () => {
    // repository = new Repository<User>();
    // service = new UserService(repository, occupationService, skillService);

    service = {
      getSkills: () => jest.fn(),
      retrieve: () => jest.fn(),
    };
    resolver = new UserResolver(service);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve a user by id', async () => {
    jest.spyOn(service, 'retrieve').mockResolvedValue(user);
    expect(await resolver.user(1)).toBe(user);
  });
});
