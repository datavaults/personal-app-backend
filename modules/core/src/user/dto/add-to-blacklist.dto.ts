import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class AddToBlacklistDTO {
  @IsNotEmpty()
  @ApiProperty()
  readonly dataSeekerUUID: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly dataSeekerName: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly dataSeekerPublicURL: string;

  @IsOptional()
  @ApiProperty()
  blacklistedById?: number;
}
