import { ApiProperty } from '@nestjs/swagger';
import { IsSmallerThan } from '@suite5/common';
import { Exclude, Expose } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsJSON,
  IsNumber,
  IsString,
} from 'class-validator';
import { Demonstrators } from '../constants';

@Exclude()
export class ViewUserDTO {
  @IsNumber()
  @ApiProperty()
  @Expose()
  readonly id: number;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly firstName: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly lastName: string;

  @IsEmail()
  @ApiProperty()
  @Expose()
  readonly email: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly street: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly postcode: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly region: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly city: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly country: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly phone: string;

  @IsDateString()
  @IsSmallerThan(new Date())
  @ApiProperty()
  @Expose()
  readonly birthDate: Date;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly placeOfBirth: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly nationalInsuranceNumber: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly socialSecurityNumber: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly nationality: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly memberNumber: string;

  @IsBoolean()
  @ApiProperty()
  @Expose()
  readonly shareProfile: boolean;

  @IsBoolean()
  @ApiProperty()
  @Expose()
  readonly requestResolverSettings: boolean;

  @IsJSON()
  @ApiProperty()
  @Expose()
  readonly profilePicture: any;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly profilePictureType: string;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly occupationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly qualificationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  @ApiProperty({})
  @Expose()
  readonly culturalInterestList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly transportationList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly civilStatusList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly disabilitiesList: Array<number>;

  @IsNumber()
  @ApiProperty()
  @Expose()
  readonly completenessPercentage: number;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly sub: string;

  @IsBoolean()
  @ApiProperty()
  @Expose()
  readonly isVerified: boolean;

  @IsEnum(Demonstrators)
  @ApiProperty({ enum: Demonstrators })
  @Expose()
  readonly demonstrator: Demonstrators;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly language: string;

  @IsDateString()
  @ApiProperty()
  @Expose()
  readonly lastTermsAcceptance: Date;
}
