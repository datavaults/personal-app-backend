import { ApiProperty } from '@nestjs/swagger';
import { IsSmallerThan } from '@suite5/common';
import { Demonstrators } from '@suite5/core/user/constants';
import {
  IsArray,
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsJSON,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateUserDTO {
  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly firstName: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly lastName: string;

  @IsEmail()
  @IsOptional()
  @ApiProperty()
  readonly email: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly street: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly postcode: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly region: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly city: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly country: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly phone: string;

  @IsDateString()
  @IsSmallerThan(new Date())
  @IsOptional()
  @ApiProperty()
  readonly birthDate: Date;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly placeOfBirth: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly nationalInsuranceNumber: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly socialSecurityNumber: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly nationality: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly memberNumber: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly shareProfile: boolean;

  @IsJSON()
  @IsOptional()
  @ApiProperty()
  readonly profilePicture: any;

  @IsArray()
  @ApiProperty()
  readonly occupationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  readonly qualificationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  @ApiProperty({})
  readonly culturalInterestList: Array<number>;

  @IsArray()
  @ApiProperty()
  readonly transportationList: Array<number>;

  @IsArray()
  @ApiProperty()
  readonly civilStatusList: Array<number>;

  @IsArray()
  @ApiProperty()
  readonly disabilitiesList: Array<number>;

  @IsEnum(Demonstrators)
  @IsOptional()
  @ApiProperty({ enum: Demonstrators })
  readonly demonstrator: Demonstrators;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly language: string;
}
