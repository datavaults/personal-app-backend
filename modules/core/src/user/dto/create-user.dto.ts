import { ApiProperty } from '@nestjs/swagger';
import { IsSmallerThan } from '@suite5/common';
import { Demonstrators } from '@suite5/core/user/constants';
import { Exclude, Expose } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsJSON,
  IsNumber,
  IsString,
} from 'class-validator';

@Exclude()
export class CreateUserDTO {
  @IsString()
  @ApiProperty()
  readonly firstName: string;

  @IsString()
  @ApiProperty()
  readonly lastName: string;

  @IsEmail()
  @ApiProperty()
  @Expose()
  readonly email: string;

  @IsString()
  @ApiProperty()
  readonly street: string;

  @IsString()
  @ApiProperty()
  readonly postcode: string;

  @IsString()
  @ApiProperty()
  readonly region: string;

  @IsString()
  @ApiProperty()
  readonly city: string;

  @IsString()
  @ApiProperty()
  readonly country: string;

  @IsString()
  @ApiProperty()
  readonly phone: string;

  @IsDateString()
  @IsSmallerThan(new Date())
  @ApiProperty()
  readonly birthDate: Date;

  @IsString()
  @ApiProperty()
  readonly placeOfBirth: string;

  @IsString()
  @ApiProperty()
  readonly nationalInsuranceNumber: string;

  @IsString()
  @ApiProperty()
  readonly socialSecurityNumber: string;

  @IsString()
  @ApiProperty()
  readonly nationality: string;

  @IsString()
  @ApiProperty()
  readonly memberNumber: string;

  @IsBoolean()
  @ApiProperty()
  readonly shareProfile: boolean;

  @IsJSON()
  @ApiProperty()
  readonly profilePicture: any;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly occupationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly qualificationsList: Array<number>;

  @IsArray()
  @ApiProperty()
  @ApiProperty({})
  @Expose()
  readonly culturalInterestList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly transportationList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly civilStatusList: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly disabilitiesList: Array<number>;

  @IsString()
  @ApiProperty()
  sub: string;

  @IsEnum(Demonstrators)
  @ApiProperty({ enum: Demonstrators })
  readonly demonstrator: Demonstrators;
}
