import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class SendEmailDTO {
  @IsNotEmpty()
  @ApiProperty()
  readonly firstName: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly lastName: string;

  @IsOptional()
  @ApiProperty()
  readonly phone?: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly email: string;

  @IsOptional()
  @ApiProperty()
  subject?: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly message: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly hearAboutUs: string;
}
