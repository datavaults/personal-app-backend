import { ApiProperty } from '@nestjs/swagger';
import { IsValidEmail } from '@suite5/common';
import { IsNotEmpty } from 'class-validator';

export class UpdateEmailDTO {
  @IsValidEmail()
  @IsNotEmpty()
  @ApiProperty()
  readonly email: string;
}
