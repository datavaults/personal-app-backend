import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsArray, IsBoolean, IsEnum, IsString } from 'class-validator';
import { Demonstrators } from '../constants';

@Exclude()
export class DataOwnerProfileDTO {
  @IsString()
  @ApiProperty()
  @Expose()
  readonly region: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly city: string;

  @IsBoolean()
  @ApiProperty()
  @Expose()
  readonly shareProfile: boolean;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly country: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly placeOfBirth: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly nationality: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly birthDate: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly postalCode: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly nationalInsuranceNumber: string;

  @IsString()
  @ApiProperty()
  @Expose()
  readonly socialSecurityNumber: string;

  @IsBoolean()
  @ApiProperty()
  @Expose()
  readonly requestResolverSettings: boolean;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly occupations: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly educations: Array<number>;

  @IsArray()
  @ApiProperty()
  @ApiProperty({})
  @Expose()
  readonly culturalInterests: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly transportationMeans: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly civilStatus: Array<number>;

  @IsArray()
  @ApiProperty()
  @Expose()
  readonly disabilities: Array<number>;

  @IsEnum(Demonstrators)
  @ApiProperty({ enum: Demonstrators })
  @Expose()
  readonly demonstrator: Demonstrators;
}
