import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { plainToClass } from 'class-transformer';
import { BasicSkillDTO } from '../dto/basic-skill.dto';
import { Skill } from '../entities';
import { SkillService } from '../services';
import { SkillController } from './skill.controller';
import { ConfigService } from '@nestjs/config';

describe('SkillController', () => {
  let controller: SkillController;
  let service: SkillService;
  let skill: Skill;

  beforeEach(async () => {
    skill = new Skill();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SkillController],
      providers: [
        {
          provide: SkillService,
          useClass: jest.fn(() => ({
            getSkills: () => jest.fn(),
            retrieve: () => jest.fn(),
          })),
        },
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<SkillController>(SkillController);
    service = module.get<SkillService>(SkillService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should return skills', async () => {
    jest.spyOn(service, 'getSkills').mockResolvedValue([skill]);
    const results = await controller.getAll();
    expect(results).toStrictEqual([plainToClass(BasicSkillDTO, skill)]);
  });

  it('should return skill by id', async () => {
    jest.spyOn(service, 'retrieve').mockResolvedValue(skill);
    await controller.getskillById(1);
    expect(service.retrieve).toBeCalled();
  });
});
