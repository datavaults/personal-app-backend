import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { BasicOccupationDTO } from '../dto/basic-occupation.dto';
import { Occupation } from '../entities';
import { OccupationService } from '../services';
import { OccupationController } from './occupation.controller';
import { ConfigService } from '@nestjs/config';

describe('OccupationController', () => {
  let controller: OccupationController;
  let service: OccupationService;
  let occupation: Occupation;

  beforeEach(async () => {
    occupation = new Occupation();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [OccupationController],
      providers: [
        {
          provide: getRepositoryToken(Occupation),
          useClass: Repository,
        },
        OccupationService,
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<OccupationController>(OccupationController);
    service = module.get<OccupationService>(OccupationService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should return occupations', async () => {
    jest.spyOn(service, 'getOccupations').mockResolvedValue([occupation]);
    const results = await controller.getAll();
    expect(results).toStrictEqual([
      plainToClass(BasicOccupationDTO, occupation),
    ]);
  });

  it('should return occupation by id', async () => {
    jest.spyOn(service, 'retrieve').mockResolvedValue(occupation);
    await controller.getOccupationById(1);
    expect(service.retrieve).toBeCalled();
  });
});
