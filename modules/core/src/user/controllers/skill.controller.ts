import {
  Controller,
  Get,
  Header,
  Inject,
  Param,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { plainToClass } from 'class-transformer';
import { BasicSkillDTO } from '../dto/basic-skill.dto';
import { Skill } from '../entities';
import { SkillService } from '../services/skill.service';

@Controller('skill')
@ApiTags('skill')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class SkillController {
  constructor(
    @Inject(SkillService)
    private readonly skillService: SkillService,
  ) {}

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve all skills',
  })
  @Header('Cache-Control', 'max-age=86400')
  async getAll(): Promise<BasicSkillDTO[]> {
    return (await this.skillService.getSkills()).map((data) =>
      plainToClass(BasicSkillDTO, data),
    );
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve skill by id',
  })
  async getskillById(@Param('id') id: number): Promise<Skill> {
    return this.skillService.retrieve(id);
  }
}
