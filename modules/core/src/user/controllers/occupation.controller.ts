import {
  Controller,
  Get,
  Header,
  Inject,
  Param,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { Occupation } from '@suite5/core/user/entities';
import { plainToClass } from 'class-transformer';
import { BasicOccupationDTO } from '../dto/basic-occupation.dto';
import { OccupationService } from '../services/occupation.service';

@Controller('occupation')
@ApiTags('occupation')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class OccupationController {
  constructor(
    @Inject(OccupationService)
    private readonly occupationService: OccupationService,
  ) {}

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve all occupations',
  })
  @Header('Cache-Control', 'max-age=86400')
  async getAll(): Promise<BasicOccupationDTO[]> {
    return (await this.occupationService.getOccupations()).map((data) => {
      return plainToClass(BasicOccupationDTO, data);
    });
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve occupation by id',
  })
  async getOccupationById(@Param('id') id: number): Promise<Occupation> {
    return this.occupationService.retrieve(id);
  }
}
