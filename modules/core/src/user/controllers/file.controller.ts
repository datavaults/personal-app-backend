import {
  BadRequestException,
  Controller,
  Get,
  Inject,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { UserService } from '@suite5/core/user/services/user.service';
import fs from 'fs';
import sizeOf from 'image-size';

@Controller('profile-picture')
@UseGuards(AuthenticationGuard)
@ApiTags('profile-picture')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class FileController {
  constructor(
    @Inject(UserService)
    private readonly userService: UserService,
  ) {}

  @Post('upload')
  @ApiOperation({
    summary: 'Upload file',
  })
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @CurrentUser() user: UserData,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (!['image/png', 'image/jpeg'].includes(file.mimetype)) {
      throw new BadRequestException('File type must be PNG or JPEG!');
    }
    if (file.size > 2000000) {
      throw new BadRequestException('File size must not exceed 2MB!');
    }
    fs.writeFileSync(file.originalname, file.buffer);
    const dimensions = sizeOf(file.originalname);
    if (dimensions.width > 400 || dimensions.height > 400) {
      fs.unlinkSync(file.originalname);
      throw new BadRequestException(
        'File dimensions cannot be greater than 400 x 400!',
      );
    }
    fs.unlinkSync(file.originalname);
    return this.userService.updateProfilePicture(
      user.id,
      file.buffer,
      file.mimetype,
    );
  }

  @Get('retrieve')
  @ApiOperation({
    summary: 'Retrieve file',
  })
  @UseInterceptors(FileInterceptor('file'))
  async retrieveFile(@CurrentUser() user: UserData) {
    const file = (
      (await this.userService.retrieve(user.id)) || { profilePicture: {} }
    ).profilePicture;
    return file;
  }
}
