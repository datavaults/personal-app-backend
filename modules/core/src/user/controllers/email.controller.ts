import { Body, Controller, Inject, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { SendEmailDTO } from '../dto/send-email.dto';
import { MailService } from '@suite5/common';

@Controller('email')
@ApiTags('email')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class EmailController {
  constructor(
    @Inject(MailService)
    private readonly mailService: MailService,
  ) {}

  @Post('send')
  @UseGuards(AuthenticationGuard)
  @ApiCreatedResponse({ description: 'Send a contact us email' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @ApiBody({ type: SendEmailDTO })
  async send(@Body() data: SendEmailDTO): Promise<void> {
    data.subject = 'Contact Us';
    await this.mailService.sendToSupport(data);
  }
}
