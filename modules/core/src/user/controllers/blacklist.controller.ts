import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AddToBlacklistDTO } from '../dto';
import { Blacklist } from '../entities';
import { BlacklistService } from '../services';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';

@Controller('blacklist')
@UseGuards(AuthenticationGuard)
@ApiTags('blacklist')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class BlacklistController {
  constructor(
    @Inject(BlacklistService)
    private readonly blacklistService: BlacklistService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Add data seeker to blacklist' })
  async addToBlacklist(
    @CurrentUser() user: UserData,
    @Body() data: AddToBlacklistDTO,
  ): Promise<Blacklist> {
    data.blacklistedById = user.id;
    return await this.blacklistService.addToBlacklist(data);
  }

  @Get()
  @ApiOperation({ summary: 'Retrieve blacklist of specific user' })
  @ApiNotFoundResponse()
  getBlacklist(@CurrentUser() user: UserData): Promise<Blacklist[]> {
    return this.blacklistService.retrieveByUserId(user.id);
  }

  @Delete(':uuid')
  @ApiOperation({ summary: 'Remove data seeker from blacklist' })
  @ApiNotFoundResponse()
  async removeFromBlacklist(
    @CurrentUser() user: UserData,
    @Param('uuid') dataSeekerUUID: string,
  ): Promise<void> {
    await this.blacklistService.removeFromBlacklist(user.id, dataSeekerUUID);
  }
}
