import {
  Body,
  Controller,
  Delete,
  forwardRef,
  Get,
  HttpException,
  Inject,
  Param,
  Patch,
  Put,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { UpdateEmailDTO } from '@suite5/core/user/dto/update-email.dto';
import { UpdateUserDTO } from '@suite5/core/user/dto/update-user.dto';
import { User } from '@suite5/core/user/entities/user.entity';
import { UserService } from '@suite5/core/user/services';
import { plainToClass } from 'class-transformer';
import { Response } from 'express';
import { ViewUserDTO } from '../dto/view-user.dto';
import {
  CivilStatus,
  CulturalInterest,
  Education,
  TransportationMeans,
} from '../constants';
import { Disabilities } from '@suite5/core/user/constants';
import { Cookies } from '@suite5/core/authentication/constants';

@Controller('user')
@ApiTags('user')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class UserController {
  constructor(
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(ConfigService)
    private readonly configService: ConfigService,
    @Inject(forwardRef(() => AuthenticationService))
    private readonly authService: AuthenticationService,
  ) {}

  @Get('profile')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve user profile',
  })
  async getProfile(@CurrentUser() user: UserData) {
    const retrievedUser = await this.userService.getUserProfile(user.id);
    const returnedUser: any = plainToClass(ViewUserDTO, retrievedUser);
    returnedUser.isVerified = user.emailVerified;
    return returnedUser;
  }

  @Put('update')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Update a user',
  })
  @ApiBody({ type: UpdateUserDTO })
  @ApiNotFoundResponse({ description: 'Not found' })
  async update(
    @CurrentUser() user: UserData,
    @Body() data: UpdateUserDTO,
  ): Promise<User> {
    return this.userService.update(user.id, data);
  }

  @Patch('language/:language')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Update user language',
  })
  @ApiNotFoundResponse({ description: 'Not found' })
  async updateLanguage(
    @CurrentUser() user: UserData,
    @Param('language') language: string,
  ): Promise<User> {
    return this.userService.updateLanguage(user.id, language);
  }

  @Patch('terms/:demonstrator')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Update user terms',
  })
  @ApiNotFoundResponse({ description: 'Not found' })
  async updateTerms(
    @CurrentUser() user: UserData,
    @Param('demonstrator') demonstrator: string,
  ): Promise<User> {
    return this.userService.updateTerms(user.id, demonstrator.toLowerCase());
  }

  @Put('changeemail')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Change email',
  })
  @ApiBody({ type: UpdateEmailDTO })
  @ApiNotFoundResponse({ description: 'Not found' })
  async changeEmail(
    @CurrentUser() user: UserData,
    @Body() data: UpdateEmailDTO,
  ): Promise<User> {
    if (!!(await this.authService.changeEmail(user.sub, data.email))) {
      return await this.userService.updateEmail(user.id, data);
    }
    throw new HttpException('Not a valid email', 401);
  }

  @Delete('delete')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Delete user',
  })
  @ApiNotFoundResponse({ description: 'Not found' })
  async delete(
    @CurrentUser() user: UserData,
    @Res() res: Response,
  ): Promise<void> {
    await this.userService.delete(user.username);
    res.clearCookie(Cookies.Token, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
    });

    res.clearCookie(Cookies.RefreshToken, {
      sameSite: true,
      httpOnly: true,
      secure: await this.configService.get('auth.module.secure_cookie'),
    });
    res.send({
      statusCode: 200,
      message: 'Success',
    });
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve user by id',
  })
  async getUserById(@Param('id') id: number): Promise<User> {
    return this.userService.retrieve(id);
  }

  @Patch('/shareProfile/:enabled')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({ summary: 'shareProfile' })
  @ApiForbiddenResponse({ description: 'Not found' })
  async shareProfile(
    @CurrentUser() user: UserData,
    @Param('enabled') enabled: boolean,
  ): Promise<boolean> {
    return this.userService.shareProfileEnabled(user.id, enabled);
  }

  @Patch('/requestResolverSettings/:enabled')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({ summary: 'requestResolverSettings' })
  @ApiForbiddenResponse({ description: 'Not found' })
  async requestResolverSettings(
    @CurrentUser() user: UserData,
    @Param('enabled') enabled: boolean,
  ): Promise<boolean> {
    return this.userService.requestResolverSettings(user.id, enabled);
  }

  @Get('/enum/:kind')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary:
      'enums list ( civilStatus,culturalInterest, transportationMeans, Disabilities, Education )',
  })
  @ApiForbiddenResponse({ description: 'Not found' })
  enumsList(@Param('kind') kind: string) {
    return Object.values(
      Object.fromEntries(
        Object.entries({
          civilStatus: Object.values(CivilStatus),
          culturalInterest: Object.values(CulturalInterest),
          transportationMeans: Object.values(TransportationMeans),
          disabilities: Object.values(Disabilities),
          education: Object.values(Education),
        }).filter(([k, v]) => k.toLowerCase() == kind.toLowerCase()),
      ),
    )[0];
  }
}
