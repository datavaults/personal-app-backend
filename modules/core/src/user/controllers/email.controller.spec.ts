import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { EmailController } from '@suite5/core/user';
import { MailService } from '@suite5/common';
import { SendEmailDTO } from '../dto';

describe('EmailController', () => {
  let controller: EmailController;
  let mailService: MailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmailController],
      providers: [
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            changeEmail: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: (param) => {
              switch (param) {
                case 'vault.vaultRootToken':
                  return 'abcd1234';
                default:
                  return 'abc';
              }
            },
          })),
        },
        {
          provide: MailService,
          useClass: jest.fn(() => ({
            send: () => jest.fn(),
            sendToSupport: () => jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<EmailController>(EmailController);
    mailService = module.get<MailService>(MailService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should send an email', async () => {
    const data: SendEmailDTO = {
      firstName: '',
      lastName: '',
      email: '',
      message: '',
      hearAboutUs: '',
    };
    jest.spyOn(mailService, 'sendToSupport').mockImplementation();
    await controller.send(data);
    expect(mailService.sendToSupport).toBeCalledWith({
      ...data,
      subject: 'Contact Us',
    });
  });
});
