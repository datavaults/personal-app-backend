import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserData } from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { Repository } from 'typeorm';
import { AddToBlacklistDTO } from '../dto';
import { Blacklist } from '../entities';
import { BlacklistService } from '@suite5/core/user';
import { BlacklistController } from '@suite5/core/user';
import { ConfigService } from '@nestjs/config';

describe('BlacklistController', () => {
  let controller: BlacklistController;
  let service: BlacklistService;
  let blacklist: Blacklist;

  const user: UserData = {
    expiry: 0,
    id: 1,
    username: 'test@test.com',
    email: 'test@test.com',
    sub: 'skldfjslkdfjs-sdfbsdjkf',
    emailVerified: true,
  };
  const data: AddToBlacklistDTO = {
    dataSeekerName: 'test',
    dataSeekerUUID: 'test',
    dataSeekerPublicURL: 'test',
  };

  beforeEach(async () => {
    blacklist = new Blacklist();
    // const mockAuthGuard: CanActivate = { canActivate: jest.fn(() => true) };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [BlacklistController],
      providers: [
        {
          provide: getRepositoryToken(Blacklist),
          useClass: Repository,
        },
        {
          provide: BlacklistService,
          useValue: {
            addToBlacklist: () => jest.fn(),
            retrieveByUserId: () => jest.fn(),
            removeFromBlacklist: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
      ],
    }).compile();
    controller = module.get<BlacklistController>(BlacklistController);
    service = module.get<BlacklistService>(BlacklistService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should add data seeker to blacklist', async () => {
    jest.spyOn(service, 'addToBlacklist').mockResolvedValue(blacklist);
    expect(await controller.addToBlacklist(user, data)).toBe(blacklist);
  });

  it('should retrieve blacklist of specific user', async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([blacklist]);
    expect(await controller.getBlacklist(user)).toEqual([blacklist]);
  });

  it('should remove data seeker from blacklist', async () => {
    jest.spyOn(service, 'removeFromBlacklist').mockImplementation();
    await controller.removeFromBlacklist(user, data.dataSeekerUUID);
    expect(service.removeFromBlacklist).toBeCalledWith(
      user.id,
      data.dataSeekerUUID,
    );
  });
});
