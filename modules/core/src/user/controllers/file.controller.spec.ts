import { BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { UserData } from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { User } from '../entities';
import { UserService } from '../services';
import { FileController } from './file.controller';

describe('FileController', () => {
  let controller: FileController;
  let service: any;
  let user: UserData;

  beforeEach(async () => {
    user = { id: 1, username: 'test@test.org' } as UserData;
    service = {
      updateProfilePicture: () => jest.fn() as any,
      retrieve: () => jest.fn() as any,
    };
    controller = new FileController(service);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    // expect(service).toBeDefined();
  });

  it('testing uploadFile method', async () => {
    const data =
      '/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQVEhgUFBUZGBgZHBgcGBoaGBwaGRgeGRgZGhgcGBocIy4nHB4rHxgYJjgnKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHjQrJSw0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIALgBEwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQIDBAUGB//EADsQAAEDAgQEAwYGAQQBBQAAAAEAAhEDIQQSMUEFUWFxIoGREzJCobHwBlJiwdHhchWCkvGiFCMzQ8L/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAkEQEBAAICAgICAgMAAAAAAAAAAQIRAyESMUFRBGETMiJxsf/aAAwDAQACEQMRAD8A9cBUzAoGBWGLMD0IQtAQhCAQhCAQhCAQhCAQhCAQhCAQhMc8DUgd0DkKu/GMHxegJ+ie3ENOjh6qbi6qZCQFKqgQhCAQhCAQhCAQhCAQhCAQhCAQhCCq1TtUTQpWqaQ9CEKqEIQgEIQgEIQgEIQgEJJVfF4tlNhe9wa0bn6DmUFhZnE+N0KAOd4kfCLnz5LleNfixzpbSljdz8bh/wDnsL9QuUfVe5wNm8iXS4dQLka8lyy5Pp1x478un4n+La7wfYscxuzsuo5g/vZZH+sYhxOd3W7hp66qmMODLjUtBn3gfmUwMowAc1zEh+Zs3N2meS5XK326zGT0028RYR4nv7hw/ePqtHDcRpRAcR/tGb/yP0WbheE4MnxOqg8mlp9NLLToYbANBhr3cs7o+YNvmpRucN4hmvTD3c4DD5mIW3RxRMBzXNPUW9VxNOlSY4Po1HU3GRlc7NOnukAH6q5TrYqQche2ZnOCPQun5K452M5YSu0lErDw/EHaOaWutaCtCnXMXEfex3XbHllcbhYuyiVA183CcHLpNVjtMhRhyUOQ2ehJKJRSoQhAIQhAIQhBAE9qYE4FVEgSpgKdKilQklEoFQmyglE2cklMLk0uV0bSFyYXJhesnjPGG0W5RBeRLQdB1KWyElt0s8V4mygzM65Nmt3J/YcyvOeN8YdUdnqPgfCJIgfoA076lQcU4s55zOcXPdZsgX7DYdFzeIeC4lz5dyvHmVwyy29GOExWsXiXOO4FzmM3nkqLXwbO8hI9IUVRwJu7958horWCwpe6ABHT+tPks3qNztLhS98sbTLyfyC/eRcLWbwiuL+xtbwuJc+OwJK3uGVKeHp5XFjP0hwBPeLuPT5KtV49TNSKcuveDkYD1DSCfMwudya8RguA1aj2A03sB94g5Y7tmCtij+FXBxa57oJ97LZw2ktd4Ssav+IzTILq7R+gFxPyJPqUN/HD2mzpnQQXeuY/RZ2vjfh2+B4U3Dmc2bWM23UKTGcQw7bEZnETlYJdy20XDYv8VueYdnJPusFgO8bp2D/EUHIxjQ8gZnuFhGyvlfiMfx/Nvbqv9Up//ZScwbS6THYG6uYeo0gOpv8AD+V0/JcVX409ha5zsxcfhABN9pGi1MNxFr3GQC46iHTpuRZPKrcI6yY5X5c+uxTm1LwTf5/2sNld7R7pc0jWxnvACsU8W0gaxyOn8jyWpnr053Bs5koesyjXebtaHjcTDh66q22qCYMtPJ1pXow5JfbllhYtByUOVcuI1Sh66MLIclDlAHpwegmzJZUIclzIJZQosyE0bNBRKgFROzKifMjMoMyXOgmzIzKDMkL0E2dBeoDUTS9BOXpheoHVFDWxAa0uOwUEfFuIilTLhdx90fuegXB4kve19aqSSYAO5OzR+8dlr18cX1HuebMb4f8AJ0Rb6eS5jjnETVcKdNoawGG394/Ff8szfdcMstvRhjpiYmtmPM7n9o5KE4Uu0A8yB8lou/8AT08wqE1XxBDPCxn+46nrCZRoOe2crabO7pPQu1PlyGi5ZZzGbrrMdoqbchHugmPC0BzvMwYWtXpOYJfUFM2IpzJdzLyIjuVnYrFikclFvjgE1CAXNB0yDY2KzaNelml5zOPvF0k+pWZvP0XWPtoVKVV5OSqx/QPGbsB9hUHNfeQbagmCPILQfVpMbnbDiNoMHzVrE1mPpB7DuACdQCND1BkT06q5f46+YY21zjs20Tz3WrwF7abi9zWl0au+HnA5lGEwYc/pJ9Br2kx6kKfFYZxaYDWMbuY+QUuTWvtNjeIhrYYWSdYAJE/4yB5yspmILnTmPqfL7so30ZvEN2c4/Qc0DSbwNJ09AtSRK63hWKlt8jTpncCQB1tErVfWw4LWU65NQi7mMaWeciSuIwuKYDmqhzwIhl47GI/ZdXwXHOe15DadEOgAMZBcPy2us2aPbSwvEa7MzdQCPE0NgzzEwOymdxEt8TgQ0mCQMzQefNo+Xbfmm1pqFrhkfoADEDY+LX6K1geIFk0qpJAkTIJ5iL9ipodS2u6A5hh0bdNuvdQj8R2AqhrhcXF/59FkYHHNY/wPkbfexW0cRhKzQyswAmfEDAzblWb9M2T6T0eLMcBkeWcviHa6st4mQJdlcPzNkH0uFzGO4K1tQNpucNC2TBNtRs7y/lMezEs95j+5Ba3zO6syynpLjjfbsGcTYf6IPqNVZZi2EwHCeWn1XDDHvMZmj6K9RxzgNcxPwl2b0krpObKe2Lwz4dkKidmXJ0+LPbqxzekGP6VhnHeY++4/hdJyyud4so6TMhYP+tj8h/5D+ELX8mP2nhfpoNqp4qLOFRPbUXTbkv8AtEe0VMVEvtFNtLXtEhqKqXppqJsWzVTDUVbOjMpsSuqKnj2ue0sB2m25+ET3U7QSQArNanlaABoRPU9Vw5ubHCdunHju7cNxqmWNe4ixqEGLeGkLDoCT8lyzcFWqGWMLiSfFEAdJO0cl6Pj8HnsbDM7N3Lg4E+g+Ss0cK0C0DrzXi5PyZP6vVjjNduFw34Wyw+qcx1gaen3/ADq/+htIGhHlaT20+i6V9H7hRHCwDlvJmCV5srln7re9PPON4Q08RmywHtaW8paII+hXLYugc5iSJ+5Xp3FS4yypSzt6ajlBG91zWJ4FJzNfA5PY4EaWka68l34Oa4TWU7nSZYzJzuctpwTcC/RatEOp4VjXe89xef0tAhvadfNXcPwZjCHVCarpkNy5GA8zmu/fpbRTDh7qlRz6kxsANTsBfQdwF28/L169p46T8Jw2anMOvbYE66Hbe/dTVsOJykAx7rRpPc6nsnexqx/8rWMFjEekjfzKlwzGNIgueRvpPd38AIrNxfDmt8b/ABP+FkQ1o6/wszEuA1GX/j9Mq6qs0EZnNBPKYH8nyWTiWszT4Z3DYjtN4W4y5s1CCYJHWf40V7C16jDnafF+cuzAffdNxNKkZgiZ2Jn1t+6gp0QCBz/UAf3Wr3EnVdOzHVcRTczNme24GSCRuWzF+0rLe8PguJa4WJiDOwcB21Chfh3syva9oM6gkERcyDefmjHYm7XF+d25INwOtj5kKSfRlTW5x4g0nTNzMHp9UjOMPa4ZtJEzawgGesXnmus4fxHDCmwvZ74guBmHEkQQbgHUbKpx/gjXt9ph3se2LtMZvWfqrPqs2qrON5qeQvkC+V/wz+XkdNOemyH8SdkAD/LNbyBXMezLTlcMv31U1JzYgvPrp2BVuKytyjiAbz8x9SVZbi2jSSsFtQCxiDpe/cEJfbZdx5KeJtvtxzvhMb6/2p21cwlx/nzXPYbEiZiY15+S0G8UaB4G5Xcz4vMToU1TcbIbyBSLE/1p5+N3pCFfGp5R6ICnAqIFOBXqeRIHJ2ZRgpZUDpRKRCBwTk0JzRKC1hmeGeenb7+imdpCgp4gEW/6WBxfidb2uSm10NGuznTpEXbHUL4H5WeeXLcb8f8AHs4sNzpuNYBM37797JlQAXsf5U7R29FG5gm6zMu500aBoSb/AM6BOeyBP39/woa7yNBJ2+ilZ9Y+/ktTKX0WKlc25SD9wsivhWuGYmRucx2335LYqlgJaCM2pbMuHkJPyVWrTpM8dQNjcv07Bpu53KyX8jGfu/puY2sIUWuMMzTt4JAjmbeqSrJAD/FsA2SD5H/pPxfFaTbBoAMkBpJzflAbGnyWdW4m0m4f1Y0GD3e428gF7cbvGVPG7TVJLgDlYRo33i3qWiwKkGIcHZGOa3m55Gd3Zo0CzX12AQ6mW5rw3NmPczZS4d7mDMyk1nIvfB+Z/ZXbWl+pnjxzl3IblBnuJJ6fJZWIwtKZqvc1v5dB5AKy7HPefGHPjdh07So6GJpyWtqOa6JDarW5XcgHx9VuVmzSljcG0jNSoyzmCSfPM2FlVGRs6OTvFHYj+FfdjXMe4MflHxseAWgzfKfvus3FVw5xcHc7fCewIC3Ns9G4amXnKQZm2vpfVHEsG6k/I+2hB2g7rZwWNpkNdAzgQRpfYnoVPi+KMrjI9mYiQCLPb0IOuuxW5bKxdVzL8WcobNgIHMQ6f77qVnEXtMtcRrI2M3NtITXUWF0NBi/cc1K7g1QjM0SB5H02WtxntWxDs5zAC/IKNuYaGOxK1GYV4bBYD0DsrvLmqdUX0cDuD/P9KbNIw91rzHf6p+bndNbVA2Tn8/sKqeaxsBb6pc/XuoQCdNNyrGGaz4rjkl6Sdo85QpXDk2210im109YBTgVGCnAru8qQFOCYE4IpyUJoTkChPc6GxuUwKngaxfmJ5n62UqyJqLi106hXBigdvMhV2slK6lay8/Lw4ct3lO28crjOlqnicx5a9ipA6bBZdZpAzExvHM6C3PX1UTeIQ2MwnYTr3K+b+Xxzh7x7j0ce8mtUyiSYPM6D1WLxPilNg8Rc4flZ4R5mQ49pA6J7qrnCYnYRoLH0v9FVdwtz7ucAdxH8Lz8P42fJ3d2frqOm8cfdZH+vPe0ik1rGjQNAGu503WPVoVHvzVAbn9Mm20mPmu1p8IE3gf4uI+UKUcMbFnHsXH6L34fj3D1ify4/bgn4V7CPDUAmbAAerD+6U2OaQydy+V2zuFwSJcNwbub9bKrVo1CcufNl1BaPpMlatv01NOXZDhfET0kg/MgKOi0MeCWB3V8EfMgH1XSYnh0iXUWuG5a2CPLX6rIdQDGuLGmJ8WYGwGsFhEeYTZtQxuKfMlpy8g1rWRN/dcQfms/FUmiHscRN8jm265TofkrmOqVKZEPBBEgA2IPMeotyWdUxDojVoJOV14kX8+vRdsY55VJiMRSc1pDSx0DMNRMfDfQ8lFWwpFMP56CNkyoc0ANg6W38jvotbF8Rp1MOxjbOpj1BtHqD8ua3pm1gODmxHwmfp8rKEvMzyWnXDS0PjUQe40P3yWc9/kVuVzqTDYhzXZwbi5XUs/EbAQx4FwIfFpH5gNO64+LdUN6/v9hWzabbXGqha7M0y1/KCyf2Kyy6blGWR/evknNbA29QpOmjmMlWXUg0ZnXHIET5oeYpyZkxlINh3CgpAGA4x3+qCxlLhmAAbyUoxDTTLQ0AxyuqrajmeGZHy+arvqQYaO6kmy6WPHyQpmG39oTdNR6iCngqIJ4Xd5UgKeFGE8IpwShNSoFebHsfosPheMGYgm63F57XxJp4h7DaCR6GFL6bxjsq/FQ0w2JVnB44luZzg0c/4nVcPhsTml+pBgA6T+rmN46LrcNhXPAc+BYaAjQbTJ8zdcri6TU9q3EK1StUIZIaPuXK3w/hwYJd43cz5zA9FoU6LWiGiPvdPAT+LG/2m2LyX4J/SchC6OZyE1OWgocq+PpeDO1odGo3MXgEaFTpQ6Fy5MJlP23hncayxXc9vgMEXYTra+V2xB0+5XN8fxTXAPHgeBqNzrlcNwRmHcdV0OQtrPZMB7S5h5EESP8AyaVgfiCk12cOADokDpJIjtDl5JLPb17l9MDH4WaTHtMgTI5Sb/MLLe8ACdxM9QrTahENzGP5+/kqFfXTy8tvRd8WanpQHNz3ENJg8wB6ifkoalKHmCY2KY1ttY5j6EIY5wJ7eWsrWmdrBpODcp094dtD5Km5munaVO+rma0bDbl23Cv06dEsGgcfzEkdim9J7YxdFoJ9ISsv+nvdTVmXsBHT/spIi8rW2TiANCe+g8kzL8Wx36jY8lI5n/thxls6WtAtPqoGPMydT80kLUrZm+ijqvIcY0my1KOELmTlJ7KCsG5g32YYedyT62U8l1VZhLtNUr2FvUro+CYMG7miOuqs8T4QwiWWOsbHsufn3p08Zpy/tEKT2TeaFrcTxr1Rqe1RhPC9DyJAlCaEoQPSpspZQOC4n8a8OcKja7RZ4ymNc4FvVo9R1XaSmYii17Cx4kH9rgg7EEAg9FlrG6rmvwNQpupOeWgvDrk3i1o5LrZXOcK4a/D4p2UE06gMkRAcLgkfDuOXiXQyrC+zpSymylVZKhJKECpU1KgVCRCDL41TcMr27OHzBB9QsD8R1M7w9ugDbcgfEPUEn1XWY2lnplu+o7gyFzuJ4b7T3HZX5S0To6bta/oTI6QI3Xl5MdZPTxZbn+nJcVwvs6pZ8Lsrm9iJIHa48lVx9GHff3quj4rSNTCUXwW1WuFOIuXAuAHfwg9UzH8OD8P7Zl8jixw3iYn1v/uUmWu27NuWLQN/v7hXcPQbUY4geJgkt/M0alvUf3zVSsy3axS4aq5hDmmHNuOvMHpEroyhqu8RjRICrOIY0nM2wdcDlO3kbKu8QFUOzJHxso2m6sYlrS0OaT1FreiQvpcw9RvsgXXLXx5OuZ6WVbFvY4jI0CJUNGoQHN1BFx20TDzhGW9wSv8ACVfrYcPkh2YbtOrSNCCsf8OPHtLlaVSqadQOBGpBadwf2WbOmt9kwuLgOZoW+n9Ix3FG+zykw7aNYOqqcTwoY9z2OBLvFAMwD+YLKMuIJEjnuufj3t0ncSVWtk3nrBuhWe72+iFpdR6S1PCjaU8Fep4TwU8FRgpQUU+UspspZRCyllNlAKB4KVNBSoHSllNSoHSllNRKB0olNSoFQhCAWdxGg0ZzB8TLEbEGbdtfNaKzuLyGtcNj67x8ly5J/jt047rLTC4i8vwrKhgubXol8aEteGh4/wAmlp8wpcNVZTe9gvSrM9oOnvCoB5QVBxegW03MGhAe0bS1zXs+eVo/SxyocQo5H0nNdbM+mTOkmQD0s6ekLzR6Ypcc4Y+i+Y8JNjtMTH8dOyxXN36r0TG4puaix7QWVmw6bxAk32iRf9JWNxn8POZLmDM3tcdxutzLUNbcqbBRO6q5iGZZ/wCtFRe5bl36Zs0MgPRSOLAzedDy6EJmUkSkItCu00Yx11pMwuemSw3i4WWGxdddwPAirQcWHxt25qZUk6cwxsd1o+3L6RabubedyN1VxVPK86i92kXB3TqL7z6q7NEa1xlzTq0Ndzgckj6joDW2A+asYUhmY6gNNt1Tp+MztCxfbePo4VBuEqr5BzQrqfa+V+nrTSnAqNpTwV6XhPBTgmApQUVIiU2UIHylTAnIhQUoKRCBydKYlQOSpoKVAqUJELKnISIlAsqpxNhNN2X3mw5vdpmPkrKQpZuaJdXbFxjRVoseD4TmpmNpEt7G7mz+srmccC6nUpviXOc5ltHskmBzcA6O55Lp8NhsprYcWD/Gzo4EER6D/gVSx/DnVGZ2C9nt3OdnvNIOh1HqvF3MtPZNWKnESamAY8C7AD1hzXNcD5Ob81o/hrjAr0cj7vpgB3NzdndeR691X4RRb7OphjOVwcWbwx0Fo/yaXELkg+rg8VMeJhuNnsPLmCFqTcsHTfinhoyio1oIOpHy8lxVYQYhepYPFU69MZbseJAPXUdLyFyP4l4E6mc7BLPosYZeN1WrNxzNJ94TRqlhI8CV6HNaODLqedug94clY/D/ABU4aqHatNnjp06qvgMZkJDrscMrx+46qriGZXEAyNjzGxU18U277i/CqeKZ7ag4Zt+vQ8iuQqYZzCZaQRqE7g3Fn0XS0y34m7ELqa+SszO2HAjbXseqxqzprbBZgjWymnobPG4KzsQ1tNxaAQ64dOis1c+HqZmOOujt/wCVO+pTxLs8ZXkeNnM8wl6XG76YzaRO4QtI8PeLezdbqhPJvT0JpTwUiF7HzjwUoKEIpwKUFCEDgllCEColCECyiUIWQ4JUIQKiUIQEoQhAIQhBR4hSdLajPeYSY5jcffVV8JVD3VGsd7/jZ0eIzNPnBP8AkkQvNy/2j0cf9Wbg6DnkvZ4WOBcJMOp1Jh7DzDsx9OyOL4BuLpEEZK9LnqRqJjVrrmdr8kqFiOjmeD4ypQqeydLTMsnTNy6tcB6x3HoWDqsxNEOGjgQQdiLEHshCucix51+IeGuo1C0ggGcpixH8rGLRzQhaw9JkGN6odJGXlohC2yjpvWtwnGPYTkPcG4PcIQpl6MW3TxdOuC14DX8tj2WBxPhr6bs7JgXtqEIWYtTUvxZXAAIYY3Iue6EIWvDE8q//2Q==';
    const buffer = Buffer.from(data, 'base64');
    const file = {
      fieldname: 'file',
      originalname: 'test.png',
      encoding: '7bit',
      mimetype: 'image/png',
      buffer: buffer,
      size: 2000000,
      stream: null,
      destination: '',
      path: '',
      filename: '',
    };
    jest.spyOn(service, 'updateProfilePicture').mockImplementation();
    await controller.uploadFile(user, file);
    expect(service.updateProfilePicture).toBeCalledWith(
      user.id,
      file.buffer,
      'image/png',
    );
  });

  it('testing uploadFile method with wrong file type', async () => {
    const file = {
      fieldname: 'file',
      originalname: 'test.csv',
      encoding: '7bit',
      mimetype: 'text/csv',
      buffer: null,
      size: 2000000,
      stream: null,
      destination: '',
      path: '',
      filename: '',
    };
    try {
      await controller.uploadFile(user, file);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect(e.message).toBe('File type must be PNG or JPEG!');
    }
  });

  it('testing uploadFile method with wrong file size', async () => {
    const file = {
      fieldname: 'file',
      originalname: 'test.png',
      encoding: '7bit',
      mimetype: 'image/png',
      buffer: null,
      size: 2500000,
      stream: null,
      destination: '',
      path: '',
      filename: '',
    };
    try {
      await controller.uploadFile(user, file);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect(e.message).toBe('File size must not exceed 2MB!');
    }
  });

  it('testing uploadFile method with wrong file dimensions', async () => {
    const data =
      'iVBORw0KGgoAAAANSUhEUgAAAaQAAAGkCAIAAADxLsZiAAAF6klEQVR4nOzXUXGsShhG0Tu3eEcXjpCAI2w1Ds5bFCTdM7PXMvD9VUl2mm2M8R/At/t/9QEAM4gdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QsE1buu5n2hbwQc5jn7DiZQckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZDwGmOsvuFTXfez+oTfdx776hN+k58RP7zsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgYZu2dN3PtC34Yt/3p3Qe+4QVLzsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBhW30A7+W6n9UnwJ94jTFW38C7+MrSnce++gTegs9YIEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IEDsgQeyABLEDEsQOSBA7IEHsgASxAxLEDkgQOyBB7IAEsQMSxA5IeI0x5ixd9zNnCPgs57FPWNkmbPBB5vzaTeNfLD98xgIJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQmvMcbqGwD+nJcdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2QIHZAgtgBCWIHJIgdkCB2QILYAQliBySIHZAgdkCC2AEJYgckiB2Q8C8AAP//Fbwqv2NwA2kAAAAASUVORK5CYII=';
    const buffer = Buffer.from(data, 'base64');
    const file = {
      fieldname: 'file',
      originalname: 'test.png',
      encoding: '7bit',
      mimetype: 'image/png',
      buffer: buffer,
      size: 2000000,
      stream: null,
      destination: '',
      path: '',
      filename: '',
    };
    try {
      await controller.uploadFile(user, file);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect(e.message).toBe(
        'File dimensions cannot be greater than 400 x 400!',
      );
    }
  });

  it('should return profile image by current User, 1', async () => {
    jest.spyOn(service, 'retrieve').mockImplementation();
    const results = await controller.retrieveFile(user);
    expect(service.retrieve).toBeCalled();
    expect(service.retrieve).toBeCalledWith(user.id);
    expect(results).toStrictEqual({});
  });

  it('should return profile image by current User, 2', async () => {
    const image = Buffer.from('hello world');
    jest.spyOn(service, 'retrieve').mockResolvedValue({
      id: 1,
      email: 'kdljflsd@sdfkjhsdkf.com',
      profilePicture: image.toJSON(),
    } as User);
    const results2 = await controller.retrieveFile(user);
    expect(results2).toStrictEqual(image.toJSON());
  });
});
