import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { UserData } from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { Response } from 'express';
import * as mocks from 'node-mocks-http';
import { UpdateEmailDTO, UpdateUserDTO } from '../dto';
import { Occupation, Skill } from '../entities';
import { User } from '../entities/user.entity';
import { UserService } from '../services';
import { UserController } from './user.controller';

describe('UserController', () => {
  let controller: UserController;
  let service: any;
  let authService: any;
  let user: User;
  let userData: UserData;
  let mockLogger: any;
  let mockAuthService: any;
  let mockOccupationService: any;
  let mockSkillService: any;
  let res: Response;
  let mockConfigService: any;

  beforeEach(async () => {
    user = new User();
    user.email = 'test@test.org';

    userData = {
      username: user.email,
      id: user.id,
    } as UserData;

    mockLogger = jest.fn(() => ({
      debug: () => jest.fn(),
      log: () => jest.fn(),
    }));

    mockAuthService = jest.fn(() => ({
      deleteUser: () => jest.fn(),
      login: () => jest.fn(),
      logout: () => jest.fn(),
      authenticate: () => jest.fn(),
      register: () => jest.fn(),
      verify: () => jest.fn(),
      resetPassword: () => jest.fn(),
      checkPasswordStrength: () => jest.fn(),
      changePassword: () => jest.fn(),
      changeEmail: () => jest.fn(),
    }));

    mockOccupationService = jest.fn();
    mockSkillService = jest.fn();

    service = {
      updateEmail: () => jest.fn(),
      retrieve: () => jest.fn(),
      delete: () => jest.fn(),
      requestResolverSettings: () => jest.fn(),
      shareProfileEnabled: () => jest.fn(),
      update: () => jest.fn(),
      getUserProfile: () => jest.fn(),
      updateLanguage: () => jest.fn(),
    };

    mockConfigService = jest.fn();

    controller = new UserController(
      service,
      mockConfigService,
      mockAuthService,
    );

    // const module: TestingModule = await Test.createTestingModule({
    //   // imports: [AuthenticationModule],
    //   controllers: [UserController],
    //   providers: [
    //     // UserService,
    //     {
    //       provide: UserService,
    //       useValue: {
    //         updateEmail: () => jest.fn(),
    //         retrieve: () => jest.fn(),
    //         delete: () => jest.fn(),
    //         requestResolverSettings: () => jest.fn(),
    //         shareProfileEnabled: () => jest.fn(),
    //         update: () => jest.fn(),
    //         getUserProfile: () => jest.fn(),
    //         updateLanguage: () => jest.fn(),
    //       },
    //     },
    //     {
    //       provide: ConfigService,
    //       useClass: jest.fn(() => ({
    //         get: (param) => {
    //           switch (param) {
    //             case 'vault.vaultRootToken':
    //               return 'abcd1234';
    //             default:
    //               return 'abc';
    //           }
    //         },
    //       })),
    //     },
    //     {
    //       provide: AuthenticationService,
    //       useValue: {
    //         login: () => jest.fn(),
    //         logout: () => jest.fn(),
    //         authenticate: () => jest.fn(),
    //         register: () => jest.fn(),
    //         verify: () => jest.fn(),
    //         resetPassword: () => jest.fn(),
    //         checkPasswordStrength: () => jest.fn(),
    //         changePassword: () => jest.fn(),
    //         changeEmail: () => jest.fn(),
    //       },
    //     },
    //   ],
    // }).compile();
    // service = module.get<UserService>(UserService);
    // authService = module.get<AuthenticationService>(AuthenticationService);
    // controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(mockAuthService).toBeDefined();
  });

  it('should return current user', async () => {
    user.qualificationsList = [1];
    jest.spyOn(service, 'getUserProfile').mockResolvedValue(user as any);
    expect((await controller.getProfile(userData)).email).toBe(
      userData.username,
    );
  });

  it('should return a user', async () => {
    jest.spyOn(service, 'retrieve').mockResolvedValue(user);
    expect(((await controller.getUserById(1)) as User).email).toBe(user.email);
  });

  // it('should delete a user', async () => {
  //   res = mocks.createResponse();
  //   // jest.spyOn(mockAuthService, 'deleteUser').mockResolvedValue(null);
  //   jest.spyOn(service, 'delete').mockImplementation();
  //   await controller.delete(userData, res);
  //   const response = res['_getData']();
  //   expect(response.statusCode).toBe(200);
  //   expect(response.message).toBe('Success');
  // });

  it('should return a civilStatus list', async () => {
    expect(await controller.enumsList('civilstatus')).toBeInstanceOf(Array);
  });
  it('should return a culturalInterest list', async () => {
    expect(await controller.enumsList('culturalinterest')).toBeInstanceOf(
      Array,
    );
  });
  it('should return a transportationMeans list', async () => {
    expect(await controller.enumsList('transportationMeans')).toBeInstanceOf(
      Array,
    );
  });
  it('should return a disabilities list', async () => {
    expect(await controller.enumsList('disabilities')).toBeInstanceOf(Array);
  });

  it('testing requestResolverSettings', async () => {
    jest.spyOn(service, 'requestResolverSettings').mockImplementation();
    controller.requestResolverSettings({ id: 1 } as UserData, true);
    expect(service.requestResolverSettings).toBeCalledWith(1, true);
  });

  it('testing shareProfile', async () => {
    jest.spyOn(service, 'shareProfileEnabled').mockImplementation();
    controller.shareProfile({ id: 1 } as UserData, true);
    expect(service.shareProfileEnabled).toBeCalledWith(1, true);
  });

  // it('testing changeEmail 1', async () => {
  //   jest.spyOn(service, 'updateEmail').mockImplementation();
  //   jest
  //     .spyOn(mockAuthService, 'changeEmail')
  //     .mockResolvedValue(Promise.resolve(true));

  //   const data = { email: 'jsdhfkjshdfkj@fjshdkfjsd.com' } as UpdateEmailDTO;
  //   await controller.changeEmail({ id: 1 } as UserData, data);
  //   expect(service.updateEmail).toBeCalledWith(1, data);
  // });

  // it('testing changeEmail 2', async () => {
  //   jest.spyOn(service, 'updateEmail').mockImplementation();
  //   jest
  //     .spyOn(mockAuthService, 'changeEmail')
  //     .mockResolvedValue(Promise.resolve(false));

  //   const data = { email: 'jsdhfkjshdfkj@fjshdkfjsd.com' } as UpdateEmailDTO;
  //   try {
  //     expect(
  //       await controller.changeEmail({ id: 1 } as UserData, data),
  //     ).toThrowError();
  //   } catch (error) {
  //     expect(error).toBeDefined();
  //   }
  // });

  it('testing update', async () => {
    jest.spyOn(service, 'update').mockImplementation();
    const data = { email: 'jsdhfkjshdfkj@fjshdkfjsd.com' } as UpdateUserDTO;
    controller.update({ id: 1 } as UserData, data);
    expect(service.update).toBeCalledWith(1, data);
  });

  it('testing update birthDate', async () => {
    jest.spyOn(service, 'update').mockImplementation();
    const data = {
      ...new UpdateUserDTO(),
      ...{ birthDate: new Date('03/03/1987') },
    };
    controller.update({ id: 1 } as UserData, data);
    expect(service.update).toBeCalledWith(1, data);
  });

  it('should update language', async () => {
    jest.spyOn(service, 'updateLanguage').mockImplementation();

    controller.updateLanguage({ id: 1 } as UserData, 'en');
    expect(service.updateLanguage).toBeCalledWith(1, 'en');
  });
});
