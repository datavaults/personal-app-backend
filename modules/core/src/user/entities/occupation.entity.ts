import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Occupation {
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column({ nullable: true })
  readonly conceptType: string;

  @Column({ nullable: true })
  readonly conceptUri: string;

  @Column({ nullable: true })
  readonly iscoGroup: string;

  @Column({ nullable: true })
  readonly skillType: string;

  @Column({ nullable: true })
  readonly reuseLevel: string;

  @Column({ nullable: true })
  readonly preferredLabel: string;

  @Column({ nullable: true })
  readonly altLabels: string;

  @Column({ nullable: true })
  readonly hiddenLabels: string;

  @Column({ nullable: true })
  readonly status: string;

  @Column({ nullable: true })
  readonly modifiedDate: Date;

  @Column({ nullable: true })
  readonly regulatedProfessionNote: string;

  @Column({ nullable: true })
  readonly scopeNote: string;

  @Column({ nullable: true })
  readonly definition: string;

  @Column({ nullable: true })
  readonly inScheme: string;

  @Column({ nullable: true })
  readonly description: string;
}
