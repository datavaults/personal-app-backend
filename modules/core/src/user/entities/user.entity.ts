/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, ObjectType } from '@nestjs/graphql';
import { PseudoID } from '@suite5/anonymiser/entities';
import { Asset } from '@suite5/asset/entities/asset.entity';
import { BaseEntity, IsValidEmail } from '@suite5/common';
import { Message } from '@suite5/message/entities';
import { Column, Entity, OneToMany } from 'typeorm';
import { Demonstrators } from '../constants';

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @Column({ nullable: true })
  @Field()
  firstName: string;

  @Column({ nullable: true })
  @Field()
  lastName: string;

  @Column({ unique: true })
  @IsValidEmail()
  email: string;

  @Column({ default: true, nullable: true })
  shareProfile: boolean;

  @Column({ default: true, nullable: true })
  requestResolverSettings: boolean;

  @Column({ nullable: true })
  street: string;

  @Column({ nullable: true })
  postcode: string;

  @Column({ nullable: true })
  region: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  country: string;

  @Column({ nullable: true })
  nationality: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  birthDate: Date;

  @Column({ nullable: true })
  placeOfBirth: string;

  @Column({ nullable: true })
  nationalInsuranceNumber: string;

  @Column({ nullable: true })
  socialSecurityNumber: string;

  @Column({ nullable: true })
  memberNumber: string;

  @Column('jsonb', { nullable: true })
  profilePicture: any;

  @Column({ nullable: true })
  profilePictureType: string;

  @Column({ type: 'int', default: [], array: true })
  qualificationsList: number[];

  @Column({ type: 'int', default: [], array: true })
  occupationsList: number[];

  @Column({ type: 'int', default: [], array: true })
  transportationList: number[];

  @Column({ type: 'int', default: [], array: true })
  disabilitiesList: number[];

  @Column({ type: 'int', default: [], array: true })
  culturalInterestList: number[];

  @Column({ type: 'int', default: [], array: true })
  civilStatusList: number[];

  @Column({ nullable: true })
  sub: string;

  @OneToMany('Message', 'receiver')
  messages: Message[];

  @OneToMany('PseudoID', 'user')
  pseudoIDs: PseudoID[];

  @Column({ nullable: true })
  @Field()
  emailVerificationToken: string;

  @Column({ nullable: true })
  @Field()
  resetPasswordVerificationToken: string;

  @OneToMany('Asset', 'createdBy')
  assets: Asset[];

  @Column({
    type: 'enum',
    enum: Demonstrators,
    array: false,
    nullable: true,
  })
  @Field()
  demonstrator: Demonstrators;

  @Column({ nullable: true })
  @Field()
  language: string;

  @Column({ nullable: true })
  @Field()
  lastTermsAcceptance: Date;
}
