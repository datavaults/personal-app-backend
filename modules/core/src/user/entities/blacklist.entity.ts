/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
@ObjectType()
export class Blacklist extends BaseEntity {
  @Column()
  @Field()
  dataSeekerUUID: string;

  @Column()
  @Field()
  dataSeekerName: string;

  @Column()
  @Field()
  dataSeekerPublicURL: string;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  blacklistedBy: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  blacklistedById: number;
}
