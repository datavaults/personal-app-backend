import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { DataOwnerProfileDTO } from '../dto';
import { CloudUserStrategy } from './update-user-profile-to-cloud.strategy';

@Injectable()
export class RealCloudUserStrategy implements CloudUserStrategy {
  private readonly UPDATE_DATA_OWNER_PORIFLE_URL = `${this.configService.get(
    'user.module.cloudUrl',
  )}/dataset/dataowner`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async login(): Promise<string> {
    const params = new URLSearchParams();
    params.append(
      'username',
      this.configService.get('user.module.cloudKeycloakUsername'),
    );
    params.append(
      'password',
      this.configService.get('user.module.cloudKeycloakPassword'),
    );
    params.append('grant_type', 'password');
    params.append(
      'client_id',
      this.configService.get('user.module.cloudKeycloakClientId'),
    );
    params.append(
      'client_secret',
      this.configService.get('user.module.cloudKeycloakClientSecret'),
    );

    return new Promise<string>((resolve, reject) => {
      this.httpService
        .post(
          `${this.configService.get(
            'user.module.cloudKeycloakUrl',
          )}/realms/${this.configService.get(
            'user.module.cloudKeycloakRealm',
          )}/protocol/openid-connect/token`,
          params,
          {
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
          },
        )
        .toPromise()
        .then((resp) => resolve(resp.data.access_token))
        .catch((e) => {
          reject(e);
        });
    });
  }

  private getHeaders = async () => {
    return {
      Accept: 'application/json',
      'content-type': 'application/json',
      Authorization: `Bearer ${await this.login()}`,
    };
  };

  private async postToCloud(configuration: any, url: any) {
    try {
      return await this.httpService
        .post(url, JSON.stringify(configuration), {
          headers: await this.getHeaders(),
        })
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        });
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  async updateDataOwnerProfile(configuration: any): Promise<boolean> {
    const cloudUser: DataOwnerProfileDTO = {
      region: configuration.region,
      city: configuration.city,
      shareProfile: configuration.shareProfile,
      country: configuration.country,
      placeOfBirth: configuration.placeOfBirth,
      nationality: configuration.nationality,
      birthDate: configuration.birthDate as any,
      postalCode: configuration.postcode,
      nationalInsuranceNumber: configuration.nationalInsuranceNumber,
      socialSecurityNumber: configuration.socialSecurityNumber,
      requestResolverSettings: configuration.requestResolverSettings,
      occupations: configuration.occupationsList,
      educations: configuration.qualificationsList,
      culturalInterests: configuration.culturalInterestList,
      transportationMeans: configuration.transportationList,
      civilStatus: configuration.civilStatusList,
      disabilities: configuration.disabilitiesList,
      demonstrator: configuration.demonstrator,
    };
    return await this.postToCloud(
      { dataOwnerProfile: cloudUser, dataOwnerUUID: configuration.sub },
      this.UPDATE_DATA_OWNER_PORIFLE_URL,
    );
  }
}
