import { Injectable } from '@nestjs/common';
import { CloudUserStrategy } from './update-user-profile-to-cloud.strategy';

@Injectable()
export class FakeCloudUserStrategy implements CloudUserStrategy {
  async updateDataOwnerProfile(configuration: any): Promise<boolean> {
    return true;
  }
}
