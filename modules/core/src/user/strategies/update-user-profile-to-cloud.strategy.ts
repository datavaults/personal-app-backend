export const CLOUD_USER_TOKEN = 'ShareUserStrategy';

export interface CloudUserStrategy {
  updateDataOwnerProfile(userData: any): Promise<any>;
}
