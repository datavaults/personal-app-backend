import { forwardRef, HttpModule, Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VaultModule } from '@suite5/vault';
import {
  BlacklistController,
  EmailController,
  FileController,
  OccupationController,
  SkillController,
  UserController,
} from './controllers';
import { Blacklist, Occupation, Skill, User } from './entities';
import { UserResolver } from './resolvers/user.resolver';
import {
  BlacklistService,
  OccupationService,
  SkillService,
  UserService,
} from './services';
import userConfig from './user.module.config';
import { AuthenticationModule } from '@suite5/core/authentication/authentication.module';
import { CommonModule } from '@suite5/common/common.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import {
  CLOUD_USER_TOKEN,
  FakeCloudUserStrategy,
  RealCloudUserStrategy,
} from './strategies';

@Module({
  imports: [
    CommonModule,
    forwardRef(() => AuthenticationModule),
    ConfigModule.forFeature(userConfig),
    forwardRef(() =>
      TypeOrmModule.forFeature([User, Occupation, Skill, Blacklist]),
    ),
    VaultModule,
    HttpModule,
    EventEmitterModule.forRoot(),
  ],
  providers: [
    UserService,
    Logger,
    UserResolver,
    OccupationService,
    SkillService,
    BlacklistService,
    {
      provide: CLOUD_USER_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeCloudUserStrategy
          : RealCloudUserStrategy,
    },
  ],
  exports: [UserService, BlacklistService],
  controllers: [
    UserController,
    FileController,
    OccupationController,
    SkillController,
    BlacklistController,
    EmailController,
  ],
})
export class UserModule {}
