export enum UserFields {
  FirstName = 'firstName',
  LastName = 'lastName',
  Street = 'street',
  Postcode = 'postcode',
  Region = 'region',
  City = 'city',
  Country = 'country',
  Nationality = 'nationality',
  Phone = 'phone',
  BirthDate = 'birthDate',
  PlaceOfBirth = 'placeOfBirth',
  NationalInsuranceNumber = 'nationalInsuranceNumber',
  SocialSecurityNumber = 'socialSecurityNumber',
  ProfilePicture = 'profilePicture',
  Occupations = 'occupations',
  Qualifications = 'qualifications',
  CulturalInterest = 'culturalInterest',
  TransportationMeans = 'transportationMeans',
  CivilStatus = 'civilStatus',
  Disabilities = 'disabilities',
}
