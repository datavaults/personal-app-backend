export enum CivilStatus {
  Married = 'Married',
  Widowed = 'Widowed',
  Separated = 'Separated',
  Divorced = 'Divorced',
  Single = 'Single',
  DoNotAnswer = 'I do not want to answer',
}
