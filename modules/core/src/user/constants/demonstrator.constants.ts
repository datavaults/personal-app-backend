export enum Demonstrators {
  Prato = 'prato',
  Olympiacos = 'olympiacos',
  Piraeus = 'piraeus',
  MIWenergia = 'miwenergia',
  Andaman7 = 'andaman7',
  ExternalParticipant = 'external participant',
}
