export enum CulturalInterest {
  VisualArts = 'Visual Arts',
  PerformingArts = 'Performing Arts',
  PlasticArts = 'Plastic Arts',
  Music = 'Music',
  Fashion = 'Fashion',
  Architecture = 'Architecture',
  History = 'History',
  DoNotAnswer = 'I do not want to answer',
}
