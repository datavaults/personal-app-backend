export enum TransportationMeans {
  OnFoot = 'On foot',
  Bicycle = 'Bicycle',
  Motorcycle = 'Motorcycle',
  Car = 'Car',
  Taxi = 'Taxi',
  PublicBus = 'Public Bus',
  Tram = 'Tram',
  Subway = 'Subway',
  DoNotAnswer = 'I do not want to answer',
}
