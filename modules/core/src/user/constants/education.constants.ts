export enum Education {
  HignSchool = 'High school',
  BachelorDegree = 'Bachelor Degree',
  MasterDegree = 'Master`s degree',
  DoctorOfPhilosophy = 'Doctor of Philosophy',
}
