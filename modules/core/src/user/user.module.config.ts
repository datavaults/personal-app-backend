import { registerAs } from '@nestjs/config';

export default registerAs('user.module', () => ({
  mock: process.env.USER_MODULE_MOCK_SERVICE === 'true',
  cloudUrl: process.env.CLOUD_PLATFORM_BACKEND_URL || '/api/v1/dataset',
  cloudKeycloakUsername: process.env.CLOUD_PLATFORM_KEYCLOAK_USERNAME,
  cloudKeycloakPassword: process.env.CLOUD_PLATFORM_KEYCLOAK_PASSWORD,
  cloudKeycloakClientId: process.env.CLOUD_PLATFORM_KEYCLOAK_CLIENT_ID,
  cloudKeycloakClientSecret: process.env.CLOUD_PLATFORM_KEYCLOAK_CLIENT_SECRET,
  cloudKeycloakUrl: process.env.CLOUD_PLATFORM_KEYCLOAK_URL,
  cloudKeycloakRealm: process.env.CLOUD_PLATFORM_KEYCLOAK_REALM,
}));
