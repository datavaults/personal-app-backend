import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  AccessPolicyTemplate,
  AccessPolicyTemplateController,
  AccessPolicyTemplateService,
} from '.';
import accessPoliciesConfig from './access-policies.module.config';

@Module({
  imports: [
    ConfigModule.forFeature(accessPoliciesConfig),
    TypeOrmModule.forFeature([AccessPolicyTemplate]),
  ],
  providers: [AccessPolicyTemplateService],
  exports: [AccessPolicyTemplateService],
  controllers: [AccessPolicyTemplateController],
})
export class AccessPoliciesModule {}
