import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DataFetcherService, TransactionService } from '@suite5/asset';
import { AnonymiserService } from '@suite5/anonymiser';
import { Repository } from 'typeorm';
import { UserService } from '@suite5/core/user';
import { UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { ConfigService } from '@nestjs/config';
import { AccessPolicyTemplateController } from '.';
import {
  AccessPolicyTemplate,
  AccessPolicyTemplateService,
  CreateAccessPolicyTemplateDTO,
  UpdateAccessPolicyTemplateDTO,
} from '..';

describe('AccessPolicyTemplateController', () => {
  let controller: AccessPolicyTemplateController;
  let service: AccessPolicyTemplateService;
  let accessPolicyTemplate: AccessPolicyTemplate;

  const owner = {
    id: 1,
    username: 'owner@test.com',
  } as UserData;

  beforeEach(async () => {
    accessPolicyTemplate = <AccessPolicyTemplate>{
      id: 1,
      userId: 1,
      name: 'policy1',
      description: 'Description Policy 1',
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccessPolicyTemplateController],
      providers: [
        {
          provide: getRepositoryToken(AccessPolicyTemplate),
          useClass: Repository,
        },
        AccessPolicyTemplateService,
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        {
          provide: UserService,
          useValue: {
            retrieve: () => jest.fn(),
            getProfileById: () => jest.fn(),
          },
        },
        {
          provide: AnonymiserService,
          useValue: {
            createPseudoID: () => jest.fn(),
            fetchAnonymisedData: () => jest.fn(),
          },
        },
        {
          provide: DataFetcherService,
          useValue: {
            retrieveAsset: () => jest.fn(),
            readMongoAsset: () => jest.fn(),
          },
        },
        {
          provide: TransactionService,
          useValue: {
            create: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<AccessPolicyTemplateController>(
      AccessPolicyTemplateController,
    );
    service = module.get<AccessPolicyTemplateService>(
      AccessPolicyTemplateService,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it("should retrieve all user's access policy template", async () => {
    jest
      .spyOn(service, 'retrieveTemplatesByUserId')
      .mockResolvedValue([accessPolicyTemplate]);
    expect(await controller.getAllAccessPolicyTemplate(owner)).toStrictEqual([
      accessPolicyTemplate,
    ]);
  });

  it('should create an access policy template', async () => {
    const template: CreateAccessPolicyTemplateDTO = {
      userId: accessPolicyTemplate.userId,
      name: accessPolicyTemplate.name,
      description: accessPolicyTemplate.description,
      configuration: accessPolicyTemplate.configuration,
    } as CreateAccessPolicyTemplateDTO;
    const result: AccessPolicyTemplate = accessPolicyTemplate;
    jest.spyOn(service, 'createTemplate').mockResolvedValue(result);

    expect(await controller.createTemplate(owner, template)).toBe(result);
  });

  it('should update an access policy template', async () => {
    const template: UpdateAccessPolicyTemplateDTO = {
      description: accessPolicyTemplate.description,
      configuration: accessPolicyTemplate.configuration,
    } as UpdateAccessPolicyTemplateDTO;
    const result: AccessPolicyTemplate = accessPolicyTemplate;
    jest.spyOn(service, 'updateTemplate').mockResolvedValue(result);

    expect(await controller.updateTemplate(owner, 1, template)).toBe(result);
  });
});
