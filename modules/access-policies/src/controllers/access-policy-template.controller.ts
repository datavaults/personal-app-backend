import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import {
  CreateAccessPolicyTemplateDTO,
  UpdateAccessPolicyTemplateDTO,
} from '../dto';
import { AccessPolicyTemplate } from '../entities';
import { AccessPolicyTemplateService } from '../services/access-policy-template.service';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
@Controller('access-policy-template')
@UseGuards(AuthenticationGuard)
@ApiTags('access-policy-template')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class AccessPolicyTemplateController {
  constructor(private readonly service: AccessPolicyTemplateService) {}

  @Post()
  @ApiOperation({ summary: 'Create an access policy template' })
  async createTemplate(
    @CurrentUser() user: UserData,
    @Body() template: CreateAccessPolicyTemplateDTO,
  ): Promise<AccessPolicyTemplate> {
    return await this.service.createTemplate(template, user.id);
  }

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: "Retrieve user's access policy templates",
  })
  async getAllAccessPolicyTemplate(@CurrentUser() user: UserData) {
    return await this.service.retrieveTemplatesByUserId(user.id);
  }

  @Patch(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Update access policy template',
  })
  @ApiBody({ type: UpdateAccessPolicyTemplateDTO })
  async updateTemplate(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @Body() data: UpdateAccessPolicyTemplateDTO,
  ): Promise<AccessPolicyTemplate> {
    return await this.service.updateTemplate(id, user.id, data);
  }
}
