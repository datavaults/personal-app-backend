import { BaseEntity } from '@suite5/common/entities';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from '@suite5/core/user/entities';
import { Field, Int } from '@nestjs/graphql';

@Entity()
export class AccessPolicyTemplate extends BaseEntity {
  @Column()
  name: string;

  @Column({ nullable: true })
  description: string;

  @Column({ type: 'jsonb' })
  configuration: any;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  user: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  userId: number;
}
