import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateAccessPolicyTemplateDTO {
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  @IsOptional()
  readonly description: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly configuration: any;
}
