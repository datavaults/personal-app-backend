import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  CreateAccessPolicyTemplateDTO,
  UpdateAccessPolicyTemplateDTO,
} from '../dto';
import { AccessPolicyTemplate } from '../entities';

@Injectable()
export class AccessPolicyTemplateService {
  constructor(
    @InjectRepository(AccessPolicyTemplate)
    private readonly accessPolicyTemplateRepository: Repository<AccessPolicyTemplate>,
  ) {}

  async createTemplate(
    templateData: CreateAccessPolicyTemplateDTO,
    userId: number,
  ): Promise<AccessPolicyTemplate> {
    const template = this.accessPolicyTemplateRepository.create({
      ...templateData,
      ...{ userId },
    });
    return await this.accessPolicyTemplateRepository.save(template);
  }

  async retrieveTemplatesByUserId(
    userId: number,
  ): Promise<AccessPolicyTemplate[]> {
    return await this.accessPolicyTemplateRepository.find({ userId });
  }

  async updateTemplate(
    id: number,
    userId: number,
    templateData: UpdateAccessPolicyTemplateDTO,
  ): Promise<AccessPolicyTemplate> {
    const template = await this.accessPolicyTemplateRepository.findOneOrFail({
      id,
      userId,
    });
    this.accessPolicyTemplateRepository.merge(template, templateData);

    return await this.accessPolicyTemplateRepository.save(template);
  }
}
