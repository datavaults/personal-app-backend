import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccessPolicyTemplateService } from '.';
import { AccessPolicyTemplate } from '../entities';
import {
  CreateAccessPolicyTemplateDTO,
  UpdateAccessPolicyTemplateDTO,
} from '../dto';

describe('AccessPolicyTemplateService', () => {
  let service: AccessPolicyTemplateService;
  let repository: Repository<AccessPolicyTemplate>;
  let accessPolicyTemplate: AccessPolicyTemplate;

  beforeEach(async () => {
    accessPolicyTemplate = <AccessPolicyTemplate>{
      id: 1,
      userId: 1,
      name: 'policy1',
      description: 'Description Policy 1',
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccessPolicyTemplateService,
        {
          provide: getRepositoryToken(AccessPolicyTemplate),
          useClass: Repository,
        },
      ],
    }).compile();
    service = module.get<AccessPolicyTemplateService>(
      AccessPolicyTemplateService,
    );
    repository = module.get<Repository<AccessPolicyTemplate>>(
      getRepositoryToken(AccessPolicyTemplate),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });

  it('should create a access policy template', async () => {
    const data: CreateAccessPolicyTemplateDTO = {
      userId: accessPolicyTemplate.userId,
      name: accessPolicyTemplate.name,
      description: accessPolicyTemplate.description,
      configuration: accessPolicyTemplate.configuration,
    } as CreateAccessPolicyTemplateDTO;
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(accessPolicyTemplate);
    expect(
      await service.createTemplate(data, accessPolicyTemplate.userId),
    ).toBe(accessPolicyTemplate);
  });

  it('should retrieve all access policy templates by user id', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([accessPolicyTemplate]);
    expect(await service.retrieveTemplatesByUserId(1)).toEqual([
      accessPolicyTemplate,
    ]);
  });

  it('should update an access policy template', async () => {
    const data: UpdateAccessPolicyTemplateDTO = {
      description: accessPolicyTemplate.description,
      configuration: accessPolicyTemplate.configuration,
    } as UpdateAccessPolicyTemplateDTO;
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    jest.spyOn(repository, 'merge').mockReturnValue(accessPolicyTemplate);
    jest.spyOn(repository, 'save').mockResolvedValue(accessPolicyTemplate);
    expect(await service.updateTemplate(1, 1, data)).toBe(accessPolicyTemplate);
  });
});
