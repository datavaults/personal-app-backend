/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { User } from '@suite5/core/user/entities';
import { GraphQLJSONObject } from 'graphql-type-json';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import {
  InboxType,
  MessageType,
  RequestResponseType,
  SenderType,
} from '../constants';

@Entity()
@ObjectType()
export class Message extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  receiver: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  receiverId: number;

  @Column()
  @Field()
  senderName: string;

  @Column()
  @Field()
  senderDescription: string;

  @Column()
  @Field()
  senderWebsite: string;

  @Column()
  @Field()
  senderUUID: string;

  @Column()
  @Field()
  topic: string;

  @Column()
  @Field()
  messageType: MessageType;

  @Column()
  @Field()
  senderType: SenderType;

  @Column()
  @Field()
  inboxType: InboxType;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  configuration: any;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  responseConfiguration: any;

  @Column()
  @Field()
  body: string;

  @Column({ nullable: true })
  @Field()
  readAt: Date;

  @Column({ nullable: false })
  @Field()
  timestamp: Date;

  @Column({ nullable: true })
  @Field()
  price: number;

  @Column({ default: RequestResponseType.Pending })
  @Field()
  response: RequestResponseType;
}
