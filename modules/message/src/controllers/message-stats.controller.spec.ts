import { MessageStatsController } from './message-stats.controller';

describe('UserAssetController', () => {
  let controller: MessageStatsController;
  let service: any;

  beforeEach(async () => {
    service = {
      delete: jest.fn(),
      retrieveTotalAssets: jest.fn(),
      retrieveTotalSharedAssets: jest.fn(),
      retrieveQuestionnaires: jest.fn(),
      retrieveMonthSharedAssets: jest.fn(),
    };
    controller = new MessageStatsController(service);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should retrieve user questionnaires per month of specific year ', async () => {
    jest.spyOn(service, 'retrieveQuestionnaires').mockImplementation();
    expect(await controller.retrieveQuestionnaires).toBeTruthy();
  });
});
