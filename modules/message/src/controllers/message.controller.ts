import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Req,
  Sse,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CloudStrategy,
  CLOUD_STRATEGY_TOKEN,
  TransactionService,
} from '@suite5/asset';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { MessageType, OfferMessageStatus } from '@suite5/message/constants';
import { WalletStrategy, WALLET_STRATEGY_TOKEN } from '@suite5/wallet';
import { plainToClass } from 'class-transformer';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { CreateMessageDTO, PublicMessageDTO } from '../dto';
import { MessageService, SseService } from '../services';

@Controller('message')
@UseGuards(AuthenticationGuard)
@ApiTags('message')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class MessageController {
  constructor(
    @Inject(MessageService)
    private readonly messageService: MessageService,
    @Inject(TransactionService)
    private readonly transactionService: TransactionService,
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly strategy: CloudStrategy,
    private readonly sseService: SseService,
    @Inject(WALLET_STRATEGY_TOKEN)
    private readonly waletStrategy: WalletStrategy,
  ) {}

  @Post('')
  @ApiBody({ type: CreateMessageDTO })
  @ApiOperation({ summary: 'Create a message' })
  async createMessage(
    @CurrentUser() user: UserData,
    @Body() data: CreateMessageDTO,
  ): Promise<PublicMessageDTO> {
    return plainToClass(
      PublicMessageDTO,
      await this.messageService.write(user.id, data),
    );
  }

  @Get('')
  @ApiOperation({ summary: 'Retrieve message of specific user' })
  @ApiNotFoundResponse()
  async getMessages(
    @CurrentUser() user: UserData,
  ): Promise<PublicMessageDTO[]> {
    return ((await this.messageService.retrieveByUserId(user.id)) || []).map(
      (v) => plainToClass(PublicMessageDTO, v),
    );
  }

  @Sse('sse')
  @ApiOperation({
    summary: 'Retrieve SSE message | push mode',
  })
  @ApiNotFoundResponse({ description: 'Not found' })
  sse(
    @CurrentUser() user: UserData,
    @Req() req: Request,
  ): Observable<MessageEvent> {
    const token: any = req.cookies.token;
    const sseBehavior = this.sseService.initSSEMsg();
    return sseBehavior.asObservable().pipe(
      switchMap(async (msg) => {
        if (!!token && user.id === msg.data.message.userId) {
          return msg;
        }
      }),
      filter((msg) => msg && msg.data && msg.data.message),
    );
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Retrieve a message by its message id' })
  @ApiNotFoundResponse()
  async getMessageById(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
  ): Promise<PublicMessageDTO> {
    return plainToClass(
      PublicMessageDTO,
      await this.messageService.retrieveById(messageId, user.id),
    );
  }

  @Put('/:id/markAsRead')
  @ApiOperation({ summary: 'Mark message is read' })
  @ApiNotFoundResponse()
  async markAsRead(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
  ): Promise<PublicMessageDTO> {
    const message = await this.messageService.retrieveById(messageId, user.id);
    return plainToClass(
      PublicMessageDTO,
      await this.messageService.markAsRead(message),
    );
  }

  @Put('/:id/accept')
  @ApiOperation({ summary: 'Accept data seeker request' })
  @ApiNotFoundResponse()
  async accept(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
    @Body() data: any,
  ): Promise<PublicMessageDTO> {
    try {
      const message = await this.messageService.retrieveById(
        messageId,
        user.id,
      );
      const transactionId = uuidv4();
      let transactionData: any;
      const receiver = await message.receiver;
      if (message.messageType === MessageType.Questionnaire) {
        // await this.transactionService.create({
        //   buyerName: message.senderName,
        //   buyerUUID: message.senderUUID,
        //   buyerDescription: message.senderDescription,
        //   buyerWebsite: message.senderWebsite,
        //   buyerType: message.senderType,
        //   sharedQuestionnaireId: message.id,
        //   points: message.price,
        //   timestamp: new Date(),
        //   hash: uuidv4(),
        //   userId: receiver.id,
        // });
        transactionData = {
          id: transactionId,
          name: `Transaction_${transactionId}`,
          description: message.senderDescription,
          points: message.price,
          date: new Date(),
          status: 'Pending',
        };
        await this.strategy.publishQuestionnaire(receiver.sub, data);
        await this.messageService.saveQuestionnaireAsset(data, user);
        await this.waletStrategy.updateTransactions(transactionData, user.sub);
      }

      //TODO - fix using response from sending the share request
      if (message.messageType === MessageType.SharingRequest) {
        // await this.transactionService.create({
        //   buyerName: message.senderName,
        //   buyerUUID: message.senderUUID,
        //   buyerDescription: message.senderDescription,
        //   buyerWebsite: message.senderWebsite,
        //   buyerType: message.senderType,
        //   sharedAssetId: message.configuration.asset,
        //   points: message.price,
        //   timestamp: new Date(),
        //   hash: uuidv4(), // TODO: replace with blockchain hash
        //   userId: receiver.id,
        // });
        await this.strategy.acceptOffer(
          message.configuration.assetUid,
          message.senderUUID,
          user.sub,
          OfferMessageStatus.Accepted,
        );
      }
      return plainToClass(
        PublicMessageDTO,
        await this.messageService.accept(message, data),
      );
    } catch (e) {
      console.error(e);
      throw Error(
        `Failed to reply to questionnaire with error: ${e?.message || e}`,
      );
    }
  }

  @Put('/:id/decline')
  @ApiOperation({ summary: 'Decline data seeker request' })
  @ApiNotFoundResponse()
  async decline(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
  ): Promise<PublicMessageDTO> {
    const message = await this.messageService.retrieveById(messageId, user.id);
    if (message.messageType === MessageType.SharingRequest) {
      await this.strategy.acceptOffer(
        message.configuration.assetUid,
        message.senderUUID,
        user.sub,
        OfferMessageStatus.Rejected,
      );
    }
    return plainToClass(
      PublicMessageDTO,
      await this.messageService.decline(message),
    );
  }

  @Put('/:id/declineAndBlock')
  @ApiOperation({ summary: 'Decline data seeker request and block them' })
  @ApiNotFoundResponse()
  async declineAndBlock(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
  ): Promise<PublicMessageDTO> {
    const message = await this.messageService.retrieveById(messageId, user.id);
    return plainToClass(
      PublicMessageDTO,
      await this.messageService.declineAndBlock(message, user),
    );
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete Message' })
  @ApiNotFoundResponse()
  async delete(
    @CurrentUser() user: UserData,
    @Param('id') messageId: number,
  ): Promise<void> {
    const message = await this.messageService.retrieveById(messageId, user.id);
    await this.messageService.delete(message);
  }
}
