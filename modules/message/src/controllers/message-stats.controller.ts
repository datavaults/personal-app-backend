import { Controller, Get, Inject, Param, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { MessageStatsService } from '../services';

@Controller('message-stats')
@UseGuards(AuthenticationGuard)
@ApiTags('message')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class MessageStatsController {
  constructor(
    @Inject(MessageStatsService)
    private readonly messageStatsService: MessageStatsService,
  ) {}

  @Get('questionnaires/:date')
  @ApiOperation({
    summary: 'Retrieve user questionnaires per month of specific year',
  })
  @ApiNotFoundResponse()
  async retrieveQuestionnaires(
    @Param('date') date: any,
    @CurrentUser() user: UserData,
  ): Promise<any> {
    return await this.messageStatsService.retrieveQuestionnaires(date, user.id);
  }

  @Get('questionnaires')
  @ApiOperation({
    summary: 'Retrieve user sold questionnaires',
  })
  @ApiNotFoundResponse()
  async retrieveSoldQuestionnaires(
    @CurrentUser() user: UserData,
  ): Promise<any> {
    return await this.messageStatsService.retrieveSoldQuestionnaires(user.id);
  }
}
