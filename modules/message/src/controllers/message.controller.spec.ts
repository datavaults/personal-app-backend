import { ConfigurationDTO } from '@suite5/asset/dto/configuration.dto';
import { User } from '@suite5/core/user/entities';
import { CreateMessageDTO, MessageController } from '..';
import { MessageType, RequestResponseType } from '../constants';
import { Message } from '../entities';

describe('MessageController', () => {
  let controller: MessageController;
  let mockTransactionService: any;
  let mockMessageService: any;
  let mockCloudStrategy: any;
  let mockSseService: any;
  let mockWalletStrategy: any;

  let data;
  let user;

  beforeEach(async () => {
    user = { id: 1 } as User;
    const configuration = {} as ConfigurationDTO;
    data = {
      body: '2kjflksjdl',
      configuration: configuration,
      senderType: 'Data Owner',
      messageType: 'System Notification',
      id: 13,
      createdAt: '2021-06-03T22:53:23.805Z',
      senderName: 'test',
      senderDescription: 'abc',
      senderWebsite: 'www',
      senderUUID: '1234',
      topic: 'topic1',
      inboxType: 'Incoming',
      receiverId: 1,
      timestamp: '2021-06-03T22:53:23.805Z',
      price: 2,
      status: 'Accepted',
    } as CreateMessageDTO;

    mockTransactionService = {
      create: () => jest.fn(),
    };

    mockMessageService = {
      write: () => jest.fn(),
      retrieveByUserId: () => jest.fn(),
      retrieveById: () => jest.fn(),
      markAsRead: () => jest.fn(),
      delete: () => jest.fn(),
      accept: () => jest.fn(),
      decline: () => jest.fn(),
      declineAndBlock: () => jest.fn(),
      saveQuestionnaireAsset: () => jest.fn(),
    };

    mockCloudStrategy = {
      publishQuestionnaire: () => jest.fn(),
      acceptOffer: () => jest.fn(),
    };

    mockWalletStrategy = {
      updateTransactions: () => jest.fn(),
    };

    controller = new MessageController(
      mockMessageService,
      mockTransactionService,
      mockCloudStrategy,
      mockSseService,
      mockWalletStrategy,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should write a message', async () => {
    jest.spyOn(mockMessageService, 'write').mockImplementation();
    await controller.createMessage(user, data);
    expect(mockMessageService.write).toBeCalledWith(user.id, data);
  });

  it('should retrieve message of specific user 1', async () => {
    jest
      .spyOn(mockMessageService, 'retrieveByUserId')
      .mockResolvedValue([user]);
    await controller.getMessages(user);
    expect(mockMessageService.retrieveByUserId).toBeCalledWith(user.id);
  });

  it('should retrieve message of specific user 2', async () => {
    jest.spyOn(mockMessageService, 'retrieveByUserId').mockResolvedValue(null);
    expect(await controller.getMessages(user)).toStrictEqual([]);
  });

  it('should retrieve a message of message id', async () => {
    jest.spyOn(mockMessageService, 'retrieveById').mockImplementation();
    await controller.getMessageById(user, 1);
    expect(mockMessageService.retrieveById).toBeCalledWith(user.id, 1);
  });

  it('should mark a message as read', async () => {
    jest
      .spyOn(mockMessageService, 'retrieveById')
      .mockResolvedValue({ readAt: null, id: 1 } as Message);
    jest
      .spyOn(mockMessageService, 'markAsRead')
      .mockResolvedValue({ readAt: new Date(), id: 1 } as Message);
    const msg = await controller.markAsRead(user, 1);
    expect(msg.readAt).not.toBeNull();
    expect(mockMessageService.markAsRead).toBeCalledWith({
      readAt: null,
      id: 1,
    } as Message);
  });

  it('should delete message', async () => {
    jest
      .spyOn(mockMessageService, 'retrieveById')
      .mockResolvedValue({ readAt: null, id: 1 } as Message);
    jest.spyOn(mockMessageService, 'delete').mockImplementation();
    await controller.delete(user, 1);
    expect(mockMessageService.delete).toBeCalledWith({
      readAt: null,
      id: 1,
    } as Message);
  });

  it('should accept an offer', async () => {
    jest.spyOn(mockMessageService, 'retrieveById').mockResolvedValue({
      readAt: null,
      id: 1,
      messageType: MessageType.Questionnaire,
      receiver: { id: 4 },
    } as Message);
    jest.spyOn(mockMessageService, 'accept').mockResolvedValue({
      readAt: new Date(),
      id: 1,
      response: RequestResponseType.Accepted,
    } as Message);
    jest.spyOn(mockCloudStrategy, 'acceptOffer').mockImplementation();
    const msg = await controller.accept(user, 1, { abc: 1 });
    expect(msg.response).toBe(RequestResponseType.Accepted);
    expect(mockMessageService.accept).toBeCalledWith(
      {
        readAt: null,
        messageType: 'Questionnaire',
        id: 1,
        receiver: { id: 4 },
      } as Message,
      { abc: 1 },
    );
  });

  it('should accept an offer and publish Questionnaire', async () => {
    const message = {
      readAt: null,
      id: 1,
      messageType: MessageType.Questionnaire,
      receiver: new User(),
    } as Message;
    jest.spyOn(mockMessageService, 'retrieveById').mockResolvedValue(message);
    jest.spyOn(mockCloudStrategy, 'publishQuestionnaire').mockImplementation();
    jest.spyOn(mockCloudStrategy, 'acceptOffer').mockImplementation();
    jest.spyOn(mockMessageService, 'accept').mockResolvedValue({
      readAt: new Date(),
      id: 1,
      response: RequestResponseType.Accepted,
    } as Message);
    const msg = await controller.accept(user, 1, { abc: 1 });
    expect(msg.response).toBe(RequestResponseType.Accepted);
    // TODO - put me back
    expect(mockCloudStrategy.publishQuestionnaire).toBeCalledWith(
      message.receiver.sub,
      { abc: 1 },
    );

    expect(mockMessageService.accept).toBeCalledWith(message, { abc: 1 });
  });

  // f (message.messageType === MessageType.Questionnaire) {
  //   await this.strategy.publishQuestionnaire(message);
  // }

  it('should decline an offer', async () => {
    jest
      .spyOn(mockMessageService, 'retrieveById')
      .mockResolvedValue({ readAt: null, id: 1 } as Message);
    jest.spyOn(mockMessageService, 'decline').mockResolvedValue({
      readAt: new Date(),
      id: 1,
      response: RequestResponseType.Declined,
    } as Message);
    const msg = await controller.decline(user, 1);
    expect(msg.response).toBe(RequestResponseType.Declined);
    expect(mockMessageService.decline).toBeCalledWith({
      readAt: null,
      id: 1,
    } as Message);
  });

  it('should decline an offer and block Seeker', async () => {
    const message = {
      readAt: null,
      id: 1,
      response: RequestResponseType.Pending,
    } as Message;
    jest.spyOn(mockMessageService, 'retrieveById').mockResolvedValue(message);
    jest.spyOn(mockMessageService, 'declineAndBlock').mockResolvedValue({
      ...message,
      response: RequestResponseType.Declined,
    } as Message);
    const msg = await controller.declineAndBlock(user, 1);
    expect(msg.response).toBe(RequestResponseType.Declined);
    expect(mockMessageService.declineAndBlock).toBeCalled();
  });
});
