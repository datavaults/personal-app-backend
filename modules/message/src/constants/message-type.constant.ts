export enum MessageType {
  SharingRequest = 'Sharing Request',
  TransactionResult = 'Transaction Result',
  SystemNotification = 'System Notification',
  Questionnaire = 'Questionnaire',
}
