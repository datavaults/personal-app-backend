export enum RequestResponseType {
  Pending = 'Pending',
  Accepted = 'Accepted',
  Declined = 'Declined',
}
