import { registerAs } from '@nestjs/config';

export default registerAs('message.module', () => ({
  rabbitmqUri:
    process.env.MESSAGE_RABBITMQ_URI || 'amqp://admin:admin@rabbitmq:5672',
  publishQuestionnnaireURL: process.env.CLOUD_PLATFORM_BACKEND_URL,
}));
