import { ConfigurationAnonymasationDTO } from '@suite5/asset/dto/configuration-anonymisation.dto';
import { ConfigurationDTO } from '@suite5/asset/dto/configuration.dto';
import { User } from '@suite5/core/user/entities';
import { MessageService } from '.';
import { RequestResponseType } from '../constants';
import { CreateMessageDTO, PublicMessageDTO } from '../dto';
import { Message } from '../entities';
import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

describe('MessageService', () => {
  let repository: any;
  let message: any;
  let data;
  let userService: any;
  let blacklistService: any;
  let user;
  let service: MessageService;
  let sseService: any;
  let dataFetcherService: any;
  let configService: any;
  let sharedAssetService: any;
  let vaultService: any;
  let logger: any;

  beforeEach(async () => {
    message = new Message();
    message.receiverId = 1;
    user = { id: 2 } as User;
    const configutration = {} as any;
    data = {
      body: '2kjflksjdl',
      configuration: configutration,
      senderType: 'Data Owner',
      messageType: 'System Notification',
      id: 13,
      createdAt: '2021-06-03T22:53:23.805Z',
      senderName: 'test',
      senderDescription: 'abc',
      senderWebsite: 'www',
      senderUUID: '1234',
      topic: 'topic1',
      inboxType: 'Incoming',
      receiverId: 1,
      timestamp: '2021-06-03T22:53:23.805Z',
      price: 3,
    } as CreateMessageDTO;

    repository = {
      create: jest.fn(),
      save: jest.fn(),
      findOneOrFail: jest.fn(),
      merge: jest.fn(),
      delete: jest.fn(),
    };

    userService = {
      retrieve: jest.fn(),
    };

    blacklistService = {
      addToBlacklist: jest.fn(),
    };

    sseService = {
      initSSEMsg: jest.fn(),
      publishSseMessage: jest.fn(),
    };

    dataFetcherService = {
      createAsset: jest.fn(),
      replaceCollection: jest.fn(),
    };

    configService = {
      get: jest.fn(),
    };

    sharedAssetService = {
      create: jest.fn(),
    };

    vaultService = {
      getSecret: jest.fn(),
    };

    logger = {
      error: jest.fn(),
    };

    service = new MessageService(
      repository,
      userService,
      blacklistService,
      sseService,
      dataFetcherService,
      configService,
      sharedAssetService,
      vaultService,
      logger,
    );
  });
  afterEach(() => {
    jest.resetAllMocks();
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should write a message', async () => {
    jest.spyOn(userService, 'retrieve').mockResolvedValue(user);
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(message);
    await sseService.initSSEMsg();
    await service.write(user.id, data);
    expect(userService.retrieve).toBeCalledWith(user.id);
    expect(repository.create).toBeCalledWith({ ...data, ...{ sender: user } });
  });

  it('should retrieve message of specific user', async () => {
    jest.spyOn(userService, 'retrieve').mockResolvedValue(user);
    jest.spyOn(service, 'retrieveByUserId').mockImplementation();

    expect(userService.retrieve).toBeTruthy();
  });

  it('should retrieve a message of message id', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    await service.retrieveById(1, 1);
    expect(repository.findOneOrFail).toBeCalledWith({ id: 1, receiverId: 1 });
  });

  it('should update a message', async () => {
    const updateData = {
      body: '22kjflksjdl',
      topic: 'skdflsdkflskdfs',
      configuration: {
        anonymisation: {
          pseudoId: '',
        } as ConfigurationAnonymasationDTO,
      } as ConfigurationDTO,
      senderType: 'Data Owner',
      messageType: 'System Notification',
      id: 1,
      createdAt: new Date(),
      readAt: null,
      price: null,
      response: RequestResponseType.Pending,
      responseConfiguration: null,
      receiver: 1,
      senderDescription: 'Test',
      senderName: 'Test',
      senderUUID: 'ab',
      senderWebsite: null,
      timestamp: '2021-06-04T05:49:26.440Z',
    } as PublicMessageDTO;
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(message);
    jest.spyOn(repository, 'merge').mockImplementation();
    jest.spyOn(repository, 'save').mockImplementation();
    await service.update(updateData);
    expect(repository.findOneOrFail).toBeCalledWith({ id: updateData.id });
    expect(repository.merge).toBeCalledWith(message, updateData);
    expect(repository.save).toBeCalledWith(message);
  });

  it('should mark message are read', async () => {
    const mockDate = new Date(1466424490000);
    const spy = jest
      .spyOn(global, 'Date')
      .mockImplementation(() => mockDate.toString());
    try {
      jest.spyOn(repository, 'save').mockImplementation();

      await service.markAsRead({ id: 1, readAt: null } as Message);

      expect(repository.save).toBeCalledWith({
        id: 1,
        readAt: new Date(1466424490000),
      });
    } finally {
      spy.mockRestore();
    }
  });

  it('should mark message are read and not change date if already read', async () => {
    jest.spyOn(repository, 'save').mockImplementation();

    await service.markAsRead({
      id: 1,
      readAt: new Date(1466424490000),
    } as Message);

    expect(repository.save).toBeCalledWith({
      id: 1,
      readAt: new Date(1466424490000),
    });
  });

  it('should mark message are read and not change date if already read', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();

    await service.delete({ id: 1, readAt: new Date(1466424490000) } as Message);

    expect(repository.delete).toBeCalledWith(1);
  });

  it('should accept an offer', async () => {
    const message = {
      id: 1,
      readAt: new Date(1466424490000),
      response: RequestResponseType.Pending,
      configuration: { abc: 2 },
      responseConfiguration: null,
    } as Message;
    const configuration = { abc: 1 };

    jest.spyOn(repository, 'save').mockResolvedValue({
      ...message,
      responseConfiguration: configuration,
      response: RequestResponseType.Accepted,
    });

    jest.spyOn(blacklistService, 'addToBlacklist').mockImplementation();

    await service.accept(message, configuration);

    expect(repository.save).toBeCalledWith({
      ...message,
      responseConfiguration: configuration,
      response: RequestResponseType.Accepted,
    });
    expect(blacklistService.addToBlacklist).not.toBeCalled();
  });

  it('should reject an offer', async () => {
    const message = {
      id: 1,
      readAt: new Date(1466424490000),
      response: RequestResponseType.Pending,
      configuration: { abc: 2 },
      responseConfiguration: null,
    } as Message;

    jest.spyOn(repository, 'save').mockResolvedValue({
      ...message,
      response: RequestResponseType.Declined,
    });
    jest.spyOn(blacklistService, 'addToBlacklist').mockImplementation();

    await service.decline(message);

    expect(repository.save).toBeCalledWith({
      ...message,
      response: RequestResponseType.Declined,
    });
    expect(blacklistService.addToBlacklist).not.toBeCalled();
  });

  it('should reject an offer and block seeker', async () => {
    const message = {
      id: 1,
      readAt: new Date(1466424490000),
      response: RequestResponseType.Pending,
      configuration: { abc: 2 },
      responseConfiguration: null,
      senderUUID: 'abc',
      senderName: 'ACME INC',
      senderWebsite: 'www.example.org',
    } as Message;

    jest.spyOn(repository, 'save').mockResolvedValue({
      ...message,
      response: RequestResponseType.Declined,
    });
    jest.spyOn(blacklistService, 'addToBlacklist').mockImplementation();

    await service.declineAndBlock(message, user);

    expect(repository.save).toBeCalledWith({
      ...message,
      response: RequestResponseType.Declined,
    });
    expect(blacklistService.addToBlacklist).toBeCalledWith({
      dataSeekerUUID: message.senderUUID,
      dataSeekerName: message.senderName,
      dataSeekerPublicURL: message.senderWebsite,
      blacklistedById: user.id,
    });
  });
});
