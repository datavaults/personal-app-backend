import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { UserService } from '@suite5/core/user';
import { User } from '@suite5/core/user/entities';
import * as R from 'ramda';
import { InboxType, MessageType, SenderType } from '../constants';
import {
  QuestionnaireMessageUser,
  QuestionnaireRabbitmqResponse,
} from '../interfaces';
import { MessageService } from './message.service';
import countries from '../constants/countries.json';
import cities from '../constants/cities.json';
import states from '../constants/states.json';
@Injectable()
export class QuestionnaireMessagingService {
  constructor(
    @Inject(Logger)
    private readonly logger: Logger,
    @Inject(UserService) private readonly userService: UserService,
    @Inject(MessageService) private readonly messageService: MessageService,
  ) {}

  applyFilters(filters: any[] | undefined, users: any[], value: string) {
    const processedFilters: any = [];
    if (filters && filters.length > 0 && !filters.includes('All')) {
      if (value === 'country') {
        countries.forEach((value: any) => {
          filters.forEach((item: any) => {
            if (item === value.id) {
              processedFilters.push(value.name);
            }
          });
        });
      } else if (value === 'region') {
        states.forEach((value: any) => {
          filters.forEach((item: any) => {
            if (item === value.id) {
              processedFilters.push(value.name);
            }
          });
        });
      } else if (value === 'city') {
        cities.forEach((value: any) => {
          filters.forEach((item: any) => {
            if (item === value.id) {
              processedFilters.push(value.name);
            }
          });
        });
      }
      return users.filter((user: User) =>
        processedFilters.includes(user[value]),
      );
    }
    return users;
  }

  async applyFiltersOnMultipleValues(
    filters: any[] | undefined,
    users: any[],
    value: string,
    findIds = false,
  ) {
    if (filters && filters.length > 0 && !filters.includes('All')) {
      const removeIds = [];

      for (let i = 0; i < users.length; i++) {
        const values = findIds
          ? (await users[i][value]).map((item: any) => item.id)
          : users[i][value];

        let found = false;
        for (let j = 0; j < values.length; j++)
          if (filters.toString().includes(values[j].toString())) {
            found = true;
            break;
          }
        if (!found) removeIds.push(users[i].id);
      }
      return users.filter((user: User) => !removeIds.includes(user.id));
    }
    return users;
  }

  applyAgeFilters(filters: any[] | undefined, users: any[], value: string) {
    if (filters && filters.length > 0 && !filters.includes('All')) {
      const removeIds = [];

      users.forEach((user: User) => {
        const birthDateTime = new Date(user[value]).getTime();
        const ageTime = new Date(Date.now() - birthDateTime);
        const userAge = Math.abs(ageTime.getUTCFullYear() - 1970);
        let found = false;

        for (let i = 0; i < filters.length; i++) {
          const ageGroup = filters[i]
            .replace('+', '')
            .split('-')
            .map((a: string) => Number(a));

          if (ageGroup.length === 1 && userAge >= ageGroup[0]) {
            found = true;
            break;
          } else if (
            ageGroup.length === 2 &&
            userAge >= ageGroup[0] &&
            userAge <= ageGroup[1]
          ) {
            found = true;
            break;
          }
        }
        if (!found) removeIds.push(user.id);
      });
      return users.filter((user: User) => !removeIds.includes(user.id));
    }
    return users;
  }

  @RabbitSubscribe({
    exchange: 'amq.direct',
    routingKey: 'questionnaires',
    queue: 'questionnaires',
  })
  async sendMessages(msg: QuestionnaireRabbitmqResponse) {
    if (msg.status !== HttpStatus.OK)
      if (msg.errors.length > 0)
        this.logger.error(
          `[Questionnaire RabbitMQ] ${msg.status} - ${msg.errors.join(', ')}`,
        );
      else
        this.logger.error(
          `[Questionnaire RabbitMQ] ${msg.status} - A generic error has occurred.`,
        );

    try {
      let users: User[] = [];

      if (R.type(msg.user) === 'String') {
        const user = await this.userService.retrieveBySub(msg.user as string);
        if (user.requestResolverSettings) users.push(user);
      } else {
        users = await this.userService.getUsersWithRequestReceivingEnabled();
        const audience = msg.user as QuestionnaireMessageUser;
        // apply filters to users
        users = this.applyFilters(audience.countries, users, 'country');
        users = this.applyFilters(audience.regions, users, 'region');
        users = this.applyFilters(audience.cities, users, 'city');
        users = this.applyFilters(audience.nationalities, users, 'nationality');
        users = this.applyAgeFilters(audience.ageGroups, users, 'birthDate');
        users = await this.applyFiltersOnMultipleValues(
          audience.occupations,
          users,
          'occupationsList',
        );
        users = await this.applyFiltersOnMultipleValues(
          audience.educations,
          users,
          'qualificationsList',
        );
        users = await this.applyFiltersOnMultipleValues(
          audience.transportationMeans,
          users,
          'transportationList',
        );
        users = await this.applyFiltersOnMultipleValues(
          audience.culturalInterests,
          users,
          'culturalInterestList',
        );

        // We didn't use civil status on frontend so users can't update this field we leave it commented for now.
        // users = this.applyFilters(
        //   audience.civilStatuses,
        //   users,
        //   'civilStatusList',
        // );
      }
      for (let u = 0; u < users.length; u++) {
        const user: User = users[u];

        await this.messageService.write(user.id, {
          senderName: msg.body.questionnaire.dataSeekerOrganization,
          senderDescription: msg.body.questionnaire.dataSeekerDescription,
          senderWebsite: msg.body.questionnaire.dataSeekerURL,
          senderUUID: msg.body.questionnaire.dataSeekerUUID,
          topic: msg.body.questionnaire.title,
          body: msg.body.dataSeekerMessage,
          inboxType: InboxType.Incoming,
          senderType: SenderType.DataSeeker,
          messageType: MessageType.Questionnaire,
          timestamp: new Date().toString(),
          configuration: {
            ...msg.body.questionnaire,
            license: msg.body.license,
          },
          price: msg.body.price,
          receiverId: user.id,
        });
      }
    } catch {
      if (R.type(msg.user) === 'String')
        this.logger.error(
          `[Questionnaire RabbitMQ] User with keycloak id '${msg.user}' does not exist`,
        );
      else
        this.logger.error(
          `[Questionnaire RabbitMQ] Something went wrong while trying to send message to users`,
        );
    }
  }
}
