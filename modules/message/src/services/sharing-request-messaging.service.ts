import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { UserService } from '@suite5/core/user';
import { SharingRequestRabbitmqResponse } from '../interfaces';
import { MessageService } from './message.service';
import { InboxType, MessageType, SenderType } from '../constants';
import { DataFetcherService } from '@suite5/asset';
import { SharedAssetService } from '@suite5/asset';

@Injectable()
export class SharingRequestMessagingService {
  constructor(
    @Inject(Logger)
    private readonly logger: Logger,
    @Inject(UserService) private readonly userService: UserService,
    @Inject(DataFetcherService)
    private readonly dataFetcherService: DataFetcherService,
    @Inject(SharedAssetService)
    private readonly sharedAssetService: SharedAssetService,
    @Inject(MessageService) private readonly messageService: MessageService,
  ) {}

  @RabbitSubscribe({
    exchange: 'amq.direct',
    routingKey: 'datasets',
    queue: 'datasets',
  })
  async sendMessage(msg: SharingRequestRabbitmqResponse) {
    if (msg.status !== HttpStatus.OK)
      if (msg.errors.length > 0)
        this.logger.error(
          `[Questionnaire RabbitMQ] ${msg.status} - ${msg.errors.join(', ')}`,
        );
      else
        this.logger.error(
          `[Questionnaire RabbitMQ] ${msg.status} - A generic error has occurred.`,
        );

    try {
      const user = await this.userService.retrieveBySub(msg.user as string);
      const sharedAsset = await this.sharedAssetService.retrieveByUid(
        msg.body.assetUUID,
      );

      const message = {
        senderName: msg.body.dataSeekerOrganization,
        senderDescription: msg.body.dataSeekerDescription,
        senderWebsite: msg.body.dataSeekerURL,
        senderUUID: msg.body.dataSeekerUUID,
        topic: sharedAsset.name,
        body: '',
        inboxType: InboxType.Incoming,
        senderType: SenderType.DataSeeker,
        messageType: MessageType.SharingRequest,
        timestamp: new Date().toString(),
        configuration: { asset: sharedAsset.id, assetUid: sharedAsset.uid },
        price: msg.body.price,
        receiverId: null,
      };
      if (user.requestResolverSettings) {
        message.receiverId = user.id;
        await this.messageService.write(user.id, message);
      } else {
        this.logger.debug(
          `Sharing request for user #${user.id} cannot be send because the user has chosen not to receive any requests`,
        );
      }
    } catch {
      this.logger.error(
        `[Sharing request RabbitMQ] User with keycloak id '${msg.user}' does not exist`,
      );
    }
  }
}
