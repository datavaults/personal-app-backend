import { Injectable } from '@nestjs/common';
import { SseQueue } from '../constants';
import { plainToClass } from 'class-transformer';
import { SseMessage } from '../interfaces';
import { BehaviorSubject } from 'rxjs';
import { SseMessageDTO } from '../dto';

@Injectable()
export class SseService {
  constructor() {}

  private sendMsg: BehaviorSubject<MessageEvent>;

  public initSSEMsg(): BehaviorSubject<MessageEvent> {
    this.sendMsg = new BehaviorSubject<MessageEvent>({
      data: { message: '', queue: SseQueue.Message },
    } as MessageEvent);
    return this.sendMsg;
  }

  public publishSseMessage(messageObj: any) {
    const message = plainToClass(SseMessageDTO, messageObj);
    const data = { message, queue: SseQueue.Message } as SseMessage;
    if (this.sendMsg) this.sendMsg.next({ data } as MessageEvent);
  }
}
