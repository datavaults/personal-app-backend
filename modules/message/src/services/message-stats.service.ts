import { Injectable } from '@nestjs/common';
import dayjs from 'dayjs';
import { MessageService } from './message.service';

@Injectable()
export class MessageStatsService {
  constructor(private readonly messageService: MessageService) {}

  private months = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
  ];

  async retrieveQuestionnaires(date: any, id: number) {
    const questionniares: any[] = await this.messageService.retrieveByUserId(
      id,
    );
    const response: any = [];
    this.months.forEach((el: string) => {
      response.push({
        month: el,
        total: questionniares
          .map((item) => {
            return (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date) &&
              item.messageType === 'Questionnaire'
            );
          })
          .filter((val: any) => {
            return val === true;
          }).length,
        respondAccepted: questionniares
          .map((item) => {
            return (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date) &&
              item.response === 'Accepted' &&
              item.messageType === 'Questionnaire'
            );
          })
          .filter((val: any) => {
            return val === true;
          }).length,
        respondDeclined: questionniares
          .map((item) => {
            return (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date) &&
              item.response === 'Declined' &&
              item.messageType === 'Questionnaire'
            );
          })
          .filter((val: any) => {
            return val === true;
          }).length,
        totalPoint: questionniares
          .map((item) => {
            if (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date) &&
              item.messageType === 'Questionnaire'
            ) {
              return item.price;
            }
          })
          .reduce((acc, item) => {
            return acc + item;
          }),
      });
    });

    return response;
  }

  async retrieveSoldQuestionnaires(id: number) {
    const questionniares: any[] = await this.messageService.retrieveByUserId(
      id,
    );
    let totalPoints = 0;
    let totalSold = 0;
    questionniares.forEach((item: any) => {
      if (item.response === 'Accepted' && item.messageType === 'Questionnaire')
        return (totalPoints += item.price), (totalSold += 1);
    });
    return { totalPoints: totalPoints, totalSold: totalSold };
  }
}
