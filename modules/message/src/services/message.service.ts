import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BlacklistService } from '@suite5/core/user/services/blacklist.service';
import { UserService } from '@suite5/core/user/services/user.service';
import { Repository } from 'typeorm';
import { RequestResponseType } from '../constants';
import { CreateMessageDTO, PublicMessageDTO } from '../dto';
import { Message } from '../entities';
import { SseService } from './sse.service';
import { UserData } from '@suite5/core/authentication/decorators';
import { DataFetcherService, SharedAssetService } from '@suite5/asset';
import mongoose, { Connection } from 'mongoose';
import { ConfigService } from '@nestjs/config';
import { v4 as uuidv4 } from 'uuid';
import * as R from 'ramda';
import { VaultService } from '@suite5/vault';
import dayjs from 'dayjs';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private readonly messageRepository: Repository<Message>,
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(BlacklistService)
    private readonly blacklistService: BlacklistService,
    private readonly sseService: SseService,
    @Inject(DataFetcherService)
    private readonly dataFetcherService: DataFetcherService,
    private readonly configService: ConfigService,
    private readonly sharedAssetService: SharedAssetService,
    private readonly vaultService: VaultService,
    @Inject(Logger)
    private readonly logger: Logger,
  ) {}

  async write(
    senderId: number,
    messageData: CreateMessageDTO,
  ): Promise<Message> {
    const sender = await this.userService.retrieve(senderId);

    const createdMessage = this.messageRepository.create({
      ...messageData,
      ...{ sender: sender },
    });
    const message = await this.messageRepository.save(createdMessage);

    await this.sseService.publishSseMessage({
      userId: message.receiverId,
      payload: {
        senderName: messageData.senderName,
        senderType: messageData.senderType,
        messageType: messageData.messageType,
        topic: messageData.topic,
      },
    });
    return null;
  }

  async retrieveByUserId(senderId: number): Promise<Message[]> {
    const currentDate = dayjs(new Date());
    const messages = [];
    (await this.userService.retrieve(senderId)).messages.forEach((msg: any) => {
      if (
        (msg.configuration.expirationDate === null ||
          dayjs(msg.configuration.expirationDate).isAfter(currentDate)) &&
        msg.response === RequestResponseType.Pending
      ) {
        messages.push(msg);
      } else if (
        msg.response === RequestResponseType.Declined ||
        msg.response === RequestResponseType.Accepted
      ) {
        messages.push(msg);
      }
    });
    return messages;
  }

  async retrieveById(messageId: number, receiverId: number): Promise<Message> {
    return await this.messageRepository.findOneOrFail({
      id: messageId,
      receiverId: receiverId,
    });
  }

  async update(data: PublicMessageDTO): Promise<Message> {
    const message = await this.messageRepository.findOneOrFail({ id: data.id });
    await this.messageRepository.merge(message, data);

    return await this.messageRepository.save(message);
  }

  async markAsRead(message: Message): Promise<Message> {
    if (!message.readAt) {
      message.readAt = new Date();
    }
    return await this.messageRepository.save(message);
  }

  async accept(message: Message, configuration: any): Promise<Message> {
    message.response = RequestResponseType.Accepted;
    message.responseConfiguration = configuration;

    return this.messageRepository.save(message);
  }

  async decline(message: Message): Promise<Message> {
    message.response = RequestResponseType.Declined;

    return this.messageRepository.save(message);
  }

  async declineAndBlock(message: Message, user: UserData): Promise<Message> {
    message.response = RequestResponseType.Declined;
    await this.blacklistService.addToBlacklist({
      dataSeekerUUID: message.senderUUID,
      dataSeekerName: message.senderName,
      dataSeekerPublicURL: message.senderWebsite,
      blacklistedById: user.id,
    });
    return this.messageRepository.save(message);
  }

  async delete(message: Message): Promise<void> {
    await this.messageRepository.delete(message.id);
  }

  async saveQuestionnaireAsset(data: any, user: any) {
    const sourceId = uuidv4();
    const sharedAssetUid = uuidv4();
    const questionnaireAsset = {
      name: data.title,
      description: data.intro,
      keywords: ['Questionnaire'],
      url: null,
      sourceType: 'Questionnaire' as any,
      createdById: user.id,
      credentials: { subId: user.sub } as any,
      schedule: null,
      errors: '',
      uid: '',
      type: 'questionnaire',
    };

    const sharedQuestionnaire = {
      assetId: 0,
      name: data.title,
      description: data.intro,
      keywords: questionnaireAsset.keywords,
      configuration: {
        level: 'custom',
        anonymisation: {
          anonymise: false,
          pseudoId: null,
          useTPM: false,
          rules: { colNames: [], buildIDs: [], anonLevels: [] },
        },
        specificPrice: true,
        price: data.price,
        licenseType: 'standard', //Hard coded because we don't have the information from questionnaires
        standardLicense: data.licence as any,
        ownLicense: null,
        encryption: false,
        persona: false,
        useTPM: false,
        tpmPseudonym: null,
        shareType: 'normal',
        accessPolicies: {
          policyId: 'new',
          existingPolicyId: null,
          sector: [],
          type: [],
          size: [],
          continent: [],
          country: [],
          reputation: null,
        },
      },
      userId: user.id,
    };

    const mongoData: any = [];
    data.questions.forEach((item: any) => {
      if (item.options.length > 0) {
        //Check if we have question with multiple answers
        item.answer.forEach((answer: any) => {
          item.options.forEach((val: any) => {
            //Check if answer is number
            if (val.value === Number.parseInt(answer)) {
              return mongoData.push({
                question: item.question,
                description: item.description != null ? item.description : '',
                answer: val.label,
              });
            } else if (val.label === 'Other' && R.is(String, answer)) {
              //Check if other option is precent or the answer is string
              return mongoData.push({
                question: item.question,
                description: item.description != null ? item.description : '',
                answer: answer,
              });
            }
          });
        });
      } else {
        return mongoData.push({
          question: item.question,
          description: item.description != null ? item.description : '',
          answer: item.answer[0],
        });
      }
    });
    const credentials = await this.vaultService.getSecret(
      'user',
      `${user.id}`,
      'mongo',
      this.configService.get<string>('vault.vaultRootToken'),
    );

    const mongoUri = `mongodb://${credentials.name}:${
      credentials.password
    }@${this.configService.get<string>(
      'mongo.dbHost',
    )}:${this.configService.get<string>('mongo.dbPort')}/${credentials.name}`;

    let mongoConnection: Connection;
    try {
      mongoConnection = await mongoose.createConnection(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    } catch (e) {
      console.log(`Mongo connection error ${e}`);
      this.logger.error(`[DataFetcher RabbitMQ] Mongo Connection  : ${e}`);
    }

    try {
      //Create asset in postgress
      const asset = await this.dataFetcherService.createAsset(
        questionnaireAsset as any,
        sourceId,
      );
      //Create shared asset in postgress
      sharedQuestionnaire.assetId = asset.id;
      await this.sharedAssetService.create(
        sharedQuestionnaire as any,
        sharedAssetUid,
      );
      //Create the collection in mongo with the data from questionnaire
      await this.dataFetcherService.replaceCollection(
        mongoConnection,
        user.id,
        `${sourceId}-data`,
        { email: String },
        mongoData,
        true,
      );
    } catch (e) {
      console.log(e);
    } finally {
      if (mongoConnection) {
        await mongoConnection.close();
      }
    }
  }
}
