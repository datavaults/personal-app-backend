import { Asset } from '@suite5/asset';
import { User } from '@suite5/core/user/entities';
import { SharingRequestRabbitmqResponse } from '../interfaces';
import { SharingRequestMessagingService } from '@suite5/message';
import { SharedAsset } from '@suite5/asset';

describe('SharingRequestMessagingService', () => {
  let service: SharingRequestMessagingService;
  let userService: any;
  let messageService: any;
  let dataFetcherService: any;
  let sharedAssetService: any;
  let logger: any;
  let msg: SharingRequestRabbitmqResponse;
  let user: User;
  let asset: Asset;
  let sharedAsset: SharedAsset;

  beforeEach(async () => {
    msg = {
      user: '',
      body: {
        dataSeekerOrganization: '',
        dataSeekerDescription: '',
        dataSeekerUUID: '',
        dataSeekerURL: '',
        assetUUID: '',
        price: 10,
      },
      status: 200,
      errors: [],
    };

    user = {
      id: 1,
    } as User;

    asset = {
      id: 1,
      uid: '',
      name: 'test',
    } as Asset;

    sharedAsset = {
      id: 1,
      assetId: 1,
    } as SharedAsset;

    logger = {
      error: () => jest.fn(),
      debug: () => jest.fn(),
    };

    userService = {
      retrieveBySub: () => jest.fn(),
    };

    dataFetcherService = {
      retrieveAssetByUid: () => jest.fn(),
    };

    sharedAssetService = {
      retrieveByAssetId: () => jest.fn(),
      retrieveByUid: () => jest.fn(),
    };

    messageService = {
      write: () => jest.fn(),
    };

    service = new SharingRequestMessagingService(
      logger,
      userService,
      dataFetcherService,
      sharedAssetService,
      messageService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should send sharing request message to specific user', async () => {
    user.requestResolverSettings = true;
    jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
    jest.spyOn(sharedAssetService, 'retrieveByUid').mockResolvedValue(asset);
    jest.spyOn(messageService, 'write').mockImplementation();
    await service.sendMessage(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(sharedAssetService.retrieveByUid).toBeCalledWith(msg.body.assetUUID);
    expect(messageService.write).toBeCalled();
  });

  it('should not send request message to specific user', async () => {
    user.requestResolverSettings = false;
    jest.spyOn(logger, 'debug').mockImplementation();
    jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
    jest.spyOn(sharedAssetService, 'retrieveByUid').mockResolvedValue(asset);

    jest.spyOn(messageService, 'write').mockImplementation();
    await service.sendMessage(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(sharedAssetService.retrieveByUid).toBeCalledWith(msg.body.assetUUID);
    expect(logger.debug).toBeCalledWith(
      'Sharing request for user #1 cannot be send because the user has chosen not to receive any requests',
    );
  });

  it('should log error', async () => {
    user.requestResolverSettings = false;
    jest.spyOn(userService, 'retrieveBySub').mockRejectedValue(user);
    jest.spyOn(logger, 'error').mockImplementation();
    await service.sendMessage(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(logger.error).toBeCalledWith(
      "[Sharing request RabbitMQ] User with keycloak id '' does not exist",
    );

    msg.status = 500;
    await service.sendMessage(msg);

    expect(logger.error).toBeCalledWith(
      '[Questionnaire RabbitMQ] 500 - A generic error has occurred.',
    );

    msg.errors = ['test'];
    await service.sendMessage(msg);

    expect(logger.error).toBeCalledWith('[Questionnaire RabbitMQ] 500 - test');
  });
});
