import { MessageStatsService } from './message-stats.service';

describe('UserAssetService', () => {
  let service: MessageStatsService;
  let mockMessageService: any;

  beforeEach(async () => {
    mockMessageService = {
      retrieveByUserId: jest.fn(),
    };

    service = new MessageStatsService(mockMessageService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve user total questionnaires per month', async () => {
    const mesages = [
      {
        body: '2kjflksjdl',
        configuration: {},
        senderType: 'Data Owner',
        messageType: 'Questionnaire',
        id: 13,
        createdAt: '2021-06-03T22:53:23.805Z',
        senderName: 'test',
        senderDescription: 'abc',
        senderWebsite: 'www',
        senderUUID: '1234',
        topic: 'topic1',
        inboxType: 'Incoming',
        receiverId: 1,
        timestamp: '2022-05-03T22:53:23.805Z',
        price: null,
        response: 'Declined',
      },
      {
        body: '2kjflksjdl',
        configuration: {},
        senderType: 'Data Owner',
        messageType: 'Questionnaire',
        id: 13,
        createdAt: '2021-06-03T22:53:23.805Z',
        senderName: 'test',
        senderDescription: 'abc',
        senderWebsite: 'www',
        senderUUID: '1234',
        topic: 'topic1',
        inboxType: 'Incoming',
        receiverId: 1,
        timestamp: '2022-05-03T22:53:23.805Z',
        price: 3,
        response: 'Accepted',
      },
    ];
    jest.spyOn(mockMessageService, 'retrieveByUserId').mockReturnValue(mesages);
    const response = [
      {
        month: '01',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '02',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '03',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '04',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '05',
        total: 2,
        respondAccepted: 1,
        respondDeclined: 1,
        totalPoint: 3,
      },
      {
        month: '06',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '07',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '08',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '09',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '10',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '11',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
      {
        month: '12',
        total: 0,
        respondAccepted: 0,
        respondDeclined: 0,
        totalPoint: 0,
      },
    ];

    expect(await service.retrieveQuestionnaires).toBeTruthy();
  });
});
