import { Education } from '@suite5/core/user/constants';
import { Occupation, Skill, User } from '@suite5/core/user/entities';
import { QuestionnaireMessagingService } from '@suite5/message';
import { QuestionnaireRabbitmqResponse } from '../interfaces';

describe('QuestionnaireMessagingService', () => {
  let service: QuestionnaireMessagingService;
  let userService: any;
  let messageService: any;
  let logger: any;
  let msg: any;
  let user: User;
  let occupation: Occupation;
  let qualification: Skill;

  beforeEach(async () => {
    msg = {
      user: '',
      body: {
        questionnaire: {
          id: 1,
          creationDate: new Date(),
          lastUpdateDate: new Date(),
          expirationDate: null,
          dataSeekerOrganization: '',
          dataSeekerDescription: '',
          dataSeekerUUID: '',
          dataSeekerURL: '',
          status: 'finalized',
          title: 'title',
          intro: '',
          questions: [],
        },
        dataSeekerMessage: '',
        license: '',
        price: null,
      },
      status: 200,
      errors: [],
    };

    occupation = { id: 1 } as Occupation;

    user = {
      id: 1,
      country: 'Cyprus',
      region: 'Larnaka',
      city: 'Larnaka',
      nationality: 'Cypriot',
      birthDate: new Date('2000-01-01T00:00:00'),
      occupationsList: [1],
      qualificationsList: [1],
      transportationList: [1],
      culturalInterestList: [1],
      civilStatusList: [1],
    } as User;

    logger = {
      error: () => jest.fn(),
    };

    userService = {
      retrieveBySub: () => jest.fn(),
      getUsersWithRequestReceivingEnabled: () => jest.fn(),
    };

    messageService = {
      write: () => jest.fn(),
    };

    service = new QuestionnaireMessagingService(
      logger,
      userService,
      messageService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should send questionnaire messages to specific user', async () => {
    user.requestResolverSettings = true;
    jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
    jest.spyOn(messageService, 'write').mockImplementation();
    await service.sendMessages(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(messageService.write).toBeCalled();
  });

  it('should not send questionnaire messages to specific user', async () => {
    user.requestResolverSettings = false;
    jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
    jest.spyOn(messageService, 'write').mockImplementation();
    await service.sendMessages(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(messageService.write).not.toBeCalled();
  });

  it('should send questionnaire messages to specific users', async () => {
    msg.user = {
      countries: [],
      regions: [],
      cities: [],
      nationalities: [],
      ageGroups: [],
      occupations: [],
      qualifications: [],
      transportationMeans: [],
      culturalInterests: [],
      civilStatuses: [],
    };
    jest
      .spyOn(userService, 'getUsersWithRequestReceivingEnabled')
      .mockResolvedValue([user]);
    jest.spyOn(messageService, 'write').mockImplementation();
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();
    expect(messageService.write).toBeCalled();

    msg.user = {
      countries: ['Cyprus'],
      regions: ['Larnaka'],
      cities: ['Larnaka'],
      nationalities: ['Cypriot'],
      ageGroups: ['18-24'],
      occupations: [1],
      qualifications: [''],
      transportationMeans: ['Car'],
      culturalInterests: ['Music'],
      civilStatuses: ['Single'],
    };
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();
    expect(messageService.write).toBeCalled();

    user.birthDate = new Date('1950-01-01T00:00:00');
    msg.user.ageGroups = ['65+'];
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();
    expect(messageService.write).toBeCalled();

    user.birthDate = new Date('2000-01-01T00:00:00');
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();

    user.birthDate = new Date('1950-01-01T00:00:00');
    msg.user.occupations = [2];
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();

    msg.user.occupations = [1];
    msg.user.qualifications = [Education.MasterDegree];
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();

    msg.user.qualifications = [Education.MasterDegree];
    msg.user.transportationMeans = ['On foot'];
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();

    msg.user.transportationMeans = ['Car'];
    msg.user.culturalInterests = ['Fashion'];
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();
  });

  it('should log error', async () => {
    jest.spyOn(userService, 'retrieveBySub').mockRejectedValue(user);
    jest.spyOn(logger, 'error').mockImplementation();
    await service.sendMessages(msg);

    expect(userService.retrieveBySub).toBeCalledWith(msg.user);
    expect(logger.error).toBeCalledWith(
      "[Questionnaire RabbitMQ] User with keycloak id '' does not exist",
    );

    msg.user = {
      countries: [],
      regions: [],
      cities: [],
      nationalities: [],
      ageGroups: [],
      occupations: [],
      qualifications: [],
      transportationMeans: [],
      culturalInterests: [],
      civilStatuses: [],
    };
    jest
      .spyOn(userService, 'getUsersWithRequestReceivingEnabled')
      .mockRejectedValue([user]);
    await service.sendMessages(msg);

    expect(userService.getUsersWithRequestReceivingEnabled).toBeCalled();
    expect(logger.error).toBeCalledWith(
      '[Questionnaire RabbitMQ] Something went wrong while trying to send message to users',
    );

    msg.status = 500;
    await service.sendMessages(msg);

    expect(logger.error).toBeCalledWith(
      '[Questionnaire RabbitMQ] 500 - A generic error has occurred.',
    );

    msg.errors = ['test'];
    await service.sendMessages(msg);

    expect(logger.error).toBeCalledWith('[Questionnaire RabbitMQ] 500 - test');
  });
});
