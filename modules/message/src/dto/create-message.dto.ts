import { ApiProperty } from '@nestjs/swagger';
import { ConfigurationDTO } from '@suite5/asset/dto/configuration.dto';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { InboxType, MessageType, SenderType } from '../constants';
import { Questionnaire, SharingRequestConfiguration } from '../interfaces';

@Exclude()
export class CreateMessageDTO {
  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly body: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly topic: string;

  @Expose()
  @IsOptional()
  @ApiProperty()
  readonly configuration?:
    | SharingRequestConfiguration
    | Questionnaire
    | ConfigurationDTO;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly senderType: SenderType;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly messageType: MessageType;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly senderName: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly senderDescription: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly senderWebsite: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly senderUUID: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly inboxType: InboxType;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly receiverId: number;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly timestamp: string;

  @Expose()
  @IsOptional()
  @ApiProperty()
  readonly price: number;
}
