import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { CreateMessageDTO } from './create-message.dto';
import { RequestResponseType } from '@suite5/message/constants';

@Exclude()
export class PublicMessageDTO extends CreateMessageDTO {
  @IsNotEmpty()
  @ApiProperty()
  @Expose()
  readonly id: number;

  @IsNotEmpty()
  @ApiProperty()
  @Expose()
  readonly createdAt: Date;

  @IsNotEmpty()
  @ApiProperty()
  @Expose()
  readonly readAt: Date;

  @ApiProperty()
  readonly receiver: any;

  @ApiProperty()
  @Expose()
  senderName: string;

  @ApiProperty()
  @Expose()
  senderDescription: string;

  @ApiProperty()
  @Expose()
  senderWebsite: string;

  @ApiProperty()
  @Expose()
  senderUUID: string;

  @ApiProperty()
  @Expose()
  timestamp: string;

  @ApiProperty()
  @Expose()
  price: number;

  @ApiProperty()
  @Expose()
  response: RequestResponseType;

  @ApiProperty()
  @Expose()
  configuration: any;

  @ApiProperty()
  @Expose()
  responseConfiguration: any;
}
