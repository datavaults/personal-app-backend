import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';

@Exclude()
export class SseMessageDTO {
  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly userId: number;

  @Expose()
  @IsNotEmpty()
  @ApiProperty()
  readonly payload: any;
}
