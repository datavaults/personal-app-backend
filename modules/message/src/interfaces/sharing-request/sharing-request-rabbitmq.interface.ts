import { RabbitmqResponse } from '@suite5/common/interfaces';
import { SharingRequestMessageBody } from './sharing-request-message-body.interface';

export interface SharingRequestRabbitmqResponse extends RabbitmqResponse {
  user: string;
  body: SharingRequestMessageBody;
}
