export interface SharingRequestConfiguration {
  asset: number;
  assetUid: string;
}
