export interface SharingRequestMessageBody {
  dataSeekerUUID: string;
  dataSeekerOrganization: string;
  dataSeekerDescription: string;
  dataSeekerURL: string;
  assetUUID: string;
  price: number | null;
}
