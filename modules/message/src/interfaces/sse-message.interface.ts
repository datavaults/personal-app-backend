import { SseQueue } from '../constants';

export interface SseMessage {
  message: any;
  queue: SseQueue;
}
