import { Question } from './question.interface';

export interface Questionnaire {
  id: number;
  creationDate: Date;
  lastUpdateDate: Date;
  expirationDate: Date | null;
  dataSeekerOrganization: string;
  dataSeekerDescription: string;
  dataSeekerUUID: string;
  dataSeekerURL: string;
  status: string;
  title: string;
  intro: string;
  license?: string;
  questions: Question[];
}
