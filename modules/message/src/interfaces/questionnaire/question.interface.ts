export interface Question {
  id: number;
  question: string;
  description: string | null;
  type:
    | 'radio'
    | 'checkbox'
    | 'text'
    | 'number'
    | 'textarea'
    | 'dropdown'
    | 'tags';
  options: any[];
  placeholder: string | null;
  default: any[];
  required: boolean;
  other: boolean;
  multiple: boolean;
  selectionsRange: { min: number | null; max: number | null };
  valueRange: { min: number | null; max: number | null };
  answer: any[];
}
