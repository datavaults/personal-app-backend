import { RabbitmqResponse } from '@suite5/common/interfaces';
import { QuestionnaireMessageUser } from './questionnaire-message-user.interface';
import { QuestionnaireMessageBody } from './questionnaire-message-body.interface';

export interface QuestionnaireRabbitmqResponse extends RabbitmqResponse {
  user: string | QuestionnaireMessageUser;
  body: QuestionnaireMessageBody;
}
