import { Questionnaire } from './questionnaire.interface';

export interface QuestionnaireMessageBody {
  questionnaire: Questionnaire;
  dataSeekerMessage: string;
  license: string;
  price: number | null;
}
