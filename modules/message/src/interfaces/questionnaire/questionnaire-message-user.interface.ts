export interface QuestionnaireMessageUser {
  countries: string[];
  regions: string[];
  cities: string[];
  nationalities: string[];
  ageGroups: string[];
  occupations: string[] | number[];
  educations?: string[] | number[];
  transportationMeans: string[];
  culturalInterests: string[];
  civilStatuses: string[];
  nationalInsuranceNumbers?: string[];
  postalCodes?: string[];
  socialSecurityNumbers?: string[];
}
