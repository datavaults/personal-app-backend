import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { HttpModule, Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  AssetModule,
  CLOUD_STRATEGY_TOKEN,
  FakeCloudStrategy,
  RealCloudStrategy,
  Transaction,
} from '@suite5/asset';
import { CoreModule } from '@suite5/core';
import { Blacklist } from '@suite5/core/user/entities';
import { MESSAGE_STRATEGY_TOKEN } from '@suite5/message/strategies';
import {
  FakeWalletStrategy,
  RealWalletStrategy,
  WalletModule,
  WALLET_STRATEGY_TOKEN,
} from '@suite5/wallet';
import { MessageController } from './controllers';
import { MessageStatsController } from './controllers/message-stats.controller';
import { Message } from './entities';
import messageModuleConfig from './message.module.config';
import {
  MessageService,
  MessageStatsService,
  QuestionnaireMessagingService,
  SharingRequestMessagingService,
} from './services';
import { SseService } from './services/sse.service';
import { FakeMessageStrategy, RealMessageStrategy } from './strategies';

@Module({
  imports: [
    HttpModule,
    CoreModule,
    AssetModule,
    ConfigModule.forFeature(messageModuleConfig),
    TypeOrmModule.forFeature([Message, Blacklist]),
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('message.module.rabbitmqUri'),
        connectionInitOptions: { wait: false },
      }),
      inject: [ConfigService],
    }),
    WalletModule,
  ],
  providers: [
    MessageService,
    Transaction,
    QuestionnaireMessagingService,
    SharingRequestMessagingService,
    SseService,
    {
      provide: MESSAGE_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev'
          ? FakeMessageStrategy
          : RealMessageStrategy,
    },
    {
      provide: CLOUD_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeCloudStrategy
          : RealCloudStrategy,
    },
    {
      provide: WALLET_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev'
          ? FakeWalletStrategy
          : RealWalletStrategy,
    },
    Logger,
    MessageStatsService,
  ],
  exports: [MessageService],
  controllers: [MessageController, MessageStatsController],
})
export class MessageModule {}
