import { Injectable } from '@nestjs/common';
import { MessageStrategy } from './message.strategy';
@Injectable()
export class FakeMessageStrategy implements MessageStrategy {
  async publishQuestionnaire(): Promise<boolean> {
    return Promise.resolve(true);
  }
}
