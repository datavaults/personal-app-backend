import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { Questionnaire } from '../interfaces';
import { MessageStrategy } from './message.strategy';
@Injectable()
export class RealMessageStrategy implements MessageStrategy {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}
  async publishQuestionnaire(
    receiverSub: string,
    data: Questionnaire,
  ): Promise<boolean> {
    const PUBLISH_SHARE_ASSET_URL = `${this.configService.get(
      'message.module.publishQuestionnnaireURL',
    )}/questionnaires/${data.id}/answer/${receiverSub}`;

    return await this.httpService
      .post(
        PUBLISH_SHARE_ASSET_URL,
        { answers: data.questions },
        {
          headers: {
            accept: 'application/json',
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }
}
