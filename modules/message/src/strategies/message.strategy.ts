import { Questionnaire } from '../interfaces';

export const MESSAGE_STRATEGY_TOKEN = 'MessageStrategy';

export interface MessageStrategy {
  publishQuestionnaire(
    receiverSub: string,
    questionnaire: Questionnaire,
  ): Promise<boolean>;
}
