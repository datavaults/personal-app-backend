import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsString,
  ValidateNested,
} from 'class-validator';

class PrivacyLevel {
  @IsInt()
  @Expose({ name: 'anonymDigits' })
  anonym_digits: number;

  @IsInt()
  @Expose({ name: 'totalDigits' })
  total_digits: number;
}

class Collection {
  @IsInt()
  @ApiProperty()
  @Expose({ name: 'piiId' })
  pii_id: number;

  @ApiProperty()
  @IsString()
  @Expose({ name: 'datasetAttribute' })
  dataset_attribute: string;

  @ApiProperty()
  @ValidateNested()
  @Type(() => PrivacyLevel)
  @Expose({ name: 'privacyLevel' })
  privacy_level: PrivacyLevel;
}

export class RiskScoreDTO {
  @IsString()
  @ApiProperty()
  @Expose({ name: 'assetId' })
  readonly asset_id: string;

  @IsString()
  @ApiProperty()
  @Expose({ name: 'assetName' })
  readonly asset_name: string;

  @IsBoolean()
  @ApiProperty()
  @Expose({ name: 'useTmp' })
  readonly use_tmp: boolean;

  @IsBoolean()
  @ApiProperty()
  @Expose({ name: 'usePersona' })
  readonly use_persona: boolean;

  @IsBoolean()
  @Expose({ name: 'useEncryption' })
  use_encryption: boolean;

  @IsArray()
  @ValidateNested({ each: true })
  @ApiProperty()
  @Type(() => Collection)
  readonly collection: Array<Collection>;
}
