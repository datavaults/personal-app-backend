import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { RiskScore } from '../interfaces';
import { RiskStrategy } from './risk.strategy';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { DataSubject } from '../interfaces';
import { RiskScoreDTO } from '../dto';
import { CloudAuthenticationService } from '@suite5/asset';

@Injectable()
export class RealRiskStrategy implements RiskStrategy {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(CloudAuthenticationService)
    private readonly cloudAuthenticationService: CloudAuthenticationService,
    @Inject(Logger)
    private readonly logger: Logger,
  ) {}

  async getDataSubject(): Promise<DataSubject> {
    // Baking in the 10000 for now since it corresponds to Datavault Users on
    // Risk backend, we might need to adjust this later in order to grab a different
    // group of piis.
    const DATA_SUBJECT_BACKEND_URL = `${this.configService.get(
      'risk-management.module.riskPlatformBackendURL',
    )}/data-subjects/10000`;

    try {
      return await this.httpService
        .get<DataSubject>(DATA_SUBJECT_BACKEND_URL)
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        });
    } catch (e) {
      this.logger.error(
        `Error fetching risk piis with error: ${e.message || e}`,
      );
      throw e;
    }
  }

  async getRisk(riskScore: RiskScoreDTO): Promise<RiskScore> {
    const RISK_SCORE_BACKEND_URL = `${this.configService.get(
      'risk-management.module.riskPlatformBackendURL',
    )}/privacy-score`;

    try {
      return await this.httpService
        .post<RiskScore>(RISK_SCORE_BACKEND_URL, riskScore, {
          headers: {
            accept: 'application/json',
            Authorization: `Bearer ${await this.cloudAuthenticationService.login()}`,
          },
        })
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        });
    } catch (e) {
      this.logger.error(
        `Error fetching risk score with error: ${e.message || e}`,
      );
      throw e;
    }
  }
}
