import { RiskScoreDTO } from '../dto';
import { DataSubject } from '../interfaces';

export const RISK_STRATEGY_TOKEN = 'RiskStrategy';

export interface RiskStrategy {
  getDataSubject(): Promise<DataSubject>;
  getRisk(risk_score: RiskScoreDTO): Promise<any>;
}
