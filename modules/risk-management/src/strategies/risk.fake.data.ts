export const fakeDataSubjectResponse = {
  id: 10000,
  name: 'DataVaults user',
  valueRank: {
    id: 'M',
    description: 'Medium',
  },
  piis: [
    {
      id: 23,
      name: 'Twitter Account',
    },
    {
      id: 24,
      name: 'Twitter Account (VERIFIED)',
    },
    {
      id: 7,
      name: 'Age',
    },
    {
      id: 11,
      name: 'Email',
    },
    {
      id: 2,
      name: 'Firstname',
    },
    {
      id: 3,
      name: 'Lastname',
    },
  ],
};
