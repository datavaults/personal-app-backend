import { HttpService, Injectable } from '@nestjs/common';
import { DataSubject, RiskScore } from '../interfaces';
import { RiskStrategy } from './risk.strategy';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { fakeDataSubjectResponse } from './risk.fake.data';
import { RiskScoreDTO } from '../dto';

@Injectable()
export class FakeRiskStrategy implements RiskStrategy {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async getDataSubject(): Promise<DataSubject> {
    return Promise.resolve(fakeDataSubjectResponse);
  }

  async getRisk(riskScore: RiskScoreDTO): Promise<RiskScore> {
    return Promise.resolve({ score: 30 });
  }
}
