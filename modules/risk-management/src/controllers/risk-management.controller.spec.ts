import { RiskScoreDTO } from '../dto';
import { fakeDataSubjectResponse, RiskStrategy } from '../strategies';
import { RiskManagementController } from './risk-management.controller';

describe('RiskManagementController', () => {
  let controller: RiskManagementController;
  let strategy: RiskStrategy;
  let mockRiskScoreRequest: {
    assetId: string;
    assetName: string;
    useTmp: boolean;
    usePersona: boolean;
    useEncryption: boolean;
    collection: {
      piiId: number;
      datasetAttribute: string;
      privacyLevel: {
        anonymDigits: number;
        totalDigits: number;
      };
    }[];
  };
  let mockRiskScoreDTO: RiskScoreDTO;

  beforeEach(async () => {
    mockRiskScoreRequest = {
      assetId: 'jfdh212312',
      assetName: 'My New Dataset',
      useTmp: true,
      usePersona: true,
      useEncryption: false,
      collection: [
        {
          piiId: 1,
          datasetAttribute: 'firstname',
          privacyLevel: {
            anonymDigits: 3,
            totalDigits: 5,
          },
        },
        {
          piiId: 3,
          datasetAttribute: 'lastname',
          privacyLevel: {
            anonymDigits: 13,
            totalDigits: 5,
          },
        },
      ],
    };

    mockRiskScoreDTO = {
      asset_id: 'jfdh212312',
      asset_name: 'My New Dataset',
      use_tmp: true,
      use_persona: true,
      use_encryption: false,
      collection: [
        {
          pii_id: 1,
          dataset_attribute: 'firstname',
          privacy_level: {
            anonym_digits: 3,
            total_digits: 5,
          },
        },
        {
          pii_id: 3,
          dataset_attribute: 'lastname',
          privacy_level: {
            anonym_digits: 13,
            total_digits: 5,
          },
        },
      ],
    };

    strategy = {
      getDataSubject: () => jest.fn() as any,
      getRisk: () => jest.fn() as any,
    };

    controller = new RiskManagementController(strategy);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return piis from data-subject result', async () => {
    jest
      .spyOn(strategy, 'getDataSubject')
      .mockResolvedValue(Promise.resolve(fakeDataSubjectResponse));
    expect(await controller.getPiis()).toEqual(fakeDataSubjectResponse.piis);
  });

  it('should fail to retrieve data-subject result', async () => {
    jest
      .spyOn(strategy, 'getDataSubject')
      .mockRejectedValue({ message: 'failed' });

    expect(controller.getPiis()).rejects.toEqual(
      Error(
        'Unable to fetch data subject from risk backend with error: failed',
      ),
    );
  });

  it('should fail to retrieve privacy-score result', async () => {
    jest.spyOn(strategy, 'getRisk').mockRejectedValue({ message: 'failed' });

    expect(controller.getPrivacyScore(mockRiskScoreRequest)).rejects.toEqual(
      Error(
        'Unable to fetch privacy score from risk backend with error: failed',
      ),
    );
  });

  it('should receive privacy-score', async () => {
    jest.spyOn(strategy, 'getRisk').mockResolvedValue({ score: 30 });
    expect(await controller.getPrivacyScore(mockRiskScoreRequest)).toEqual({
      score: 30,
    });
    expect(strategy.getRisk).toBeCalledWith(mockRiskScoreDTO);
  });
});
