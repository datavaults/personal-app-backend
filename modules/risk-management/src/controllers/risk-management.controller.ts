import {
  Controller,
  Get,
  Inject,
  Post,
  Body,
  UseGuards,
  InternalServerErrorException,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { RiskScoreDTO } from '../dto';
import { Pii, RiskScore } from '../interfaces';
import { RiskStrategy, RISK_STRATEGY_TOKEN } from '../strategies/risk.strategy';

@Controller('risk-management')
@UseGuards(AuthenticationGuard)
@ApiTags('risk-management')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class RiskManagementController {
  constructor(
    @Inject(RISK_STRATEGY_TOKEN)
    private readonly strategy: RiskStrategy,
  ) {}

  @Get('/piis')
  async getPiis(): Promise<Pii[]> {
    try {
      const dataSubject = await this.strategy.getDataSubject();
      return dataSubject.piis;
    } catch (e) {
      throw new InternalServerErrorException(
        `Unable to fetch data subject from risk backend with error: ${
          e.message || e
        }`,
      );
    }
  }

  @Post('/privacy-score')
  async getPrivacyScore(
    @Body()
    riskScore: {
      assetId: string;
      assetName: string;
      useTmp: boolean;
      usePersona: boolean;
      useEncryption: boolean;
      collection: {
        piiId: number;
        datasetAttribute: string;
        privacyLevel: {
          anonymDigits: number;
          totalDigits: number;
        };
      }[];
    },
  ): Promise<RiskScore> {
    try {
      return await this.strategy.getRisk({
        asset_id: riskScore.assetId,
        asset_name: riskScore.assetName,
        use_tmp: riskScore.useTmp,
        use_persona: riskScore.usePersona,
        use_encryption: riskScore.useEncryption,
        collection: riskScore.collection.map(
          (collection: {
            piiId: number;
            datasetAttribute: string;
            privacyLevel: {
              anonymDigits: number;
              totalDigits: number;
            };
          }) => {
            return {
              pii_id: collection.piiId,
              dataset_attribute: collection.datasetAttribute,
              privacy_level: {
                anonym_digits: collection.privacyLevel.anonymDigits,
                total_digits: collection.privacyLevel.totalDigits,
              },
            };
          },
        ),
      });
    } catch (e) {
      throw new InternalServerErrorException(
        `Unable to fetch privacy score from risk backend with error: ${
          e.message || e
        }`,
      );
    }
  }
}
