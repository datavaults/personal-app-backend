import { Pii } from './pii.interface';

export interface DataSubject {
  id: number;
  name: string;
  valueRank: {
    id: string;
    description: string;
  };
  piis: Pii[];
}
