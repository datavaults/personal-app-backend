import riskModuleConfig from './risk-management.module.config';
import { HttpModule, Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RiskManagementController } from './src/controllers';
import { RISK_STRATEGY_TOKEN } from './src/strategies/risk.strategy';
import { FakeRiskStrategy } from './src/strategies/risk.fake.strategy';
import { RealRiskStrategy } from './src/strategies/risk.real.strategy';
import { AssetModule } from '@suite5/asset';

@Module({
  imports: [ConfigModule.forFeature(riskModuleConfig), HttpModule, AssetModule],
  providers: [
    Logger,
    {
      provide: RISK_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeRiskStrategy
          : RealRiskStrategy,
    },
  ],
  exports: [],
  controllers: [RiskManagementController],
})
export class RiskManagementModule {}
