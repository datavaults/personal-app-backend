import { registerAs } from '@nestjs/config';

export default registerAs('risk-management.module', () => ({
  riskPlatformBackendURL: process.env.RISK_PLATFORM_BACKEND_URL,
}));
