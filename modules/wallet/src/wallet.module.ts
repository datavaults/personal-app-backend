import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import walletConfig from './wallet.module.config';
import { CoreModule } from '@suite5/core';
import {
  FakeWalletStrategy,
  WALLET_STRATEGY_TOKEN,
  RealWalletStrategy,
} from './strategies';
import { WalletController } from './controllers';

@Module({
  imports: [CoreModule, HttpModule, ConfigModule.forFeature(walletConfig)],
  providers: [
    {
      provide: WALLET_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev'
          ? FakeWalletStrategy
          : RealWalletStrategy,
    },
  ],
  controllers: [WalletController],
  exports: [],
})
export class WalletModule {}
