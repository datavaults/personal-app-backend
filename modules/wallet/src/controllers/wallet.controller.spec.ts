import { WalletStrategy } from '../strategies';
import { WalletController } from './wallet.controller';

describe('WalletController', () => {
  let controller: WalletController;
  let mockWalletStrategy: WalletStrategy;
  const id = '99088887-5d38-4d56-b064-65f212a96410';
  const data = {};

  beforeEach(async () => {
    mockWalletStrategy = {
      getCompensations: (id: any) => jest.fn() as any,
      getMerchants: () => jest.fn() as any,
      getMerchant: (id: any) => jest.fn() as any,
      createAccount: (id: any, data: any) => jest.fn() as any,
      purchase: (data: any) => jest.fn() as any,
      updateTransactions: (data: any, id: any) => jest.fn() as any,
      useCompensation: (compensation: {
        compensationId: string;
        name: string;
      }) => jest.fn() as any,
      invoices:(userId: string) => jest.fn() as any,
    };

    controller = new WalletController(mockWalletStrategy);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should retrieve compensation', async () => {
    jest.spyOn(mockWalletStrategy, 'getCompensations').mockImplementation();
    await controller.retrieveCompensations(id);
    expect(mockWalletStrategy.getCompensations).toBeCalledTimes(1);
  });

  it('should retrieve merchants', async () => {
    jest.spyOn(mockWalletStrategy, 'getMerchants').mockImplementation();
    await controller.retrieveMerchants();
    expect(mockWalletStrategy.getMerchants).toBeCalledTimes(1);
  });

  it('should retrieve merchant', async () => {
    jest.spyOn(mockWalletStrategy, 'getMerchant').mockImplementation();
    await controller.retrieveMerchant(id);
    expect(mockWalletStrategy.getMerchant).toBeCalledTimes(1);
  });

  it('should create wallet account', async () => {
    jest.spyOn(mockWalletStrategy, 'createAccount').mockImplementation();
    await controller.createAccount(id, data);
    expect(mockWalletStrategy.createAccount).toBeCalledTimes(1);
  });

  it('should purchase merchant', async () => {
    jest.spyOn(mockWalletStrategy, 'purchase').mockImplementation();
    await controller.purchase(data);
    expect(mockWalletStrategy.purchase).toBeCalledTimes(1);
  });
});
