import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CurrentUser, UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import {
  WalletStrategy,
  WALLET_STRATEGY_TOKEN,
} from '../strategies/wallet.strategy';

@Controller('wallet')
@UseGuards(AuthenticationGuard)
@ApiTags('wallet')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class WalletController {
  constructor(
    @Inject(WALLET_STRATEGY_TOKEN)
    private readonly strategy: WalletStrategy,
  ) {}

  @Get('compensations/:id')
  @ApiOperation({ summary: 'Retrieve compensations' })
  @ApiNotFoundResponse()
  async retrieveCompensations(@Param('id') id: any): Promise<any> {
    return await this.strategy.getCompensations(id);
  }

  @Post('compensations/use/:id')
  @ApiOperation({ summary: 'Retrieve compensations' })
  @ApiNotFoundResponse()
  async useCompensation(
    @CurrentUser() user: UserData,
    @Param('id') compensationId: string,
  ): Promise<any> {
    return await this.strategy.useCompensation({
      compensationId,
      name: user.sub,
    });
  }

  @Get('merchants')
  @ApiOperation({ summary: 'Retrieve merchants' })
  @ApiNotFoundResponse()
  async retrieveMerchants(): Promise<any> {
    return await this.strategy.getMerchants();
  }

  @Get('merchant/:id')
  @ApiOperation({ summary: 'Retrieve merchant' })
  @ApiNotFoundResponse()
  async retrieveMerchant(@Param('id') id: any): Promise<any> {
    return await this.strategy.getMerchant(id);
  }

  @Post(':id')
  @ApiOperation({ summary: 'Create wallet account' })
  @ApiNotFoundResponse()
  async createAccount(@Body() data: any, @Param('id') id: any): Promise<any> {
    return await this.strategy.createAccount(id, data);
  }

  @Post('purchase')
  @ApiOperation({ summary: 'Purchase merchant' })
  @ApiNotFoundResponse()
  async purchase(@Body() data: any): Promise<any> {
    return await this.strategy.purchase(data);
  }

  @Get('invoices')
  @ApiOperation({ summary: 'Invoices' })
  @ApiNotFoundResponse()
  async invoices(@CurrentUser() user: UserData): Promise<any> {
    return await this.strategy.invoices(user.sub);
  }
}
