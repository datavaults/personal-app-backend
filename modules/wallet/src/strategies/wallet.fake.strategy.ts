import { Injectable } from '@nestjs/common';
import { WalletStrategy } from './wallet.strategy';

@Injectable()
export class FakeWalletStrategy implements WalletStrategy {
  async createAccount(nationalInsuranceNumber: any): Promise<any> {
    const wallet = { wallet: { numEcoins: '30' } };
    return wallet;
  }

  async getCompensations(accountId: any): Promise<any> {
    return [
      {
        id: 'dc6e1584-dfb9-4986-8e23-439311af44a7',
        name: 'Transaction_dc6e1584-dfb9-4986-8e23-439311af44a7',
        description: 'miwenergia - vtest 2',
        points: '2.0',
        date: '2023-02-28T07:39:32.091921',
        status: 'Used',
      },
      {
        id: '5863c56c-2fbc-4718-995b-a8653644cdc8',
        name: 'Transaction_5863c56c-2fbc-4718-995b-a8653644cdc8',
        description: 'datavaultsh2020',
        points: '1.0',
        date: '2023-02-28T11:24:11.564039',
        status: 'Used',
      },
      {
        id: 'b747bcfc-80df-4e30-84fb-702cba28298d',
        name: 'Transaction_b747bcfc-80df-4e30-84fb-702cba28298d',
        description: 'Miwenergia - vtest3',
        points: '3.0',
        date: '2023-03-01T11:00:31.775537',
        status: 'Used',
      },
    ];
  }

  async getMerchants(): Promise<any> {
    return true;
  }

  async getMerchant(id: any): Promise<any> {
    return true;
  }

  async purchase(shoppingBasket: any): Promise<any> {
    return true;
  }

  async updateTransactions(data: any, userSub: any): Promise<any> {
    return true;
  }

  async useCompensation(compensation: {
    compensationId: string;
    name: string;
  }): Promise<any> {
    return true;
  }

  async invoices(userId: any): Promise<any> {
    return [
      {
        contractNumber: '870280189',
        cost: 5,
        merchantName: 'THE GLOBE',
        products: [
          {
            idProduct: 1629954220,
            amount: '1',
            price: '5',
          },
        ],
      },
    ];
  }
}
