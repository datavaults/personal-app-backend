export const WALLET_STRATEGY_TOKEN = 'WalletStrategy';

export interface WalletStrategy {
  createAccount(accountId: string, customer: any): Promise<any>;
  getCompensations(accountId: any): Promise<any>;
  getMerchants(): Promise<any>;
  getMerchant(id: any): Promise<any>;
  purchase(shoppingBasket: any): Promise<any>;
  updateTransactions(data: any, userSub: any): Promise<any>;
  useCompensation(compensation: {
    compensationId: string;
    name: string;
  }): Promise<void>;
  invoices(userId: string): Promise<any>;
}
