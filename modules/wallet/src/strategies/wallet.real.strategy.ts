import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { WalletStrategy } from './wallet.strategy';

@Injectable()
export class RealWalletStrategy implements WalletStrategy {
  private readonly WALLET_URL = `${this.configService.get(
    'wallet.module.walletUrl',
  )}/wallet/`;
  private readonly MERCHANT_URL = `${this.configService.get(
    'wallet.module.merchantUrl',
  )}/merchant/`;
  private readonly WALLET_POINTS = `${this.configService.get(
    'wallet.module.walletUrl',
  )}/exchange/`;
  private readonly WALLET_INVOICES = `${this.WALLET_URL}invoices/`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async createAccount(accountId: string, customer: any): Promise<any> {
    return await this.httpService
      .post(this.WALLET_URL + accountId, customer, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Account---------', e));
  }

  async getCompensations(accountId: any): Promise<any> {
    return await this.httpService
      .get(this.WALLET_URL + `compensations/${accountId}`, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Compensations---------', e));
  }

  async getMerchants(): Promise<any> {
    return await this.httpService
      .get(this.MERCHANT_URL, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Merchants---------', e));
  }

  async getMerchant(id: any): Promise<any> {
    return await this.httpService
      .get(this.MERCHANT_URL + id, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Merchant---------', e));
  }

  async purchase(shoppingBasket: any): Promise<any> {
    return await this.httpService
      .post(this.WALLET_URL + `purchase`, shoppingBasket, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Purchase---------', e));
  }

  async updateTransactions(data: any, userSub: any): Promise<any> {
    return await this.httpService
      .post(this.WALLET_POINTS + `compensation/${userSub}`, [data], {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log('---------Wallet Transaction---------', e));
  }

  async useCompensation(compensation: {
    compensationId: string;
    name: string;
  }): Promise<any> {
    return await this.httpService
      .post(this.WALLET_URL + `compensation/withdrawal/`, compensation, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  async invoices(userId: string): Promise<any> {
    const userInvoices = `${this.WALLET_INVOICES}${userId}`;
    console.log(userInvoices);
    return await this.httpService
      .get(userInvoices, {
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }
}
