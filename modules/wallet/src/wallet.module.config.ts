import { registerAs } from '@nestjs/config';

export default registerAs('wallet.module', () => ({
  walletUrl: process.env.WALLET_BASE_URL,
  merchantUrl: process.env.WALLET_MERCHANT_URL,
  invoicesUrl: process.env.WALLET_INVOICES_URL,
}));
