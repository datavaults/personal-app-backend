import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateMobileAppDataDTO {
  @ApiProperty()
  subId: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly recordDate: Date;

  @IsNotEmpty()
  @ApiProperty()
  readonly dataSource: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly dataSourceId: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly data: Record<string, any>;

  @ApiProperty()
  status?: boolean;
}
