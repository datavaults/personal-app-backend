import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateAssetDTO {
  @IsOptional()
  @ApiProperty()
  readonly subId: string;

  @IsOptional()
  @ApiProperty()
  readonly recordDate: Date;

  @IsOptional()
  @ApiProperty()
  readonly dataSource: string;

  @IsOptional()
  @ApiProperty()
  readonly dataSourceId: string;

  @IsOptional()
  @ApiProperty()
  readonly data: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  status?: boolean;
}
