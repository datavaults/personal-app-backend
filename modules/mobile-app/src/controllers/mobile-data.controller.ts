import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UserSources } from '../constants';
import { MobileDataGuard } from '../guards/mobile-data.guard';
import { MobileDataService } from '../services/mobile-data.service';

@Controller('mobile-data')
@UseGuards(MobileDataGuard)
@ApiTags('mobile-data')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
export class MobileDataController {
  constructor(private readonly mobileDataService: MobileDataService) {}

  @Get(':sub/:source')
  @ApiOperation({ summary: 'Retrieve user data' })
  @ApiNotFoundResponse()
  async getSourceData(
    @Param('sub') sub: any,
    @Param('source') source: UserSources,
  ) {
    return await this.mobileDataService.getSourceData(sub, source);
  }
}
