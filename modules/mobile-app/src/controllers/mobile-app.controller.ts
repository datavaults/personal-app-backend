import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CurrentUser, UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { CreateMobileAppDataDTO } from '../dto';
import { MobileAppService } from '../services/mobile-app.service';

@Controller('mobile-app')
@UseGuards(AuthenticationGuard)
@ApiTags('mobile-app')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class MobileAppController {
  constructor(private readonly mobileAppService: MobileAppService) {}

  @Get(':id')
  @ApiOperation({ summary: 'Confirm user source' })
  @ApiNotFoundResponse()
  async confirmUserSource(@Param('id') userSubId: any) {
    return await this.mobileAppService.confirmUserSource(userSubId);
  }

  @Post(':id')
  @ApiOperation({ summary: 'Upload data for user source' })
  @ApiNotFoundResponse()
  async uploadUserData(
    @Param('id') userSubId: any,
    @Body() mobileAppData: CreateMobileAppDataDTO,
  ) {
    return await this.mobileAppService.createMobileAppData(
      userSubId,
      mobileAppData,
    );
  }

  @Patch('/shareProfile/:sourceId/:enabled')
  @ApiOperation({ summary: 'enable or disable source' })
  async shareProfile(
    @CurrentUser() user: UserData,
    @Param('enabled') enabled: boolean,
    @Param('sourceId') sourceId: string,
  ): Promise<boolean> {
    return this.mobileAppService.sourceEnabled(user.sub, enabled, sourceId);
  }
}
