import { CreateMobileAppDataDTO } from '../dto';
import { MobileAppController } from './mobile-app.controller';

describe('UserController', () => {
  let mockMobileAppController: MobileAppController;
  let mockMobileAppService: any;
  const dataFetcherResult = [
    {
      createdAt: '2022-03-23T14:18:48.411Z',
      updatedAt: '2022-03-23T14:18:48.411Z',
      id: 33,
      name: 'test file',
      description: 'test file',
      keywords: ['test file'],
      url: null,
      sourceType: 'Mobile App',
      createdById: 1,
      credentials: {},
      schedule: null,
      errors: '',
      uid: 'b24c0fdc-a845-4e53-9ee7-2385905dcede',
      type: 'dataset',
      firstCollection: null,
      shared: false,
    },
  ];

  const result = {
    id: '1',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    createdDate: new Date(),
    recordDate: '2022-03-31 13:41:43Z',
  };

  const data: CreateMobileAppDataDTO = {
    dataSource: 'Location',
    recordDate: '2022-03-31 13:41:43Z' as any,
    data: {},
    dataSourceId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
  };

  beforeEach(async () => {
    mockMobileAppService = {
      confirmUserSource: () => jest.fn(),
      createMobileAppData: () => jest.fn(),
    };
    mockMobileAppController = new MobileAppController(mockMobileAppService);
  });

  it('should be defined', () => {
    expect(mockMobileAppController).toBeDefined();
  });

  it('should return user sources for mobile app', async () => {
    jest
      .spyOn(mockMobileAppService, 'confirmUserSource')
      .mockResolvedValue(dataFetcherResult);
    expect(
      await mockMobileAppController.confirmUserSource(
        'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
      ),
    ).toBe(dataFetcherResult);
  });

  // it('should upload user data', async () => {
  //   jest
  //     .spyOn(mockMobileAppService, 'createMobileAppData')
  //     .mockResolvedValue(result);
  //   expect(
  //     await mockMobileAppController.uploadUserData(
  //       'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
  //       data,
  //     ),
  //   ).toBe(result);
  // });
});
