import { UserSources } from '../constants';
import { CreateMobileAppDataDTO } from '../dto';
import { MobileDataController } from './mobile-data.controller';

describe('MobileDataController', () => {
  let mockMobileDataController: MobileDataController;
  let mockMobileDataService: any;

  const data: CreateMobileAppDataDTO = {
    dataSource: 'Location',
    recordDate: '2022-03-31 13:41:43Z' as any,
    data: {},
    dataSourceId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
  };

  beforeEach(async () => {
    mockMobileDataService = {
      getSourceData: () => jest.fn(),
    };
    mockMobileDataController = new MobileDataController(mockMobileDataService);
  });

  it('should be defined', () => {
    expect(mockMobileDataController).toBeDefined();
  });

  it('should upload user data', async () => {
    jest.spyOn(mockMobileDataService, 'getSourceData').mockResolvedValue(data);
    expect(
      await mockMobileDataController.getSourceData(
        'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
        UserSources.Location,
      ),
    ).toBe(data);
  });
});
