export enum UserSources {
  Location = 'Location',
  Route = 'Route',
  GoogleFit = 'Google Fit',
}
