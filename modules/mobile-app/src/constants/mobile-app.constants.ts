export enum MobileAppType {
  Location = 'Location',
  Route = 'Route',
  GoogleFit = 'Google Fit',
  MobileApp = 'MOBILEAPP',
}
