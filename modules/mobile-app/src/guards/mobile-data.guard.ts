import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { UserService } from '@suite5/core/user';

@Injectable()
export class MobileDataGuard implements CanActivate {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {}

  private readonly DATA_FETCHER_HASH = this.configService.get(
    'mobile-data.module.dataFetcherToken',
  );

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const transport = context.switchToHttp().getRequest();
    if (
      transport.headers['authorization'].split(' ')[1] ===
      this.DATA_FETCHER_HASH
    ) {
      const request = context.switchToHttp().getRequest();
      const user = await this.userService.retrieveBySub(request.params.sub);
      if (user) {
        return true;
      }
    } else {
      return false;
    }
  }
}
