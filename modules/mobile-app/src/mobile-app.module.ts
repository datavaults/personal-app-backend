import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssetModule } from '@suite5/asset';
import { UserModule } from '@suite5/core/user';
import { MobileAppController } from './controllers/mobile-app.controller';
import { MobileApp } from './entities/mobile-app.entity';
import { MobileAppService } from './services/mobile-app.service';
import mobileDataConfig from './mobile-data.module.config';
import { ConfigModule } from '@nestjs/config';
import { MobileDataController } from './controllers';
import { MobileDataService } from './services';
import { MobileDataGuard } from './guards';

@Module({
  imports: [
    ConfigModule.forFeature(mobileDataConfig),
    TypeOrmModule.forFeature([MobileApp]),
    AssetModule,
    UserModule,
  ],
  providers: [MobileAppService, MobileDataService, MobileDataGuard],
  exports: [],
  controllers: [MobileAppController, MobileDataController],
})
export class MobileAppModule {}
