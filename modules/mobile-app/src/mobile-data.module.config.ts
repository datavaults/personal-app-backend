import { registerAs } from '@nestjs/config';

export default registerAs('mobile-data.module', () => ({
  dataFetcherToken: process.env.DATA_FETCHER_HASH,
}));
