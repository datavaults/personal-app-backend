import { CreateMobileAppDataDTO } from '../dto';
import { MobileAppService } from './mobile-app.service';

describe('MobileAppService', () => {
  let service: MobileAppService;
  let repository: any;
  let mockUserService: any;
  let mockDataFetcherService: any;
  const data: CreateMobileAppDataDTO = {
    dataSource: 'Location',
    recordDate: '2022-03-31 13:41:43Z' as any,
    data: {},
    dataSourceId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
  };

  const result = {
    id: '1',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    createdDate: new Date(),
    recordDate: '2022-03-31 13:41:43Z',
  };

  const dataFetcherResult = [
    {
      createdAt: '2022-03-23T14:18:48.411Z',
      updatedAt: '2022-03-23T14:18:48.411Z',
      id: 33,
      name: 'test file',
      description: 'test file',
      keywords: ['test file'],
      url: null,
      sourceType: 'Location',
      createdById: 1,
      credentials: {},
      schedule: null,
      errors: '',
      uid: 'b24c0fdc-a845-4e53-9ee7-2385905dcede',
      type: 'dataset',
      firstCollection: null,
      shared: false,
    },
  ];

  beforeAll(async () => {
    repository = {
      create: jest.fn(),
      save: jest.fn(),
    };

    mockUserService = {
      retrieveBySub: () => jest.fn(),
    };
    mockDataFetcherService = {
      retrieveByUserId: () => jest.fn(),
    };
    service = new MobileAppService(
      repository,
      mockDataFetcherService,
      mockUserService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a source', async () => {
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(data);
    expect(
      await service.createMobileAppData(
        'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
        data,
      ),
    ).toBeTruthy();
  });

  it('should confirm user sources', async () => {
    jest
      .spyOn(mockUserService, 'retrieveBySub')
      .mockResolvedValue('cc27f52e-c09d-49ee-a83b-071eb23f6d17');
    jest
      .spyOn(mockDataFetcherService, 'retrieveByUserId')
      .mockResolvedValue(dataFetcherResult);
    expect(
      await service.confirmUserSource('cc27f52e-c09d-49ee-a83b-071eb23f6d17'),
    ).toEqual(dataFetcherResult);
  });
});
