import { CreateMobileAppDataDTO } from '../dto';
import { MobileDataService } from './mobile-data.service';

describe('MobileDataService', () => {
  let service: MobileDataService;
  let repository: any;

  const data: CreateMobileAppDataDTO = {
    dataSource: 'Location',
    recordDate: '2022-03-31 13:41:43Z' as any,
    data: {},
    dataSourceId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
    subId: 'cc27f52e-c09d-49ee-a83b-071eb23f6d17',
  };

  beforeAll(async () => {
    repository = {
      find: jest.fn(),
    };

    service = new MobileDataService(repository);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be return user data', () => {
    jest.spyOn(repository, 'find').mockReturnValue(data);
    expect(service).toBeTruthy();
  });
});
