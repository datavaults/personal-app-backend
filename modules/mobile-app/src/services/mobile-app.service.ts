import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataFetcherService } from '@suite5/asset';
import { UserService } from '@suite5/core/user';
import { Repository } from 'typeorm';
import { MobileAppType } from '../constants';
import { CreateMobileAppDataDTO } from '../dto';
import { MobileApp } from '../entities/mobile-app.entity';

@Injectable()
export class MobileAppService {
  constructor(
    @InjectRepository(MobileApp)
    private readonly mobileAppRepository: Repository<MobileApp>,
    private readonly dataFetcherService: DataFetcherService,
    private readonly userService: UserService,
  ) {}

  async confirmUserSource(userSubId: any) {
    const user = await this.userService.retrieveBySub(userSubId);
    const userAssets = await this.dataFetcherService.retrieveByUserId(
      user.id as any,
    );
    const filteredAssets = userAssets.filter((item) => {
      if (Object.values(MobileAppType).includes(item.sourceType as any))
        return item.id;
    });
    return filteredAssets;
  }

  async createMobileAppData(
    userSub: string,
    mobileAppDataDTO: CreateMobileAppDataDTO,
  ) {
    mobileAppDataDTO.subId = userSub;
    mobileAppDataDTO.status = true;
    const mobileAppData = await this.mobileAppRepository.create(
      mobileAppDataDTO,
    );
    const data = await this.mobileAppRepository.save(mobileAppData);
    return {
      id: data.id,
      subId: data.subId,
      createdDate: data.createdAt,
      recordDate: data.recordDate,
    };
  }

  async sourceEnabled(userId: string, enabled: boolean, sourceId: string) {
    const source = await this.mobileAppRepository.findOneOrFail({
      subId: userId,
      dataSourceId: sourceId,
    });
    await this.mobileAppRepository.merge(source, { status: enabled });
    await this.mobileAppRepository.save(source);
    return true;
  }
}
