import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserSources } from '../constants';
import { MobileApp } from '../entities/mobile-app.entity';

@Injectable()
export class MobileDataService {
  constructor(
    @InjectRepository(MobileApp)
    private readonly mobileAppRepository: Repository<MobileApp>,
  ) {}

  async getSourceData(userSubId: any, userSource: UserSources) {
    const data = await this.mobileAppRepository.find({
      subId: userSubId,
      dataSource: userSource,
      fetched: false,
      status: true,
    });
    if (data) {
      data.forEach(async (item: any) => {
        const asset = await this.mobileAppRepository.findOne(item.id);
        item.fetched = true;
        await this.mobileAppRepository.merge(asset, item);
        await this.mobileAppRepository.save(asset);
      });
    }
    return data;
  }
}
