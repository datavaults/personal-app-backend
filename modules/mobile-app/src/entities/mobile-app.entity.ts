/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { GraphQLJSONObject } from 'graphql-type-json';
import { Column, Entity } from 'typeorm';

@Entity()
@ObjectType()
export class MobileApp extends BaseEntity {
  @Column()
  @Field()
  subId: string;

  @Column()
  @Field()
  recordDate: Date;

  @Column({ nullable: true })
  @Field()
  dataSource: string;

  @Column()
  @Field()
  dataSourceId: string;

  @Column({ type: 'json' })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
  )
  data: any;

  @Column({ nullable: true })
  @Field()
  status?: boolean;

  @Column({ default: false })
  @Field()
  fetched: boolean;
}
