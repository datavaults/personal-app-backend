/* eslint-disable @typescript-eslint/camelcase */
import {
  BadRequestException,
  HttpException,
  HttpModule,
  HttpService,
  InternalServerErrorException,
  NotFoundException,
  ServiceUnavailableException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';
import { VaultPolicyDTO } from './dto/vault-policy.dto';
import { VaultTokenDTO } from './dto/vault-token.dto';
import { VaultService } from './vault.service';

const internalServerErrorObject = {
  status: 500,
  data: {
    errors: ['Internal server error'],
  },
};

const vaultIsSealedErrorObject = {
  status: 503,
  data: {
    errors: ['error performing token check: Vault is sealed'],
  },
};

describe('VaultService', () => {
  let mockConfigService;
  let service: VaultService;
  let httpService: HttpService;

  beforeEach(async () => {
    // Mocks
    mockConfigService = jest.fn(() => ({
      get: (param) => {
        switch (param) {
          case 'vault.vaultUrl':
            return 'http://127.0.0.1:8200/v1';
          case 'vault.rootHeader':
            return {
              headers: { Authorization: 'Bearer abcd' },
            };
          case 'vault.tokenTTL':
            return '1h';
          case 'vault.tokenRenewable':
            return true;
          default:
            return 'abc';
        }
      },
    }));

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        VaultService,
        { provide: ConfigService, useClass: mockConfigService }, // mocked ConfigService
      ],
    }).compile();

    service = module.get<VaultService>(VaultService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return the vault status', async () => {
    const mockResult: AxiosResponse<{ initialized: boolean }> = {
      data: {
        initialized: true,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult = mockResult.data;

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult));
    expect(await service.getStatus()).toEqual(expectedResult);
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should return the seal status', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        type: 'shamir',
        initialized: true,
        sealed: true,
        t: 1,
        n: 1,
        progress: 0,
        nonce: '',
        version: '1.3.2',
        migration: false,
        recovery_seal: false,
        storage_type: 'file',
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult = mockResult.data;
    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult));
    expect(await service.getSealStatus()).toEqual(expectedResult);
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should unseal the vault and activate the KV-v1 secrets engine', async () => {
    const mockResult1: AxiosResponse<any> = {
      data: {
        type: 'shamir',
        initialized: true,
        sealed: false,
        t: 1,
        n: 1,
        progress: 0,
        nonce: '',
        version: '1.3.2',
        migration: false,
        cluster_name: 'vault-cluster-9a88a138',
        cluster_id: 'fc518629-12d1-cbd3-a36b-d8e1a74cc344',
        recovery_seal: false,
        storage_type: 'file',
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const mockResult2: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    const expectedResult = mockResult1.data;

    jest
      .spyOn(httpService, 'post')
      .mockImplementationOnce(() => of(mockResult1))
      .mockImplementationOnce(() => of(mockResult2));
    expect(await service.unsealVault('abcd')).toEqual(expectedResult);
    expect(httpService.post).toBeCalledTimes(2);
  });

  it('should unseal the vault and fail to activate the KV-v1 secrets engine', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        type: 'shamir',
        initialized: true,
        sealed: false,
        t: 1,
        n: 1,
        progress: 0,
        nonce: '',
        version: '1.3.2',
        migration: false,
        cluster_name: 'vault-cluster-9a88a138',
        cluster_id: 'fc518629-12d1-cbd3-a36b-d8e1a74cc344',
        recovery_seal: false,
        storage_type: 'file',
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const alreadyInUsePathErrorObject = {
      status: 400,
      data: {
        errors: ['path is already in use at secret/'],
      },
    };

    const expectedResult = mockResult.data;

    jest
      .spyOn(httpService, 'post')
      .mockImplementationOnce(() => of(mockResult))
      .mockImplementationOnce(() => {
        throw new BadRequestException(alreadyInUsePathErrorObject);
      });
    expect(await service.unsealVault('abcd')).toEqual(expectedResult);
    expect(httpService.post).toBeCalledTimes(2);
  });

  it('should create a write policy', async () => {
    const mockResult: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    jest.spyOn(httpService, 'put').mockImplementation(() => of(mockResult));
    expect(
      await service.createWritePolicy('organization1', 'user1', 'api'),
    ).toEqual({
      name: 'write-organization1-user1-api',
    });
    expect(httpService.put).toBeCalledTimes(1);
  });

  it('should return a vault policy', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        name: 'test-policy',
        rules: 'path "secret/test" {\n capabilities = ["read"]\n}\n',
        request_id: '9cc597b4-cd54-474c-356b-a4f7e05dba65',
        lease_id: '',
        renewable: false,
        lease_duration: 0,
        data: {
          name: 'test-policy',
          rules: 'path "secret/test" {\n capabilities = ["read"]\n}\n',
        },
        wrap_info: null,
        warnings: null,
        auth: null,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult: VaultPolicyDTO = mockResult.data.data;

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult));
    expect(await service.getPolicy('test-policy')).toEqual(expectedResult);
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should return all vault policies', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        keys: ['default', 'test-policy', 'root'],
        policies: ['default', 'test-policy', 'root'],
        request_id: '0c89c53f-80d5-e0cc-8852-4457d14aa07b',
        lease_id: '',
        renewable: false,
        lease_duration: 0,
        data: {
          keys: ['default', 'test-policy', 'root'],
          policies: ['default', 'test-policy', 'root'],
        },
        wrap_info: null,
        warnings: null,
        auth: null,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult: string[] = mockResult.data.data.policies;

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult));
    expect(await service.getPolicies()).toEqual(expectedResult);
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should create a token, if a secret exists', async () => {
    const mockResult1: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    const mockResult2: AxiosResponse<any> = {
      data: {
        request_id: '32036e73-950c-49f4-8999-2873539be186',
        lease_id: '',
        renewable: false,
        lease_duration: 0,
        data: null,
        wrap_info: null,
        warnings: null,
        auth: {
          client_token: 's.bFxL4lFEYxOSQzn0cdDvbQjv',
          accessor: 'BlEe3wM4hNKQy95zFgeiIlYO',
          policies: ['default', 'read-organization1-user1-api'],
          token_policies: ['default', 'read-organization1-user1-api'],
          metadata: null,
          lease_duration: 3600,
          renewable: true,
          entity_id: '',
          token_type: 'service',
          orphan: false,
        },
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const mockResult3: AxiosResponse<any> = {
      data: {
        request_id: 'f421cf34-8e51-1092-0d5a-432c2ad3e2d2',
        lease_id: '',
        renewable: false,
        lease_duration: 0,
        data: {
          accessor: 'mOTf8298fgKZmYiCAA5uWVnd',
          creation_time: 1581936159,
          creation_ttl: 3600,
          display_name: 'token',
          entity_id: '',
          expire_time: '2020-02-17T13:42:39.774835315+02:00',
          explicit_max_ttl: 0,
          id: 's.bFxL4lFEYxOSQzn0cdDvbQjv',
          issue_time: '2020-02-17T12:42:39.77483902+02:00',
          meta: null,
          num_uses: 0,
          orphan: false,
          path: 'auth/token/create',
          policies: ['default', 'read-organization1-user1'],
          renewable: true,
          ttl: 3585,
          type: 'service',
        },
        wrap_info: null,
        warnings: null,
        auth: null,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult: VaultTokenDTO = {
      token: mockResult2.data.auth.client_token,
      expireTime: mockResult3.data.data.expire_time,
      url: 'http://127.0.0.1:8200/v1/secret/organization1/user1/api',
    };

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult1));
    jest.spyOn(httpService, 'put').mockImplementation(() => of(mockResult1));
    jest.spyOn(httpService, 'put').mockImplementation(() => of(mockResult1));
    jest
      .spyOn(httpService, 'post')
      .mockImplementationOnce(() => of(mockResult2))
      .mockImplementationOnce(() => of(mockResult3));
    expect(await service.createToken('organization1', 'user1', 'api')).toEqual(
      expectedResult,
    );
    expect(httpService.put).toBeCalledTimes(1);
    expect(httpService.post).toBeCalledTimes(2);
  });

  it('should return null token, if a secret does not exist', async () => {
    jest.spyOn(service, 'secretExists').mockResolvedValue(false);
    expect(await service.createToken('organization1', 'user1', 'api')).toBe(
      null,
    );
  });

  it('should renew a token', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        request_id: 'f468e0fd-caaf-2cc5-3919-0baae46ce480',
        lease_id: '',
        renewable: false,
        lease_duration: 0,
        data: null,
        wrap_info: null,
        warnings: null,
        auth: {
          client_token: 's.RvtXGRGwBYZBkl3BFRDA8Ip0',
          accessor: 'mOTf8298fgKZmYiCAA5uWVnd',
          policies: ['default', 'test-policy'],
          token_policies: ['default', 'test-policy'],
          metadata: null,
          lease_duration: 3600,
          renewable: true,
          entity_id: '',
          token_type: 'service',
          orphan: false,
        },
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult = mockResult.data.auth;

    jest.spyOn(httpService, 'post').mockImplementation(() => of(mockResult));
    expect(await service.renewToken('abcd')).toEqual(expectedResult);
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should revoke a token', async () => {
    const mockResult: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    jest.spyOn(httpService, 'post').mockImplementation(() => of(mockResult));
    expect(await service.revokeToken('abcd')).toBeUndefined();
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should create a secret', async () => {
    const mockResult: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    jest.spyOn(httpService, 'post').mockImplementation(() => of(mockResult));
    expect(
      await service.createSecret('organization1', 'user1', 'api', {
        password: 'test-password',
      }),
    ).toEqual({
      path: 'http://127.0.0.1:8200/v1/secret/organization1/user1/api',
    });
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should update a secret', async () => {
    const mockResult1: AxiosResponse<any> = {
      data: {
        request_id: '77093ee2-3d75-b8d3-4e3e-8790980dfca2',
        lease_id: '',
        renewable: false,
        lease_duration: 2764800,
        data: {
          password: 'test-password',
          username: 'test-username',
        },
        wrap_info: null,
        warnings: null,
        auth: null,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const mockResult2: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult1));
    jest.spyOn(httpService, 'post').mockImplementation(() => of(mockResult2));
    expect(
      await service.updateSecret('organization1', 'user1', 'api', {
        password: 'test-password-updated',
      }),
    ).toBeUndefined();
    expect(httpService.get).toBeCalledTimes(1);
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should return a secret', async () => {
    const mockResult: AxiosResponse<any> = {
      data: {
        request_id: '77093ee2-3d75-b8d3-4e3e-8790980dfca2',
        lease_id: '',
        renewable: false,
        lease_duration: 2764800,
        data: {
          password: 'test-password',
          username: 'test-username',
        },
        wrap_info: null,
        warnings: null,
        auth: null,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    const expectedResult = mockResult.data.data;

    jest.spyOn(httpService, 'get').mockImplementation(() => of(mockResult));
    expect(
      await service.getSecret('organization1', 'user1', 'api', 'abcd'),
    ).toEqual(expectedResult);
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should delete a secret', async () => {
    const mockResult: AxiosResponse<void> = {
      data: null,
      status: 204,
      statusText: 'No Content',
      headers: {},
      config: {},
    };

    jest.spyOn(httpService, 'delete').mockImplementation(() => of(mockResult));
    expect(
      await service.deleteSecret('organization1', 'user1', 'api'),
    ).toBeUndefined();
    expect(httpService.delete).toBeCalledTimes(2);
  });

  it('should return false if a secret if it does not exist', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new NotFoundException();
    });

    expect(await service.secretExists('organization1', 'user1', 'api')).toBe(
      false,
    );
  });

  // Exception tests

  it('should fail to return the vault status if server is down', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new InternalServerErrorException(internalServerErrorObject);
    });

    try {
      expect(await service.getStatus()).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('Internal server error');
      expect(e.status).toBe(500);
    }
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should fail to unseal the vault if server is down', async () => {
    jest.spyOn(httpService, 'post').mockImplementation(() => {
      throw new InternalServerErrorException(internalServerErrorObject);
    });

    try {
      expect(await service.unsealVault('abcd')).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('Internal server error');
      expect(e.status).toBe(500);
    }
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should fail to create a write policy if vault is sealed', async () => {
    jest.spyOn(httpService, 'put').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(
        await service.createWritePolicy('organization1', 'user1', 'api'),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.put).toBeCalledTimes(1);
  });

  it('should fail to return a vault policy if vault is sealed', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(await service.getPolicy('test')).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should fail to return all vault policies if vault is sealed', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(await service.getPolicies()).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should fail to create token if vault is sealed', async () => {
    jest.spyOn(httpService, 'put').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });
    jest.spyOn(service, 'secretExists').mockResolvedValue(true);

    try {
      expect(
        await service.createToken('organization1', 'user1', 'api'),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.put).toBeCalledTimes(1);
  });

  it('should fail to renew a token if vault is sealed', async () => {
    jest.spyOn(httpService, 'post').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(await service.renewToken('abcd')).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should fail to revoke a token if vault is sealed', async () => {
    jest.spyOn(httpService, 'post').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(await service.revokeToken('abcd')).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should fail to create a secret if vault is sealed', async () => {
    jest.spyOn(httpService, 'post').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(
        await service.createSecret('organization1', 'user1', 'api', {}),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.post).toBeCalledTimes(1);
  });

  it('should fail to update a secret if vault is sealed', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(
        await service.updateSecret('organization1', 'user1', 'api', {}),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should fail to return a secret if vault is sealed', async () => {
    jest.spyOn(httpService, 'get').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(
        await service.getSecret('organization1', 'user1', 'api', 'abcd'),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.get).toBeCalledTimes(1);
  });

  it('should fail to delete a secret if vault is sealed', async () => {
    jest.spyOn(httpService, 'delete').mockImplementation(() => {
      throw new ServiceUnavailableException(vaultIsSealedErrorObject);
    });

    try {
      expect(
        await service.deleteSecret('organization1', 'user1', 'api'),
      ).toThrow();
      expect(true).toBe(false);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      expect(e.message).toBe('error performing token check: Vault is sealed');
      expect(e.status).toBe(503);
    }
    expect(httpService.delete).toBeCalledTimes(1);
  });
});
