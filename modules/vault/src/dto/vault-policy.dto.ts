import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
export class VaultPolicyDTO {
    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsOptional()
    readonly rules?: string;
}
