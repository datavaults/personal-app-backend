import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class VaultKeysDTO {
    @IsArray()
    @IsNotEmpty()
    readonly keys: string[];

    @IsArray()
    @IsNotEmpty()
    readonly keys_base64: string[];

    @IsString()
    @IsNotEmpty()
    readonly root_token: string;
}
