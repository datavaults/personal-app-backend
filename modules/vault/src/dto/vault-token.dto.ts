import { IsNotEmpty, IsString, IsUrl } from 'class-validator';

export class VaultTokenDTO {
    @IsString()
    @IsNotEmpty()
    readonly token: string;

    @IsString()
    @IsNotEmpty()
    readonly expireTime: string;

    @IsUrl()
    @IsNotEmpty()
    readonly url: string;
}
