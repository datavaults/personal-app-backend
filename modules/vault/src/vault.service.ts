import {
  HttpException,
  HttpService,
  Injectable,
  OnModuleInit,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { VaultPolicyDTO } from './dto/vault-policy.dto';
import { VaultTokenDTO } from './dto/vault-token.dto';

@Injectable()
export class VaultService implements OnModuleInit {
  constructor(
    private readonly config: ConfigService,
    private readonly httpService: HttpService,
  ) {}
  async getStatus(): Promise<{ initialized: boolean }> {
    try {
      const response = this.httpService.get(
        `${this.config.get('vault.vaultUrl')}/sys/init`,
        this.config.get('vault.rootHeader'),
      );
      return (await response.toPromise()).data;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  private async activateKV1(): Promise<void> {
    try {
      await this.httpService
        .post(
          `${this.config.get('vault.vaultUrl')}/sys/mounts/secret`,
          { type: 'kv-v1' },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
    } catch (error) {}
  }

  async getSealStatus(): Promise<any> {
    const response = await this.httpService
      .get(`${this.config.get('vault.vaultUrl')}/sys/seal-status`)
      .toPromise();
    return response.data;
  }

  async unsealVault(key: string): Promise<any> {
    try {
      const response = await this.httpService
        .post(
          `${this.config.get('vault.vaultUrl')}/sys/unseal`,
          { key },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
      await this.activateKV1();
      return response.data;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  private async createReadPolicy(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<VaultPolicyDTO> {
    try {
      const path = `${organization}/${user}/${secretName}`;
      const policyName = `read-${organization}-${user}-${secretName}`;
      const policy = `path \"secret/${path}\" {\n capabilities = [\"read\"]\n}\n`;
      await this.httpService
        .put(
          `${this.config.get('vault.vaultUrl')}/sys/policy/${policyName}`,
          { policy },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
      return {
        name: policyName,
      };
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async createWritePolicy(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<VaultPolicyDTO> {
    try {
      const path = `${organization}/${user}/${secretName}`;
      const policyName = `write-${organization}-${user}-${secretName}`;
      const policy = `path \"secret/${path}\" {\n capabilities = [\"create\", \"update\"]\n}\n`;
      await this.httpService
        .put(
          `${this.config.get('vault.vaultUrl')}/sys/policy/${policyName}`,
          { policy },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
      return {
        name: policyName,
      };
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async getPolicy(policy: string): Promise<VaultPolicyDTO> {
    try {
      const response = await this.httpService.get(
        `${this.config.get('vault.vaultUrl')}/sys/policy/${policy}`,
        this.config.get('vault.rootHeader'),
      );
      return (await response.toPromise()).data.data;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async getPolicies(): Promise<string[]> {
    try {
      const response = await this.httpService.get(
        `${this.config.get('vault.vaultUrl')}/sys/policy`,
        this.config.get('vault.rootHeader'),
      );
      return (await response.toPromise()).data.data.policies;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  private async deletePolicy(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<void> {
    try {
      const policy = `read-${organization}-${user}-${secretName}`;
      await this.httpService
        .delete(
          `${this.config.get('vault.vaultUrl')}/sys/policy/${policy}`,
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async createToken(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<VaultTokenDTO | null> {
    const url = `${this.config.get(
      'vault.vaultUrl',
    )}/secret/${organization}/${user}/${secretName}`;

    // Check if secret exists, if not return null
    const exists = await this.secretExists(organization, user, secretName);
    if (!exists) return Promise.resolve(null);

    const policy = (await this.createReadPolicy(organization, user, secretName))
      .name;
    const response = await this.httpService
      .post(
        `${this.config.get('vault.vaultUrl')}/auth/token/create`,
        {
          policies: [policy],
          ttl: this.config.get('vault.tokenTTL'),
          renewable: this.config.get('vault.tokenRenewable'),
        },
        this.config.get('vault.rootHeader'),
      )
      .toPromise();
    const token = response.data.auth.client_token;
    const expireTime = (
      await this.httpService
        .post(
          `${this.config.get('vault.vaultUrl')}/auth/token/lookup`,
          { token },
          this.config.get('vault.rootHeader'),
        )
        .toPromise()
    ).data.data.expire_time;
    return {
      token,
      expireTime,
      url,
    };
  }

  async renewToken(token: string): Promise<any> {
    try {
      const response = await this.httpService.post(
        `${this.config.get('vault.vaultUrl')}/auth/token/renew`,
        { token },
        this.config.get('vault.rootHeader'),
      );
      return (await response.toPromise()).data.auth;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async revokeToken(token: string): Promise<any> {
    try {
      await this.httpService
        .post(
          `${this.config.get('vault.vaultUrl')}/auth/token/revoke`,
          { token },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async createSecret(
    organization: string,
    user: string,
    secretName: string,
    data: any,
  ): Promise<{ path: string }> {
    try {
      await this.httpService
        .post(
          `${this.config.get(
            'vault.vaultUrl',
          )}/secret/${organization}/${user}/${secretName}`,
          { ...data },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
      return {
        path: `${this.config.get(
          'vault.vaultUrl',
        )}/secret/${organization}/${user}/${secretName}`,
      };
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async updateSecret(
    organization: string,
    user: string,
    secretName: string,
    data: any,
  ): Promise<void> {
    try {
      const secret = (
        await this.httpService
          .get(
            `${this.config.get(
              'vault.vaultUrl',
            )}/secret/${organization}/${user}/${secretName}`,
            this.config.get('vault.rootHeader'),
          )
          .toPromise()
      ).data.data;
      const keys = Object.keys(data);
      for (let i = 0; i < keys.length; i += 1) {
        secret[keys[i]] = data[keys[i]];
      }
      await this.httpService
        .post(
          `${this.config.get(
            'vault.vaultUrl',
          )}/secret/${organization}/${user}/${secretName}`,
          { ...secret },
          this.config.get('vault.rootHeader'),
        )
        .toPromise();
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async getSecret(
    organization: string,
    user: string,
    secretName: string,
    clientToken: string,
  ): Promise<any> {
    try {
      const response = await this.httpService.get(
        `${this.config.get(
          'vault.vaultUrl',
        )}/secret/${organization}/${user}/${secretName}`,
        {
          headers: { Authorization: `Bearer ${clientToken}` },
        },
      );
      return (await response.toPromise()).data.data;
    } catch (error) {
      const status = error.response.status;
      const message = error.response.data.errors[0];
      throw new HttpException(message, status);
    }
  }

  async deleteSecret(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<void> {
    await this.deletePolicy(organization, user, secretName);
    await this.httpService
      .delete(
        `${this.config.get(
          'vault.vaultUrl',
        )}/secret/${organization}/${user}/${secretName}`,
        this.config.get('vault.rootHeader'),
      )
      .toPromise();
  }

  async secretExists(
    organization: string,
    user: string,
    secretName: string,
  ): Promise<boolean> {
    try {
      const response = await this.httpService
        .get(
          `${this.config.get(
            'vault.vaultUrl',
          )}/secret/${organization}/${user}/${secretName}`,
          {
            headers: {
              Authorization: `Bearer ${this.config.get(
                'vault.vaultRootToken',
              )}`,
            },
          },
        )
        .toPromise();
      return response.status !== 404;
    } catch (error) {
      return false;
    }
  }

  async onModuleInit() {
    console.log(`Unsealing vault...`);
    const response = await this.unsealVault(
      this.config.get<string>('vault.vaultKey'),
    );
    if (response.sealed) throw Error('Vault could not be unsealed');
    else console.log('Vault unsealed');
  }
}
