import { registerAs } from '@nestjs/config';

export default registerAs('vault', () => ({
  vaultUrl: process.env.VAULT_URL,
  rootHeader: {
    headers: { Authorization: `Bearer ${process.env.VAULT_ROOT_TOKEN}` },
  },
  vaultRootToken: process.env.VAULT_ROOT_TOKEN,
  vaultKey: process.env.VAULT_KEY,
  tokenTTL: process.env.VAULT_TOKEN_TTL || '1h',
  tokenRenewable: process.env.VAULT_TOKEN_RENEWABLE || true,
}));
