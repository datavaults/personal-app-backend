import {
  Body,
  Controller,
  forwardRef,
  Get,
  Inject,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AnonymiserService } from '@suite5/anonymiser/services/anonymiser.service';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { AnonRulesObject } from '../dto/anonymiser.dto';

@Controller('anonymiser')
@ApiTags('anonymiser')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
@UseGuards(AuthenticationGuard)
export class AnonymiserController {
  constructor(
    @Inject(forwardRef(() => AnonymiserService))
    private readonly service: AnonymiserService,
  ) {}

  @Get('pseudoIds')
  @ApiCreatedResponse({ description: 'Get user pseudo ids' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @ApiOperation({ summary: 'pseudoIds' })
  async pseudoIds(@CurrentUser() user: UserData) {
    return await this.service.fetchAllUserPseudoIDs(user.id);
  }

  @Put('pseudoId')
  @ApiCreatedResponse({ description: 'Create user pseudo id' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @ApiOperation({ summary: 'pseudoIds' })
  async createPseudoId(@CurrentUser() user: UserData) {
    return await this.service.createPseudoID(user.id);
  }

  @Get('hierarchies')
  @ApiCreatedResponse({
    description: 'Fetch all available types with their hierarchies',
  })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  async fetchOrganisedHierarchies() {
    return await this.service.fetchOrganisedHierarchies();
  }

  @Post('preview')
  @ApiCreatedResponse({
    description:
      'Fetch anonymisation preview by passing sample data and configurations',
  })
  @ApiBody({ type: AnonRulesObject })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  async fetchAnonymisationPreview(
    @CurrentUser() user: UserData,
    @Body() rules: AnonRulesObject,
  ) {
    return await this.service.fetchAnonymisationPreview(rules);
  }
}
