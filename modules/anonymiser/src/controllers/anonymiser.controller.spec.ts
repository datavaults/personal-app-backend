import { Test, TestingModule } from '@nestjs/testing';
import { AnonymiserService } from '@suite5/anonymiser';
import { AnonRulesObject } from '../dto/anonymiser.dto';
import { AnonymiserController } from './anonymiser.controller';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { ConfigService } from '@nestjs/config';
import { KeycloakUser } from '@suite5/core/authentication/entities';

describe('AnonymiserController', () => {
  let controller: AnonymiserController;
  let service: AnonymiserService;
  const user: KeycloakUser = {} as KeycloakUser;

  beforeEach(async () => {
    Object.assign(user, {
      id: 1,
      username: 'test@test.org',
      sub: 'abcdefghijklmnop',
    });

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnonymiserController],
      providers: [
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
        {
          provide: AnonymiserService,
          useValue: {
            fetchAllUserPseudoIDs: () => jest.fn(),
            fetchAnonymisationPreview: () => jest.fn(),
          },
        },
      ],
    }).compile();
    controller = module.get<AnonymiserController>(AnonymiserController);
    service = module.get<AnonymiserService>(AnonymiserService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should fetch user pseudo ids', async () => {
    jest.spyOn(service, 'fetchAllUserPseudoIDs').mockImplementation();
    await controller.pseudoIds(user);
    expect(service.fetchAllUserPseudoIDs).toBeCalledWith(user.id);
  });

  it('should create user pseudo id', async () => {
    jest.spyOn(service, 'fetchAllUserPseudoIDs').mockImplementation();
    await controller.pseudoIds(user);
    expect(service.fetchAllUserPseudoIDs).toBeCalledWith(user.id);
  });

  it('should fetch AnonymisationPreview', async () => {
    jest.spyOn(service, 'fetchAnonymisationPreview').mockImplementation();

    const rules: AnonRulesObject = {
      dataset: [],
      structure: {},
      colNames: ['abc'],
      buildIDs: ['foo'],
      anonLevels: [0],
    };
    await controller.fetchAnonymisationPreview(user, rules);
    expect(service.fetchAnonymisationPreview).toBeCalledWith(rules);
  });
});
