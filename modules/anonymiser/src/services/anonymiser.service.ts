import { Inject, Injectable } from '@nestjs/common';
import { AnonRulesObject, organisedBuilderList } from '../dto';
import {
  AnonymiserStrategy,
  ANONYMISER_STRATEGY_TOKEN,
} from '../strategies/anonymiser.strategy';

export class AuthenticationError extends Error {}

@Injectable()
export class AnonymiserService {
  constructor(
    @Inject(ANONYMISER_STRATEGY_TOKEN)
    private readonly strategy: AnonymiserStrategy,
  ) {}

  async createPseudoID(userID: number): Promise<string> {
    return this.strategy.createPseudoID(userID);
  }

  async fetchAllUserPseudoIDs(userID: number): Promise<string[]> {
    return await this.strategy.fetchAllUserPseudoIDs(userID);
  }

  async fetchAnonymisationPreview(rules: AnonRulesObject): Promise<any> {
    return await this.strategy.fetchAnonymisationPreview(rules);
  }

  async fetchAnonymisedData(rules: AnonRulesObject): Promise<any> {
    return await this.strategy.fetchAnonymisedData(rules);
  }

  async fetchOrganisedHierarchies(): Promise<organisedBuilderList> {
    return await this.strategy.fetchOrganisedHierarchies();
  }
}
