import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakUserInfoResponse } from '@suite5/core/authentication/strategies/authentication.strategy';
import { User } from '@suite5/core/user/entities/user.entity';
import { UserService } from '@suite5/core/user/services';
import { AnonRulesObject } from '../dto/anonymiser.dto';
import {
  AnonymiserStrategy,
  ANONYMISER_STRATEGY_TOKEN,
} from '@suite5/anonymiser';
import { AnonymiserService } from '@suite5/anonymiser';

describe('AnonymiserService', () => {
  let service: AnonymiserService;
  let userService: UserService;
  let anonymiserStrategy: AnonymiserStrategy;
  let userInfo: KeycloakUserInfoResponse;
  const user: User = { id: 1 } as User;
  let rules: AnonRulesObject;

  beforeEach(async () => {
    rules = {
      dataset: [],
      structure: {},
      colNames: ['abc'],
      buildIDs: ['foo'],
      anonLevels: [0],
    };
    userInfo = {
      sub: 'hkfjsdhkfjhsdkfj-s928dajfksdj-hkdjfhskd',
      email_verified: true,
      name: 'test',
      preferred_username: 'test',
      given_name: '',
      family_name: '',
      email: 'test@test.com',
    } as KeycloakUserInfoResponse;
    Object.assign(user, userInfo);
    const mockedUserService = jest.fn(() => ({
      login: () => jest.fn(),
      retrieveBySub: () => jest.fn(),
      retrieveByEmail: () => jest.fn(),
    }));
    const mockedAnonymiserStrategy = {
      createPseudoID: () => jest.fn(),
      fetchAllUserPseudoIDs: () => jest.fn(),
      fetchAnonymisationPreview: () => jest.fn(),
      fetchAnonymisedData: () => jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ANONYMISER_STRATEGY_TOKEN,
          useValue: mockedAnonymiserStrategy,
        },
        {
          provide: UserService,
          useClass: mockedUserService,
        },
        AnonymiserService,
      ],
    }).compile();

    service = await module.get<AnonymiserService>(AnonymiserService);
    anonymiserStrategy = await module.get(ANONYMISER_STRATEGY_TOKEN);
    userService = await module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(anonymiserStrategy).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('createPseudoID | test 1', async () => {
    jest.spyOn(anonymiserStrategy, 'createPseudoID').mockImplementation();
    await service.createPseudoID(user.id);
    expect(anonymiserStrategy.createPseudoID).toBeCalledWith(user.id);
  });

  it('fetchAllUserPseudoIDs | test 1', async () => {
    jest
      .spyOn(anonymiserStrategy, 'fetchAllUserPseudoIDs')
      .mockImplementation();
    await service.fetchAllUserPseudoIDs(user.id);
    expect(anonymiserStrategy.fetchAllUserPseudoIDs).toBeCalledWith(user.id);
  });

  it('fetchAnonymisationPreview | test 1', async () => {
    jest
      .spyOn(anonymiserStrategy, 'fetchAnonymisationPreview')
      .mockImplementation();
    await service.fetchAnonymisationPreview(rules);
    expect(anonymiserStrategy.fetchAnonymisationPreview).toBeCalledWith(rules);
  });

  it('fetchAnonymisedData | test 1', async () => {
    jest.spyOn(anonymiserStrategy, 'fetchAnonymisedData').mockImplementation();
    await service.fetchAnonymisedData(rules);
    expect(anonymiserStrategy.fetchAnonymisedData).toBeCalledWith(rules);
  });
});
