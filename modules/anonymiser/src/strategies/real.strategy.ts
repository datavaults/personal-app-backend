import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from '@suite5/core/user';
import { User } from '@suite5/core/user/entities';
import { Repository } from 'typeorm';
import { AnonRulesObject, organisedBuilderList } from '../dto/anonymiser.dto';
import { PseudoID } from '../entities';
import { AnonymiserStrategy } from './anonymiser.strategy';

@Injectable()
export class RealAnonymiserStrategy implements AnonymiserStrategy {
  private readonly ANONYMISER_ENDPOINT_URL = this.configService.get(
    'anonymiser.module.endpointUrl',
  );

  private readonly PSEUDOID_ENDPOINT_URL = this.configService.get(
    'anonymiser.module.pseudoIdUrl',
  );

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    private readonly userService: UserService,
    @InjectRepository(PseudoID)
    private readonly pseudoIdRepository: Repository<PseudoID>,
  ) {}
  async fetchOrganisedHierarchies(): Promise<organisedBuilderList> {
    return await this.httpService
      .get(
        `${this.ANONYMISER_ENDPOINT_URL}/hierarchy/getOrganisedHierarchies`,
        {
          headers: {
            accept: 'application/json',
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  async createPseudoID(userID: number): Promise<string> {
    let pseudo = null;
    const user = await this.userService.retrieve(userID);
    let counter = 0;
    while (!pseudo && counter < 500) {
      // TODO - PUT REAL public key
      pseudo = await this.httpService
        .post(
          `${this.PSEUDOID_ENDPOINT_URL}/pseudoid`,
          { publicKey: user.email },
          {
            headers: {
              'Content-Type': 'application/json',
              accept: 'application/json',
            },
          },
        )
        .toPromise()
        .then(async (resp) => {
          return resp.data.pseudoId;
        });

      const pseudoIDsCount = await this.getPseudoIdsCount(pseudo);

      if (!!pseudo && pseudoIDsCount < 1) {
        await this.createPseudoId(user, pseudo);
        return pseudo;
      }
      counter += 1;
    }
    return '';
  }
  async fetchAllUserPseudoIDs(userID: number): Promise<string[]> {
    return (await this.pseudoIdRepository.find({ userId: userID })).map(
      (v) => v.pseudoId,
    );
  }

  async fetchAnonymisationPreview(rules: AnonRulesObject): Promise<any> {
    return await this.httpService
      .post(
        `${this.ANONYMISER_ENDPOINT_URL}/dataset/preview`,
        JSON.stringify(rules),
        {
          headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  async fetchAnonymisedData(rules: AnonRulesObject): Promise<any> {
    return await this.httpService
      .post(
        `${this.ANONYMISER_ENDPOINT_URL}/dataset/anonymize`,
        JSON.stringify(rules),
        {
          headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  private async getPseudoIdsCount(pseudo: string): Promise<number> {
    const [pseudoIDs, pseudoIDsCount] =
      await this.pseudoIdRepository.findAndCount({ pseudoId: pseudo });

    return pseudoIDsCount;
  }

  private async createPseudoId(user: User, pseudo: any): Promise<PseudoID> {
    const pseudoId = new PseudoID();
    pseudoId.user = user;
    pseudoId.pseudoId = pseudo;
    return this.pseudoIdRepository.save(pseudoId);
  }
}
