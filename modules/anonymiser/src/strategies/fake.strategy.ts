import { Injectable } from '@nestjs/common';
import { AnonRulesObject, organisedBuilderList } from '../dto/anonymiser.dto';
import { AnonymiserStrategy } from './anonymiser.strategy';
import * as R from 'ramda';

@Injectable()
export class FakeAnonymiserStrategy implements AnonymiserStrategy {
  async fetchOrganisedHierarchies(): Promise<organisedBuilderList> {
    return Promise.resolve({
      string: ['remove-characters-from-right', 'remove-characters-from-left'],
      number: ['replace-number-with-interval'],
    } as organisedBuilderList);
  }

  async createPseudoID(userID: number): Promise<string> {
    return await Promise.resolve('pseudoId1');
  }

  async fetchAllUserPseudoIDs(userID: number): Promise<string[]> {
    return Promise.resolve([
      'pseudoId1',
      'pseudoId2',
      'pseudoId3',
      'pseudoId4',
      'pseudoId5',
    ]);
  }

  async fetchAnonymisationPreview(rules: AnonRulesObject): Promise<any> {
    return Promise.resolve(this.anononymise(rules));
  }

  async fetchAnonymisedData(rules: AnonRulesObject): Promise<any> {
    return Promise.resolve(this.anononymise(rules));
  }

  private anononymise(rules: AnonRulesObject): any {
    const anonymisedData = R.clone(rules.dataset).map((dataset: any) => {
      const newDataset = {};
      for (let f = 0; f < Object.keys(dataset).length; f++) {
        const field = Object.keys(dataset)[f];
        if (rules.colNames.includes(field)) {
          let val =
            !R.isNil(dataset[field]) && !R.isEmpty(dataset[field])
              ? dataset[field]
              : null;
          if (val) {
            for (
              let r = 0;
              r < rules.anonLevels[rules.colNames.indexOf(field)];
              r++
            ) {
              if (r < val.length) {
                val = val.substring(0, r) + '*' + val.substring(r + 1);
              } else {
                val = val + '*';
              }
            }
            newDataset[field] = val;
          } else {
            newDataset[field] = null;
          }
        } else {
          newDataset[field] = dataset[field];
        }
      }
      return newDataset;
    });

    return anonymisedData;
  }
}
