import { AnonRulesObject, organisedBuilderList } from '../dto/anonymiser.dto';

export const ANONYMISER_STRATEGY_TOKEN = 'AnonymiserStrategy';

export interface AnonymiserStrategy {
  createPseudoID(userID: number): Promise<string>;
  fetchAllUserPseudoIDs(userID: number): Promise<string[]>;
  fetchAnonymisationPreview(rules: AnonRulesObject): Promise<any>;
  fetchAnonymisedData(rules: AnonRulesObject): Promise<any>;
  fetchOrganisedHierarchies(): Promise<organisedBuilderList>;
}
