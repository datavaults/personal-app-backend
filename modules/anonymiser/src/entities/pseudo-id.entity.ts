/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { User } from '@suite5/core/user/entities';
import { Column, Entity, ManyToOne } from 'typeorm';

@Entity()
@ObjectType()
export class PseudoID extends BaseEntity {
  @Column({ nullable: false })
  pseudoId: string;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  user: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  userId: number;
}
