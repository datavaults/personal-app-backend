import { ApiProperty } from '@nestjs/swagger/dist/decorators';
import { IsArray, IsObject } from 'class-validator';

// The dataset file to be passed to the anonymiser
export class loadDatasetObject {
  @ApiProperty()
  Dataset: string;
}

export class UploadSuccessResponse {
  @ApiProperty()
  status: string;
}

export class AnonRulesObject {
  @ApiProperty()
  @IsArray()
  readonly dataset: any[];
  @ApiProperty()
  @IsObject()
  readonly structure: any;
  @ApiProperty()
  @IsArray()
  readonly colNames: string[]; // The names of the columns to be anonymized
  @ApiProperty()
  @IsArray()
  readonly buildIDs: string[]; // The hierarchies to be applied to the selected columns
  @ApiProperty()
  @IsArray()
  readonly anonLevels: number[]; // The anonymization levels of the selected columns
}

export class accountObject {
  @ApiProperty()
  accountName: string; // example: suziesuez
  @ApiProperty()
  publicKey: string; // example: 02178c1bcdb25407394348f1ff5273adae287d8ea328184546837957e71c7de57a
}

export class pseudonymObject {
  @ApiProperty()
  accountName: string; // example: suziesuez
  @ApiProperty()
  pseudonym: string; // example: taylorsanchez
  @ApiProperty()
  publicKey: string; // example: 02178c1bcdb25407394348f1ff5273adae287d8ea328184546837957e71c7de57a
}

export class ClientErrorObject {
  @ApiProperty()
  error: string; // description: A message describing the error that occured
  // example: "Client request was invalid: {errorMessage}"
}

export class APIErrorObject {
  @ApiProperty()
  error: string; // description: A message describing the error that occured
  // example: "API Error: {errorMessage}"
}

export class pseudoList {
  @ApiProperty()
  items: Array<string>;
  // description: A pseudonym for the user account
  // example: "[\"Derrick\",\"Rahim\",\"Hasan\",\"Paul\"]"
}
export class hierObject {
  @ApiProperty()
  ID: string; // example: 4effcf1a-9c61-11eb-a8b3-0242ac130003
  @ApiProperty()
  name: string; // example: age_hier.txt
}

export class hierList {
  // description: An array containing a list of hierarchies available and their IDs
  @ApiProperty()
  items: Array<hierObject>;
}

export class parameters {
  @ApiProperty()
  FileID: string; // example: 2530f7c6-4240-4487-ab8e-1238d9197844
  @ApiProperty()
  publicKey: string; // description: Public key of the user
  // example: 02178c1bcdb25407394348f1ff5273adae287d8ea328184546837957e71c7de57a
  @ApiProperty()
  HierID: string; // description: Hierarchy name property
}

export class organisedBuilderList {
  // An object containing all available hierarchies organised by data type.
  @ApiProperty()
  string: Array<string>; // [ "redact-fromleft", "redact-fromright" ]
  @ApiProperty()
  number: Array<string>; // [ "age" ]
}
