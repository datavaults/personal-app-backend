import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import anonymiserConfig from './anonymiser.module.config';
import { AnonymiserController } from './controllers';
import { AnonymiserService } from '@suite5/anonymiser/services';
import {
  ANONYMISER_STRATEGY_TOKEN,
  FakeAnonymiserStrategy,
  RealAnonymiserStrategy,
} from './strategies';
import { CoreModule } from '@suite5/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PseudoID } from '@suite5/anonymiser/entities';

@Module({
  imports: [
    CoreModule,
    TypeOrmModule.forFeature([PseudoID]),
    HttpModule,
    ConfigModule.forFeature(anonymiserConfig),
  ],
  providers: [
    {
      provide: ANONYMISER_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeAnonymiserStrategy
          : RealAnonymiserStrategy,
    },
    AnonymiserService,
  ],
  controllers: [AnonymiserController],
  exports: [AnonymiserService],
})
export class AnonymiserModule {}
