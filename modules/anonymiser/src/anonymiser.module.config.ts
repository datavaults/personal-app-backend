import { registerAs } from '@nestjs/config';

export default registerAs('anonymiser.module', () => ({
  endpointUrl: process.env.ANONYMIZER_ENDPOINT_URL || '/api/v1/',
  pseudoIdUrl: process.env.ANONYMIZER_PSEUDOID_URL,
}));
