import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { VaultService } from '@suite5/vault';
import { EncryptionStrategy } from './encryption.strategy';

@Injectable()
export class RealEncryptionStrategy implements EncryptionStrategy {
  private readonly ENCRYPTION_ASSET_URL = `${this.configService.get(
    'encryption.module.osfpUrl',
  )}`;

  private readonly ENCRYPTION_TOKEN = `${this.configService.get(
    'encryption.module.encryptionToken',
  )}`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(Logger)
    private readonly logger: Logger,
    private readonly vaultService: VaultService,
  ) {}

  private async createSecretUponRegistration(userId: any, key: any) {
    try {
      const credentials: any = {
        authMechanism: 'SCRAM-SHA-1',
        name: `Encryption - ${userId}`,
        key: { key },
      };
      await this.vaultService.createSecret(
        'user',
        String(userId),
        'encryption',
        credentials.key,
      );
      return credentials.key;
    } catch (e) {
      this.logger.error(`Error during creation of secret for Encryption: ${e}`);
    }
  }

  private async retrieveSecret(userSub: any) {
    try {
      return await this.vaultService.getSecret(
        'user',
        `${userSub}`,
        'encryption',
        this.configService.get<string>('vault.vaultRootToken'),
      );
    } catch (e) {
      return null;
    }
  }

  async encryptDataset(
    userId: any,
    policy: any,
    data: any,
    assetId: any,
  ): Promise<any> {
    let userKey: any;
    const config = {
      headers: {
        Accept: 'application/json',
        authorization: `${this.ENCRYPTION_TOKEN}`,
      },
    };
    try {
      await this.httpService
        .get(`${this.ENCRYPTION_ASSET_URL}/abe/api/ta/registerUser`, config)
        .toPromise()
        .then(async (resp) => {
          const key = await this.retrieveSecret(userId);
          if (key === null) {
            await this.createSecretUponRegistration(userId, resp.data);
          }
          userKey = key.key;
        })
        .catch((e) => {
          throw e;
        });
    } catch (e) {
      console.log(e);
    }
    try {
      const dataset = {
        userKey: userKey,
        policy: policy,
        data: JSON.stringify(data),
        assetId: assetId,
      };
      return await this.httpService
        .post(
          `${this.ENCRYPTION_ASSET_URL}/abe/api/ta/encrypt`,
          dataset,
          config,
        )
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        })
        .catch((e) => {
          console.log(e);
        });
    } catch (e) {
      console.log('-----------Encryption Strategy-------------');
      console.log(e);
    }
  }
}
