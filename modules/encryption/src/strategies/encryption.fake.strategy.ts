import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { EncryptionStrategy } from './encryption.strategy';

@Injectable()
export class FakeEncryptionStrategy implements EncryptionStrategy {
  private readonly ENCRYPTION_ASSET_URL = `${this.configService.get(
    'asset.module.osfpUrl',
  )}`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(Logger)
    private readonly logger: Logger,
  ) {}

  async encryptDataset(
    userKey: any,
    policy: any,
    data: any,
    assetId: any,
  ): Promise<any> {
    return true;
  }
}
