export const ENCRYPTION_STRATEGY_TOKEN = 'EncryptionStrategy';

export interface EncryptionStrategy {
  encryptDataset(
    userKey: any,
    policy: any,
    data: any,
    assetId: any,
  ): Promise<any>;
}
