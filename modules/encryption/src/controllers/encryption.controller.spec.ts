import { EncryptionController } from './encryption.controller';

describe('EncryptionController', () => {
  let mockAssetEncryptionController: EncryptionController;
  let mockEncryptionService: any;

  const attributes = {
    configuration: {
      accessPolicies: {
        policyId: 'new',
        existingPolicyId: null,
        sector: ['Energy'],
        type: [],
        size: [],
        continent: [],
        country: [],
        reputation: null,
      },
    },
    data: [],
  };

  const uid = '6df06979-41ea-4cdf-887c-aa0ea0480590';

  beforeEach(async () => {
    mockEncryptionService = {
      encryptData: () => jest.fn(),
    };
    mockAssetEncryptionController = new EncryptionController(
      mockEncryptionService,
    );
  });

  it('should be defined', () => {
    expect(mockAssetEncryptionController).toBeDefined();
  });

  // it('should encrypt data', async () => {
  //   jest.spyOn(mockEncryptionService, 'encryptData').mockImplementation();
  //   expect(
  //     await mockAssetEncryptionController.encryptData(uid as any, attributes),
  //   ).toReturn();
  // });
});
