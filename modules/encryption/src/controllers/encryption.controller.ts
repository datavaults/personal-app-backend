import { Body, Controller, Get, UseGuards } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CurrentUser, UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { EncryptionService } from '../services';

@Controller('asset-encryption')
@UseGuards(AuthenticationGuard)
@ApiTags('asset-encryption')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
export class EncryptionController {
  constructor(private readonly encryptionService: EncryptionService) {}

  @Get('encrypt')
  @ApiOperation({ summary: 'Encrypt asset data' })
  @ApiNotFoundResponse()
  async encryptData(@CurrentUser() user: UserData, @Body('data') data: any) {
    return await this.encryptionService.encryptData(data, user.sub);
  }
}
