import { HttpModule, Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CoreModule } from '@suite5/core';
import { AuthenticationModule } from '@suite5/core/authentication/authentication.module';
import { EncryptionController } from './controllers';
import {
  ENCRYPTION_STRATEGY_TOKEN,
  FakeEncryptionStrategy,
  RealEncryptionStrategy,
} from './strategies';
import encryptionConfig from './encryption.module.config';
import { EncryptionService } from './services';

@Module({
  imports: [
    ConfigModule.forFeature(encryptionConfig),
    CoreModule,
    HttpModule,
    AuthenticationModule,
  ],
  providers: [
    Logger,
    EncryptionController,
    {
      provide: ENCRYPTION_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeEncryptionStrategy
          : RealEncryptionStrategy,
    },
    EncryptionService,
  ],
  exports: [],
  controllers: [EncryptionController],
})
export class EncryptionModule {}
