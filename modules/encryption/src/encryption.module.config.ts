import { registerAs } from '@nestjs/config';

export default registerAs('encryption.module', () => ({
  encryptionToken: process.env.ENCRYPTION_HASH,
  osfpUrl: process.env.OSFP_SERVER || '',
}));
