import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { VaultService } from '@suite5/vault/vault.service';
import * as R from 'ramda';
import {
  ENCRYPTION_STRATEGY_TOKEN,
  RealEncryptionStrategy,
} from '../strategies';

@Injectable()
export class EncryptionService {
  constructor(
    private readonly configService: ConfigService,
    private readonly vaultService: VaultService,
    @Inject(ENCRYPTION_STRATEGY_TOKEN)
    private readonly encryptionStrategy: RealEncryptionStrategy,
    @Inject(Logger)
    private readonly logger: Logger,
  ) {}

  async encryptData(data: any, userId: any) {
    const policies = {
      sector: data.policy.sector,
      type: data.policy.type,
      size: data.policy.size,
      continent: data.policy.continent,
      country: data.policy.country,
      // reputation: [data.policy.reputation],
    };

    try {
      return await this.encryptionStrategy.encryptDataset(
        userId,
        policies,
        data.asset,
        data.assetId,
      );
    } catch (e) {
      this.logger.error(`Error during encryption: ${e}`);
    }
  }
}
