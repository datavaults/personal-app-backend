import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateRecentUpdateDTO {
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly lastDate: Date;

  @IsNotEmpty()
  @ApiProperty()
  readonly successful: boolean;

  @IsNotEmpty()
  @ApiProperty()
  readonly assetId: number;
}
