import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString, IsObject } from 'class-validator';

export class UserSourcesDTO {
  @IsString()
  @ApiProperty()
  readonly id: string;

  @IsString()
  @ApiProperty()
  readonly name: string;

  @IsNumber()
  @ApiProperty()
  readonly userId: number;

  @IsString()
  @ApiProperty()
  readonly sourceType: string;

  @IsString()
  @ApiProperty()
  readonly status: string;

  @IsObject()
  @ApiProperty()
  readonly config: Record<string, any>;

  @IsObject()
  @ApiProperty()
  readonly schedule: Record<string, any>;
}
