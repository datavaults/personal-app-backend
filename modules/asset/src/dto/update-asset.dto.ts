import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateAssetDTO {
  @IsOptional()
  @ApiProperty()
  readonly name?: string;

  @IsOptional()
  @ApiProperty()
  readonly description?: string;

  @IsOptional()
  @ApiProperty()
  readonly keywords?: string[];

  @IsOptional()
  @ApiProperty()
  readonly url?: string;

  @IsOptional()
  @ApiProperty()
  credentials?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  schedule?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  readonly errors?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  status?: boolean;
}
