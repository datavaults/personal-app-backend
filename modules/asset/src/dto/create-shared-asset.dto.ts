import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNumber,
  IsObject,
  IsString,
  IsBoolean,
} from 'class-validator';
import { ConfigurationDTO } from './configuration.dto';

export class CreateSharedAssetDTO {
  @IsNumber()
  @ApiProperty()
  readonly assetId: number;

  @IsString()
  @ApiProperty()
  readonly assetUUID: string;

  @IsString()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @ApiProperty()
  readonly description?: string;

  @IsArray()
  @ApiProperty()
  readonly keywords: string[];

  @IsObject()
  @ApiProperty()
  readonly configuration: ConfigurationDTO;

  @ApiProperty()
  userId?: number;
}
