import { ApiProperty } from '@nestjs/swagger';
import { IsObject, IsString } from 'class-validator';
import { ConfigurationDTO } from './configuration.dto';

export class CreateSharingConfigurationDTO {
  @IsString()
  @ApiProperty()
  readonly name: string;

  @IsObject()
  @ApiProperty()
  readonly configuration: ConfigurationDTO;

  @ApiProperty()
  userId?: number;
}
