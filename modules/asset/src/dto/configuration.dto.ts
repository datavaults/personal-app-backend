import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsObject, IsOptional, IsString } from 'class-validator';
import { ConfigurationAnonymasationDTO } from './configuration-anonymisation.dto';

export class ConfigurationDTO {
  @IsObject()
  @IsOptional()
  @ApiProperty()
  readonly anonymisation: ConfigurationAnonymasationDTO;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly specificPrice: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly price: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly licenseType: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly standardLicense: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly ownLicense: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly encryption: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly persona: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly useTPM: boolean;

  @IsObject()
  @IsOptional()
  @ApiProperty()
  readonly accessPolicies: any;
}
