import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { SourceType } from '../constants';

export class CreateAssetDTO {
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @IsOptional()
  @ApiProperty()
  readonly description?: string;

  @IsOptional()
  @ApiProperty()
  readonly keywords?: string[];

  @IsOptional()
  @ApiProperty()
  readonly url?: string;

  @IsNotEmpty()
  @ApiProperty()
  readonly sourceType: SourceType;

  @IsOptional()
  @ApiProperty()
  createdById?: number;

  @IsOptional()
  @ApiProperty()
  credentials?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  schedule?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  readonly errors?: Record<string, any>;

  @IsOptional()
  @ApiProperty()
  uid?: string;

  @IsOptional()
  @ApiProperty()
  type?: string;

  @IsOptional()
  @ApiProperty()
  status?: boolean;
}
