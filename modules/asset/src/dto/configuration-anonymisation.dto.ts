import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsObject, IsOptional, IsString } from 'class-validator';

export class ConfigurationAnonymasationDTO {
  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly anonymise: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  pseudoId: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly useTPM: boolean;

  @IsObject()
  @IsOptional()
  @ApiProperty()
  readonly rules: {
    colNames: string[];
    buildIDs: string[];
    anonLevels: number[];
  };
}
