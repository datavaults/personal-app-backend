import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateTransactionDTO {
  @IsString()
  @ApiProperty()
  readonly buyerName?: string;

  @IsString()
  @ApiProperty()
  readonly buyerUUID?: string;

  @IsString()
  @ApiProperty()
  readonly buyerDescription?: string;

  @IsString()
  @ApiProperty()
  readonly buyerWebsite?: string;

  @IsString()
  @ApiProperty()
  readonly buyerType?: string;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  readonly sharedAssetId?: number;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  readonly sharedQuestionnaireId?: number;

  @IsNumber()
  @ApiProperty()
  readonly points?: number;

  @IsDateString()
  @ApiProperty()
  readonly timestamp: Date;

  @IsString()
  @ApiProperty()
  readonly hash: string;

  @ApiProperty()
  userId: number;
}
