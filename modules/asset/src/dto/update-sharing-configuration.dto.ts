import { ApiProperty } from '@nestjs/swagger';
import { IsObject, IsOptional, IsString } from 'class-validator';
import { ConfigurationDTO } from './configuration.dto';

export class UpdateSharingConfigurationDTO {
  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly name: string;

  @IsObject()
  @IsOptional()
  @ApiProperty()
  readonly configuration: ConfigurationDTO;
}
