import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber, IsString } from 'class-validator';

export class SourceOptionDTO {
  @IsString()
  @ApiProperty()
  readonly id: string;

  @IsString()
  @ApiProperty()
  readonly sourcetype: string;

  @IsString()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @ApiProperty()
  readonly url: string;

  @IsString()
  @ApiProperty()
  readonly auth_type: string;

  @IsArray()
  @ApiProperty()
  readonly auth_properties: string[];

  @IsArray()
  @ApiProperty()
  readonly config_properties: string[];
}
