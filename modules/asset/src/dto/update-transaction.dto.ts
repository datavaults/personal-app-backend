import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateTransactionDTO {
  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly buyerName?: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly buyerUUID?: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly buyerDescription?: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly buyerWebsite?: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly buyerType?: string;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  readonly sharedAssetId?: number;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  readonly sharedQuestionnaireId?: number;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  readonly points?: number;

  @IsOptional()
  @IsDateString()
  @ApiProperty()
  readonly timestamp?: Date;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly hash?: string;

  @IsOptional()
  @ApiProperty()
  userId?: number;
}
