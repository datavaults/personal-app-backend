import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateRecentUpdateDTO {
  @IsOptional()
  @ApiProperty()
  readonly name?: string;

  @IsOptional()
  @ApiProperty()
  readonly lastDate?: Date;

  @IsOptional()
  @ApiProperty()
  readonly successful?: boolean;
}
