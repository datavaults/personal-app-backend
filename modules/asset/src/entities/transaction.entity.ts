/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from '@suite5/core/user/entities';
import { SharedAsset } from '../entities';
import { Message } from '@suite5/message/entities';

@Entity()
@ObjectType()
export class Transaction extends BaseEntity {
  @Column({ nullable: true })
  @Field()
  buyerName: string;

  @Column({ nullable: true })
  @Field()
  buyerUUID: string;

  @Column({ nullable: true })
  @Field()
  buyerDescription: string;

  @Column({ nullable: true })
  @Field()
  buyerWebsite: string;

  @Column({ nullable: true })
  @Field()
  buyerType: string;

  @ManyToOne(
    /* istanbul ignore next */
    () => SharedAsset,
    { nullable: false, eager: true, onDelete: 'SET NULL' },
  )
  @Field(
    /* istanbul ignore next */
    () => SharedAsset,
  )
  sharedAsset: SharedAsset;

  @ManyToOne(
    /* istanbul ignore next */
    'Message',
    { nullable: false, eager: true, onDelete: 'SET NULL' },
  )
  sharedQuestionnaire: Message;

  @Column({ nullable: true })
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  sharedAssetId: number;

  @Column({ nullable: true })
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  sharedQuestionnaireId: number;

  @Column({ nullable: true })
  @Field()
  points: number;

  @Column()
  @Field()
  timestamp: Date;

  @Column()
  @Field()
  hash: string;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'SET NULL' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  user: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  userId: number;
}
