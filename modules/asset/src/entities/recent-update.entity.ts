/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { Asset } from './asset.entity';

@Entity()
@ObjectType()
export class RecentUpdate extends BaseEntity {
  @Column()
  @Field()
  name: string;

  @Column()
  @Field()
  lastDate: Date;

  @Column()
  @Field()
  successful: boolean;

  @ManyToOne(
    /* istanbul ignore next */
    () => Asset,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => Asset,
  )
  asset: Asset;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  assetId: number;
}
