/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { GraphQLJSONObject } from 'graphql-type-json';
import { BaseEntity } from '@suite5/common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from '@suite5/core/user/entities';

@Entity()
@ObjectType()
export class SharingConfiguration extends BaseEntity {
  @Column()
  @Field()
  name: string;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  configuration: any;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  user: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  userId: number;
}
