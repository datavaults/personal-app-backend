/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { User } from '@suite5/core/user/entities';
import { GraphQLJSONObject } from 'graphql-type-json';
import { Column, Entity, ManyToOne } from 'typeorm';
import { SourceType } from '../constants';

@Entity()
@ObjectType()
export class Asset extends BaseEntity {
  @Column()
  @Field()
  name: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  description?: string;

  @Column({ type: 'simple-array', nullable: true })
  @Field(() => [String], { nullable: true })
  keywords?: string[];

  @Column({ nullable: true })
  @Field({ nullable: true })
  url?: string;

  @Column()
  @Field()
  sourceType: SourceType;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'CASCADE' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  createdBy: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  createdById: number;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  credentials?: any;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  schedule?: any;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  errors?: any;

  @Column()
  @Field()
  uid: string;

  @Column({ default: 'dataset' })
  @Field()
  type: string;

  @Column({ nullable: true })
  @Field()
  firstCollection: Date;

  @Column({ nullable: true })
  @Field()
  status?: boolean;
}
