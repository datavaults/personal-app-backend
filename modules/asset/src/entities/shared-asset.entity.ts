/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@suite5/common';
import { Asset } from '@suite5/asset/entities/asset.entity';
import { User } from '@suite5/core/user/entities/user.entity';
import { GraphQLJSONObject } from 'graphql-type-json';
import { Column, Entity, ManyToOne } from 'typeorm';

@Entity()
@ObjectType()
export class SharedAsset extends BaseEntity {
  @ManyToOne(
    /* istanbul ignore next */
    () => Asset,
    { nullable: false, lazy: true },
  )
  @Field(
    /* istanbul ignore next */
    () => Asset,
  )
  asset: Asset;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  assetId: number;

  @Column()
  @Field()
  name: string;

  @Column({ nullable: true })
  @Field()
  description: string;

  @Column({ type: 'simple-array', nullable: true })
  @Field(() => [String], { nullable: true })
  keywords: string[];

  @Column({ nullable: true, default: false })
  @Field()
  anonymized: boolean;

  @Column({ nullable: true, default: false })
  @Field()
  encrypted: boolean;

  @Column({ type: 'json', nullable: true })
  @Field(
    /* istanbul ignore next */
    () => GraphQLJSONObject,
    { nullable: true },
  )
  configuration: any;

  @ManyToOne(
    /* istanbul ignore next */
    () => User,
    { nullable: false, lazy: true, onDelete: 'SET NULL' },
  )
  @Field(
    /* istanbul ignore next */
    () => User,
  )
  user: User;

  @Column()
  @Field(
    /* istanbul ignore next */
    () => Int,
  )
  userId: number;

  @Column()
  @Field()
  uid: string;

  @Column({ nullable: true })
  @Field()
  points: number;

  @Column({ default: new Date() })
  @Field()
  sharedDate: Date;

  @Column({ default: new Date() })
  @Field()
  lastUpdatedAt: Date;
}
