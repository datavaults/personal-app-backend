import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { forwardRef, HttpModule, Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnonymiserModule } from '@suite5/anonymiser';
import {
  CLOUD_STRATEGY_TOKEN,
  DATA_FETCHER_STRATEGY_TOKEN,
  FakeCloudStrategy,
  FakeDataFetcherStrategy,
  RealCloudStrategy,
  RealDataFetcherStrategy,
  MIWENERGIA_STRATEGY_TOKEN,
  RealMiwenergiaStrategy,
  FakeMiwenergiaStrategy,
  OLYMPIAKOS_STRATEGY_TOKEN,
  FakeOlympiakosStrategy,
  RealOlympiakosStrategy,
} from '@suite5/asset/strategies';
import { MailService } from '@suite5/common/services/mail.service';
import { CoreModule } from '@suite5/core';
import { AuthenticationModule } from '@suite5/core/authentication/authentication.module';
import { AuthenticationMailService } from '@suite5/core/authentication/services/authentication-mail.service';
import {
  AUTHENTICATION_STRATEGY_TOKEN,
  KeycloakAuthenticationStrategy,
} from '@suite5/core/authentication/strategies';
import assetConfig from './asset.module.config';
import {
  DataFetcherController,
  RecentUpdateController,
  SharedAssetController,
  SharingConfigurationController,
  TransactionController,
  PratoController,
} from './controllers';
import { MIWenergiaController } from './controllers/miwenergia.controller';
import { AssetStatsController } from './controllers/asset-stats.controller';
import { UserAssetController } from './controllers/user-asset.controller';
import {
  Asset,
  RecentUpdate,
  SharedAsset,
  SharingConfiguration,
  Transaction,
} from './entities';
import {
  AssetStatsService,
  CloudAuthenticationService,
  DataFetcherMessagingService,
  DataFetcherService,
  DownloadService,
  RecentUpdateService,
  SharedAssetService,
  SharingConfigurationService,
  TransactionService,
} from './services';
import { UserAssetService } from './services/user-asset.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { CloudUpdateAssetService } from './services/cloud-update-asset.service';
import { MongoService } from '@suite5/common';
import { DownloadController } from './controllers/download.controller';
import { OlympiakosController } from './controllers/olympiakos.controller';
import { PRATO_STRATEGY_TOKEN } from './strategies/prato.strategy';
import { FakePratoStrategy } from './strategies/prato.fake.strategy';
import { RealPratoStrategy } from './strategies/prato.real.strategy';
import { UserService } from '@suite5/core/user';
import { User } from '@suite5/core/user/entities';
import {
  EncryptionModule,
  ENCRYPTION_STRATEGY_TOKEN,
  FakeEncryptionStrategy,
  RealEncryptionStrategy,
} from '../../encryption/src/';
import { EncryptionService } from '../../encryption/src/services';
import { TransactionMessagingService } from './services/transactions-messaging.service';
import { CloudUpdateUserDataService } from './services/cloud-update-user-data.service';
import {
  CLOUD_USER_TOKEN,
  FakeCloudUserStrategy,
  RealCloudUserStrategy,
} from '@suite5/core/user/strategies';
import {
  FakeWalletStrategy,
  RealWalletStrategy,
  WalletModule,
  WALLET_STRATEGY_TOKEN,
} from '@suite5/wallet';

@Module({
  imports: [
    ConfigModule.forFeature(assetConfig),
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('asset.module.rabbitmqUri'),
        connectionInitOptions: { wait: false },
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([
      Asset,
      RecentUpdate,
      SharingConfiguration,
      SharedAsset,
      Transaction,
      User,
    ]),
    forwardRef(() => CoreModule),
    AnonymiserModule,
    HttpModule,
    AuthenticationModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secret: config.get<string>('keycloak.module.secret'),
        signOptions: {
          expiresIn: config.get<string>('keycloak.module.expiration_time'),
        },
      }),
    }),
    EventEmitterModule.forRoot(),
    EncryptionModule,
    WalletModule,
  ],
  providers: [
    Logger,
    SharingConfigurationService,
    SharedAssetService,
    TransactionService,
    DataFetcherService,
    RecentUpdateService,
    DataFetcherMessagingService,
    TransactionMessagingService,
    CloudAuthenticationService,
    CloudUpdateAssetService,
    CloudUpdateUserDataService,
    DownloadService,
    MongoService,
    {
      provide: CLOUD_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeCloudStrategy
          : RealCloudStrategy,
    },
    {
      provide: DATA_FETCHER_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeDataFetcherStrategy
          : RealDataFetcherStrategy,
    },
    {
      provide: AUTHENTICATION_STRATEGY_TOKEN,
      useClass: KeycloakAuthenticationStrategy,
    },
    {
      provide: MIWENERGIA_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeMiwenergiaStrategy
          : RealMiwenergiaStrategy,
    },
    {
      provide: OLYMPIAKOS_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeOlympiakosStrategy
          : RealOlympiakosStrategy,
    },
    {
      provide: PRATO_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakePratoStrategy
          : RealPratoStrategy,
    },
    {
      provide: ENCRYPTION_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeEncryptionStrategy
          : RealEncryptionStrategy,
    },
    {
      provide: CLOUD_USER_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev' ||
        process.env.STARTER_KIT_DEMO === 'true'
          ? FakeCloudUserStrategy
          : RealCloudUserStrategy,
    },
    {
      provide: WALLET_STRATEGY_TOKEN,
      useClass:
        process.env.NODE_ENV === 'dev'
          ? FakeWalletStrategy
          : RealWalletStrategy,
    },
    UserAssetService,
    AuthenticationMailService,
    MailService,
    AssetStatsService,
    UserService,
    EncryptionService,
  ],
  exports: [
    SharingConfigurationService,
    SharedAssetService,
    TransactionService,
    DataFetcherService,
    RecentUpdateService,
    UserAssetService,
    CloudAuthenticationService,
    CloudUpdateUserDataService,
  ],
  controllers: [
    SharingConfigurationController,
    SharedAssetController,
    TransactionController,
    DataFetcherController,
    RecentUpdateController,
    UserAssetController,
    MIWenergiaController,
    AssetStatsController,
    PratoController,
    DownloadController,
    OlympiakosController,
  ],
})
export class AssetModule {}
