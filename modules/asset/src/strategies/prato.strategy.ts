export const PRATO_STRATEGY_TOKEN = 'PratoStrategy';

export interface PratoStrategy {
  searchPersonByFiscalCode(nationalInsuranceNumber: any): Promise<any>;
}
