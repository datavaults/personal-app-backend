import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { OlympiakosStrategy } from './olympiakos.strategy';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

@Injectable()
export class RealOlympiakosStrategy implements OlympiakosStrategy {
  private readonly OLYMPIAKOS_URL = `${this.configService.get(
    'asset.module.osfpUrl',
  )}/olympiakos/api`;
  private readonly OLYMPIAKOS_USER_URL = '/user';

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async ckeckUser(memberId: string, birthDate: Date, token: any): Promise<any> {
    dayjs.extend(utc);
    const date = dayjs(birthDate)
      .utc()
      .set('hour', 0)
      .set('minute', 0)
      .set('second', 0)
      .toISOString()
      .replace(/:/g, '%3A');
    return await this.httpService
      .get(
        this.OLYMPIAKOS_URL +
          this.OLYMPIAKOS_USER_URL +
          `?birthDate=${date}&uuid=${memberId}`,
        {
          headers: {
            accept: 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log(e));
  }
}
