import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { SourceOptionDTO } from '@suite5/asset/dto';
import { DataFetcherStrategy } from '@suite5/asset/strategies/data-fetcher.strategy';
import { Method } from 'axios';
import FormData from 'form-data';
import * as R from 'ramda';
import { UserSourcesDTO } from '../dto/user-sources.dto';

@Injectable()
export class RealDataFetcherStrategy implements DataFetcherStrategy {
  private readonly DATA_FETCHER_URL = this.configService.get(
    'asset.module.dataFetcherUrl',
  );

  private readonly DATA_FETCHER_TOKEN = this.configService.get(
    'asset.module.dataFetcherToken',
  );

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  private urlChoose(userId: string, configuration: any) {
    let dataFetcherUrl: string;
    let contentType: string;
    if (R.has('file', configuration)) {
      dataFetcherUrl = `${this.DATA_FETCHER_URL}/data`;
      contentType = 'multipart/form-data';
      configuration['userId'] = userId;
    } else {
      dataFetcherUrl = `${this.DATA_FETCHER_URL}/user/${userId}/sources`;
      contentType = 'application/json';
    }
    return { dataFetcherUrl, contentType, configuration };
  }

  async sendSource(
    userId: string,
    configuration: any,
    method: string,
  ): Promise<any> {
    let dataFetcherUrl: string;
    let contentType: string;
    let data: any;
    let headers = {};
    if (R.has('file', configuration)) {
      data = new FormData();
      dataFetcherUrl = `${this.DATA_FETCHER_URL}/data`;
      contentType = 'multipart/form-data';
      data.append('file', configuration.file.buffer, {
        filename: configuration.file.originalname,
      });
      data.append('sourceId', configuration.id);
      data.append('userID', userId);
      headers = {
        ...data.getHeaders(),
        'Content-Length': data.getLengthSync(),
      };
    } else if (method === 'PUT') {
      dataFetcherUrl = `${this.DATA_FETCHER_URL}/user/${userId}/sources/${configuration.id}`;
      contentType = 'application/json';
      headers = { 'Content-Type': contentType, accept: 'application/json' };
      data = configuration;
    } else {
      dataFetcherUrl = `${this.DATA_FETCHER_URL}/user/${userId}/sources`;
      contentType = 'application/json';
      headers = { 'Content-Type': contentType, accept: 'application/json' };
      data = configuration;
    }

    return await this.httpService
      .request({
        method: method as Method,
        url: dataFetcherUrl,
        headers: {
          ...headers,
          authorization: `Bearer ${this.DATA_FETCHER_TOKEN.trim()}`,
        },
        data,
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => {
        console.error(e);
        throw e;
      });
  }

  async createSource(userId: string, configuration: any): Promise<any> {
    return await this.sendSource(userId, configuration, 'POST');
  }

  async getSources(): Promise<SourceOptionDTO[]> {
    return await this.httpService
      .get(`${this.DATA_FETCHER_URL}/sources`, {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${this.DATA_FETCHER_TOKEN}`,
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => {
        throw e;
      });
  }

  async getUserSources(userId: string): Promise<UserSourcesDTO[]> {
    return await this.httpService
      .get(`${this.DATA_FETCHER_URL}/user/${userId}/sources`, {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${this.DATA_FETCHER_TOKEN}`,
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => {
        throw e;
      });
  }

  async deleteSource(userId: string, assetUid: string): Promise<any> {
    return await this.httpService
      .delete(`${this.DATA_FETCHER_URL}/user/${userId}/sources/${assetUid}`, {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${this.DATA_FETCHER_TOKEN}`,
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => {
        console.log(e);
      });
  }

  async updateSource(
    userId: string,
    sourceId: string,
    newConfiguration: {
      id: string;
      name: string;
      sourceType: string;
      status: string;
      config: any;
      schedule?: any;
      file?: any;
    },
  ): Promise<any> {
    const configuration: {
      id: string;
      name: string;
      sourceType: string;
      status: string;
      config: any;
      schedule?: any;
      file?: any;
    } = {
      id: newConfiguration.id,
      name: newConfiguration.name,
      sourceType: newConfiguration.sourceType,
      status: newConfiguration.status,
      config: newConfiguration.config,
    };
    if (newConfiguration.schedule) {
      configuration.schedule = newConfiguration.schedule;
    }
    if (newConfiguration.file) {
      configuration.file = newConfiguration.file;
    }
    return await this.sendSource(userId, configuration, 'PUT');
  }
}
