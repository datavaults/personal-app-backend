import { Injectable } from '@nestjs/common';
import { SourceOptionDTO } from '@suite5/asset/dto';
import { DataFetcherStrategy } from '@suite5/asset/strategies/data-fetcher.strategy';
import { UserSourcesDTO } from '../dto/user-sources.dto';

@Injectable()
export class FakeDataFetcherStrategy implements DataFetcherStrategy {
  async createSource(userId: string, configuration: any): Promise<any> {
    return {};
  }

  async getSources(): Promise<SourceOptionDTO[]> {
    return Promise.resolve([
      {
        id: 'miwenergia',
        sourcetype: 'MIWENERGIA',
        name: 'MiwEnergia',
        url: '',
        auth_type: 'OAUTH',
        auth_properties: ['access_token'],
        config_properties: ['fields'],
      },
      {
        id: 'olympiakos',
        sourcetype: 'OLYMPIAKOS',
        name: 'Olympiakos',
        url: '',
        auth_type: 'OAUTH',
        auth_properties: ['access_token'],
        config_properties: ['fields'],
      },
      {
        id: 'twitter',
        sourcetype: 'TWITTER',
        name: 'Twitter',
        url: '',
        auth_type: 'OAUTH',
        auth_properties: ['access_token'],
        config_properties: ['fields'],
      },
      {
        id: 'mobileApp',
        sourcetype: 'MOBILEAPP',
        name: 'Mobile App',
        url: '',
        auth_type: '',
        auth_properties: [],
        config_properties: [],
      },
      {
        id: 'andaman7',
        sourcetype: 'ANDAMAN7',
        name: 'Andaman7',
        url: '',
        auth_type: 'OAUTH',
        auth_properties: ['access_token'],
        config_properties: ['fields'],
      },
      {
        id: 'prato',
        sourcetype: 'PRATO',
        name: 'Prato',
        url: '',
        auth_type: 'OAUTH',
        auth_properties: ['access_token'],
        config_properties: ['fields'],
      },
    ]);
  }

  async getUserSources(userId: string): Promise<UserSourcesDTO[]> {
    return Promise.resolve([
      {
        id: 'twitter-feed-1',
        name: 'My Twitter Feed',
        userId: userId as any,
        sourceType: 'TWITTER',
        status: 'enabled',
        config: {
          username: 'abce3',
          password: 'pw12',
          access_token: 'asda_asdiuiooo',
        },
        schedule: {
          interval: {
            unit: 'DAY',
            value: 31,
          },
          next: '2019-08-24T14:15:22Z',
        },
      },
      {
        id: 'google-fit-1',
        name: 'Google Fit',
        userId: userId as any,
        sourceType: 'MOBILEAPP',
        status: 'enabled',
        config: {
          subId: userId as any,
          sourceType: 'Google Fit',
        },
        schedule: {
          interval: {
            unit: 'DAY',
            value: 31,
          },
          next: '2019-08-24T14:15:22Z',
        },
      },
      {
        id: 'route-fit-1',
        name: 'Location',
        userId: userId as any,
        sourceType: 'MOBILEAPP',
        status: 'enabled',
        config: {
          subId: userId as any,
          sourceType: 'Location',
        },
        schedule: {
          interval: {
            unit: 'DAY',
            value: 31,
          },
          next: '2019-08-24T14:15:22Z',
        },
      },
    ]);
  }

  deleteSource(userId: string, asset: string): Promise<any> {
    return Promise.resolve(true);
  }

  updateSource(
    userId: string,
    sourceId: string,
    newConfiguration: {
      name: string;
      sourceType: string;
      status: string;
      config: any;
      schedule?: any;
      file?: any;
    },
  ): Promise<any> {
    return Promise.resolve({
      id: sourceId,
      userid: userId,
      ...newConfiguration,
    });
  }
}
