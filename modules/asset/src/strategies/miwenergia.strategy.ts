export const MIWENERGIA_STRATEGY_TOKEN = 'MIWenergiaStrategy';

export interface MIWenergiaStrategy {
  login(username: string, password: string): Promise<any>;
  retrieveUspcList(token: any): Promise<any>;
}
