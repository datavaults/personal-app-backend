import { ForbiddenException, Injectable } from '@nestjs/common';
import { MIWenergiaStrategy } from './miwenergia.strategy';
@Injectable()
export class FakeMiwenergiaStrategy implements MIWenergiaStrategy {
  async login(username: string, password: string): Promise<any> {
    if (username === 'test' && password === 'test')
      return {
        username: 'user1@miw.es',
        token:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
      };
    throw new ForbiddenException('Incorrect username/password');
  }
  async retrieveUspcList(token: any): Promise<any> {
    return [
      {
        uspcText: 'ES1111111111111111AA',
      },
      {
        uspcText: 'ES2222222222222222BB',
      },
      {
        uspcText: 'ES3333333333333333CC',
      },
      {
        uspcText: 'ES4444444444444444DD',
      },
    ];
  }
}
