import { Injectable } from '@nestjs/common';
import { OlympiakosStrategy } from './olympiakos.strategy';

@Injectable()
export class FakeOlympiakosStrategy implements OlympiakosStrategy {
  async ckeckUser(memberId: string, birthDate: Date, token: any): Promise<any> {
    return true;
  }
}
