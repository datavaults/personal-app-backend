import { OfferMessageStatus } from '@suite5/message/constants';
import { Questionnaire } from '../../../message/src/interfaces';
export const CLOUD_STRATEGY_TOKEN = 'ShareStrategy';

export interface CloudStrategy {
  publishDataset(configuration: any): Promise<boolean>;
  updateDataset(configuration: any): Promise<any>;
  updateDataOwnerProfile(userData: any): Promise<any>;
  deleteDataOwner(userId: string, assetIds: any[]): Promise<any>;
  publishQuestionnaire(
    receiverSub: string,
    questionnaire: Questionnaire,
  ): Promise<boolean>;
  acceptOffer(
    assetId: string,
    dataseekerId: string,
    dataownerId: string,
    messageStatus: OfferMessageStatus,
  ): Promise<any>;
  deleteAsset(assetUid: string): Promise<any>;
}
