import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { PratoStrategy } from './prato.strategy';
import parse from 'xml-parser';

@Injectable()
export class RealPratoStrategy implements PratoStrategy {
  private readonly PRATO_URL = `${this.configService.get(
    'asset.module.pratoUrl',
  )}`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  private xmlBody = `<?xml version="1.0" encoding="UTF-8"?>
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Body>
    <ns1:getSoggetti xmlns:ns1="urn:SsdWSSCertificazioneRemotaPrato" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
   <getSoggettiRequest href="#id0"/>
 </ns1:getSoggetti>
 <multiRef xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns2="http://types.SsdWSSCertificazioneRemotaPrato.impl.webservices.serviziDemografici.pubblici.saga.it" id="id0" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:_GetSoggettiRequest">
   <richiestaBase href="#id1"/>
 </multiRef>
 <multiRef xmlns:ns3="http://types.SsdWSSCertificazioneRemotaPrato.impl.webservices.serviziDemografici.pubblici.saga.it" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" id="id1" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns3:RichiestaBaseType">`;

  async searchPersonByFiscalCode(nationalInsuranceNumber: any): Promise<any> {
    const body =
      this.xmlBody +
      `<idOperatore xsi:type="xsd:string">${nationalInsuranceNumber.value}</idOperatore>
         <idSistema xsi:type="xsd:string">DATAVAULTS</idSistema>
         <idRichiedente xsi:type="xsd:string">${nationalInsuranceNumber.value}</idRichiedente>
         <idLogin xsi:type="xsd:string">@prato_test_postgres</idLogin>
         </multiRef>
       </soapenv:Body>
     </soapenv:Envelope>`;

    return await this.httpService
      .request({
        method: 'POST',
        url: this.PRATO_URL,
        headers: {
          'Content-Type': 'text/xml',
          SOAPAction: '"#POST"',
        },
        data: body,
      })
      .toPromise()
      .then(async (resp) => {
        const parsedXml = parse(resp.data); // parsing the response from Prato
        let response;
        parsedXml.root.children.forEach((xmlChild: any) => {
          // loop inside response to validate national insurance number
          xmlChild.children.forEach((xmlItem: any) => {
            xmlItem.children.forEach((xmlValue: any) => {
              if (xmlValue.content === nationalInsuranceNumber.value) {
                response = xmlValue.content;
              }
            });
          });
        });
        return response;
      })
      .catch((e) => {
        console.error(JSON.stringify(e));
        throw e;
      });
  }
}
