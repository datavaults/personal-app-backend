export const OLYMPIAKOS_STRATEGY_TOKEN = 'OlympiakosStrategy';

export interface OlympiakosStrategy {
  ckeckUser(memberId: string, birthDate: Date, token: any): Promise<any>;
}
