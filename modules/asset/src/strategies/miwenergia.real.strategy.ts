import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { MIWenergiaStrategy } from './miwenergia.strategy';

@Injectable()
export class RealMiwenergiaStrategy implements MIWenergiaStrategy {
  private readonly MIWENERGIA_LOGIN_URL = `${this.configService.get(
    'asset.module.miwenergiaUrl',
  )}user/login/`;

  private readonly MIWENERGIA_USPC_LIST_URL = `${this.configService.get(
    'asset.module.miwenergiaUrl',
  )}hecdata/getuspc/`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async login(username: string, password: string): Promise<any> {
    const data = {
      username: username,
      password: password,
    };
    return await this.httpService
      .post(this.MIWENERGIA_LOGIN_URL, data, {
        headers: {
          accept: 'application/json',
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => {
        return { status: '500' };
      });
  }

  async retrieveUspcList(data: any): Promise<any> {
    return await this.httpService
      .get(this.MIWENERGIA_USPC_LIST_URL, {
        headers: {
          accept: 'application/json',
          Authorization: 'Bearer ' + data.token,
        },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      }).catch((e) => {
        return e;
      });
  }
}
