import { SourceOptionDTO } from '@suite5/asset/dto';
import { UserSourcesDTO } from '../dto/user-sources.dto';

export const DATA_FETCHER_STRATEGY_TOKEN = 'DataFetcherStrategy';

export interface DataFetcherStrategy {
  createSource(
    userId: string,
    configuration: {
      id: string;
      name: string;
      sourceType: string;
      category: string;
      status: string;
      config: any;
      schedule?: any;
      file?: any;
    },
  ): Promise<any>;

  getSources(): Promise<SourceOptionDTO[]>;
  getUserSources(userId: string): Promise<UserSourcesDTO[]>;
  updateSource(
    userId: string,
    sourceId: string,
    newConfiguration: {
      id: string;
      name: string;
      sourceType: string;
      status: string;
      config: any;
      schedule?: any;
      file?: any;
    },
  ): Promise<any>;
  deleteSource(userId: string, assetUid: string): Promise<any>;
}
