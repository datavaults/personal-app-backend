import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { CloudAuthenticationService } from '@suite5/asset/services';
import { OfferMessageStatus } from '@suite5/message/constants';
import { Questionnaire } from '@suite5/message/interfaces';
import { CloudStrategy } from './cloud.strategy';

@Injectable()
export class RealCloudStrategy implements CloudStrategy {
  private readonly PUBLISH_SHARE_ASSET_URL = `${this.configService.get(
    'asset.module.cloudUrl',
  )}/dataset`;

  private readonly UPDATE_DATA_OWNER_PORIFLE_URL = `${this.configService.get(
    'asset.module.cloudUrl',
  )}/dataset/dataowner`;

  private readonly DELETE_DATA_OWNER_PORIFLE_URL = `${this.configService.get(
    'asset.module.cloudUrl',
  )}/dataset/dataowner`;
  private readonly DELETE_DATA_OWNER_ASSETS = `${this.configService.get(
    'asset.module.cloudUrl',
  )}/dataset/delete`;

  private readonly PUBLISH_QUESTIONNAIRE_URL = `${this.configService.get(
    'message.module.publishQuestionnnaireURL',
  )}/questionnaires`;

  private readonly PUBLISH_QUESTIONNAIRE_ACCEPTANCE_URL = `${this.configService.get(
    'message.module.publishQuestionnnaireURL',
  )}/dataset/requests`;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(CloudAuthenticationService)
    private readonly cloudAuthenticationService: CloudAuthenticationService,
    @Inject(Logger)
    private readonly logger: Logger,
  ) {}

  private getHeaders = async () => {
    return {
      Accept: 'application/json',
      'content-type': 'application/json',
      Authorization: `Bearer ${await this.cloudAuthenticationService.login()}`,
    };
  };

  private async postToCloud(configuration: any, url: any) {
    try {
      return await this.httpService
        .post(url, JSON.stringify(configuration), {
          headers: await this.getHeaders(),
        })
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        });
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  private async putToCloud(configuration: any, url: any) {
    try {
      return await this.httpService
        .put(url, JSON.stringify(configuration), {
          headers: await this.getHeaders(),
        })
        .toPromise()
        .then(async (resp) => {
          return resp.data;
        });
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  async publishDataset(configuration: any): Promise<boolean> {
    try {
      return await this.postToCloud(
        configuration,
        this.PUBLISH_SHARE_ASSET_URL,
      );
    } catch (e) {
      console.log('-----------Cloud Strategy-------------');
      console.log(e);
    }
  }

  async updateDataset(configuration: any): Promise<boolean> {
    return await this.putToCloud(configuration, this.PUBLISH_SHARE_ASSET_URL);
  }

  async updateDataOwnerProfile(configuration: any): Promise<boolean> {
    return await this.postToCloud(
      configuration,
      this.UPDATE_DATA_OWNER_PORIFLE_URL,
    );
  }

  async deleteDataOwner(userId: string, assetIds: any[]): Promise<any> {
    const token = await this.cloudAuthenticationService.login();

    return await this.httpService
      .delete(this.DELETE_DATA_OWNER_PORIFLE_URL, {
        headers: {
          Accept: 'application/json',
          'content-type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        data: { dataOwnerUUID: userId, assetUUIDs: assetIds },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  async deleteAsset(assetUid: string): Promise<any> {
    return await this.httpService
      .delete(this.DELETE_DATA_OWNER_ASSETS, {
        headers: await this.getHeaders(),
        data: { assetUUID: assetUid },
      })
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((e) => console.log(e));
  }

  async publishQuestionnaire(
    receiverSub: string,
    data: Questionnaire,
  ): Promise<boolean> {
    return await this.httpService
      .post(
        this.PUBLISH_QUESTIONNAIRE_URL + `/${data.id}/answer/${receiverSub}`,
        { answers: data.questions, receivedDate: new Date().toISOString() },
        {
          headers: await this.getHeaders(),
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      });
  }

  async acceptOffer(
    assetId: string,
    dataseekerId: string,
    dataownerId: string,
    messageStatus: OfferMessageStatus,
  ): Promise<any> {
    return await this.httpService
      .put(
        this.PUBLISH_QUESTIONNAIRE_ACCEPTANCE_URL,
        {
          assetUUID: assetId,
          dataseekerUUID: dataseekerId,
          dataOwnerUUID: dataownerId,
          status: messageStatus,
        },
        {
          headers: await this.getHeaders(),
        },
      )
      .toPromise()
      .then(async (resp) => {
        return resp.data;
      })
      .catch((error) => {
        console.log('-----------Accept Offer Cloud-----------');
        console.log(error);
      });
  }
}
