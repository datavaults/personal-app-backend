import { Injectable } from '@nestjs/common';
import { OfferMessageStatus } from '@suite5/message/constants';
import { Questionnaire } from '@suite5/message/interfaces';
import { CloudStrategy } from './cloud.strategy';
@Injectable()
export class FakeCloudStrategy implements CloudStrategy {
  async publishDataset(configuration: any): Promise<boolean> {
    return Promise.resolve(true);
  }

  async updateDataset(configuration: any): Promise<any> {
    return Promise.resolve(true);
  }

  async updateDataOwnerProfile(): Promise<boolean> {
    return Promise.resolve(true);
  }
  async deleteDataOwner(userId: string, assetIds: any[]): Promise<any> {
    return Promise.resolve(true);
  }
  async deleteAsset(assetId: string): Promise<any> {
    return Promise.resolve(true);
  }
  async publishQuestionnaire(
    receiverSub: string,
    data: Questionnaire,
  ): Promise<boolean> {
    return Promise.resolve(true);
  }
  async acceptOffer(
    assetId: string,
    dataseekerId: string,
    dataownerId: string,
    messageStatus: OfferMessageStatus,
  ): Promise<any> {
    return Promise.resolve(true);
  }
}
