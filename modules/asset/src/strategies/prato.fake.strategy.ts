import { Injectable } from '@nestjs/common';
import { PratoStrategy } from './prato.strategy';

@Injectable()
export class FakePratoStrategy implements PratoStrategy {
  async searchPersonByFiscalCode(nationalInsuranceNumber: any): Promise<any> {
    return nationalInsuranceNumber.value;
  }
}
