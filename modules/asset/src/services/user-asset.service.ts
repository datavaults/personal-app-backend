import { Inject, Injectable } from '@nestjs/common';
import { Connection } from 'mongoose';
import { VaultService } from '@suite5/vault/vault.service';
import { KeycloakAuthenticationStrategy } from '@suite5/core/authentication/strategies/keycloak.strategy';
import {
  CloudStrategy,
  CLOUD_STRATEGY_TOKEN,
} from '../strategies/cloud.strategy';
import { UserService } from '@suite5/core/user/services/user.service';
import { AUTHENTICATION_STRATEGY_TOKEN } from '@suite5/core/authentication/strategies';
import { DataFetcherService } from './data-fetcher.service';
import { SharedAssetService } from './shared-asset.service';

@Injectable()
export class UserAssetService {
  constructor(
    private readonly sharedAssetService: SharedAssetService,
    private readonly userService: UserService,
    private readonly vaultService: VaultService,
    @Inject(AUTHENTICATION_STRATEGY_TOKEN)
    private readonly keycloakStrategy: KeycloakAuthenticationStrategy,
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly strategy: CloudStrategy,
    private readonly dataFetcherService: DataFetcherService,
  ) {}

  private async deleteUser(user, mongoConnection: Connection) {
    await mongoConnection.collection(`user${user.id}`).drop;
    await this.vaultService.deleteSecret('user', String(user.id), 'mongo');

    await this.keycloakStrategy.deleteUser(user.sub);
    await this.userService.delete(user.email);
  }

  async delete(id: number, mongoConnection: Connection): Promise<void> {
    const assetIds = [];
    const user = await this.userService.retrieve(id);
    const userAssets = await this.dataFetcherService.retrieveByUserId(user.id);
    userAssets.forEach(async (asset) => {
      assetIds.push(asset.id);
    });
    const userSharedAssets = await this.sharedAssetService.retrieveByIds(
      assetIds,
    );
    if (userSharedAssets.length > 0) {
      await this.strategy.deleteDataOwner(user.id as any, assetIds);
    }

    return await this.deleteUser(user, mongoConnection);
  }
}
