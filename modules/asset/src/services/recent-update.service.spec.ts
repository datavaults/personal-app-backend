import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  CreateRecentUpdateDTO,
  RecentUpdate,
  RecentUpdateService,
  UpdateRecentUpdateDTO,
} from '@suite5/asset';

describe('RecentUpdateService', () => {
  let service: RecentUpdateService;
  let repository: Repository<RecentUpdate>;
  let recentUpdate: RecentUpdate;

  beforeEach(async () => {
    recentUpdate = new RecentUpdate();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RecentUpdateService,
        {
          provide: getRepositoryToken(RecentUpdate),
          useClass: Repository,
        },
      ],
    }).compile();
    service = module.get<RecentUpdateService>(RecentUpdateService);
    repository = module.get<Repository<RecentUpdate>>(
      getRepositoryToken(RecentUpdate),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a recent update', async () => {
    const data: CreateRecentUpdateDTO = {
      name: '',
      assetId: 1,
      lastDate: new Date(),
      successful: true,
    };
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(recentUpdate);
    expect(await service.createRecentUpdate(data)).toBe(recentUpdate);
  });

  it('should retrieve recent updates by source id', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([recentUpdate]);
    expect(await service.retrieveRecentUpdatesByAssetId(1)).toEqual([
      recentUpdate,
    ]);
  });

  it('should retrieve a recent update', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(recentUpdate);
    expect(await service.retrieveRecentUpdate(1)).toEqual(recentUpdate);
  });

  it('should update a recent update', async () => {
    const data: UpdateRecentUpdateDTO = { name: '' };
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    jest.spyOn(repository, 'merge').mockReturnValue(recentUpdate);
    jest.spyOn(repository, 'save').mockResolvedValue(recentUpdate);
    expect(await service.updateRecentUpdate(1, data)).toBe(recentUpdate);
  });

  it('should delete a recent update', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();
    await service.deleteRecentUpdate(1);
    expect(repository.delete).toBeCalledWith(1);
  });
});
