import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AnonymiserService } from '@suite5/anonymiser/services/anonymiser.service';
import { Repository } from 'typeorm';
import { Transaction } from '@suite5/asset/entities/transaction.entity';
import { TransactionService } from '@suite5/asset/services/transaction.service';

describe('TransactionService', () => {
  let service: TransactionService;
  let repository: Repository<Transaction>;
  let transaction: Transaction;

  beforeEach(async () => {
    transaction = new Transaction();
    transaction.userId = 1;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionService,
        {
          provide: getRepositoryToken(Transaction),
          useClass: Repository,
        },
        {
          provide: AnonymiserService,
          useValue: {
            createPseudoID: () => jest.fn(),
          },
        },
      ],
    }).compile();
    service = module.get<TransactionService>(TransactionService);
    repository = module.get<Repository<Transaction>>(
      getRepositoryToken(Transaction),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });

  it('should create a transaction', async () => {
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(transaction);
    expect(await service.create(transaction)).toBe(transaction);
  });

  it('should retrieve a transaction', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(transaction);
    expect(await service.retrieveById(1)).toEqual(transaction);
  });

  it('should retrieve all transactions of a user', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([transaction]);
    expect(await service.retrieveByUserId(1)).toEqual([transaction]);
  });

  it('should delete a transaction', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();
    expect(await service.remove(transaction)).toBeUndefined();
  });
});
