import { Inject, Injectable, Logger } from '@nestjs/common';
import * as R from 'ramda';
import { OnEvent } from '@nestjs/event-emitter';
import { Events } from '@suite5/constants/events';
import { Asset } from '@suite5/asset/entities/asset.entity';
import { User } from '@suite5/core/user/entities/user.entity';
import { DataFetcherMessageBody } from '../interfaces';
import { CloudStrategy, CLOUD_STRATEGY_TOKEN } from '../strategies';
import { SharedAssetService } from '../services/shared-asset.service';
import { EncryptionService } from '../../../encryption/src/services';
@Injectable()
export class CloudUpdateAssetService {
  constructor(
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly strategy: CloudStrategy,
    private readonly sharedAssetService: SharedAssetService,
    @Inject(Logger)
    private readonly logger: Logger,
    @Inject(EncryptionService)
    private readonly encryptionService: EncryptionService,
  ) {}

  @OnEvent(Events.UpdateSharedAsset)
  async updateSharedAsset(
    body: DataFetcherMessageBody,
    user: User,
    asset: Asset,
  ) {
    const sharedAsset = await this.sharedAssetService.retrieveByAssetId(
      asset.id,
      true,
    );
    if (sharedAsset) {
      const payload = {
        assetId: body.asset,
        // there is a data object inside data and we should use that if available, also cloud platform await the data like this data: [{data:""}],
        // so we add [] here but we need to find a better solution or cloud should accept both data: [{data:""}] and data: {data:""}
        data: null,
        dataOwnerProfile: {
          birthDate: user.birthDate,
          city: user.city,
          civilStatus: user.civilStatusList,
          country: user.country,
          culturalInterests: user.culturalInterestList,
          demonstrator: user.demonstrator,
          disabilities: user.disabilitiesList,
          educations: user.qualificationsList,
          nationalInsuranceNumber: user.nationalInsuranceNumber,
          nationality: user.nationality,
          occupations: user.occupationsList,
          placeOfBirth: user.placeOfBirth,
          postalCode: user.postcode,
          region: user.region,
          requestResolverSettings: user.requestResolverSettings,
          shareProfile: user.shareProfile,
          socialSecurityNumber: user.socialSecurityNumber,
          transportationMeans: user.transportationList,
        },
        dataOwnerUUID: user.sub,
        metadata: body.metadata,
        dataSchema: R.is(Array, body.dataSchema)
          ? body.dataSchema
          : [body.dataSchema],
        append: true,
      };
      if (sharedAsset.encrypted) {
        payload.data = [
          await this.encryptionService.encryptData(body.data, user.id),
        ];
      } else {
        payload.data = body.data.data ? [body.data.data] : [body.data];
      }
      try {
        await this.strategy.updateDataset(payload);
        await this.sharedAssetService.update(asset.id);
        this.logger.log(`Shared asset '${sharedAsset.id}' updated`);
      } catch (e) {
        this.logger.error(`Error: ${e}`);
        this.logger.error(
          `[UpdateSharedAsset Event]: Update - failed for '${body.asset}'`,
        );
      }
    } else {
      this.logger.log(`Asset '${asset.id}' not shared yet - skipping update`);
    }
  }
}
