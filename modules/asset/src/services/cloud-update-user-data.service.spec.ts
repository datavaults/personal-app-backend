import { CloudStrategy, SharedAsset } from '@suite5/asset';
import { CloudUpdateUserDataService } from './cloud-update-user-data.service';

describe('CloudUpdateUserDataService', () => {
  let service: CloudUpdateUserDataService;
  let mockSharedAssetService: any;
  let mockUserService: any;
  let mockShareStrategy: CloudStrategy;
  let userData: any;
  let dataOwnerProfile: any;
  let mocksharedAsset: SharedAsset;

  beforeEach(async () => {
    mocksharedAsset = new SharedAsset();
    mocksharedAsset.name = 'test';
    mocksharedAsset.userId = 1;
    mocksharedAsset.assetId = 1;

    mockUserService = {
      getProfileById: () => jest.fn(),
    };

    mockSharedAssetService = {
      retrieveByUserId: () => jest.fn(),
    };

    userData = {
      id: 1,
      firstName: 'Test',
      lastName: 'Cloud',
      email: 'testCloudData@suite5.eu',
      street: 'Klisthenous Avenue',
      postcode: '15344',
      region: 'Gerakas',
      city: 'Gerakas',
      country: 'Greece',
      phone: '0000000000',
      birthDate: '15-10-2021',
      placeOfBirth: 'Athens',
      nationalInsuranceNumber: '000000',
      socialSecurityNumber: '000000',
      nationality: 'Greek',
      memberNumber: '1',
      shareProfile: true,
      occupations: [],
      qualifications: [],
      culturalInterest: [],
      transportationMeans: [],
      civilStatus: 'Single',
      disabilities: [],
    };

    dataOwnerProfile = {
      region: 'Gerakas',
      city: 'Gerakas',
      country: 'Greece',
      phone: '0000000000',
      birthDate: '15-10-2021',
      placeOfBirth: 'Athens',
      nationalInsuranceNumber: '000000',
      socialSecurityNumber: '000000',
      nationality: 'Greek',
      memberNumber: '1',
      shareProfile: true,
      occupations: [],
      qualifications: [],
      culturalInterest: [],
      transportationMeans: [],
      civilStatus: 'single',
      disabilities: [],
    };

    mockShareStrategy = {
      updateDataOwnerProfile: () => jest.fn() as any,
      publishDataset: () => jest.fn() as any,
      updateDataset: () => jest.fn() as any,
      deleteAsset: () => jest.fn() as any,
      deleteDataOwner: () => jest.fn() as any,
      acceptOffer: () => jest.fn() as any,
      publishQuestionnaire: () => jest.fn() as any,
    };

    service = new CloudUpdateUserDataService(
      mockSharedAssetService,
      mockShareStrategy,
      mockUserService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be update owner cloud data', async () => {
    jest
      .spyOn(mockSharedAssetService, 'retrieveByUserId')
      .mockResolvedValue(mocksharedAsset);

    jest
      .spyOn(mockUserService, 'getProfileById')
      .mockResolvedValue(dataOwnerProfile);
    jest
      .spyOn(mockShareStrategy, 'updateDataOwnerProfile')
      .mockResolvedValue(dataOwnerProfile);
    await service.updateDataOwnerProfile(userData);
    expect(await mockShareStrategy.updateDataOwnerProfile).toBeCalledWith(
      dataOwnerProfile,
    );
  });

  it(`shouldn't be update owner cloud data`, async () => {
    userData.shareProfile = false;
    dataOwnerProfile.shareProfile = false;
    jest
      .spyOn(mockSharedAssetService, 'retrieveByUserId')
      .mockResolvedValue(mocksharedAsset);

    jest
      .spyOn(mockUserService, 'getProfileById')
      .mockResolvedValue(dataOwnerProfile);

    jest
      .spyOn(mockShareStrategy, 'updateDataOwnerProfile')
      .mockResolvedValue(dataOwnerProfile);
    await service.updateDataOwnerProfile(userData);
    expect(await mockShareStrategy.updateDataOwnerProfile).not.toBeCalledWith(
      dataOwnerProfile,
    );
  });
});
