import { Events } from '@suite5/constants/events';
import { User } from '@suite5/core/user/entities';
import { Exception } from 'handlebars';
import { v4 as uuidv4 } from 'uuid';
import { mock_values } from '../mocks/datafetcher.mock.response';
import { DataFetcherMessagingService } from './data-fetcher-messaging.service';

describe('DataFetcherMessagingService', () => {
  let userService: any;
  let logger: any;
  const user: User = { sub: '99088887-5d38-4d56-b064-65f212a96410' } as User;
  let asset: any;
  let configService: any;
  let eventEmitter: any;
  let dataFetcherService: any;
  let service: any;
  let message: any;
  let mongoService: any;
  let vaultService: any;

  beforeEach(async () => {
    eventEmitter = {
      emit: () => jest.fn(),
    };

    asset = { createdById: 1, uid: uuidv4() };
    message = mock_values(user.sub, asset.uid);
    logger = {
      error: () => jest.fn(),
      log: () => jest.fn(),
    };

    userService = {
      retrieveBySub: () => jest.fn(),
    };

    mongoService = {
      connection: jest.fn(() => {
        close: () => jest.fn();
      }),
      runWithConnection: jest.fn(),
    };

    dataFetcherService = {
      createAsset: () => jest.fn(),
      replaceCollection: () => jest.fn(),
      retrieveAssetByUid: () => jest.fn(),
      setAssetDataUpdated: () => jest.fn(),
    };

    configService = {
      get: () => jest.fn(),
    };

    vaultService = {
      getSecret: () => jest.fn(),
    };

    service = new DataFetcherMessagingService(
      mongoService,
      logger,
      dataFetcherService,
      userService,
      eventEmitter,
      configService,
      vaultService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  // it('should emit a cloud asset update event', async () => {
  //   jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
  //   jest.spyOn(dataFetcherService, 'replaceCollection').mockResolvedValue(true);
  //   jest
  //     .spyOn(dataFetcherService, 'retrieveAssetByUid')
  //     .mockResolvedValue(asset);
  //   jest
  //     .spyOn(dataFetcherService, 'setAssetDataUpdated')
  //     .mockResolvedValue(asset);
  //   jest.spyOn(eventEmitter, 'emit');
  //   await service.dataFetched(message);
  //   expect(eventEmitter.emit).toBeCalledTimes(1);
  //   expect(eventEmitter.emit).toBeCalledWith(
  //     Events.UpdateSharedAsset,
  //     message.body,
  //     user,
  //     asset,
  //   );
  // });

  // it('should not emit a cloud asset update event in case of error', async () => {
  //   jest.spyOn(userService, 'retrieveBySub').mockResolvedValue(user);
  //   jest.spyOn(dataFetcherService, 'replaceCollection').mockResolvedValue(true);
  //   jest
  //     .spyOn(dataFetcherService, 'retrieveAssetByUid')
  //     .mockResolvedValue(asset);
  //   jest
  //     .spyOn(dataFetcherService, 'setAssetDataUpdated')
  //     .mockImplementation(() => {
  //       throw new Exception('Mock exception');
  //     });
  //   jest.spyOn(eventEmitter, 'emit');
  //   await service.dataFetched(message);
  //   expect(eventEmitter.emit).toBeCalledTimes(0);
  // });
});
