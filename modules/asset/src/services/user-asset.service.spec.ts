import { UserAssetService } from './user-asset.service';

describe('UserAssetService', () => {
  let service: UserAssetService;
  let mockUserService: any;
  let mockVaultService: any;
  let mockKeycloakStrategy: any;
  let mockShareStrategy: any;
  let mockDataFetcherService: any;
  let mockSharedAssetService: any;

  const user = {
    id: 1,
    username: 'test@test.com',
    email: 'test@test.com',
    sub: 'skldfjslkdfjs-sdfbsdjkf',
    emailVerified: true,
  };

  beforeEach(async () => {
    mockUserService = {
      retrieveByEmail: jest.fn(),
      delete: jest.fn(),
    };

    mockVaultService = {
      deleteSecret: jest.fn(),
    };

    mockKeycloakStrategy = {
      deleteUser: jest.fn(),
    };

    mockShareStrategy = {
      deleteDataOwner: jest.fn(),
    };

    mockDataFetcherService = {
      retrieveByUserId: jest.fn(),
    };

    mockSharedAssetService = {
      retrieveByIds: jest.fn(),
    };

    service = new UserAssetService(
      mockSharedAssetService,
      mockUserService,
      mockVaultService,
      mockKeycloakStrategy,
      mockShareStrategy,
      mockDataFetcherService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should delete a user', async () => {
    const asset = [
      {
        id: 1,
        name: 'Test',
        sourceType: 'Upload',
        createdById: 1,
        uid: '8451be53-2a0b-42fc-b441-e66e8522a1ce',
        type: 'dataset',
        firstCollection: '19/11/2021',
      },
    ];
    jest.spyOn(mockUserService, 'retrieveByEmail').mockReturnValue(user);
    jest
      .spyOn(mockDataFetcherService, 'retrieveByUserId')
      .mockReturnValue(asset);
    jest.spyOn(mockSharedAssetService, 'retrieveByIds').mockReturnValue([]);
    expect(await service.delete).toBeTruthy();
  });

  it('should delete a cloud user', async () => {
    const asset = [
      {
        id: 1,
        name: 'Test',
        sourceType: 'Upload',
        createdById: 1,
        uid: '8451be53-2a0b-42fc-b441-e66e8522a1ce',
        type: 'dataset',
        firstCollection: '19/11/2021',
      },
    ];
    jest.spyOn(mockUserService, 'retrieveByEmail').mockReturnValue(user);
    jest
      .spyOn(mockDataFetcherService, 'retrieveByUserId')
      .mockReturnValue(asset);
    jest.spyOn(mockSharedAssetService, 'retrieveByIds').mockReturnValue([
      {
        assetId: 1,
        name: 'Test',
        userId: 1,
      },
    ]);
    expect(await service.delete).toBeTruthy();
  });
});
