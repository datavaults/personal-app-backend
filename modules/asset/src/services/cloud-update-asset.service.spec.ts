import { User } from '@suite5/core/user/entities';
import { CloudUpdateAssetService } from './cloud-update-asset.service';
import { mock_values } from '../mocks/datafetcher.mock.response';
import { v4 as uuidv4 } from 'uuid';

describe('CloudUpdateAssetService', () => {
  let logger: any;
  let cloudStrategy: any;
  let updateService: any;
  let sharedAssetService: any;
  let message: any;
  const user: User = { sub: '99088887-5d38-4d56-b064-65f212a96410' } as User;
  const asset: any = { createdById: 1, uid: uuidv4() };
  let mockEncryptionService: any;

  beforeEach(async () => {
    cloudStrategy = {
      updateDataset: () => jest.fn(),
    };

    sharedAssetService = {
      update: () => jest.fn(),
      retrieveByAssetId: () => jest.fn(),
    };

    mockEncryptionService = {
      encryptData: () => jest.fn(),
    }

    message = mock_values(user.sub, asset.uid);
    logger = {
      error: () => jest.fn(),
      log: () => jest.fn(),
    };

    updateService = new CloudUpdateAssetService(
      cloudStrategy,
      sharedAssetService,
      logger,
      mockEncryptionService,
    );
  });

  it('should be defined', () => {
    expect(updateService).toBeDefined();
  });

  it('should call updateDataset if asset has already been shared before', async () => {
    jest.spyOn(cloudStrategy, 'updateDataset');
    jest.spyOn(sharedAssetService, 'update');
    jest.spyOn(logger, 'log');
    jest
      .spyOn(sharedAssetService, 'retrieveByAssetId')
      .mockResolvedValue(asset);
    await updateService.updateSharedAsset(message.body, user, asset);
    expect(cloudStrategy.updateDataset).toBeCalledTimes(1);
    expect(cloudStrategy.updateDataset).toBeCalledWith({
      assetId: message.body.asset,
      data: [message.body.data.data],
      dataOwnerProfile: {
        birthDate: user.birthDate,
        city: user.city,
        civilStatus: user.civilStatusList,
        country: user.country,
        culturalInterests: user.culturalInterestList,
        demonstrator: user.demonstrator,
        disabilities: user.disabilitiesList,
        educations: user.qualificationsList,
        nationalInsuranceNumber: user.nationalInsuranceNumber,
        nationality: user.nationality,
        occupations: user.occupationsList,
        placeOfBirth: user.placeOfBirth,
        postalCode: user.postcode,
        region: user.region,
        requestResolverSettings: user.requestResolverSettings,
        shareProfile: user.shareProfile,
        socialSecurityNumber: user.socialSecurityNumber,
        transportationMeans: user.transportationList,
      },
      dataOwnerUUID: user.sub,
      metadata: message.body.metadata,
      dataSchema: [message.body.dataSchema],
      append: true,
    });
    expect(sharedAssetService.update).toBeCalledTimes(1);
    expect(logger.log).toBeCalledTimes(1);
  });

  it('should fail if asset has not already been shared', async () => {
    jest
      .spyOn(sharedAssetService, 'retrieveByAssetId')
      .mockResolvedValue(undefined);
    jest.spyOn(cloudStrategy, 'updateDataset');
    jest.spyOn(sharedAssetService, 'update');
    await updateService.updateSharedAsset(message.body, user, asset);
    expect(cloudStrategy.updateDataset).toBeCalledTimes(0);
    expect(sharedAssetService.update).toBeCalledTimes(0);
  });
});
