import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class CloudAuthenticationService {
  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  /**
   * Logs in to keycloak cloud and retrieves token
   */
  async login(): Promise<string> {
    const params = new URLSearchParams();
    params.append(
      'username',
      this.configService.get('asset.module.cloudKeycloakUsername'),
    );
    params.append(
      'password',
      this.configService.get('asset.module.cloudKeycloakPassword'),
    );
    params.append('grant_type', 'password');
    params.append(
      'client_id',
      this.configService.get('asset.module.cloudKeycloakClientId'),
    );
    params.append(
      'client_secret',
      this.configService.get('asset.module.cloudKeycloakClientSecret'),
    );

    return new Promise<string>((resolve, reject) => {
      this.httpService
        .post(
          `${this.configService.get(
            'asset.module.cloudKeycloakUrl',
          )}/realms/${this.configService.get(
            'asset.module.cloudKeycloakRealm',
          )}/protocol/openid-connect/token`,
          params,
          {
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
          },
        )
        .toPromise()
        .then((resp) => resolve(resp.data.access_token))
        .catch((e) => {
          reject(e);
        });
    });
  }
}
