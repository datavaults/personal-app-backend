import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Asset, SharedAsset } from '@suite5/asset/entities';
import dayjs from 'dayjs';
import { Between, Repository } from 'typeorm';
import { TransactionService } from './transaction.service';

@Injectable()
export class AssetStatsService {
  constructor(
    @InjectRepository(Asset)
    private readonly assetRepository: Repository<Asset>,
    @InjectRepository(SharedAsset)
    private readonly sharedAssetRepository: Repository<SharedAsset>,
    @Inject(TransactionService)
    private readonly transactionService: TransactionService,
  ) {}

  private async retrieveAssets(userId: number) {
    return await this.assetRepository.find({ createdById: userId });
  }

  private months = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
  ];

  async retrieveListOfYears(id: number): Promise<any> {
    const assets = (await this.retrieveAssets(id)).map((item) => {
      const date = dayjs(item.updatedAt).format('DD/MM/YYYY').split('/');
      return date[2];
    });
    return assets;
  }

  async retrieveTotalAssets(start: any, end: any, id: number) {
    const assets = await this.assetRepository.find({
      where: { createdAt: Between(start, end), createdById: id },
    });
    return assets.length;
  }

  async retrieveTotalSharedAssetsByYear(start: any, end: any, id: number) {
    const sharedAssets = await this.sharedAssetRepository.find({
      where: { createdAt: Between(start, end), userId: id },
    });
    const transactions = await this.transactionService.retrieveByUserId(id);
    const response = [];
    response.push({
      total: sharedAssets.length,
      unsold: sharedAssets
        .map((item) => {
          let count = 0;
          transactions.forEach((elem) => {
            if (item.id === elem.sharedAssetId && elem.points === null) {
              return (count = count + 1);
            }
          });
          return count;
        })
        .reduce((acc, item) => {
          return acc + item;
        }),
      sold: sharedAssets
        .map((item) => {
          let count = 0;
          transactions.forEach((elem) => {
            if (item.id === elem.sharedAssetId && elem.points != null) {
              return (count = count + 1);
            }
          });
          return count;
        })
        .reduce((acc, item) => {
          return acc + item;
        }),
      totalUnique: sharedAssets
        .map((item: any) => item['assetId'])
        .filter((val, index, arr) => {
          return arr.indexOf(val) === index;
        }).length,
    });
    return response;
  }

  async retrieveMonthSharedAssets(date: any, id: number) {
    const sharedAssets = await this.sharedAssetRepository.find({ userId: id });
    const response: any = [];
    const transaction = await this.transactionService.retrieveByUserId(id);
    this.months.forEach((el: string) => {
      response.push({
        month: el,
        totalshared: sharedAssets
          .map((item) => {
            return (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date)
            );
          })
          .filter((val: any) => {
            return val === true;
          }).length,
        totalsold: sharedAssets
          .map((item) => {
            return (
              dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
              dayjs(item.createdAt).year() === Number.parseInt(date) &&
              item.points !== 0
            );
          })
          .filter((val: any) => {
            return val === true;
          }).length,
        totalPoint: sharedAssets
          .map((item) => {
            let count = 0;
            transaction.forEach((elem) => {
              if (
                dayjs(item.createdAt).month() + 1 === Number.parseInt(el) &&
                dayjs(item.createdAt).year() === Number.parseInt(date) &&
                elem.sharedAssetId === item.id
              ) {
                if (elem.points != null) {
                  return (count = count + elem.points);
                }
              }
            });
            return count;
          })
          .reduce((acc, item) => {
            return acc + item;
          }),
      });
    });

    return response;
  }

  async retrieveTotalSharedAssets(id: number) {
    const sharedAssets = await this.sharedAssetRepository.find({
      userId: id,
    });
    const response = [];
    response.push({
      total: sharedAssets.length,
      sold: sharedAssets
        .map((item) => item.points !== null)
        .filter((val: any) => {
          return val === true;
        }).length,
      totalPoint: sharedAssets
        .map((item) => {
          return item.points;
        })
        .reduce((acc, item) => {
          return acc + item;
        }, 0),
    });
    return response;
  }
}
