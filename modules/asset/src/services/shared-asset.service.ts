import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateSharedAssetDTO } from '@suite5/asset/dto';
import { In, Repository } from 'typeorm';
import { SharedAsset } from '@suite5/asset/entities';

@Injectable()
export class SharedAssetService {
  constructor(
    @InjectRepository(SharedAsset)
    private readonly repository: Repository<SharedAsset>,
  ) {}

  async create(data: CreateSharedAssetDTO, uid: string): Promise<SharedAsset> {
    const sharedAsset = await this.repository.create({ ...data, uid: uid });
    return await this.repository.save(sharedAsset);
  }

  async update(assetId: number): Promise<SharedAsset[]> {
    const updateData: Partial<SharedAsset> = { lastUpdatedAt: new Date() };
    const sharedAssets = await this.repository.find({ assetId });
    for (const sharedAsset of sharedAssets) {
      await this.repository.merge(sharedAsset, updateData);
    }
    return await this.repository.save(sharedAssets);
  }

  async retrieveById(id: number, graceful = false): Promise<SharedAsset> {
    if (graceful) return await this.repository.findOne({ id });
    return await this.repository.findOneOrFail({ id });
  }

  async retrieveByUid(uid: string, graceful = false): Promise<SharedAsset> {
    if (graceful) return await this.repository.findOne({ uid });
    return await this.repository.findOneOrFail({ uid });
  }

  async retrieveByAssetId(
    assetId: number,
    graceful = false,
  ): Promise<SharedAsset> {
    if (graceful) return await this.repository.findOne({ assetId });
    return await this.repository.findOneOrFail({ assetId });
  }

  async retrieveByUserId(userId: number): Promise<SharedAsset[]> {
    return await this.repository.find({ userId });
  }

  async remove(sharedAsset: SharedAsset): Promise<void> {
    await this.repository.delete(sharedAsset);
  }

  async retrieveByIds(ids: number[]): Promise<SharedAsset[]> {
    return await this.repository.find({ id: In(ids) });
  }

  async updatePoints(assetId: number, points: any): Promise<void> {
    const sharedAssets = await this.repository.find({ assetId });
    for (const sharedAsset of sharedAssets) {
      await this.repository.merge(sharedAsset, { points: points });
      await this.repository.save(sharedAssets);
    }
  }
}
