import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { TransactionMessageBody } from '@suite5/asset/interfaces';
import { DataFetcherService } from '@suite5/asset/services/data-fetcher.service';
import { UserService } from '@suite5/core/user';
import { SharedAssetService } from './shared-asset.service';
import { TransactionService } from './transaction.service';
import { v4 as uuidv4 } from 'uuid';
import { WalletStrategy, WALLET_STRATEGY_TOKEN } from '@suite5/wallet';

@Injectable()
export class TransactionMessagingService {
  constructor(
    @Inject(Logger)
    private readonly logger: Logger,
    @Inject(DataFetcherService)
    private readonly service: DataFetcherService,
    @Inject(SharedAssetService)
    private readonly sharedAssetservice: SharedAssetService,
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(TransactionService)
    private readonly transactionService: TransactionService,
    @Inject(WALLET_STRATEGY_TOKEN)
    private readonly waletStrategy: WalletStrategy,
  ) {}

  @RabbitSubscribe({
    exchange: 'amq.direct',
    routingKey: 'transactions',
    queue: 'transactions',
  })
  async transaction(msg: TransactionMessageBody) {
    const user = await this.userService.retrieveBySub(
      msg.dataownerID as string,
      true,
    );
    if (!user) {
      this.logger.error(
        `User with keycloak id "${msg.dataownerID} does not exist.`,
      );
      return;
    }

    const sharedAsset = await this.sharedAssetservice.retrieveByUid(
      msg.assetId,
    );

    if (!sharedAsset) {
      this.logger.error(`Asset with uid "${msg.assetId} does not exist.`);
      return;
    }
    try {
      await this.transactionService.create({
        buyerUUID: msg.userID,
        buyerName: msg.dataSeekerUsername,
        buyerDescription: msg.assetDescription,
        buyerType: msg.type,
        timestamp: msg.date as any,
        hash: uuidv4(),
        sharedAssetId: sharedAsset.id,
        userId: user.id,
        points: msg.price,
      });
      await this.sharedAssetservice.updatePoints(sharedAsset.id, msg.price);
    } catch (e) {
      this.logger.error(
        `[Transaction RabbitMQ] Error processing update for transaction ${
          msg.assetId
        } for user with keycloak id '${
          user.sub
        }', and with error : ${JSON.stringify(e)}`,
      );
    }
  }
}
