import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AnonymiserService } from '@suite5/anonymiser/services';
import { Repository } from 'typeorm';
import {
  SharingConfiguration,
  SharingConfigurationService,
  UpdateSharingConfigurationDTO,
} from '@suite5/asset';

describe('SharingConfigurationService', () => {
  let service: SharingConfigurationService;
  let repository: Repository<SharingConfiguration>;
  let sharingConfiguration: SharingConfiguration;

  beforeEach(async () => {
    sharingConfiguration = new SharingConfiguration();
    sharingConfiguration.name = 'test';
    sharingConfiguration.userId = 1;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SharingConfigurationService,
        {
          provide: getRepositoryToken(SharingConfiguration),
          useClass: Repository,
        },
        {
          provide: AnonymiserService,
          useValue: {
            createPseudoID: () => jest.fn(),
          },
        },
      ],
    }).compile();
    service = module.get<SharingConfigurationService>(
      SharingConfigurationService,
    );
    repository = module.get<Repository<SharingConfiguration>>(
      getRepositoryToken(SharingConfiguration),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });

  it('should create a sharing configuration', async () => {
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(sharingConfiguration);
    expect(await service.create(sharingConfiguration)).toBe(
      sharingConfiguration,
    );
  });

  it('should retrieve a sharing configuration', async () => {
    jest
      .spyOn(repository, 'findOneOrFail')
      .mockResolvedValue(sharingConfiguration);
    expect(await service.retrieveById(1)).toEqual(sharingConfiguration);
  });

  it('should retrieve all sharing configurations of a user', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([sharingConfiguration]);
    expect(await service.retrieveByUserId(1)).toEqual([sharingConfiguration]);
  });

  it('should update a sharing configuration', async () => {
    const data: UpdateSharingConfigurationDTO = {
      name: 'test',
      configuration: null,
    };
    jest
      .spyOn(repository, 'findOneOrFail')
      .mockResolvedValue(sharingConfiguration);
    jest.spyOn(repository, 'merge').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(sharingConfiguration);
    expect(await service.update(1, data)).toEqual(sharingConfiguration);
  });

  it('should delete a sharing configuration', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();
    expect(await service.remove(sharingConfiguration)).toBeUndefined();
  });
});
