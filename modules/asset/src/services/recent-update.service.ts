import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRecentUpdateDTO, UpdateRecentUpdateDTO } from '../dto';
import { RecentUpdate } from '../entities';

@Injectable()
export class RecentUpdateService {
  constructor(
    @InjectRepository(RecentUpdate)
    private readonly recentUpdateRepository: Repository<RecentUpdate>,
  ) {}

  async createRecentUpdate(
    recentUpdateData: CreateRecentUpdateDTO,
  ): Promise<RecentUpdate> {
    const recentUpdate = this.recentUpdateRepository.create(recentUpdateData);
    return await this.recentUpdateRepository.save(recentUpdate);
  }

  async retrieveRecentUpdatesByAssetId(
    assetId: number,
  ): Promise<RecentUpdate[]> {
    return await this.recentUpdateRepository.find({ assetId });
  }

  async retrieveRecentUpdate(id: number): Promise<RecentUpdate> {
    return await this.recentUpdateRepository.findOneOrFail({ id });
  }

  async updateRecentUpdate(
    id: number,
    recentUpdateData: UpdateRecentUpdateDTO,
  ): Promise<RecentUpdate> {
    const recentUpdate = await this.recentUpdateRepository.findOneOrFail({
      id,
    });
    this.recentUpdateRepository.merge(recentUpdate, recentUpdateData);

    return await this.recentUpdateRepository.save(recentUpdate);
  }

  async deleteRecentUpdate(id: number): Promise<void> {
    await this.recentUpdateRepository.delete(id);
  }
}
