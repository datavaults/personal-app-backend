import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Asset } from '@suite5/asset/entities';
import { DataFetcherRabbitmqResponse } from '@suite5/asset/interfaces';
import { DataFetcherService } from '@suite5/asset/services/data-fetcher.service';
import { MongoService } from '@suite5/common';
import { Events } from '@suite5/constants/events';
import { UserService } from '@suite5/core/user';
import { VaultService } from '@suite5/vault';
import mongoose, { Connection } from 'mongoose';
import { SourceType } from '../constants';

@Injectable()
export class DataFetcherMessagingService {
  constructor(
    @Inject(MongoService)
    private readonly mongoService: MongoService,
    @Inject(Logger)
    private readonly logger: Logger,
    @Inject(DataFetcherService)
    private readonly service: DataFetcherService,
    @Inject(UserService)
    private readonly userService: UserService,
    private readonly eventEmitter: EventEmitter2,
    private readonly configService: ConfigService,
    private readonly vaultService: VaultService,
  ) {}

  @RabbitSubscribe({
    exchange: 'amq.direct',
    routingKey: 'datavaults.fetcher',
    queue: 'datavaults.fetcher',
  })
  async dataFetched(msg: DataFetcherRabbitmqResponse) {
    if (msg.status !== HttpStatus.OK) {
      if (msg.errors.length > 0) {
        this.logger.error(
          `[DataFetcher RabbitMQ] ${msg.status} - ${msg.errors.join(', ')} - ${
            msg.body.asset
          }`,
        );
      } else {
        this.logger.error(
          `[DataFetcher RabbitMQ] ${msg.status} - A generic error has occurred.`,
        );
      }
      return;
    }

    const asset: Asset = await this.service.retrieveAssetByUid(
      msg.body.asset,
      true,
    );

    if (!asset) {
      this.logger.error(
        `Asset with uid "${msg.body.asset} does not exist. Skipping update in mongo`,
      );
      return;
    }
    if (asset.sourceType.toUpperCase() === SourceType.Andaman7.toUpperCase()) {
      if (msg.body.data.data.length) {
        for (let i = 0; i < msg.body.data.data.length; i++) {
          msg.body.data.data[i].originalId = msg.body.data.data[i].id;
        }
      } else {
        msg.body.data.data.originalId = msg.body.data.data.id;
      }
    }

    if (asset.sourceType.toUpperCase() === SourceType.MobileApp.toUpperCase()) {
      console.log('Mobile Data from Fetcher');
      console.log(msg.body.data);
    }

    const user = await this.userService.retrieveBySub(msg.user as string, true);
    if (!user) {
      this.logger.error(
        `User with keycloak id "${msg.user} does not exist. Skipping update in mongo`,
      );
      return;
    }
    try {
      // const params = [
      //   [
      //     user.id,
      //     `${msg.body.asset}-data`,
      //     { email: String },
      //     msg.body.data.data,
      //     msg.append,
      //   ],
      //   [user.id, `${msg.body.asset}-metadata`, {}, msg.body.metadata, false],
      //   [
      //     user.id,
      //     `${msg.body.asset}-dataSchema`,
      //     {},
      //     msg.body.dataSchema,
      //     false,
      //   ],
      // ];
      const credentials = await this.vaultService.getSecret(
        'user',
        `${user.id}`,
        'mongo',
        this.configService.get<string>('vault.vaultRootToken'),
      );

      const mongoUri = `mongodb://${credentials.name}:${
        credentials.password
      }@${this.configService.get<string>(
        'mongo.dbHost',
      )}:${this.configService.get<string>('mongo.dbPort')}/${credentials.name}`;

      // await this.mongoService.runWithConnection(
      //   this.service.replaceCollection,
      //   params,
      //   mongoUri,
      // );

      let mongoConnection: Connection;
      try {
        mongoConnection = await mongoose.createConnection(mongoUri, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        });
      } catch (e) {
        console.log(`Mongo connection error ${e}`);
        this.logger.error(`[DataFetcher RabbitMQ] Mongo Connection  : ${e}`);
      }
      if (msg.body.data.file !== undefined) {
        await this.service.replaceCollection(
          mongoConnection,
          user.id,
          `${msg.body.asset}-data`,
          { email: String },
          msg.body.data,
          msg.append,
        );
      } else {
        const data = await this.service.replaceCollection(
          mongoConnection,
          user.id,
          `${msg.body.asset}-data`,
          { email: String },
          msg.body.data.data,
          msg.append,
        );
      }
      await this.service.replaceCollection(
        mongoConnection,
        user.id,
        `${msg.body.asset}-metadata`,
        {},
        msg.body.metadata,
        false,
      );
      await this.service.replaceCollection(
        mongoConnection,
        user.id,
        `${msg.body.asset}-dataSchema`,
        {},
        msg.body.dataSchema,
        false,
      );

      await this.service.setAssetDataUpdated(asset);
      // Trigger event to update shared asset
      this.eventEmitter.emit(Events.UpdateSharedAsset, msg.body, user, asset);
    } catch (e) {
      this.logger.error(
        `[DataFetcher RabbitMQ] Error processing update for asset ${
          msg.body.asset
        } for user with keycloak id '${
          msg.user
        }', and with error : ${JSON.stringify(e)}`,
      );
    }
  }
}
