import { Injectable } from '@nestjs/common';
import { Readable } from 'stream';

@Injectable()
export class DownloadService {
  async zippify(zip: any, buffer: any, filename = 'file', extension = 'json') {
    zip = zip.file(`${filename}.${extension}`, buffer, { binary: true });
    return zip;
  }

  async buffer(zip) {
    return await zip.generateAsync({
      type: 'nodebuffer',
      compression: 'DEFLATE',
    });
  }

  async pushResponse(buffer, response) {
    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);
    stream.pipe(response);
  }
}
