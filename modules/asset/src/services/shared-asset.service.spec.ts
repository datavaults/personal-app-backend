import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SharedAsset, SharedAssetService } from '@suite5/asset';
import { AnonymiserService } from '@suite5/anonymiser';

describe('SharedAssetService', () => {
  let service: SharedAssetService;
  let repository: Repository<SharedAsset>;
  let sharedAsset: any;

  beforeEach(async () => {
    sharedAsset = new SharedAsset();
    sharedAsset.name = 'test';
    sharedAsset.userId = 1;
    sharedAsset.assetId = 1;
    sharedAsset.lastUpdatedAt = new Date();
    sharedAsset.assetUUID = sharedAsset.uid;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SharedAssetService,
        {
          provide: getRepositoryToken(SharedAsset),
          useClass: Repository,
        },
        {
          provide: AnonymiserService,
          useValue: {
            createPseudoID: () => jest.fn(),
          },
        },
      ],
    }).compile();
    service = module.get<SharedAssetService>(SharedAssetService);
    repository = module.get<Repository<SharedAsset>>(
      getRepositoryToken(SharedAsset),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });

  it('should create a shared asset', async () => {
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(sharedAsset);
    expect(await service.create(sharedAsset, 'abcd')).toBe(sharedAsset);
  });

  it('should retrieve a shared asset', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(sharedAsset);
    expect(await service.retrieveById(1)).toEqual(sharedAsset);
  });

  it('should retrieve a shared asset by its asset id', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(sharedAsset);
    expect(await service.retrieveByAssetId(1)).toEqual(sharedAsset);
  });

  it('should retrieve all shared assets of a user', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([sharedAsset]);
    expect(await service.retrieveByUserId(1)).toEqual([sharedAsset]);
  });

  it('should delete a shared asset', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();
    expect(await service.remove(sharedAsset)).toBeUndefined();
  });

  it('should update a shared asset lastUpdateDate', async () => {
    const mock_date = new Date();
    jest.spyOn(repository, 'find').mockResolvedValue([sharedAsset]);
    jest.spyOn(repository, 'merge').mockImplementation((sharedAsset) => {
      sharedAsset.lastUpdatedAt = mock_date;
      return sharedAsset;
    });
    jest
      .spyOn(repository, 'save')
      .mockResolvedValue(Promise.resolve(sharedAsset));
    expect(await service.update(1)).toEqual(sharedAsset);
  });
  
});
