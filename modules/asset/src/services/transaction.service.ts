import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as R from 'ramda';
import { CreateTransactionDTO } from '../dto';
import { Repository } from 'typeorm';
import { Transaction } from '../entities';
import { UpdateTransactionDTO } from '../dto/update-transaction.dto';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private readonly repository: Repository<Transaction>,
  ) {}

  async create(data: CreateTransactionDTO): Promise<Transaction> {
    const transaction = this.repository.create(data);
    return await this.repository.save(transaction);
  }

  async update(data: UpdateTransactionDTO): Promise<Transaction> {
    const transaction = await this.repository.findOne({
      sharedAssetId: data.sharedAssetId,
    });
    this.repository.merge(transaction, data);

    return await this.repository.save(transaction);
  }

  async retrieveById(id: number): Promise<Transaction> {
    return await this.repository.findOneOrFail({ id });
  }

  async retrieveByUserId(userId: number): Promise<Transaction[]> {
    return await this.repository.find({ userId });
  }

  async retrieveByUserIdAndSharedAsset(
    userId: number,
    sharedAssetId: number,
    graceful = false,
  ): Promise<Transaction> {
    if (graceful)
      return await this.repository.findOne({ userId, sharedAssetId });
    return await this.repository.findOneOrFail({ userId, sharedAssetId });
  }

  async remove(Transaction: Transaction): Promise<void> {
    await this.repository.delete(Transaction);
  }

  async getTotalUserPoints(id: number): Promise<number> {
    const { sum: totalPointsString } = await this.repository
      .createQueryBuilder('transaction')
      .select('SUM(transaction.points)', 'sum')
      .where('transaction.userId = :id', { id })
      .getRawOne();

    return Number(totalPointsString);
  }

  async getTotalMonthPoints(id: number, month: any, year: any) {
    const startDate = new Date(year + '-' + month + '-01');
    const endDate = new Date(year + '-' + month + '-31');
    const { sum: totalPointsString } = await this.repository
      .createQueryBuilder('transaction')
      .select('SUM(transaction.points)', 'sum')
      .where('transaction.sharedAssetId = :id', { id })
      .andWhere('transaction.createdAt > :startDate', { startDate: startDate })
      .andWhere('transaction.createdAt < :endDate', { endDate: endDate })
      .getRawOne();
    // console.log('Number Transactions', totalPointsString);

    return Number(totalPointsString);
  }
}
