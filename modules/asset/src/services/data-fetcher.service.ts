import { ForbiddenException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Asset } from '@suite5/asset/entities';
import mongoose, { Connection } from 'mongoose';
import { Repository } from 'typeorm';
import { CreateAssetDTO, UpdateAssetDTO } from '../dto';
import { SharedAssetService } from './shared-asset.service';
import { RecentUpdate } from '../entities/recent-update.entity';
import { CreateRecentUpdateDTO } from '../dto/create-recent-update.dto';
import * as R from 'ramda';
import { SourceType } from '../constants';
@Injectable()
export class DataFetcherService {
  constructor(
    @InjectRepository(Asset)
    private readonly assetRepository: Repository<Asset>,
    @InjectRepository(RecentUpdate)
    private readonly recentUpdateRepository: Repository<RecentUpdate>,
    private readonly configService: ConfigService,
    private readonly sharedAssetService: SharedAssetService,
  ) {}

  async addDocument(
    mongoConnection: Connection,
    userId: number,
    collection: string,
    schema: any,
    data: any,
  ): Promise<void> {
    // console.log(`Adding document to collection: ${collection}`);
    const userDB = await mongoConnection.useDb(`user${userId}`);
    const sourceSchema = new mongoose.Schema(schema, { strict: false });
    const Asset = userDB.model(collection, sourceSchema, collection);
    await Asset.create(data);
  }

  async replaceCollection(
    mongoConnection: Connection,
    userId: number,
    collection: string,
    schema: any,
    data: any,
    append: boolean,
  ): Promise<void> {
    // console.log(`Replacing collection: ${collection}`);
    const userDB = await mongoConnection.useDb(`user${userId}`);
    const sourceSchema = new mongoose.Schema(schema, { strict: false });
    const asset = await userDB.model(collection, sourceSchema, collection);
    if (!asset)
      throw Error(
        `Collection ${collection} not found in mongo under user ${userId}`,
      );
    if (!append) {
      await asset.deleteMany({}, async () => {
        await asset.create(data);
      });
    } else {
      await asset.create(data);
    }
  }

  private async getMongoModel(
    db: any,
    collection: string,
  ): Promise<mongoose.Model<any>> {
    return await db.model(collection, new mongoose.Schema(), collection);
  }

  async readMongoAsset(
    mongoConnection: Connection,
    userId: number,
    assetUid: string,
    offset?: number,
    limit?: number,
  ): Promise<{
    data: any[] | any;
    metadata: any;
    dataSchema: any;
    dataSize: number;
    metadataSize: number;
    dataSchemaSize: number;
    total: number;
  }> {
    const database = `user${userId}`;
    const userDB = mongoConnection.useDb(database);
    const dataCollection = `${assetUid}-data`;
    const metadataCollection = `${assetUid}-metadata`;
    const dataSchemaCollection = `${assetUid}-dataSchema`;

    const dataModel = await this.getMongoModel(userDB, dataCollection);
    const metadataModel = await this.getMongoModel(userDB, metadataCollection);
    const dataSchemaModel = await this.getMongoModel(
      userDB,
      dataSchemaCollection,
    );

    const dataSize = (await dataModel.collection.stats()).size;
    const metadataSize = (await metadataModel.collection.stats()).size;
    const dataSchemaSize = (await dataSchemaModel.collection.stats()).size;

    const dataEntries =
      !R.isNil(limit) && !R.isNil(offset)
        ? await dataModel.find().limit(limit).skip(offset)
        : await dataModel.find();

    const data = dataEntries.map((model: { _doc: any }) =>
      R.omit(['__v', '_id'], model._doc),
    );

    let total = data.length;
    if (!R.isNil(limit)) {
      total = await dataModel.countDocuments();
    }

    const metadata = (await metadataModel.find()).map((model: { _doc: any }) =>
      R.omit(['__v', '_id'], model._doc),
    );
    const dataSchema = (await dataSchemaModel.find()).map(
      (model: { _doc: any }) => R.omit(['__v', '_id'], model._doc),
    );

    return {
      data,
      metadata:
        !R.isNil(metadata) && R.is(Array, metadata) && metadata.length === 1
          ? metadata[0]
          : metadata,
      dataSchema,
      dataSize,
      metadataSize,
      dataSchemaSize,
      total,
    };
  }

  async createAsset(
    sourceData: CreateAssetDTO,
    sourceId: string,
  ): Promise<Asset> {
    sourceData.uid = sourceId;
    const source = this.assetRepository.create(sourceData);
    return await this.assetRepository.save(source);
  }

  async retrieveByUserId(userId: number): Promise<Asset[]> {
    const sharedAssetIds = (
      await this.sharedAssetService.retrieveByUserId(userId)
    ).map((asset: any) => asset.assetId);
    const assets = await this.assetRepository.find({ createdById: userId });
    assets.forEach((asset: any) => {
      asset['shared'] = sharedAssetIds.includes(asset.id);
    });
    return assets;
  }

  async retrieveAsset(
    mongoConnection: Connection,
    id: number,
    userId: number,
    offset?: number,
    limit?: number,
  ): Promise<any> {
    const asset = await this.assetRepository.findOneOrFail({ id });
    try {
      const { data, metadata, dataSize, metadataSize, dataSchemaSize, total } =
        await this.readMongoAsset(
          mongoConnection,
          userId,
          asset.uid,
          offset,
          limit,
        );
      if (asset.sourceType === SourceType.Andaman7) {
        data.forEach((value: any, idx: any) => {
          data[idx].id = value.originalId;
        });
      }
      asset['data'] = data;
      asset['dataSize'] = dataSize;
      asset['metadata'] = metadata;
      asset['metadataSize'] = metadataSize;
      asset['dataSchemaSize'] = dataSchemaSize;
      asset['total'] = total;
    } catch (error) {
      console.log('-----------Retrieve Asset ----------------');
      console.log(error);
      // do nothing
    }
    let shared = false;
    try {
      await this.sharedAssetService.retrieveByAssetId(asset.id);
      shared = true;
    } catch {
      // do nothing
    }
    asset['shared'] = shared;
    return asset;
  }

  async retrieveAssetData(
    mongoConnection: Connection,
    id: number,
    userId: number,
  ): Promise<any> {
    const asset = await this.assetRepository.findOneOrFail({ id });
    if (asset.createdById !== userId) throw new ForbiddenException();
    const { data } = await this.readMongoAsset(
      mongoConnection,
      userId,
      asset.uid,
    );
    if (asset.sourceType === SourceType.Andaman7) {
      data.forEach((value: any, idx: any) => {
        data[idx].id = value.originalId;
      });
    }
    return data;
  }

  async retrieveAssetMetadata(
    mongoConnection: Connection,
    id: number,
    userId: number,
  ): Promise<any> {
    const asset = await this.assetRepository.findOneOrFail({ id });
    if (asset.createdById !== userId) throw new ForbiddenException();
    const { metadata } = await this.readMongoAsset(
      mongoConnection,
      userId,
      asset.uid,
    );
    return metadata;
  }

  async retrieveAssetDataSchema(
    mongoConnection: Connection,
    id: number,
    userId: number,
  ): Promise<any> {
    const asset = await this.assetRepository.findOneOrFail({ id });
    if (asset.createdById !== userId) throw new ForbiddenException();
    const { dataSchema } = await this.readMongoAsset(
      mongoConnection,
      userId,
      asset.uid,
    );
    return dataSchema;
  }

  async retrieveAssetByUid(
    uid: string,
    graceful: boolean = false,
  ): Promise<Asset> {
    if (graceful) return await this.assetRepository.findOne({ uid });

    return await this.assetRepository.findOneOrFail({ uid });
  }

  async retrieveAssetById(id: number): Promise<Asset> {
    return await this.assetRepository.findOneOrFail({ id });
  }

  async updateAsset(id: number, sourceData: UpdateAssetDTO): Promise<Asset> {
    const source = await this.assetRepository.findOneOrFail({ id });
    this.assetRepository.merge(source, sourceData);

    return await this.assetRepository.save(source);
  }

  async deleteAsset(id: number): Promise<void> {
    await this.assetRepository.delete(id);
  }

  async setAssetDataUpdated(asset: Asset): Promise<Asset> {
    // setting first collection if this is the first time
    if (!asset.firstCollection) asset.firstCollection = new Date();
    asset.updatedAt = new Date();
    const createAsset = await this.assetRepository.save(asset);
    const recentUpdate: CreateRecentUpdateDTO = {
      name: asset.name,
      lastDate: asset.updatedAt,
      successful: true,
      assetId: createAsset.id,
    };
    await this.recentUpdateRepository.save(recentUpdate);
    return createAsset;
  }

  async assetEnabled(id: number, enabled: boolean) {
    const source = await this.assetRepository.findOneOrFail({
      id: id,
    });
    await this.assetRepository.merge(source, { status: enabled });
    await this.assetRepository.save(source);
    return true;
  }
}
