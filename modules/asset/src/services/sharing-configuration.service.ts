import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  CreateSharingConfigurationDTO,
  UpdateSharingConfigurationDTO,
} from '../dto';
import { SharingConfiguration } from '../entities';

@Injectable()
export class SharingConfigurationService {
  constructor(
    @InjectRepository(SharingConfiguration)
    private readonly repository: Repository<SharingConfiguration>,
  ) {}

  async create(
    configuration: CreateSharingConfigurationDTO,
  ): Promise<SharingConfiguration> {
    const sharingConfiguration = this.repository.create(configuration);
    return await this.repository.save(sharingConfiguration);
  }

  async retrieveById(id: number): Promise<SharingConfiguration> {
    return await this.repository.findOneOrFail({ id });
  }

  async retrieveByUserId(userId: number): Promise<SharingConfiguration[]> {
    return await this.repository.find({ userId });
  }

  async update(
    id: number,
    data: UpdateSharingConfigurationDTO,
  ): Promise<SharingConfiguration> {
    const sharingConfiguration = await this.repository.findOneOrFail({ id });
    this.repository.merge(sharingConfiguration, data);
    return await this.repository.save(sharingConfiguration);
  }

  async remove(sharingConfiguration: SharingConfiguration): Promise<void> {
    await this.repository.delete(sharingConfiguration);
  }
}
