import { SharedAsset } from '@suite5/asset';
import { AssetStatsService } from './asset-stats.service';

describe('UserAssetService', () => {
  let service: AssetStatsService;
  let mockAssetRepository: any;
  let mockSharedAssetRepository: any;
  let mockTransactionService: any;

  beforeEach(async () => {
    mockAssetRepository = {
      find: jest.fn(),
    };

    mockSharedAssetRepository = {
      find: jest.fn(),
    };

    mockTransactionService = {
      retrieveByUserId: jest.fn(),
    }

    service = new AssetStatsService(
      mockAssetRepository,
      mockSharedAssetRepository,
      mockTransactionService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should retrieve user total assets', async () => {
    const asset = [
      {
        id: 1,
        name: 'Test',
        sourceType: 'Upload',
        createdById: 1,
        uid: '8451be53-2a0b-42fc-b441-e66e8522a1ce',
        type: 'dataset',
        firstCollection: '19/11/2021',
        createdAt: '2022-03-23 16:18:48.411',
      },
      {
        id: 1,
        name: 'Test 1',
        sourceType: 'Upload 1',
        createdById: 1,
        uid: '8451be53-2a0b-42fc-b441-e66e8522a1ce',
        type: 'dataset',
        firstCollection: '19/11/2021',
        createdAt: '2022-03-23 16:18:48.411',
      },
    ];
    jest.spyOn(mockAssetRepository, 'find').mockReturnValue(asset);
    expect(await service.retrieveTotalAssets).toBeTruthy();
  });

  it('should retrieve user total shared assets', async () => {
    const sharedAsset = [new SharedAsset(), new SharedAsset()];
    sharedAsset[0].name = 'test';
    sharedAsset[0].userId = 1;
    sharedAsset[0].assetId = 1;
    sharedAsset[0].points = 1;
    sharedAsset[1].name = 'test3';
    sharedAsset[1].userId = 1;
    sharedAsset[1].assetId = 2;
    sharedAsset[1].points = 2;
    jest.spyOn(mockSharedAssetRepository, 'find').mockReturnValue(sharedAsset);

    expect((await service.retrieveTotalSharedAssets(1))[0].totalPoint).toBe(3);
  });

  it('should retrieve user 0 total shared assets if none shared', async () => {
    jest.spyOn(mockSharedAssetRepository, 'find').mockReturnValue([]);
    expect((await service.retrieveTotalSharedAssets(1))[0].totalPoint).toBe(0);
  });

  it('should retrieve user total shared assets per month', async () => {
    const sharedAsset = [new SharedAsset(), new SharedAsset()];
    sharedAsset[0].name = 'test';
    sharedAsset[0].userId = 1;
    sharedAsset[0].assetId = 1;
    sharedAsset[0].points = 1;
    jest.spyOn(mockSharedAssetRepository, 'find').mockReturnValue(sharedAsset);
    const response = [
      { month: '01', total: 0, totalPoint: 0 },
      { month: '02', total: 0, totalPoint: 0 },
      { month: '03', total: 0, totalPoint: 0 },
      { month: '04', total: 0, totalPoint: 0 },
      { month: '05', total: 2, totalPoint: 1 },
      { month: '06', total: 0, totalPoint: 0 },
      { month: '07', total: 0, totalPoint: 0 },
      { month: '08', total: 0, totalPoint: 0 },
      { month: '09', total: 0, totalPoint: 0 },
      { month: '10', total: 0, totalPoint: 0 },
      { month: '11', total: 0, totalPoint: 0 },
      { month: '12', total: 0, totalPoint: 0 },
    ];
    expect(await service.retrieveMonthSharedAssets).toBeTruthy();
  });
});
