import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Asset, DataFetcherService, SharedAsset } from '@suite5/asset';
import { SharedAssetService } from '@suite5/asset/services/shared-asset.service';
import mongoose from 'mongoose';
import { Repository } from 'typeorm';
import { CreateAssetDTO, UpdateAssetDTO } from '../dto';
import { SourceType } from '../constants';
import { RecentUpdate } from '../entities/recent-update.entity';
import { ForbiddenException } from '@nestjs/common';

describe('DataFetcherService', () => {
  let service: DataFetcherService;
  let sharedAssetService: SharedAssetService;
  let repository: Repository<Asset>;
  let source: Asset;
  let mongoConnection: any;
  let recentUpdateRepository: Repository<RecentUpdate>;

  beforeEach(async () => {
    mongoConnection = jest.fn();
    source = new Asset();
    source.createdById = 1;
    source.uid = 'test';

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: SharedAssetService,
          useValue: {
            retrieveByUserId: jest.fn(),
            retrieveByAssetId: jest.fn(),
          },
        },
        DataFetcherService,
        ConfigService,
        {
          provide: getRepositoryToken(Asset),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(RecentUpdate),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(SharedAsset),
          useClass: Repository,
        },
        {
          provide: 'MONGO_CONNECTION',
          useClass: mongoose.Connection,
        },
      ],
    }).compile();
    service = module.get<DataFetcherService>(DataFetcherService);
    sharedAssetService = module.get<SharedAssetService>(SharedAssetService);
    repository = module.get<Repository<Asset>>(getRepositoryToken(Asset));
    recentUpdateRepository = module.get<Repository<RecentUpdate>>(
      getRepositoryToken(RecentUpdate),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a source', async () => {
    const data: CreateAssetDTO = {
      name: '',
      sourceType: SourceType.Upload,
    };
    jest.spyOn(repository, 'create').mockImplementation();
    jest.spyOn(repository, 'save').mockResolvedValue(source);
    expect(await service.createAsset(data, 'testasset')).toBe(source);
  });

  it('should retrieve sources of specific user', async () => {
    jest.spyOn(repository, 'find').mockResolvedValue([source]);
    jest.spyOn(sharedAssetService, 'retrieveByUserId').mockResolvedValue([]);
    expect(await service.retrieveByUserId(1)).toEqual([source]);
  });

  it('should retrieve a source', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata: [],
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    expect(await service.retrieveAsset(mongoConnection, 1, 1)).toEqual(source);
  });

  it('should retrieve an asset by its uid', async () => {
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    expect(await service.retrieveAssetByUid('test')).toEqual(source);
  });

  it('should update a source', async () => {
    const data: UpdateAssetDTO = { name: '' };
    jest.spyOn(repository, 'findOneOrFail').mockImplementation();
    jest.spyOn(repository, 'merge').mockReturnValue(source);
    jest.spyOn(repository, 'save').mockResolvedValue(source);
    expect(await service.updateAsset(1, data)).toBe(source);
  });

  it('should delete a source', async () => {
    jest.spyOn(repository, 'delete').mockImplementation();
    await service.deleteAsset(1);
    expect(repository.delete).toBeCalledWith(1);
  });

  it('should set updated asset data', async () => {
    jest.spyOn(repository, 'save').mockResolvedValue(source);
    jest.spyOn(recentUpdateRepository, 'save').mockImplementation();
    await service.setAssetDataUpdated(source);
    expect(repository.save).toBeCalledWith(source);
    expect(recentUpdateRepository.save).toBeCalledWith({
      name: source.name,
      lastDate: expect.any(Date),
      successful: true,
      assetId: source.id,
    });
  });

  it('should retrieve the asset data from mongo', async () => {
    const data = [{ a: 1 }];
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data,
      metadata: [],
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    expect(await service.retrieveAssetData(mongoConnection, 1, 1)).toBe(data);
  });

  it('should fail to retrieve the asset data from mongo if not owner', async () => {
    source.createdById = 2;
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata: [],
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    try {
      await service.retrieveAssetData(mongoConnection, 1, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should retrieve the asset metadata from mongo', async () => {
    const metadata = [{ a: 1 }];
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata,
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    expect(await service.retrieveAssetMetadata(mongoConnection, 1, 1)).toBe(
      metadata,
    );
  });

  it('should fail to retrieve the asset metadata from mongo if not owner', async () => {
    source.createdById = 2;
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata: [],
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    try {
      await service.retrieveAssetMetadata(mongoConnection, 1, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should retrieve the asset data schema from mongo', async () => {
    const dataSchema = [{ a: 1 }];
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata: [],
      dataSchema,
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    expect(await service.retrieveAssetDataSchema(mongoConnection, 1, 1)).toBe(
      dataSchema,
    );
  });

  it('should fail to retrieve the asset data schema from mongo if not owner', async () => {
    source.createdById = 2;
    jest.spyOn(repository, 'findOneOrFail').mockResolvedValue(source);
    jest.spyOn(service, 'readMongoAsset').mockResolvedValue({
      data: [],
      metadata: [],
      dataSchema: [],
      dataSize: null,
      metadataSize: null,
      dataSchemaSize: null,
      total: 0,
    });
    try {
      await service.retrieveAssetDataSchema(mongoConnection, 1, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });
});
