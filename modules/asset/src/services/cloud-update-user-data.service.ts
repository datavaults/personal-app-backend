import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { Events } from '@suite5/constants/events';
import { SharedAssetService } from '../services/shared-asset.service';
import {
  CLOUD_STRATEGY_TOKEN,
  CloudStrategy,
} from '../strategies/cloud.strategy';
import { UserService } from '../../../core/src/user/services/user.service';
import { DataOwnerProfileDTO } from '../../../core/src/user/dto/data-owner-profile.dto';

@Injectable()
export class CloudUpdateUserDataService {
  constructor(
    private readonly sharedAssetService: SharedAssetService,
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly strategy: CloudStrategy,
    private readonly userService: UserService,
  ) {}

  @OnEvent(Events.UpdateUserProfile, { async: true })
  async updateDataOwnerProfile(updatedUser: any) {
    const updatedDataOwner: DataOwnerProfileDTO =
      await this.userService.getProfileById(updatedUser.id);

    if (updatedDataOwner.shareProfile) {
      return await this.strategy.updateDataOwnerProfile(updatedDataOwner);
    }
  }
}
