import { RabbitmqResponse } from '@suite5/common/interfaces';

export interface DataFetcherMessageBody extends RabbitmqResponse {
  asset: string;
  data: { data: any[] | any; file: any[] };
  metadata: any;
  dataSchema: any;
}
