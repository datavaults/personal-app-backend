import { RabbitmqResponse } from '@suite5/common/interfaces';

export interface TransactionMessageBody extends RabbitmqResponse {
  assetId: string;
  userID: string;
  dataownerID: string;
  assetDescription: string;
  date: string;
  type: string;
  price: any;
  dataSeekerUsername: string;
}
