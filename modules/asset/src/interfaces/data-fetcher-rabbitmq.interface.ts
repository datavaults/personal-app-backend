import { RabbitmqResponse } from '@suite5/common/interfaces';
import { DataFetcherMessageBody } from '@suite5/asset/interfaces/data-fetcher-message-body.interface';

export interface DataFetcherRabbitmqResponse extends RabbitmqResponse {
  append: boolean;
  body: DataFetcherMessageBody;
}
