import { registerAs } from '@nestjs/config';

export default registerAs('asset.module', () => ({
  cloudUrl: process.env.CLOUD_PLATFORM_BACKEND_URL || '/api/v1/dataset',
  mongoUri: `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_NAME}`,
  rabbitmqUri:
    process.env.DATA_FETCHER_RABBITMQ_URI || 'amqp://admin:admin@rabbitmq:5672',
  dataFetcherUrl: process.env.DATA_FETCHER_URL,
  dataFetcherToken: process.env.DATA_FETCHER_TOKEN,
  cloudKeycloakUrl: process.env.CLOUD_PLATFORM_KEYCLOAK_URL,
  cloudKeycloakRealm: process.env.CLOUD_PLATFORM_KEYCLOAK_REALM,
  cloudKeycloakUsername: process.env.CLOUD_PLATFORM_KEYCLOAK_USERNAME,
  cloudKeycloakPassword: process.env.CLOUD_PLATFORM_KEYCLOAK_PASSWORD,
  cloudKeycloakClientId: process.env.CLOUD_PLATFORM_KEYCLOAK_CLIENT_ID,
  cloudKeycloakClientSecret: process.env.CLOUD_PLATFORM_KEYCLOAK_CLIENT_SECRET,
  miwenergiaUrl:
    process.env.MIWENERGIA_URL || 'http://api-dv.miwenergia.com/apidv/',
  osfpUrl: process.env.OSFP_SERVER || 'https://osfp-service.datavaults.eu/',
  pratoUrl:
    process.env.PRATO_URL ||
    'https://sicrawebtest.comune.prato.it:58443/client/services/SsdWSSCertificazioneRemotaPrato',
  encryptionToken: process.env.ENCRYPTION_HASH,
}));
