export const mocked_response = {
  status: 200,
  errors: [],
  user: '',
  body: {
    asset: '',
    data: {
      data: [
        {
          picture:
            'https://scontent.fham2-1.fna.fbcdn.net/v/t1.6435-9/s130x130/197441103_110368467936005_8654989637944721957_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8024bb&_nc_ohc=DIcjMedD7zEAX_c8xyn&_nc_ht=scontent.fham2-1.fna&edm=AP4hL3IEAAAA&oh=d5618ddcd690bddcf6a50a87c735a81d&oe=6127C640',
          type: 'photo',
          status_type: 'added_photos',
          object_id: '110368464602672',
          link: 'https://www.facebook.com/photo.php?fbid=110368464602672&set=p.110368464602672&type=3',
          created_time: '2021-06-09T08:33:39+0000',
          message:
            'https://www.datavaults.eu/\n\nNam malesuada lacus in feugiat scelerisque. Etiam sit amet velit est. Nam quis arcu euismod, tempor diam nec, aliquam justo. Donec pretium, elit in vestibulum ultricies, odio lacus vehicula enim, sed dapibus ipsum erat quis nisi. Fusce dapibus sed nunc at finibus. Sed augue dolor, bibendum quis ornare sed, pellentesque ut tellus. Phasellus gravida nisi sit amet augue eleifend, in varius nulla pharetra. In hac habitasse platea dictumst. Praesent eu magna convallis, tincidunt orci at, porttitor erat. Proin posuere scelerisque tortor, non aliquam nunc hendrerit non. Mauris eget ultricies neque, id molestie justo. Duis a suscipit dui, id venenatis nulla. Nunc id tellus turpis. Pellentesque et eleifend ligula.',
          'from-name': 'Emma Alfhhheegfcdj Seligsteinsky',
          'from-id': '110359621270223',
          id: '110359621270223_110368494602669',
        },
        {
          type: 'status',
          status_type: 'mobile_status_update',
          created_time: '2021-06-09T08:32:29+0000',
          message:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas cursus egestas massa nec finibus. In pellentesque leo sem, vel cursus enim efficitur et. Vestibulum sed venenatis nisi, eget euismod velit. Ut a lectus quam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sagittis mollis lacus ac commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut molestie nisl vel odio volutpat rutrum.',
          'from-name': 'Emma Alfhhheegfcdj Seligsteinsky',
          'from-id': '110359621270223',
          id: '110359621270223_110367501269435',
        },
      ],
    },
    metadata: {
      '@context': {
        seeAlso: {
          '@id': 'http://www.w3.org/2000/01/rdf-schema#seeAlso',
          '@type': '@id',
        },
        name: 'http://xmlns.com/foaf/0.1/name',
        id: 'http://rdfs.org/sioc/ns#id',
        email: 'http://rdfs.org/sioc/ns#email',
        content: 'http://rdfs.org/sioc/ns#content',
        has_creator: {
          '@id': 'http://rdfs.org/sioc/ns#has_creator',
          '@type': '@id',
        },
        created: 'http://purl.org/dc/terms/created',
        title: 'http://purl.org/dc/terms/title',
        accessURL: {
          '@id': 'http://www.w3.org/ns/dcat#accessURL',
          '@type': '@id',
        },
        distribution: {
          '@id': 'http://www.w3.org/ns/dcat#distribution',
          '@type': '@id',
        },
        feed: {
          '@id': 'http://rdfs.org/sioc/ns#feed',
          '@type': '@id',
        },
        creator_of: {
          '@id': 'http://rdfs.org/sioc/ns#creator_of',
          '@type': '@id',
        },
        hasValueColumn: {
          '@id': 'http://www.datavaults.eu/ns/dv#hasValueColumn',
          '@type': '@id',
        },
        hasKeyColumn: {
          '@id': 'http://www.datavaults.eu/ns/dv#hasKeyColumn',
          '@type': '@id',
        },
        columnType: {
          '@id': 'http://www.datavaults.eu/ns/dv#columnType',
          '@type': '@id',
        },
        columnHeader: {
          '@id': 'http://www.datavaults.eu/ns/dv#columnHeader',
          '@type': '@id',
        },
      },
      '@graph': [
        {
          '@id': 'https://facebook.com/110359621270223',
          '@type': 'http://rdfs.org/sioc/ns#UserAccount',
          email: 'zkvvbnmwch_1623226820@tfbnw.net',
          id: '110359621270223',
          name: 'Emma Alfhhheegfcdj Seligsteinsky',
          seeAlso:
            'https://datavaults.eu/id/user/Emma_Alfhhheegfcdj_Seligsteinsky',
        },
        {
          '@id': 'https://facebook.com/110359621270223/posts/110368494602669',
          '@type': ['http://rdfs.org/sioc/ns#Post'],
          created: '2021-06-09T08:33:39+0000',
          content:
            'https://www.datavaults.eu/\n\nNam malesuada lacus in feugiat scelerisque. Etiam sit amet velit est. Nam quis arcu euismod, tempor diam nec, aliquam justo. Donec pretium, elit in vestibulum ultricies, odio lacus vehicula enim, sed dapibus ipsum erat quis nisi. Fusce dapibus sed nunc at finibus. Sed augue dolor, bibendum quis ornare sed, pellentesque ut tellus. Phasellus gravida nisi sit amet augue eleifend, in varius nulla pharetra. In hac habitasse platea dictumst. Praesent eu magna convallis, tincidunt orci at, porttitor erat. Proin posuere scelerisque tortor, non aliquam nunc hendrerit non. Mauris eget ultricies neque, id molestie justo. Duis a suscipit dui, id venenatis nulla. Nunc id tellus turpis. Pellentesque et eleifend ligula.',
          has_creator: 'https://facebook.com/110359621270223',
        },
        {
          '@id': 'https://facebook.com/110359621270223/posts/110367501269435',
          '@type': ['http://rdfs.org/sioc/ns#Post'],
          created: '2021-06-09T08:32:29+0000',
          content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas cursus egestas massa nec finibus. In pellentesque leo sem, vel cursus enim efficitur et. Vestibulum sed venenatis nisi, eget euismod velit. Ut a lectus quam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sagittis mollis lacus ac commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut molestie nisl vel odio volutpat rutrum.',
          has_creator: 'https://facebook.com/110359621270223',
        },
        {
          '@id':
            'https://datavaults.eu/set/distribution/facebook_110359621270223',
          '@type': ['http://www.w3.org/ns/dcat#Distribution'],
          accessURL: 'https://datavaults.eu/dist/fb_110359621270223',
        },
        {
          '@id': 'https://datavaults.eu/set/data/facebook_110359621270223',
          '@type': [
            'http://www.w3.org/ns/dcat#Dataset',
            'http://www.datavaults.eu/ns/dv#TabularDataSet',
          ],
          distribution:
            'https://datavaults.eu/set/distribution/facebook_110359621270223',
          creator_of: 'https://facebook.com/110359621270223',
          feed: [
            'https://facebook.com/110359621270223/posts/110368494602669',
            'https://facebook.com/110359621270223/posts/110367501269435',
          ],
          hasKeyColumn: '_:c0',
          hasValueColumn: [
            '_:c1',
            '_:c2',
            '_:c3',
            '_:c4',
            '_:c5',
            '_:c6',
            '_:c7',
            '_:c8',
            '_:c9',
          ],
        },
        {
          '@id': '_:c0',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'id',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.datavaults.eu/ns/dv#DatabaseKey',
            },
          ],
        },
        {
          '@id': '_:c1',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'message',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.datavaults.eu/ns/dv#FreeText',
            },
          ],
        },
        {
          '@id': '_:c2',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'created_time',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#dateTime',
            },
          ],
        },
        {
          '@id': '_:c3',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'picture',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#anyURL',
            },
          ],
        },
        {
          '@id': '_:c4',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'type',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#string',
            },
          ],
        },
        {
          '@id': '_:c5',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'status_type',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#string',
            },
          ],
        },
        {
          '@id': '_:c6',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'object_id',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#integer',
            },
          ],
        },
        {
          '@id': '_:c7',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'link',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#anyURL',
            },
          ],
        },
        {
          '@id': '_:c8',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'from-name',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#string',
            },
          ],
        },
        {
          '@id': '_:c9',
          '@type': ['http://www.datavaults.eu/ns/dv#Column'],
          columnHeader: [
            {
              '@value': 'from-id',
            },
          ],
          columnType: [
            {
              '@id': 'http://www.w3.org/2001/XMLSchema#integer',
            },
          ],
        },
      ],
    },
  },
};

export function mock_values(user: string, asset: string) {
  mocked_response.user = user;
  mocked_response.body.asset = asset;
  return mocked_response;
}
