const categories: { [key: string]: string } = {};

categories['TWITTER'] = 'SOCIAL_MEDIA';
categories['MIWENERGIA'] = 'ENERGY_DATA';
categories['ANDAMAN7'] = 'MEDICAL_DATA';
categories['OLYMPIAKOS'] = 'OTHER';
categories['PRATO'] = 'OTHER';
categories['MOBILEAPP_Route'] = 'ROUTE';
categories['MOBILEAPP_Location'] = 'LOCATION';
categories['MOBILEAPP_Google Fit'] = 'PHYSICAL_ACTIVITY_DATA';

export default categories;
