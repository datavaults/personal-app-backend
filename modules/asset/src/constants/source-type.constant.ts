export enum SourceType {
  Facebook = 'Facebook',
  Twitter = 'Twitter',
  Upload = 'Upload',
  Analytics = 'Analytics',
  MobileApp = 'MOBILEAPP',
  Olympiakos = 'OLYMPIAKOS',
  Questionnaire = 'Questionnaire',
  Prato = 'PRATO',
  Andaman7 = 'ANDAMAN7',
}
