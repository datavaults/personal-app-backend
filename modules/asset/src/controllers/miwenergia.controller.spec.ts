import { MIWenergiaStrategy } from '@suite5/asset';
import { MIWenergiaLoginDTO } from '../dto';
import { MIWenergiaController } from './miwenergia.controller';

describe('MIWenergiaController', () => {
  let controller: MIWenergiaController;
  let mockMIWenergiaStrategy: MIWenergiaStrategy;
  const user: MIWenergiaLoginDTO = {
    username: 'user1@miw.es',
    password: 'pw1234',
  };
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

  beforeEach(async () => {
    mockMIWenergiaStrategy = {
      login: () => jest.fn() as any,
      retrieveUspcList: () => jest.fn() as any,
    };

    controller = new MIWenergiaController(mockMIWenergiaStrategy);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should login user', async () => {
    jest.spyOn(mockMIWenergiaStrategy, 'login').mockImplementation();
    await controller.login(user);
    expect(mockMIWenergiaStrategy.login).toBeCalledTimes(1);
  });

  it('should retrieve uspc list', async () => {
    // jest.spyOn(mockMIWenergiaStrategy, 'retrieveUspcList').mockImplementation();
    // await controller.retrieve(token);
    // expect(mockMIWenergiaStrategy.retrieveUspcList).toBeCalledWith(token);
  });
});
