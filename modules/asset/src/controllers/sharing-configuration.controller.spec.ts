import { BadRequestException, ForbiddenException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AnonymiserService } from '@suite5/anonymiser/services';
import { UserData } from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { Repository } from 'typeorm';
import {
  CreateSharingConfigurationDTO,
  SharingConfiguration,
  SharingConfigurationController,
  SharingConfigurationService,
  UpdateSharingConfigurationDTO,
} from '@suite5/asset';
import { ConfigService } from '@nestjs/config';

describe('SharingConfigurationController', () => {
  let controller: SharingConfigurationController;
  let service: SharingConfigurationService;
  let sharingConfiguration: SharingConfiguration;
  const owner = {
    id: 1,
    username: 'owner@test.com',
  } as UserData;
  const intruder = {
    id: 2,
    username: 'intruder@test.com',
  } as UserData;

  beforeEach(async () => {
    sharingConfiguration = new SharingConfiguration();
    sharingConfiguration.name = 'test';
    sharingConfiguration.userId = 1;

    const module: TestingModule = await Test.createTestingModule({
      controllers: [SharingConfigurationController],
      providers: [
        {
          provide: getRepositoryToken(SharingConfiguration),
          useClass: Repository,
        },
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        SharingConfigurationService,
        {
          provide: AnonymiserService,
          useValue: {
            createPseudoID: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<SharingConfigurationController>(
      SharingConfigurationController,
    );
    service = module.get<SharingConfigurationService>(
      SharingConfigurationService,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it("should retrieve all user's sharing configurations", async () => {
    jest
      .spyOn(service, 'retrieveByUserId')
      .mockResolvedValue([sharingConfiguration]);
    expect(await controller.getAllSharingConfigurations(owner)).toStrictEqual([
      sharingConfiguration,
    ]);
  });

  it('should retrieve a sharing configuration', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    expect(await controller.getSharingConfiguration(1, owner)).toBe(
      sharingConfiguration,
    );
  });

  it('should throw on retrieve if the sharing configuration does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    try {
      await controller.getSharingConfiguration(1, intruder);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should create a sharing configuration', async () => {
    const data: CreateSharingConfigurationDTO = {
      name: 'test2',
      configuration: null,
    };
    const result: SharingConfiguration = sharingConfiguration;
    jest
      .spyOn(service, 'retrieveByUserId')
      .mockResolvedValue([sharingConfiguration]);
    jest.spyOn(service, 'create').mockResolvedValue(result);
    expect(await controller.create(owner, data)).toBe(result);
    expect(service.create).toBeCalledWith(data);
  });

  it('should throw on create if name already exists', async () => {
    const data: CreateSharingConfigurationDTO = {
      name: 'test',
      configuration: null,
    };
    jest
      .spyOn(service, 'retrieveByUserId')
      .mockResolvedValue([sharingConfiguration]);

    try {
      await controller.create(owner, data);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
    }
  });

  it('should update a sharing configuration', async () => {
    const data: UpdateSharingConfigurationDTO = {
      name: 'test',
      configuration: null,
    };
    const result: SharingConfiguration = sharingConfiguration;
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    jest.spyOn(service, 'update').mockResolvedValue(result);
    expect(await controller.update(owner, 1, data)).toBe(result);
  });

  it('should throw on update if the sharing configuration does not belong to the user', async () => {
    const data: UpdateSharingConfigurationDTO = {
      name: 'test',
      configuration: null,
    };
    const result: SharingConfiguration = sharingConfiguration;
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    try {
      await controller.update(intruder, 1, data);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should delete a sharing configuration', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    jest.spyOn(service, 'remove').mockImplementation();
    expect(await controller.remove(owner, 1)).toBeUndefined();
  });

  it('should throw on delete if the sharing configuration does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharingConfiguration);
    try {
      await controller.remove(intruder, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });
});
