import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { CreateTransactionDTO } from '@suite5/asset/dto/create-transaction.dto';
import { Transaction } from '@suite5/asset/entities/transaction.entity';
import { TransactionService } from '@suite5/asset/services/transaction.service';

@Controller('transaction')
@ApiTags('transaction')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class TransactionController {
  constructor(private readonly service: TransactionService) {}

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: "Retrieve user's transactions",
  })
  async getAllTransactions(@CurrentUser() user: UserData) {
    return this.service.retrieveByUserId(user.id);
  }

  @Get('points')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: "Retrieve user's transactions",
  })
  async getTotalPoints(@CurrentUser() user: UserData) {
    return { points: await this.service.getTotalUserPoints(user.id) };
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve a transaction',
  })
  async getTransaction(@Param('id') id: number, @CurrentUser() user: UserData) {
    const transaction = await this.service.retrieveById(id);
    if (transaction.userId !== user.id) throw new ForbiddenException();
    return transaction;
  }

  @Post()
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Create new transaction',
  })
  @ApiBody({ type: CreateTransactionDTO })
  async create(
    @CurrentUser() user: UserData,
    @Body() data: CreateTransactionDTO,
  ): Promise<Transaction> {
    data.userId = user.id;
    return this.service.create(data);
  }

  @Post('many')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Create many transactions',
  })
  @ApiBody({ type: CreateTransactionDTO })
  async createMany(
    @CurrentUser() user: UserData,
    @Body() data: CreateTransactionDTO[],
  ): Promise<void> {
    data.forEach(async (item: Transaction) => {
      item.userId = user.id;
      await this.service.create(item);
    });
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a transaction' })
  @ApiNotFoundResponse()
  async remove(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
  ): Promise<void> {
    const transaction = await this.service.retrieveById(id);
    if (transaction.userId !== user.id) throw new ForbiddenException();
    await this.service.remove(transaction);
  }
}
