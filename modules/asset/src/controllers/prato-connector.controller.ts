import { Body, Controller, Inject, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { PratoStrategy, PRATO_STRATEGY_TOKEN } from '../strategies';

@Controller('prato')
@UseGuards(AuthenticationGuard)
@ApiTags('prato')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class PratoController {
  constructor(
    @Inject(PRATO_STRATEGY_TOKEN)
    private readonly strategy: PratoStrategy,
  ) {}

  @Post('search')
  @ApiOperation({ summary: 'Search user by fiscal code' })
  @ApiNotFoundResponse()
  async searchPersonByFiscalCode(@Body() data: any): Promise<any> {
    return await this.strategy.searchPersonByFiscalCode(data);
  }
}
