import { ForbiddenException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AnonymiserService } from '@suite5/anonymiser';
import {
  CloudStrategy,
  CLOUD_STRATEGY_TOKEN,
  CreateSharedAssetDTO,
  DataFetcherService,
  SharedAsset,
  SharedAssetController,
  SharedAssetService,
  TransactionService,
} from '@suite5/asset';
import { ConfigurationAnonymasationDTO } from '@suite5/asset/dto/configuration-anonymisation.dto';
import { ConfigurationDTO } from '@suite5/asset/dto/configuration.dto';
import { UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { UserService } from '@suite5/core/user';
import { EncryptionService } from '../../../encryption/src/services';
import { VaultService } from '@suite5/vault';
import { Repository } from 'typeorm';

describe('SharedAssetController', () => {
  let controller: SharedAssetController;
  let service: any;
  let sharedAsset: SharedAsset;
  let anonymiserService: any;
  let shareStrategy: any;
  let mongoConnection: any;
  let mockEncryptionService: any;
  let mockDataFetcherService: any;
  let mockUserService: any;
  let mockTransactionService: any;
  let mockVaultService: any;

  const owner = {
    id: 1,
    username: 'owner@test.com',
  } as UserData;
  const intruder = {
    id: 2,
    username: 'intruder@test.com',
  } as UserData;

  beforeEach(async () => {
    mongoConnection = jest.fn();
    sharedAsset = new SharedAsset();
    sharedAsset.name = 'test';
    sharedAsset.userId = 1;

    service = {
      retrieveByUid: () => jest.fn() as any,
      retrieveByUserId: () => jest.fn() as any,
      create: () => jest.fn() as any,
      remove: () => jest.fn() as any,
      retrieveById: () => jest.fn() as any,
    };

    anonymiserService = {
      createPseudoID: () => jest.fn() as any,
      fetchAnonymisedData: () => jest.fn() as any,
    };

    mockDataFetcherService = {
      retrieveAsset: () => jest.fn(),
      readMongoAsset: () => jest.fn(),
    };

    mockUserService = {
      retrieve: () => jest.fn(),
      getProfileById: () => jest.fn(),
    };

    mockTransactionService = {
      create: () => jest.fn(),
    };

    shareStrategy = {
      publishDataset: () => jest.fn(),
    };

    mockEncryptionService = jest.fn();

    controller = new SharedAssetController(
      service,
      anonymiserService,
      mockDataFetcherService,
      mockUserService,
      mockTransactionService,
      shareStrategy,
      mockEncryptionService,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should retrieve a shared asset', async () => {
    jest.spyOn(service, 'retrieveByUid').mockResolvedValue(sharedAsset);
    expect(await controller.getSharedAsset('abc', owner)).toBe(sharedAsset);
  });

  it("should retrieve all user's shared assets", async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([sharedAsset]);
    expect(await controller.getAllSharedAssets(owner)).toStrictEqual([
      sharedAsset,
    ]);
  });

  it('should throw on retrieve if the shared asset does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveByUid').mockResolvedValue(sharedAsset);
    try {
      await controller.getSharedAsset('abc', intruder);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should create a shared asset', async () => {
    const data: CreateSharedAssetDTO = {
      assetId: 1,
      assetUUID: 'test',
      name: 'test',
      keywords: ['test'],
      configuration: {
        anonymisation: {
          anonymise: true,
          pseudoId: '',
        } as ConfigurationAnonymasationDTO,
      } as ConfigurationDTO,
    };
    const result: SharedAsset = sharedAsset;
    jest.spyOn(service, 'create').mockResolvedValue(result);

    const pseudoId = 'pseudoId1';
    jest.spyOn(anonymiserService, 'createPseudoID').mockResolvedValue(pseudoId);
    jest.spyOn(shareStrategy, 'publishDataset').mockResolvedValue(true);
    const sharedData = {
      ...data,
      anonymized: data.configuration.anonymisation.anonymise,
      encrypted: data.configuration.encryption,
    };
    expect(await controller.create(owner, sharedData, mongoConnection)).toBe(
      result,
    );
    Object.assign(data.configuration.anonymisation, { pseudoId: pseudoId });
    // expect(shareStrategy.publishDataset).toBeCalledWith({
    //   ...data,
    //   metadata: undefined,
    //   schedule: undefined,
    //   dataSchema: undefined,
    //   description: undefined,
    //   dataOwnerUUID: undefined,
    //   dataOwnerProfile: expect.anything(),
    //   data: expect.anything(),
    // });
    expect(anonymiserService.createPseudoID).toBeCalledWith(owner.id);
    expect(service.create).toBeCalledWith(sharedData, expect.anything());
  });

  it('not publish a shared asset', async () => {
    const data: CreateSharedAssetDTO = {
      assetId: 1,
      assetUUID: 'test',
      name: 'test',
      keywords: ['test'],
      configuration: {
        anonymisation: {
          anonymise: true,
          pseudoId: '',
        } as ConfigurationAnonymasationDTO,
      } as ConfigurationDTO,
    };
    jest.spyOn(service, 'create').mockResolvedValue(sharedAsset);

    const pseudoId = 'pseudoId1';
    jest.spyOn(anonymiserService, 'createPseudoID').mockResolvedValue(pseudoId);
    jest.spyOn(shareStrategy, 'publishDataset').mockResolvedValue(false);
    try {
      expect(
        await controller.create(owner, data, mongoConnection),
      ).toThrowError();
    } catch (error) {
      expect(error).toBeDefined();
    }
    Object.assign(data.configuration.anonymisation, { pseudoId: pseudoId });
    expect(anonymiserService.createPseudoID).toBeCalledWith(owner.id);
  });

  it('should create a shared asset , null configuration', async () => {
    const data: CreateSharedAssetDTO = {
      assetId: 1,
      assetUUID: 'test',
      name: 'test',
      keywords: ['test'],
      configuration: {
        anonymisation: {
          anonymise: true,
          pseudoId: '',
        } as ConfigurationAnonymasationDTO,
      } as ConfigurationDTO,
    };
    const result: SharedAsset = sharedAsset;
    jest.spyOn(service, 'create').mockResolvedValue(result);
    const sharedData = {
      ...data,
      anonymized: data.configuration.anonymisation.anonymise,
      encrypted: data.configuration.encryption,
    };
    const pseudoId = 'pseudoId1';
    jest.spyOn(anonymiserService, 'createPseudoID').mockResolvedValue(pseudoId);
    expect(await controller.create(owner, sharedData, mongoConnection)).toBe(
      result,
    );
    expect(anonymiserService.createPseudoID).toBeCalledWith(owner.id);
    expect(service.create).toBeCalledWith(sharedData, expect.anything());
  });

  it('should delete a shared asset', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharedAsset);
    jest.spyOn(service, 'remove').mockImplementation();
    expect(await controller.remove(owner, 1)).toBeUndefined();
  });

  it('should throw on delete if the shared asset does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(sharedAsset);
    try {
      await controller.remove(intruder, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });
});
