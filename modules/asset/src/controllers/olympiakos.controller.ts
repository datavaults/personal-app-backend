import {
  Body,
  Controller,
  Inject,
  Get,
  UseGuards,
  Param,
  Req,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { OlympiakosStrategy, OLYMPIAKOS_STRATEGY_TOKEN } from '../strategies';
import { Request } from 'express';

@Controller('olympiakos')
@UseGuards(AuthenticationGuard)
@ApiTags('olympiakos')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class OlympiakosController {
  constructor(
    @Inject(OLYMPIAKOS_STRATEGY_TOKEN)
    private readonly strategy: OlympiakosStrategy,
  ) {}

  @Get('checkUser/:id/:date')
  @ApiOperation({ summary: 'Check user in olympiakos db' })
  @ApiNotFoundResponse()
  async ckeckUser(
    @Req() request: Request,
    @Param('id') id: any,
    @Param('date') date: any,
  ): Promise<any> {
    return await this.strategy.ckeckUser(id, date, request.cookies.token);
  }
}
