import { PratoController, PratoStrategy } from '@suite5/asset';

describe('PratoController', () => {
  let controller: PratoController;
  let mockPratoStrategy: PratoStrategy;
  const user: any = 'BNCSLV70M42G999G';

  beforeEach(async () => {
    mockPratoStrategy = {
      searchPersonByFiscalCode: () => jest.fn() as any,
    };

    controller = new PratoController(mockPratoStrategy);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should login user', async () => {
    jest
      .spyOn(mockPratoStrategy, 'searchPersonByFiscalCode')
      .mockReturnValue(user);
    await controller.searchPersonByFiscalCode(user);
    expect(mockPratoStrategy.searchPersonByFiscalCode).toBeCalledTimes(1);
  });
});
