import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Inject,
  InternalServerErrorException,
  Param,
  Patch,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  Asset,
  CloudStrategy,
  CLOUD_STRATEGY_TOKEN,
  CreateAssetDTO,
  DataFetcherService,
  DataFetcherStrategy,
  DATA_FETCHER_STRATEGY_TOKEN,
  SourceOptionDTO,
  UpdateAssetDTO,
} from '@suite5/asset';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { MongoConnection } from '@suite5/core/authentication/decorators/mongo-connection.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { Connection } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { MongoInterceptor } from '../../../../src/interceptors';
import { SharedAssetService } from '../services/shared-asset.service';
import { OnEvent } from '@nestjs/event-emitter';
import { Events } from '@suite5/constants/events';
import categories from '../constants/category.constants';
import * as R from 'ramda';
import { UserService } from '@suite5/core/user';

@Controller('data-fetcher')
@UseGuards(AuthenticationGuard)
@ApiTags('data-fetcher')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class DataFetcherController {
  constructor(
    private readonly dataFetcherService: DataFetcherService,
    @Inject(DATA_FETCHER_STRATEGY_TOKEN)
    private readonly dataFetcherStrategy: DataFetcherStrategy,
    private readonly sharedAssetService: SharedAssetService,
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly shareStrategy: CloudStrategy,
    private readonly userService: UserService,
  ) {}

  @OnEvent(Events.UserCreated)
  private async createUserAsset(
    user: UserData,
    assetsData: CreateAssetDTO | CreateAssetDTO[],
    calledFrom: any,
    file?: Express.Multer.File,
  ): Promise<Asset[]> {
    let assets: CreateAssetDTO[];
    if (!R.is(Array, assetsData)) assets = [assetsData] as CreateAssetDTO[];
    else assets = assetsData as CreateAssetDTO[];
    const created: Asset[] = [];
    for (let a = 0; a < assets.length; a++) {
      const assetData = assets[a];
      assetData.createdById = user.id;
      if (calledFrom != 'userCreation' && calledFrom != 'autoCreation') {
        assetData.schedule = JSON.parse(assetData.schedule as any);
        assetData.credentials = JSON.parse(assetData.credentials as any);
      }
      const { name, sourceType, schedule, credentials } = assetData;
      const sourceId = uuidv4();
      let categoryKey: string;
      if (credentials.sourceType !== undefined) {
        categoryKey = `${sourceType}_${credentials.sourceType}`;
      } else {
        categoryKey = sourceType;
      }
      const createSourceData = {
        id: sourceId,
        name,
        sourceType,
        status: 'enabled',
        category: categories[categoryKey],
        config: credentials,
      };
      if (schedule) createSourceData['schedule'] = schedule;
      if (file) createSourceData['file'] = file;
      try {
        await this.dataFetcherStrategy.createSource(user.sub, createSourceData);
      } catch (e) {
        throw new InternalServerErrorException(
          `Unexpectedly was unable to create source in data fetcher with information: ${JSON.stringify(
            createSourceData,
          )}`,
        );
      }

      created.push(
        await this.dataFetcherService.createAsset(assetData, sourceId),
      );
    }
    return created;
  }

  @Post('asset')
  @ApiOperation({ summary: 'Create an asset' })
  @UseInterceptors(FileInterceptor('file'))
  async createAsset(
    @CurrentUser() user: UserData,
    @Body() assetData: CreateAssetDTO,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Asset> {
    return (await this.createUserAsset(user, assetData, 'controller', file))[0];
  }

  @Post('asset/mobile-app')
  @ApiOperation({ summary: 'Create mobile app asset' })
  @UseInterceptors(FileInterceptor('file'))
  async createMobileAppAsset(@Body() data: any): Promise<any> {
    const mobileSources = [
      {
        name: 'Location',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Location'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: '',
        credentials: { subId: '', sourceType: 'Location' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Route',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Route'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: '',
        credentials: { subId: '', sourceType: 'Route' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Google Fit',
        description: 'Mobile App',
        keywords: ['Mobile App', 'Google Fit'],
        url: null,
        sourceType: 'MOBILEAPP',
        createdById: '',
        credentials: { subId: '', sourceType: 'Google Fit' },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
    ];
    let users: any;
    if (!data.id) {
      users = await this.userService.getUsers();
    } else {
      users = [await this.userService.retrieve(Number.parseInt(data.id))];
    }
    for (let i = 0; i < users.length; i++) {
      const assets = await this.dataFetcherService.retrieveByUserId(
        users[i].id,
      );
      const dataFetcherSource = await this.dataFetcherStrategy.getUserSources(
        users[i].sub,
      );
      if (!assets.some((asset: any) => asset.sourceType === 'MOBILEAPP')) {
        for (let j = 0; j < mobileSources.length; j++) {
          mobileSources[j].createdById = users[i].id as any;
          mobileSources[j].credentials.subId = users[i].sub;
          await this.createUserAsset(
            users[i] as any,
            mobileSources[j] as any,
            'autoCreation',
          );
        }
      } else if (
        assets.some((asset: any) => asset.sourceType === 'MOBILEAPP') &&
        dataFetcherSource.length > 0
      ) {
        const missing = mobileSources.filter(
          (ms) =>
            !dataFetcherSource.some(
              (dfs) =>
                dfs.sourceType === 'MOBILEAPP' &&
                dfs.config.sourceType === ms.credentials.sourceType,
            ),
        );
        for (let k = 0; k < missing.length; k++) {
          missing[k].createdById = users[i].id as any;
          missing[k].credentials.subId = users[i].sub;
        }
        await this.createUserAsset(
          users[i] as any,
          missing as any,
          'autoCreation',
        );
      }
    }
    return;
  }

  @Get('asset')
  @ApiOperation({ summary: 'Retrieve assets of specific user' })
  @ApiNotFoundResponse()
  getAssets(@CurrentUser() user: UserData): Promise<Asset[]> {
    return this.dataFetcherService.retrieveByUserId(user.id);
  }

  @Get('asset/:id')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Retrieve an asset by its id' })
  @ApiNotFoundResponse()
  async getAsset(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<Asset> {
    const asset = await this.dataFetcherService.retrieveAsset(
      mongoConnection,
      id,
      user.id,
      0,
      50,
    );
    if (asset.createdById !== user.id) throw new ForbiddenException();
    return asset;
  }

  @Get('asset/:id/data')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Retrieve an asset by its id' })
  @ApiNotFoundResponse()
  async getAssetData(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<any> {
    const assetData = await this.dataFetcherService.retrieveAssetData(
      mongoConnection,
      id,
      user.id,
    );
    return assetData;
  }

  @Get('asset/:id/metadata')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Retrieve an asset by its id' })
  @ApiNotFoundResponse()
  async getAssetMetadata(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<any> {
    const assetData = await this.dataFetcherService.retrieveAssetMetadata(
      mongoConnection,
      id,
      user.id,
    );
    return assetData;
  }

  @Get('asset/:id/data-schema')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Retrieve an asset by its id' })
  @ApiNotFoundResponse()
  async getAssetDataSchema(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<any> {
    const assetData = await this.dataFetcherService.retrieveAssetDataSchema(
      mongoConnection,
      id,
      user.id,
    );
    return assetData;
  }

  @Put('asset/:id')
  @ApiOperation({ summary: 'Update an asset' })
  @UseInterceptors(FileInterceptor('file'))
  @ApiNotFoundResponse()
  async updateAsset(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @Body() assetData: UpdateAssetDTO,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Asset> {
    const source = await this.dataFetcherService.retrieveAssetById(id);
    assetData.schedule = JSON.parse(assetData.schedule as any);
    assetData.credentials = JSON.parse(assetData.credentials as any);
    const { name, schedule, credentials } = assetData;
    try {
      const updateAsset = {
        id: source.uid,
        name,
        sourceType: source.sourceType,
        status: assetData.status ? 'enabled' : 'disabled',
        config: credentials,
      };
      if (schedule) updateAsset['schedule'] = schedule;
      if (file) updateAsset['file'] = file;
      await this.dataFetcherStrategy.updateSource(
        user.sub,
        source.uid,
        updateAsset,
      );
    } catch (e) {
      console.log(e);
      throw new InternalServerErrorException(
        `Unexpectedly was unable to update source in data fetcher with information: ${JSON.stringify(
          assetData,
        )} and error ${JSON.stringify(e)}`,
      );
    }
    return this.dataFetcherService.updateAsset(id, assetData);
  }

  private async deleteUserAssets(id: number, source: Asset, user: UserData) {
    // only delete from the datafetcher if there is a schedule
    if (source.schedule) {
      await this.dataFetcherStrategy.deleteSource(user.sub, source.uid);
    }
    await this.dataFetcherService.deleteAsset(id);
  }

  @Delete('asset/:id')
  @ApiOperation({ summary: 'Delete an asset' })
  @ApiNotFoundResponse()
  async deleteAsset(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
  ): Promise<void> {
    const source = await this.dataFetcherService.retrieveAssetById(id);
    const sharedAsset = await this.sharedAssetService.retrieveByAssetId(
      source.id,
      true,
    );
    if (sharedAsset) {
      try {
        await this.shareStrategy.deleteAsset(sharedAsset.uid);
      } catch (e: any) {
        throw new InternalServerErrorException(
          'Unable to delete assets from cloud',
        );
      }
    }
    await this.deleteUserAssets(id, source, user);
  }

  @OnEvent(Events.DeleteUser)
  @Delete('assetsDeletion')
  @ApiOperation({ summary: 'Delete an asset' })
  @ApiNotFoundResponse()
  async deleteAssetsOnUserDeletion(@CurrentUser() user: any): Promise<void> {
    const assets = await this.dataFetcherService.retrieveByUserId(user.id);
    for (let i = 0; i < assets.length; i++) {
      if (assets[i].schedule) {
        await this.dataFetcherStrategy.deleteSource(user.sub, assets[i].uid);
      }
    }
  }

  @Get('source-options')
  @ApiOperation({ summary: 'Get available source options' })
  async sourceOptions(): Promise<SourceOptionDTO[]> {
    return await this.dataFetcherStrategy.getSources();
  }

  @Patch('/status/:enabled/:id')
  @ApiOperation({ summary: 'Enable or disable status' })
  async shareProfile(
    @CurrentUser() user: UserData,
    @Param('enabled') enabled: boolean,
    @Param('id') id: number,
  ): Promise<boolean> {
    return this.dataFetcherService.assetEnabled(id, enabled);
  }

  @Get('create-olumpiakos-source')
  @ApiOperation({ summary: 'Manualy creation of olympiakos sources' })
  async createOlySources(): Promise<any> {
    const olympiakosSources = [
      {
        name: 'Olympiakos - Shop purchases',
        description: 'Olympiakos',
        keywords: ['Olympiakos', 'Shop purchases'],
        url: null,
        sourceType: 'OLYMPIAKOS',
        createdById: '',
        credentials: {
          subId: '',
          birthDate: '',
          sourceType: 'Shop purchases',
        },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Olympiakos - Daily tickets',
        description: 'Olympiakos',
        keywords: ['Olympiakos', 'Daily tickets'],
        url: null,
        sourceType: 'OLYMPIAKOS',
        createdById: '',
        credentials: {
          subId: '',
          birthDate: '',
          sourceType: 'Daily tickets',
        },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Olympiakos - Season tickets',
        description: 'Olympiakos',
        keywords: ['Olympiakos', 'Season tickets'],
        url: null,
        sourceType: 'OLYMPIAKOS',
        createdById: '',
        credentials: {
          subId: '',
          birthDate: '',
          sourceType: 'Season tickets',
        },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
      {
        name: 'Olympiakos - Event attendances',
        description: 'Olympiakos',
        keywords: ['Olympiakos', 'Event attendances'],
        url: null,
        sourceType: 'OLYMPIAKOS',
        createdById: '',
        credentials: {
          subId: '',
          birthDate: '',
          sourceType: 'Event attendances',
        },
        schedule: { interval: { value: 1, unit: 'DAY' } },
        errors: '',
        uid: '',
        type: 'dataset',
      },
    ];
    const users = await (
      await this.userService.getUsers()
    ).filter((val: any) => {
      return val.demonstrator === 'olympiacos' && val.birthDate !== null;
    });

    for (let i = 0; i < users.length; i++) {
      const assets = await this.dataFetcherService.retrieveByUserId(
        users[i].id,
      );
      if (!assets.some((asset: any) => asset.sourceType === 'OLYMPIAKOS')) {
        for (let j = 0; j < olympiakosSources.length; j++) {
          olympiakosSources[j].createdById = users[i].id as any;
          olympiakosSources[j].credentials.subId = '';
          olympiakosSources[j].credentials.birthDate = users[i]
            .birthDate as any;
          await this.createUserAsset(
            users[i] as any,
            olympiakosSources[j] as any,
            'autoCreation',
          );
        }
      }
    }
    return;
  }
}
