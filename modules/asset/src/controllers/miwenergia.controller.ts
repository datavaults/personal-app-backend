import { Body, Controller, Inject, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  MIWenergiaLoginDTO,
  MIWenergiaStrategy,
  MIWENERGIA_STRATEGY_TOKEN,
} from '@suite5/asset';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';

@Controller('miwenergia')
@UseGuards(AuthenticationGuard)
@ApiTags('miwenergia')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class MIWenergiaController {
  constructor(
    @Inject(MIWENERGIA_STRATEGY_TOKEN)
    private readonly strategy: MIWenergiaStrategy,
  ) {}

  @Post('login')
  @ApiOperation({ summary: 'User login' })
  @ApiNotFoundResponse()
  async login(@Body() data: MIWenergiaLoginDTO): Promise<any> {
    return await this.strategy.login(data.username, data.password);
  }

  @Post('list')
  @ApiOperation({ summary: 'Retrieve user USPC list' })
  @ApiNotFoundResponse()
  async retrieve(@Body() data: any): Promise<any> {
    const list = await this.strategy.retrieveUspcList(data);
    const uspcList = [];
    list.forEach((item) => {
      uspcList.push({ value: item.uspcText, label: item.uspcText });
    });
    return uspcList;
  }
}
