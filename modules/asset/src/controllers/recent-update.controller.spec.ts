import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AuthenticationService } from '@suite5/core/authentication/services/authentication.service';
import { Repository } from 'typeorm';
import {
  CreateRecentUpdateDTO,
  RecentUpdate,
  RecentUpdateController,
  RecentUpdateService,
  UpdateRecentUpdateDTO,
} from '@suite5/asset';
import { ConfigService } from '@nestjs/config';

describe('RecentUpdateController', () => {
  let controller: RecentUpdateController;
  let service: RecentUpdateService;
  let recentUpdate: RecentUpdate;

  beforeEach(async () => {
    recentUpdate = new RecentUpdate();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecentUpdateController],
      providers: [
        {
          provide: getRepositoryToken(RecentUpdate),
          useClass: Repository,
        },
        RecentUpdateService,
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
      ],
    }).compile();
    controller = module.get<RecentUpdateController>(RecentUpdateController);
    service = module.get<RecentUpdateService>(RecentUpdateService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it('should create a recent update', async () => {
    const data: CreateRecentUpdateDTO = {
      name: '',
      assetId: 1,
      lastDate: new Date(),
      successful: true,
    };
    jest.spyOn(service, 'createRecentUpdate').mockResolvedValue(recentUpdate);
    expect(await controller.createRecentUpdate(data)).toBe(recentUpdate);
  });

  it('should retrieve recent updates of specific asset', async () => {
    jest
      .spyOn(service, 'retrieveRecentUpdatesByAssetId')
      .mockResolvedValue([recentUpdate]);
    expect(await controller.getRecentUpdates(1)).toEqual([recentUpdate]);
  });

  it('should retrieve a recent update', async () => {
    jest.spyOn(service, 'retrieveRecentUpdate').mockResolvedValue(recentUpdate);
    expect(await controller.getRecentUpdate(1)).toEqual(recentUpdate);
  });

  it('should update a recent update', async () => {
    const data: UpdateRecentUpdateDTO = { name: '' };
    jest.spyOn(service, 'updateRecentUpdate').mockResolvedValue(recentUpdate);
    expect(await controller.updateRecentUpdate(1, data)).toBe(recentUpdate);
  });

  it('should delete a recent update', async () => {
    jest.spyOn(service, 'deleteRecentUpdate').mockImplementation();
    await controller.deleteRecentUpdate(1);
    expect(service.deleteRecentUpdate).toBeCalledWith(1);
  });
});
