import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AnonRulesObject } from '@suite5/anonymiser/dto/anonymiser.dto';
import { AnonymiserService } from '@suite5/anonymiser/services';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { MongoConnection } from '@suite5/core/authentication/decorators/mongo-connection.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { UserService } from '@suite5/core/user';
import { EncryptionService } from '../../../encryption/src/services';
import { Connection } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { MongoInterceptor } from '../../../../src/interceptors';
import { CreateSharedAssetDTO } from '../dto';
import { ConfigurationDTO } from '../dto/configuration.dto';
import { SharingConfiguration } from '../entities';
import {
  DataFetcherService,
  SharedAssetService,
  TransactionService,
} from '../services';
import {
  CloudStrategy,
  CLOUD_STRATEGY_TOKEN,
} from '../strategies/cloud.strategy';

@Controller('shared-asset')
@ApiTags('shared-asset')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class SharedAssetController {
  constructor(
    private readonly service: SharedAssetService,
    @Inject(AnonymiserService)
    private readonly serviceAnonymiser: AnonymiserService,
    @Inject(DataFetcherService)
    private readonly dataFetcherService: DataFetcherService,
    @Inject(UserService)
    private readonly userService: UserService,
    @Inject(TransactionService)
    private readonly transactionService: TransactionService,
    @Inject(CLOUD_STRATEGY_TOKEN)
    private readonly strategy: CloudStrategy,
    @Inject(EncryptionService)
    private readonly encryptionService: EncryptionService,
  ) {}

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: "Retrieve user's shared assets",
  })
  async getAllSharedAssets(@CurrentUser() user: UserData) {
    return this.service.retrieveByUserId(user.id);
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve a shared asset',
  })
  async getSharedAsset(@Param('id') id: string, @CurrentUser() user: UserData) {
    const sharedAsset = await this.service.retrieveByUid(id);
    if (sharedAsset.userId !== user.id) throw new ForbiddenException();
    return sharedAsset;
  }

  @Post()
  @UseInterceptors(MongoInterceptor)
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Create new shared asset',
  })
  @ApiBody({ type: CreateSharedAssetDTO })
  async create(
    @CurrentUser() user: UserData,
    @Body() data: CreateSharedAssetDTO,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<SharingConfiguration> {
    console.log('--- Starting share ---');
    const asset = await this.dataFetcherService.retrieveAsset(
      mongoConnection,
      data.assetId,
      user.id,
    );
    data.userId = user.id;
    console.log('--- Asset found ---');

    let {
      data: dataset,
      metadata,
      dataSchema,
    } = await this.dataFetcherService.readMongoAsset(
      mongoConnection,
      user.id,
      asset.uid,
    );
    console.log('--- Asset read from mongo ---');

    if (
      !!data.configuration.anonymisation &&
      !!data.configuration.anonymisation.anonymise
    ) {
      console.log('--- Asset will be anonymised---');
      if (!data.configuration.anonymisation.pseudoId) {
        console.log('--- Generate pseudo id ---');
        const pseudoId: string = await this.serviceAnonymiser.createPseudoID(
          user.id,
        );
        console.log(`--- PseudoId ${pseudoId}---`);
        if (!data.configuration) {
          Object.assign(data, { configuration: {} as ConfigurationDTO });
        }
        data.configuration.anonymisation.pseudoId = pseudoId;
      }
      console.log(`--- Fetch anonymised data ---`);
      dataset = await this.serviceAnonymiser.fetchAnonymisedData({
        ...data.configuration.anonymisation.rules,
        dataset: dataset,
        structure: metadata,
      } as AnonRulesObject);
    }

    if (data.configuration.encryption) {
      let newData;
      if (dataset.file as any) {
        newData = {
          asset: dataset.file,
          assetId: asset.uid,
          policy: data.configuration.accessPolicies,
        };
      } else {
        newData = {
          asset: dataset,
          assetId: asset.uid,
          policy: data.configuration.accessPolicies,
        };
      }
      dataset = [await this.encryptionService.encryptData(newData, user.id)];
    }
    console.log(`--- Ready to publish ---`);
    if (
      await this.strategy.publishDataset({
        assetId: data.assetUUID,
        source: asset.sourceType,
        name: data.name,
        description: data.description,
        keywords: data.keywords,
        configuration: data.configuration,
        dataOwnerUUID: user.sub,
        schedule: asset.schedule,
        data: dataset,
        dataOwnerProfile: await this.userService.getProfileById(user.id),
        metadata,
        dataSchema,
      })
    ) {
      const sharedData = {
        ...data,
        anonymized: data.configuration.anonymisation.anonymise,
        encrypted: data.configuration.encryption,
      };
      console.log(`--- Before shared asset creation ---`);
      try {
        const sharedAsset = await this.service.create(
          sharedData,
          data.assetUUID,
        );

        console.log(`--- Before transaction creation ---`);
        await this.transactionService.create({
          sharedAssetId: sharedAsset.id,
          timestamp: new Date(),
          hash: uuidv4(),
          userId: user.id,
        });
        return sharedAsset;
      } catch (e) {
        console.log(
          '-------------- Shared asset controller--------------------',
        );
        console.log(e);
      }
    }
    throw new HttpException(
      'New shared asset is not published',
      HttpStatus.NOT_ACCEPTABLE,
    );
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a shared asset' })
  @ApiNotFoundResponse()
  async remove(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
  ): Promise<void> {
    const sharedAsset = await this.service.retrieveById(id);
    if (sharedAsset.userId !== user.id) throw new ForbiddenException();
    await this.service.remove(sharedAsset);
  }
}
