import { OlympiakosStrategy } from '@suite5/asset';
import { OlympiakosController } from './olympiakos.controller';

describe('MIWenergiaController', () => {
  let controller: OlympiakosController;
  let mockOlympiakosStrategy: OlympiakosStrategy;
  const req: any = { cookie: { token: 'sdjfsdfjhs' } };
  const data = {
    memberId: '99088887-5d38-4d56-b064-65f212a96410',
    birthDate: '2022-08-10 13:46:52.451',
  };

  beforeEach(async () => {
    mockOlympiakosStrategy = {
      ckeckUser: () => jest.fn() as any,
    };

    controller = new OlympiakosController(mockOlympiakosStrategy);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  // it('should check user', async () => {
  //   jest.spyOn(mockOlympiakosStrategy, 'ckeckUser').mockImplementation();
  //   console.log(req.cookie.token)
  //   await controller.ckeckUser(
  //     data.memberId as any,
  //     data.memberId as any,
  //     req.cookie.token as any,
  //   );
  //   expect(mockOlympiakosStrategy.ckeckUser).toBeCalledTimes(1);
  // });
});
