import { Asset, CloudStrategy, DataFetcherController } from '@suite5/asset';
import { UserData } from '@suite5/core/authentication/decorators';
import { KeycloakUser } from '@suite5/core/authentication/entities';
import { ForbiddenException } from '@nestjs/common';

describe('DataFetcherController', () => {
  let controller: DataFetcherController;
  let service: any;
  let asset: Asset;
  let sharedAssetService: any;
  let mockShareStrategy: CloudStrategy;
  let mockAuthenticationService: any;
  let mockSourceStrategy: any;
  let user: UserData;
  let mongoConnection: any;
  let keyCloakUser: KeycloakUser;
  let userService: any;

  beforeEach(async () => {
    asset = new Asset();
    asset.createdById = 1;
    user = { id: 1 } as UserData;
    keyCloakUser = { id: 1 } as KeycloakUser;

    service = {
      createAsset: jest.fn(),
      retrieveByUserId: jest.fn(),
      retrieveAsset: jest.fn(),
      updateAsset: jest.fn(),
      deleteAsset: jest.fn(),
      retrieveAssetById: jest.fn(),
      retrieveAssetData: jest.fn(),
      retrieveAssetMetadata: jest.fn(),
      retrieveAssetDataSchema: jest.fn(),
    };

    mongoConnection = jest.fn();

    sharedAssetService = {
      retrieveByUserId: () => jest.fn(),
      retrieveByAssetId: () => jest.fn(),
    };

    mockAuthenticationService = {
      login: () => jest.fn(),
      logout: () => jest.fn(),
      authenticate: () => jest.fn(),
      register: () => jest.fn(),
      verify: () => jest.fn(),
      resetPassword: () => jest.fn(),
      checkPasswordStrength: () => jest.fn(),
      changePassword: () => jest.fn(),
      sendVerificationEmail: () => jest.fn(),
      sendResetPasswordEmail: () => jest.fn(),
    };

    mockShareStrategy = {
      updateDataOwnerProfile: () => jest.fn() as any,
      publishDataset: () => jest.fn() as any,
      updateDataset: () => jest.fn() as any,
      deleteAsset: () => jest.fn() as any,
      deleteDataOwner: () => jest.fn() as any,
      acceptOffer: () => jest.fn() as any,
      publishQuestionnaire: () => jest.fn() as any,
    };

    mockSourceStrategy = {
      createSource: () => jest.fn(),
      deleteSource: () => jest.fn(),
    };

    controller = new DataFetcherController(
      service,
      mockSourceStrategy,
      sharedAssetService,
      mockShareStrategy,
      userService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(controller).toBeDefined();
  });

  // it('should create an asset', async () => {
  //   const data: CreateAssetDTO = {
  //     name: '',
  //     sourceType: SourceType.Upload,
  //     schedule: { mock: { key: 'value' } },
  //     credentials: { mock: { key: 'value' } },
  //   };
  //   const mockFile = {
  //     fieldname: 'file',
  //     originalname: 'mockfile.json',
  //     encoding: '7bit',
  //     mimetype: 'application/json',
  //     buffer: Buffer.from(JSON.stringify({ mock: 'mock' })),
  //     size: 51828,
  //   } as Express.Multer.File;
  //   jest.spyOn(service, 'createAsset').mockResolvedValue(asset);
  //   expect(await controller.createAsset(keyCloakUser, data, mockFile)).toBe(
  //     asset,
  //   );
  // });

  it('should retrieve assets of specific user', async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([asset]);
    expect(await controller.getAssets(user)).toEqual([asset]);
  });

  it('should retrieve an asset', async () => {
    jest.spyOn(service, 'retrieveAsset').mockResolvedValue(asset);
    expect(await controller.getAsset(keyCloakUser, 1, mongoConnection)).toEqual(
      asset,
    );
  });

  it('should fail to retrieve an asset if user in not the owner', async () => {
    jest.spyOn(service, 'retrieveAsset').mockResolvedValue(asset);
    try {
      await controller.getAsset(keyCloakUser, 1, mongoConnection);
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  // it('should update an asset', async () => {
  //   const data: UpdateAssetDTO = {
  //     name: '',
  //     schedule: { mock: { key: 'value' } },
  //     credentials: { mock: { key: 'value' } },
  //   };
  //   const mockFile = {
  //     fieldname: 'file',
  //     originalname: 'mockfile.json',
  //     encoding: '7bit',
  //     mimetype: 'application/json',
  //     buffer: Buffer.from(JSON.stringify({ mock: 'mock' })),
  //     size: 51828,
  //   } as Express.Multer.File;
  //   jest.spyOn(service, 'updateAsset').mockResolvedValue(asset);
  //   expect(await controller.updateAsset(keyCloakUser, 1, data, mockFile)).toBe(
  //     asset,
  //   );
  // });

  it('should delete an asset and not call cloud if not shared', async () => {
    jest.spyOn(service, 'retrieveAssetById').mockResolvedValue({ uid: 'abc' });
    jest.spyOn(sharedAssetService, 'retrieveByAssetId').mockResolvedValue(null);
    jest.spyOn(mockShareStrategy, 'deleteAsset').mockImplementation();
    await controller.deleteAsset(user, 1);
    expect(mockShareStrategy.deleteAsset).toBeCalledTimes(0);
  });

  it('should delete an asset and call cloud if asset is shared', async () => {
    jest.spyOn(service, 'retrieveAssetById').mockResolvedValue({ uid: 'abc' });
    jest
      .spyOn(sharedAssetService, 'retrieveByAssetId')
      .mockResolvedValue({ uid: 'bcd' });
    jest.spyOn(mockShareStrategy, 'deleteAsset').mockImplementation();
    await controller.deleteAsset(user, 1);
    expect(mockShareStrategy.deleteAsset).toBeCalledWith('bcd');
  });

  it('should throw error if unable to delete from cloud', async () => {
    jest.spyOn(service, 'retrieveAssetById').mockResolvedValue({ uid: 'abc' });
    jest
      .spyOn(sharedAssetService, 'retrieveByAssetId')
      .mockResolvedValue({ uid: 'bcd' });
    jest
      .spyOn(mockShareStrategy, 'deleteAsset')
      .mockRejectedValue({ message: 'failed' });
    expect(controller.deleteAsset(user, 1)).rejects.toThrowError(
      'Unable to delete assets from cloud',
    );
  });

  it('should fetch asset data', async () => {
    const data = [{ a: 2 }];
    jest.spyOn(service, 'retrieveAssetData').mockResolvedValue(data);
    expect(await controller.getAssetData(user, 1, mongoConnection)).toBe(data);
  });

  it('should fetch asset metadata', async () => {
    const data = [{ a: 2 }];
    jest.spyOn(service, 'retrieveAssetMetadata').mockResolvedValue(data);
    expect(await controller.getAssetMetadata(user, 1, mongoConnection)).toBe(
      data,
    );
  });

  it('should fetch asset data schema', async () => {
    const data = [{ a: 2 }];
    jest.spyOn(service, 'retrieveAssetDataSchema').mockResolvedValue(data);
    expect(await controller.getAssetDataSchema(user, 1, mongoConnection)).toBe(
      data,
    );
  });
});
