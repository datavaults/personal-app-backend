import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import { CreateRecentUpdateDTO, UpdateRecentUpdateDTO } from '../dto';
import { RecentUpdate } from '../entities';
import { RecentUpdateService } from '../services';

@Controller('data-fetcher')
@UseGuards(AuthenticationGuard)
@ApiTags('data-fetcher')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class RecentUpdateController {
  constructor(private readonly recentUpdateService: RecentUpdateService) {}

  @Post('recent-update')
  @ApiOperation({ summary: 'Create a recent update' })
  async createRecentUpdate(
    @Body() recentUpdateData: CreateRecentUpdateDTO,
  ): Promise<RecentUpdate> {
    return await this.recentUpdateService.createRecentUpdate(recentUpdateData);
  }

  @Get('recent-update/asset-id/:id')
  @ApiOperation({ summary: 'Retrieve recent updates of specific asset' })
  @ApiNotFoundResponse()
  getRecentUpdates(@Param('id') id: number): Promise<RecentUpdate[]> {
    return this.recentUpdateService.retrieveRecentUpdatesByAssetId(id);
  }

  @Get('recent-update/:id')
  @ApiOperation({ summary: 'Retrieve a recent update by its id' })
  @ApiNotFoundResponse()
  getRecentUpdate(@Param('id') id: number): Promise<RecentUpdate> {
    return this.recentUpdateService.retrieveRecentUpdate(id);
  }

  @Put('recent-update/:id')
  @ApiOperation({ summary: 'Update a recent update' })
  @ApiNotFoundResponse()
  updateRecentUpdate(
    @Param('id') id: number,
    @Body() recentUpdateData: UpdateRecentUpdateDTO,
  ): Promise<RecentUpdate> {
    return this.recentUpdateService.updateRecentUpdate(id, recentUpdateData);
  }

  @Delete('recent-update/:id')
  @ApiOperation({ summary: 'Delete a recent update' })
  @ApiNotFoundResponse()
  async deleteRecentUpdate(@Param('id') id: number): Promise<void> {
    await this.recentUpdateService.deleteRecentUpdate(id);
  }
}
