import { Controller, Get, Inject, Param, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { AssetStatsService } from '../services';

//TODO Create test file and resolve circular dependency
@Controller('assets-stats')
@UseGuards(AuthenticationGuard)
@ApiTags('assets-stats')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class AssetStatsController {
  constructor(
    @Inject(AssetStatsService)
    private readonly assetStatsService: AssetStatsService,
  ) {}

  @Get('years')
  @ApiOperation({ summary: 'Retrieve user assets years' })
  @ApiNotFoundResponse()
  async retrieveListOfYears(@CurrentUser() user: UserData): Promise<any> {
    return await this.assetStatsService.retrieveListOfYears(user.id);
  }

  @Get('totalassets/:start/:end')
  @ApiOperation({ summary: 'Retrieve user total assets number' })
  @ApiNotFoundResponse()
  async retrieveTotalAssets(
    @CurrentUser() user: UserData,
    @Param('start') start: any,
    @Param('end') end: any,
  ): Promise<any> {
    return await this.assetStatsService.retrieveTotalAssets(
      start,
      end,
      user.id,
    );
  }

  @Get('totalsharedassets/:start/:end')
  @ApiOperation({ summary: 'Retrieve user total shared assets number' })
  @ApiNotFoundResponse()
  async retrieveTotalSharedAssetsByYear(
    @CurrentUser() user: UserData,
    @Param('start') start: any,
    @Param('end') end: any,
  ): Promise<any> {
    return await this.assetStatsService.retrieveTotalSharedAssetsByYear(
      start,
      end,
      user.id,
    );
  }

  @Get('shared-asset/:date')
  @ApiOperation({
    summary: 'Retrieve user shared assets per month of specific year',
  })
  @ApiNotFoundResponse()
  async retrieveMonthSharedAssets(
    @CurrentUser() user: UserData,
    @Param('date') date: any,
  ): Promise<any> {
    return await this.assetStatsService.retrieveMonthSharedAssets(
      date,
      user.id,
    );
  }

  @Get('totalsharedassets')
  @ApiOperation({ summary: 'Retrieve user total shared assets number' })
  @ApiNotFoundResponse()
  async retrieveTotalSharedAssets(@CurrentUser() user: UserData): Promise<any> {
    return await this.assetStatsService.retrieveTotalSharedAssets(user.id);
  }
}
