import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { UserAssetService } from '../services/user-asset.service';
import { MongoConnection } from '@suite5/core/authentication/decorators/mongo-connection.decorator';
import { Connection } from 'mongoose';
import { MongoInterceptor } from '../../../../src/interceptors';

@Controller('user-asset')
@UseGuards(AuthenticationGuard)
@ApiTags('user-asset')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class UserAssetController {
  constructor(private readonly userAssetService: UserAssetService) {}

  @Delete()
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Delete a user' })
  @ApiNotFoundResponse()
  async deleteUser(
    @CurrentUser() user: UserData,
    @MongoConnection() mongoConnection: Connection,
  ): Promise<void> {
    await this.userAssetService.delete(user.id, mongoConnection);
  }
}
