import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  CurrentUser,
  UserData,
} from '@suite5/core/authentication/decorators/current-user.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards/authentication.guard';
import {
  CreateSharingConfigurationDTO,
  UpdateSharingConfigurationDTO,
} from '../dto';
import { SharingConfigurationService } from '../services';
import { SharingConfiguration } from '@suite5/asset/entities';

@Controller('sharing-configuration')
@ApiTags('sharing-configuration')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class SharingConfigurationController {
  constructor(private readonly service: SharingConfigurationService) {}

  @Get('all')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: "Retrieve user's sharing configurations",
  })
  async getAllSharingConfigurations(@CurrentUser() user: UserData) {
    return this.service.retrieveByUserId(user.id);
  }

  @Get(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Retrieve a sharing configuration',
  })
  async getSharingConfiguration(
    @Param('id') id: number,
    @CurrentUser() user: UserData,
  ) {
    const sharingConfiguration = await this.service.retrieveById(id);
    if (sharingConfiguration.userId !== user.id) throw new ForbiddenException();
    return sharingConfiguration;
  }

  @Post()
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Create new sharing configuration',
  })
  @ApiBody({ type: CreateSharingConfigurationDTO })
  async create(
    @CurrentUser() user: UserData,
    @Body() data: CreateSharingConfigurationDTO,
  ): Promise<SharingConfiguration> {
    const sharingConfigurationNames = (
      await this.service.retrieveByUserId(user.id)
    ).map((config: SharingConfiguration) => config.name);
    if (sharingConfigurationNames.includes(data.name)) {
      throw new BadRequestException(
        'This configuration name already exists! Please give a different name.',
      );
    }
    data.userId = user.id;
    return this.service.create(data);
  }

  @Put(':id')
  @UseGuards(AuthenticationGuard)
  @ApiOperation({
    summary: 'Update sharing configuration',
  })
  @ApiBody({ type: UpdateSharingConfigurationDTO })
  async update(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
    @Body() data: UpdateSharingConfigurationDTO,
  ): Promise<SharingConfiguration> {
    const sharingConfiguration = await this.service.retrieveById(id);
    if (sharingConfiguration.userId !== user.id) throw new ForbiddenException();
    return this.service.update(id, data);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a sharing configuration' })
  @ApiNotFoundResponse()
  async remove(
    @CurrentUser() user: UserData,
    @Param('id') id: number,
  ): Promise<void> {
    const sharingConfiguration = await this.service.retrieveById(id);
    if (sharingConfiguration.userId !== user.id) throw new ForbiddenException();
    await this.service.remove(sharingConfiguration);
  }
}
