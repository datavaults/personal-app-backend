import { DownloadController } from '@suite5/asset/controllers/download.controller';

describe('DownloadController', () => {
  let controller: DownloadController;
  let downloadService;
  let service: any;
  let mongoConnection: any;
  let asset: any;
  let response: any;
  let user: any;

  beforeEach(async () => {
    mongoConnection = jest.fn();

    asset = {
      id: 1,
      data: { info: 'Lorem Ipsum' },
    };

    user = {
      id: 1,
    };

    downloadService = {
      zippify: jest.fn(),
      buffer: jest.fn(),
      pushResponse: jest.fn(),
    };

    service = {
      retrieveByUserId: jest.fn(),
      retrieveAssetById: jest.fn(),
      retrieveAssetData: jest.fn(),
    };

    response = {
      set: () => jest.fn(),
      on: () => jest.fn(),
      write: () => jest.fn(),
      end: () => jest.fn(),
      once: () => jest.fn(),
      emit: () => jest.fn(),
    };

    controller = new DownloadController(service, downloadService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should gather all assets from dataFetcherService', async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([asset]);
    jest.spyOn(service, 'retrieveAssetData').mockResolvedValue(asset.data);
    await controller.downloadAllAssets(user, mongoConnection, response);
    expect(service.retrieveByUserId).toBeCalledTimes(1);
    expect(service.retrieveAssetData).toBeCalledTimes(1);
  });

  it('should gather specific assets from dataFetcherService', async () => {
    jest.spyOn(service, 'retrieveAssetById').mockResolvedValue([asset]);
    jest.spyOn(service, 'retrieveAssetData').mockResolvedValue(asset.data);
    await controller.downloadAssets(user, mongoConnection, [1], response);
    expect(service.retrieveAssetData).toBeCalledTimes(1);
    expect(service.retrieveAssetById).toBeCalledTimes(1);
  });

  it('should return an error message if no assets are found when requesting specific', async () => {
    jest.spyOn(service, 'retrieveAssetById').mockResolvedValue([]);
    expect(
      await controller.downloadAssets(user, mongoConnection, [], response),
    ).toEqual({ message: 'No assets found' });
    expect(service.retrieveAssetById).toBeCalledTimes(0);
  });

  it('should return an error message if no assets are found when requesting all', async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([]);
    expect(
      await controller.downloadAllAssets(user, mongoConnection, response),
    ).toEqual({ message: 'No assets found' });
    expect(service.retrieveAssetData).toBeCalledTimes(0);
  });
});
