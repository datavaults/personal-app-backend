import { UserAssetController } from './user-asset.controller';

describe('UserAssetController', () => {
  let controller: UserAssetController;
  let service: any;

  beforeEach(async () => {
    service = {
      delete: jest.fn(),
    };
    controller = new UserAssetController(service);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should delete a user', async () => {
    jest.spyOn(service, 'delete').mockImplementation();
    expect(await controller.deleteUser).toBeTruthy();
  });
});
