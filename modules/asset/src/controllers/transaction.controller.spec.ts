import { ForbiddenException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import {
  CreateTransactionDTO,
  Transaction,
  TransactionController,
  TransactionService,
} from '@suite5/asset';
import { Repository } from 'typeorm';
import { UserData } from '@suite5/core/authentication/decorators';
import { AuthenticationService } from '@suite5/core/authentication/services';
import { ConfigService } from '@nestjs/config';

describe('TransactionController', () => {
  let controller: TransactionController;
  let service: TransactionService;
  let transaction: Transaction;
  const owner = {
    id: 1,
    email: 'owner@test.com',
  } as UserData;
  const intruder = {
    id: 2,
    email: 'intruder@test.com',
  } as UserData;

  beforeEach(async () => {
    transaction = new Transaction();
    transaction.userId = 1;

    const module: TestingModule = await Test.createTestingModule({
      controllers: [TransactionController],
      providers: [
        {
          provide: getRepositoryToken(Transaction),
          useClass: Repository,
        },
        {
          provide: AuthenticationService,
          useValue: {
            login: () => jest.fn(),
            logout: () => jest.fn(),
            authenticate: () => jest.fn(),
            register: () => jest.fn(),
            verify: () => jest.fn(),
            resetPassword: () => jest.fn(),
            checkPasswordStrength: () => jest.fn(),
            changePassword: () => jest.fn(),
            sendVerificationEmail: () => jest.fn(),
            sendResetPasswordEmail: () => jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useClass: jest.fn(() => ({
            get: jest.fn(),
          })),
        },
        TransactionService,
      ],
    }).compile();
    controller = module.get<TransactionController>(TransactionController);
    service = module.get<TransactionService>(TransactionService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it("should retrieve all user's transactions", async () => {
    jest.spyOn(service, 'retrieveByUserId').mockResolvedValue([transaction]);
    expect(await controller.getAllTransactions(owner)).toStrictEqual([
      transaction,
    ]);
  });

  it('should retrieve a transaction', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(transaction);
    expect(await controller.getTransaction(1, owner)).toBe(transaction);
  });

  it('should throw on retrieve if the transaction does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(transaction);
    try {
      await controller.getTransaction(1, intruder);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });

  it('should create a transaction', async () => {
    const data: CreateTransactionDTO = {
      buyerName: 'test',
      buyerUUID: 'test',
      buyerDescription: 'test',
      buyerWebsite: 'test',
      buyerType: 'test',
      sharedAssetId: 1,
      points: 10,
      timestamp: new Date(),
      hash: 'test',
      userId: 4,
    };
    const result: Transaction = transaction;
    jest.spyOn(service, 'create').mockResolvedValue(result);
    expect(await controller.create(owner, data)).toBe(result);
    expect(service.create).toBeCalledWith(data);
  });

  it('should create many transactions', async () => {
    const data: CreateTransactionDTO[] = [
      {
        buyerName: 'test',
        buyerUUID: 'test',
        buyerDescription: 'test',
        buyerWebsite: 'test',
        buyerType: 'test',
        sharedAssetId: 1,
        points: 10,
        timestamp: new Date(),
        hash: 'test',
        userId: 4,
      },
    ];
    jest.spyOn(service, 'create').mockResolvedValue(transaction);
    expect(await controller.createMany(owner, data)).toBeUndefined();
    expect(service.create).toBeCalledWith(data[0]);
  });

  it('should delete a transaction', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(transaction);
    jest.spyOn(service, 'remove').mockImplementation();
    expect(await controller.remove(owner, 1)).toBeUndefined();
  });

  it('should throw on delete if the transaction does not belong to the user', async () => {
    jest.spyOn(service, 'retrieveById').mockResolvedValue(transaction);
    try {
      await controller.remove(intruder, 1);
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeInstanceOf(ForbiddenException);
    }
  });
});
