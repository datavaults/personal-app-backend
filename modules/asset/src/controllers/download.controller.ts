import {
  Controller,
  Get,
  Res,
  UseGuards,
  UseInterceptors,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CurrentUser, UserData } from '@suite5/core/authentication/decorators';
import { MongoConnection } from '@suite5/core/authentication/decorators/mongo-connection.decorator';
import { AuthenticationGuard } from '@suite5/core/authentication/guards';
import { Connection } from 'mongoose';
import { DataFetcherService } from '../services';
import JSZip from 'jszip';
import { MongoInterceptor } from '../../../../src/interceptors';
import { Asset } from '../entities';
import { DownloadService } from '../services/download.service';

@Controller('download')
@UseGuards(AuthenticationGuard)
@ApiTags('download')
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiBearerAuth()
export class DownloadController {
  constructor(
    private readonly dataFetcherService: DataFetcherService,
    private readonly downloadService: DownloadService,
  ) {}

  @Get('/assets/specific')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Download specific assets' })
  async downloadAssets(
    @CurrentUser() user: UserData,
    @MongoConnection() mongoConnection: Connection,
    @Query('ids') ids: number[] = [],
    @Res() response: Response,
  ) {
    const assets: Asset[] = [];
    for (const id of ids) {
      assets.push(await this.dataFetcherService.retrieveAssetById(id));
    }
    if (!assets.length) {
      return { message: 'No assets found' };
    } else if (assets.length === 1) {
      const data = await this.dataFetcherService.retrieveAssetData(
        mongoConnection,
        assets[0].id,
        user.id,
      );
      const buffer = this.prepareBufferFile(assets[0], data, response);
      this.downloadService.pushResponse(buffer, response);
    } else {
      let zip = new JSZip();
      for (const asset of assets) {
        const data = await this.dataFetcherService.retrieveAssetData(
          mongoConnection,
          asset.id,
          user.id,
        );
        if (!data.length) {
          continue;
        }
        await this.fileToZip(asset, data, zip);
      }
      const buffer = await this.prepareBufferZip(zip, response, 'SelectedData');
      await this.downloadService.pushResponse(buffer, response);
    }
  }

  @Get('/asset/:id')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Download singular asset' })
  async downloadAsset(
    @CurrentUser() user: UserData,
    @MongoConnection() MongoConnection: Connection,
    @Res() response: Response,
    @Param('id') id: number,
  ) {
    const asset = await this.dataFetcherService.retrieveAssetById(id);
    if (!asset) {
      return { message: 'Asset not found' };
    }
    const data = await this.dataFetcherService.retrieveAssetData(
      MongoConnection,
      asset.id,
      user.id,
    );
    if (!data.length) {
      return { message: 'No data found for specified asset' };
    }
    const buffer = this.prepareBufferFile(asset, data, response);
    this.downloadService.pushResponse(buffer, response);
  }

  @Get('/assets/all')
  @UseInterceptors(MongoInterceptor)
  @ApiOperation({ summary: 'Download all assets' })
  async downloadAllAssets(
    @CurrentUser() user: UserData,
    @MongoConnection() mongoConnection: Connection,
    @Res() response: Response,
  ) {
    const assets = await this.dataFetcherService.retrieveByUserId(user.id);
    if (!assets.length) {
      return { message: 'No assets found' };
    }
    let zip = new JSZip();
    for (const asset of assets) {
      const data = await this.dataFetcherService.retrieveAssetData(
        mongoConnection,
        asset.id,
        user.id,
      );
      if (!data.length) {
        continue;
      }
      await this.fileToZip(asset, data, zip);
    }
    const buffer = await this.prepareBufferZip(zip, response, 'AllMyData');
    await this.downloadService.pushResponse(buffer, response);
  }

  prepareBufferFile(asset, data, response) {
    let buffer;
    if (asset.sourceType == 'PRATO') {
      buffer = Buffer.from(data[0].file[0], 'base64');
      response.set({
        'Content-Type': 'application/pdf',
        'Content-Disposition': `attachment; filename=${asset.name}-${asset.id}.pdf`,
        'Content-Length': buffer.length,
      });
    } else {
      buffer = JSON.stringify({ ...data, assetId: asset.uid });
      response.set({
        'Content-Type': 'application/json',
        'Content-Disposition': `attachment; filename=${asset.name}-${asset.id}.json`,
        'Content-Length': buffer.length,
      });
    }
    return buffer;
  }

  async prepareBufferZip(zip, response, filename) {
    const buffer = await zip.generateAsync({
      type: 'nodebuffer',
      compression: 'DEFLATE',
    });

    response.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `attachment; filename=${filename}.zip`,
      'Content-Length': buffer.length,
    });

    return buffer;
  }

  async fileToZip(asset, data, zip) {
    if (asset.sourceType == 'PRATO') {
      zip = await this.downloadService.zippify(
        zip,
        Buffer.from(data[0].file[0], 'base64'),
        `${asset.name}-${asset.id}`,
        'pdf',
      );
    } else {
      zip = await this.downloadService.zippify(
        zip,
        JSON.stringify({ ...data, assetId: asset.uid }),
        `${asset.name}-${asset.id}`,
      );
    }
  }
}
