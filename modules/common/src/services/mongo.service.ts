import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import mongoose from 'mongoose';

@Injectable()
export class MongoService {
  mongoUri: string;
  options: any;
  constructor(private readonly configService: ConfigService) {
    this.mongoUri = this.configService.get<string>('mongo.uri');
    this.options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
  }
  async connection(mongoUri = this.mongoUri, options = {}) {
    return await mongoose.createConnection(mongoUri, options);
  }

  async runWithConnection(
    callback,
    funcOptions: Array<any>,
    mongoUri = this.mongoUri,
    options = this.options,
  ) {
    const connection = await this.connection(mongoUri, options);
    for (const params of funcOptions) {
      await callback(connection, ...params);
    }
    await connection.close();
  }
}
