import { MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SendEmailDTO } from '@suite5/core/user/dto';
import dayjs from 'dayjs';

@Injectable()
export class MailService {
  constructor(
    private readonly config: ConfigService,
    @Inject(MailerService)
    private readonly mailerService: MailerService,
  ) {}

  async send(to: string, subject: string, template: string, context = {}) {
    await this.mailerService.sendMail({
      to,
      subject,
      template,
      context,
    });
    return true;
  }

  async sendToSupport(data: SendEmailDTO) {
    await this.mailerService.sendMail({
      to: this.config.get('email.supportAddress'),
      subject: data.subject,
      template: './support-email',
      context: {
        data,
        currentDate: dayjs(new Date()).format('DD MMMM YYYY HH:mm'),
      },
    });
    return true;
  }
}
