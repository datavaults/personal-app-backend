import { UserDescriptor } from './user-descriptor.interface';

export interface RabbitmqResponse {
  status: number;
  errors: string[];
  user: string | UserDescriptor;
  body: any;
}
