import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { join } from 'path';
import { MailService } from '@suite5/common/services';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        transport: `smtps://${config.get<string>(
          'email.user',
        )}:${config.get<string>('email.pass')}@${config.get<string>(
          'email.host',
        )}`,
        defaults: {
          from: `"Datavaults" <${config.get<string>('email.user')}>`,
        },
        template: {
          dir: join(__dirname, '../../../src/templates'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
            disableTemplateCache: true,
          },
        },
      }),
    }),
  ],
  providers: [MailService],
  exports: [MailService],
})
export class CommonModule {}
