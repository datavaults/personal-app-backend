import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';

//@IsSmallerThan(new Date(), { message: "receiptDate must be smaller than purchaseDate" })

export function IsSmallerThan(
  property: Date | string,
  validationOptions?: ValidationOptions,
) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsSmallerThan',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [maxDate] = args.constraints;
          const olderDate = Date.parse(value);
          return olderDate < maxDate;
        },
      },
    });
  };
}
