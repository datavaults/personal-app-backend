# Personal App Backend

The personal app backend application

## Installation

1. Make sure you have node version 14

```bash
# install version 14
$ nvm install 14

# use version 14
$ nvm use 14

# make it the default
$ nvm alias default 14
```

2. Install dependencies

```bash
# install dependencies
$ yarn
```

3. Create the `.env` file

```bash
# copy config
$ cp .env.template .env
```

4. Install submodules

```bash
$ git submodule update --init --recursive
```

5. Define an app secret in the `.env` file by adding a random string in `AUTH_MODULE_SECRET`

6. Follow Vault instruction below

7. Follow MongoDB instruction below

## Running the app

```bash
$ yarn start:dev
```

## Test

```bash
# unit tests
$ yarn test


# test coverage
$ yarn test:cov
```

## Configurations

### Mail Server ( Fake Mail Server)

```
hostname: localhost
port: 1025
```

mail inbox: ./data/email

### Postgres DB

```
hostname: localhost
port: 54320
POSTGRES_PASSWORD: datavaults
POSTGRES_DB: datavaults
```

### Vault

1. Add the following environment variables in your .env file:

```
VAULT_URL = http://127.0.0.1:8200/v1
VAULT_KEY = <YOUR_VAULT_KEY>
VAULT_ROOT_TOKEN = <YOUR_VAULT_ROOT_TOKEN>
VAULT_TOKEN_TTL = 1h
VAULT_TOKEN_RENEWABLE = true
```

### Mongo DB

1. Remove your existing user from keycloack/postgres
2. Add the following envirnonment variables in your .env file:

```
MONGO_HOST = localhost
MONGO_PORT = 27017
MONGO_NAME = admin
MONGO_USERNAME = admin
MONGO_PASSWORD= <ADMIN_PASSWORD>
```

### Run the init commnad

Initialize MongoDB, RabbitMQ,Vault, Postgres

```bash
bash scripts/db-vault-rabbitmq-init.sh
```

## Migrations

### Running migrations on postgres

```bash
$ yarn pg:migration:run
```

### Creating a migration for postgres

1. Make sure to add your entity inside `src/pg.entities.ts```

2. Run the below command where `[MIGRATION_NAME]` should be the migration name

```bash
$ yarn pg:migration:generate -n  [MIGRATION_NAME]
```

### Running seeds

1. Make sure to run above migrations
2. Run the below command:

```bash
$ yarn pg:migration:run:seed
```


### Troubleshooting

If you encounter any problem during setup you can follow the next steps:

1. Open a terminal window and run 
    ```bash
    docker ps
    ```
    propably you will see 5 containers up and running.

    You have to run 
    ```bash 
    docker-compose down -v
    ```
    to stop and remove containers.
    Then run again  
    ```bash
    docker ps
    ```
    to confirm it.

2. Remove all folders under the data folder
    ```
    ├── db 
    ├── mongodata
    └── vault
    ```

3. Go to .env file and remove values from (if any)
    ```
    VAULT_KEY=
    VAULT_ROOT_TOKEN=
    ```

4. Run the following command to initialize MongoDB, RabbitMQ,Vault, Postgres again.
    ```bash
    bash scripts/db-vault-rabbitmq-init.sh
    ```
    At the end of this process you will se a message to run the migrations. You can do that with 
    ```bash
    yarn pg:migration:run:seed
    ```

5. If everything is succeed you can run the command
    ```bash
    yarn start:dev
    ```
    and confirm that the application runs without problems.
    