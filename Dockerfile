FROM node:14-alpine as builder
WORKDIR /app
RUN apk add --update --no-cache python3 make g++ git
RUN pwd
COPY package.json ./
RUN npm_config_build_from_source=true yarn install
COPY . .
RUN yarn build -p tsconfig.build.json
RUN yarn tsc -p tsconfig.migrations.json
RUN yarn tsc -p tsconfig.seeds.json
RUN ls -rtla /app/dist


FROM node:14-alpine
WORKDIR /app
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist
RUN ls -rtla /app
COPY --from=builder /app/src/config ./config
COPY --from=builder /app/nest-cli.json .
COPY --from=builder /app/ormconfig.prod.js /app/ormconfig.js
COPY --from=builder /app/src/seeds/*.csv /app/dist/seeds/
COPY --from=builder /app/ormconfig.prod.seeds.js /app/ormconfig.seeds.js
CMD ["node", "/app/dist/src/main.js"]
