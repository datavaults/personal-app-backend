import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddUidToDataFetcher1626158392299 implements MigrationInterface {
  name = 'AddUidToDataFetcher1626158392299';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "source" ADD "uid" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "source" DROP COLUMN "uid"`);
  }
}
