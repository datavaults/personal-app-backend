import {MigrationInterface, QueryRunner} from "typeorm";

export class AddSourceIdInMobileApp1663754456746 implements MigrationInterface {
    name = 'AddSourceIdInMobileApp1663754456746'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" ADD "data_source_id" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" DROP COLUMN "data_source_id"`);
    }

}
