import {MigrationInterface, QueryRunner} from "typeorm";

export class AddPointsToSharedAsset1653902787650 implements MigrationInterface {
    name = 'AddPointsToSharedAsset1653902787650'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "points" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "points"`);
    }

}
