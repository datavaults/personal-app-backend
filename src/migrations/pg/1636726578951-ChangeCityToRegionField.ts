import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeCityToRegionField1636726578951 implements MigrationInterface {
    name = 'ChangeCityToRegionField1636726578951'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" RENAME COLUMN "city" TO "region"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" RENAME COLUMN "region" TO "city"`);
    }

}
