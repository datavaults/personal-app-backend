import {MigrationInterface, QueryRunner} from "typeorm";

export class AddStatusToMobileApp1654170091878 implements MigrationInterface {
    name = 'AddStatusToMobileApp1654170091878'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" ADD "status" boolean`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" DROP COLUMN "status"`);
    }

}
