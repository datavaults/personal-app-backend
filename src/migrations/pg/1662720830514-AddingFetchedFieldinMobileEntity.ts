import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingFetchedFieldinMobileEntity1662720830514 implements MigrationInterface {
    name = 'AddingFetchedFieldinMobileEntity1662720830514'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" ADD "fetched" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mobile_app" DROP COLUMN "fetched"`);
    }

}
