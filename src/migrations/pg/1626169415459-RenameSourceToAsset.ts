import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameSourceToAsset1626169415459 implements MigrationInterface {
  name = 'RenameSourceToAsset1626169415459';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "recent_update" RENAME COLUMN "source_id" TO "asset_id"`,
    );
    await queryRunner.query(
      `CREATE TABLE "asset" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "keywords" text, "url" character varying, "source_type" character varying NOT NULL, "created_by_id" integer NOT NULL, "credentials" json, "schedule" json, "errors" json, "uid" character varying NOT NULL, CONSTRAINT "PK_1209d107fe21482beaea51b745e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "asset" ADD CONSTRAINT "FK_772165b6f71d0f93dc7a031dfd2" FOREIGN KEY ("created_by_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "recent_update" ADD CONSTRAINT "FK_a78ebb5303c23e61bc964128657" FOREIGN KEY ("asset_id") REFERENCES "asset"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(`DROP TABLE "source" CASCADE`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "recent_update" DROP CONSTRAINT "FK_a78ebb5303c23e61bc964128657"`,
    );
    await queryRunner.query(
      `ALTER TABLE "asset" DROP CONSTRAINT "FK_772165b6f71d0f93dc7a031dfd2"`,
    );
    await queryRunner.query(`DROP TABLE "asset"`);
    await queryRunner.query(
      `ALTER TABLE "recent_update" RENAME COLUMN "asset_id" TO "source_id"`,
    );
    await queryRunner.query(
      `CREATE TABLE "source" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "source_id" character varying NOT NULL, "name" character varying NOT NULL, "keywords" text, "url" character varying, "source_type" character varying NOT NULL, "created_by_id" integer NOT NULL, "credentials" json, "schedule" json, "errors" json, CONSTRAINT "PK_018c433f8264b58c86363eaadde" PRIMARY KEY ("id"))`,
    );
  }
}
