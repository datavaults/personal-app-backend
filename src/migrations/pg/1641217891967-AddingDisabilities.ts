import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingDisabilities1641217891967 implements MigrationInterface {
    name = 'AddingDisabilities1641217891967'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."user_disabilities_enum" RENAME TO "user_disabilities_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_disabilities_enum" AS ENUM('Acquired brain injury', 'Anxiety disorders and stress', 'Autism spectrum disorder', 'Bipolar disorder', 'Cancer', 'Chronic pain', 'Dementia', 'Depression', 'Diabetes', 'Disability etiquette', 'Dyslexia', 'Dyspraxia', 'Epilepsy', 'Hearing Impairment (deafness)', 'Learning disability', 'Limb loss', 'Mental health', 'Migraine', 'Multiple sclerosis', 'Musculoskeletal disorders', 'Non Disabled', 'Schizophrenia', 'Visual impairment (blindness).')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" TYPE "public"."user_disabilities_enum"[] USING "disabilities"::"text"::"public"."user_disabilities_enum"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_disabilities_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_disabilities_enum_old" AS ENUM('Acquired brain injury', 'Anxiety disorders and stress', 'Autism spectrum disorder', 'Bipolar disorder', 'Cancer', 'Chronic pain', 'Dementia', 'Depression', 'Diabetes', 'Disability etiquette', 'Dyslexia', 'Dyspraxia', 'Epilepsy', 'Hearing Impairment (deafness)', 'Learning disability', 'Limb loss', 'Mental health', 'Migraine', 'Multiple sclerosis', 'Musculoskeletal disorders', 'Schizophrenia', 'Visual impairment (blindness).')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" TYPE "public"."user_disabilities_enum_old"[] USING "disabilities"::"text"::"public"."user_disabilities_enum_old"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_disabilities_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_disabilities_enum_old" RENAME TO "user_disabilities_enum"`);
    }

}
