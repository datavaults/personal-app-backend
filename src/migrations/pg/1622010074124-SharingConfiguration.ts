import {MigrationInterface, QueryRunner} from "typeorm";

export class SharingConfiguration1622010074124 implements MigrationInterface {
    name = 'SharingConfiguration1622010074124'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "sharing_configuration" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "configuration" json, "user_id" integer NOT NULL, CONSTRAINT "PK_9d629d77c00d91130dfb0c1dc10" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "sharing_configuration" ADD CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "sharing_configuration" DROP CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7"`);
        await queryRunner.query(`DROP TABLE "sharing_configuration"`);
    }

}
