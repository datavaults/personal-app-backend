import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeTypesOfFields1663679145901 implements MigrationInterface {
  name = 'ChangeTypesOfFields1663679145901';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN IF EXISTS "qualification_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "qualifications_list" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "occupations_list" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "transportation_list" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "disabilities_list" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "cultural_interest_list" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "civil_status_list" integer `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "civil_status_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "cultural_interest_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "disabilities_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "transportation_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "occupations_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "qualifications_list"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "qualification_list" integer array NOT NULL DEFAULT '{}'`,
    );
  }
}
