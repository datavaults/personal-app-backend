import {MigrationInterface, QueryRunner} from "typeorm";

export class message1623145775079 implements MigrationInterface {
    name = 'message1623145775079'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "message" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "sender_name" character varying NOT NULL, "sender_description" character varying NOT NULL, "sender_website" character varying NOT NULL, "sender_uuid" character varying NOT NULL, "topic" character varying NOT NULL, "message_type" character varying NOT NULL, "sender_type" character varying NOT NULL, "inbox_type" character varying NOT NULL, "configuration" json, "body" character varying NOT NULL, "reciever_id" integer, CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_c82ed4cf2450026636f654b3638" FOREIGN KEY ("reciever_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_c82ed4cf2450026636f654b3638"`);
        await queryRunner.query(`DROP TABLE "message"`);
    }

}
