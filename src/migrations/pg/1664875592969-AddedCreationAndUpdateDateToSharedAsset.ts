import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedCreationAndUpdateDateToSharedAsset1664875592969
  implements MigrationInterface
{
  name = 'AddedCreationAndUpdateDateToSharedAsset1664875592969';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "shared_asset" ADD "shared_date" TIMESTAMP NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" ADD "last_updated_at" TIMESTAMP NOT NULL DEFAULT now()`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "shared_asset" DROP COLUMN "last_updated_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" DROP COLUMN "shared_date"`,
    );
  }
}
