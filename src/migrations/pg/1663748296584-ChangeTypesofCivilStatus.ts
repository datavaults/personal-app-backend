import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeTypesofCivilStatus1663748296584 implements MigrationInterface {
    name = 'ChangeTypesofCivilStatus1663748296584'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "civil_status_list"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "civil_status_list" integer array NOT NULL DEFAULT '{}'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "civil_status_list"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "civil_status_list" integer`);
    }

}
