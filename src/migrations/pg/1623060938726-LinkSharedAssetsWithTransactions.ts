import {MigrationInterface, QueryRunner} from "typeorm";

export class LinkSharedAssetsWithTransactions1623060938726 implements MigrationInterface {
    name = 'LinkSharedAssetsWithTransactions1623060938726'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" RENAME COLUMN "asset" TO "shared_asset_id"`);
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "sharing_type" character varying DEFAULT 'normal'`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "shared_asset_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "shared_asset_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_bc6a22a2860827764270aad78c0" FOREIGN KEY ("shared_asset_id") REFERENCES "shared_asset"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_bc6a22a2860827764270aad78c0"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "shared_asset_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "shared_asset_id" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "sharing_type"`);
        await queryRunner.query(`ALTER TABLE "transaction" RENAME COLUMN "shared_asset_id" TO "asset"`);
    }

}
