import { MigrationInterface, QueryRunner } from 'typeorm';

export class BlacklistCascade1621935986176 implements MigrationInterface {
  name = 'BlacklistCascade1621935986176';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "blacklist" DROP CONSTRAINT "FK_88007aa2318bc77eeb17013e20e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blacklist" ADD CONSTRAINT "FK_88007aa2318bc77eeb17013e20e" FOREIGN KEY ("blacklisted_by_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "blacklist" DROP CONSTRAINT "FK_88007aa2318bc77eeb17013e20e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blacklist" ADD CONSTRAINT "FK_88007aa2318bc77eeb17013e20e" FOREIGN KEY ("blacklisted_by_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
