import {MigrationInterface, QueryRunner} from "typeorm";

export class verificationTokens1628666597678 implements MigrationInterface {
    name = 'verificationTokens1628666597678'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" DROP CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1"`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" DROP CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea"`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" DROP CONSTRAINT "FK_d3b364dd006397babf8d411add2"`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" DROP CONSTRAINT "FK_5773897736ac42599dd998d6258"`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "email_verification_token" character varying`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "reset_password_verification_token" character varying`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" ADD CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" ADD CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1" FOREIGN KEY ("occupation_id") REFERENCES "occupation"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" ADD CONSTRAINT "FK_5773897736ac42599dd998d6258" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" ADD CONSTRAINT "FK_d3b364dd006397babf8d411add2" FOREIGN KEY ("skill_id") REFERENCES "skill"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" DROP CONSTRAINT "FK_d3b364dd006397babf8d411add2"`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" DROP CONSTRAINT "FK_5773897736ac42599dd998d6258"`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" DROP CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1"`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" DROP CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea"`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "reset_password_verification_token"`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "email_verification_token"`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" ADD CONSTRAINT "FK_5773897736ac42599dd998d6258" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."user_qualifications_skill" ADD CONSTRAINT "FK_d3b364dd006397babf8d411add2" FOREIGN KEY ("skill_id") REFERENCES "skill"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" ADD CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."user_occupations_occupation" ADD CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1" FOREIGN KEY ("occupation_id") REFERENCES "occupation"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
