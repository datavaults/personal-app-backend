import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTypeToAsset1627041983469 implements MigrationInterface {
  name = 'AddTypeToAsset1627041983469';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "asset" ADD "type" character varying NOT NULL DEFAULT 'dataset'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "type"`);
  }
}
