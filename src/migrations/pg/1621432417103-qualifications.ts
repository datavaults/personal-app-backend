import {MigrationInterface, QueryRunner} from "typeorm";

export class qualifications1621432417103 implements MigrationInterface {
    name = 'qualifications1621432417103'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_qualifications_skill" ("user_id" integer NOT NULL, "skill_id" integer NOT NULL, CONSTRAINT "PK_842b06badf273d4830e9ea601ae" PRIMARY KEY ("user_id", "skill_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_5773897736ac42599dd998d625" ON "user_qualifications_skill" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_d3b364dd006397babf8d411add" ON "user_qualifications_skill" ("skill_id") `);
        await queryRunner.query(`ALTER TABLE "user_qualifications_skill" ADD CONSTRAINT "FK_5773897736ac42599dd998d6258" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_qualifications_skill" ADD CONSTRAINT "FK_d3b364dd006397babf8d411add2" FOREIGN KEY ("skill_id") REFERENCES "skill"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_qualifications_skill" DROP CONSTRAINT "FK_d3b364dd006397babf8d411add2"`);
        await queryRunner.query(`ALTER TABLE "user_qualifications_skill" DROP CONSTRAINT "FK_5773897736ac42599dd998d6258"`);
        await queryRunner.query(`DROP INDEX "IDX_d3b364dd006397babf8d411add"`);
        await queryRunner.query(`DROP INDEX "IDX_5773897736ac42599dd998d625"`);
        await queryRunner.query(`DROP TABLE "user_qualifications_skill"`);
    }

}
