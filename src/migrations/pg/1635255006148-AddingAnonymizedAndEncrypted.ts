import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingAnonymizedAndEncrypted1635255006148 implements MigrationInterface {
    name = 'AddingAnonymizedAndEncrypted1635255006148'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "sharing_type"`);
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "anonymized" boolean DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "encrypted" boolean DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "encrypted"`);
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "anonymized"`);
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "sharing_type" character varying DEFAULT 'normal'`);
    }

}
