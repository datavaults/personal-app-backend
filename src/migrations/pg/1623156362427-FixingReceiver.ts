import {MigrationInterface, QueryRunner} from "typeorm";

export class FixingReceiver1623156362427 implements MigrationInterface {
    name = 'FixingReceiver1623156362427'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_c82ed4cf2450026636f654b3638"`);
        await queryRunner.query(`ALTER TABLE "message" RENAME COLUMN "reciever_id" TO "receiver_id"`);
        await queryRunner.query(`ALTER TABLE "message" ALTER COLUMN "receiver_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_f4da40532b0102d51beb220f16a" FOREIGN KEY ("receiver_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_f4da40532b0102d51beb220f16a"`);
        await queryRunner.query(`ALTER TABLE "message" ALTER COLUMN "receiver_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "message" RENAME COLUMN "receiver_id" TO "reciever_id"`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_c82ed4cf2450026636f654b3638" FOREIGN KEY ("reciever_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
