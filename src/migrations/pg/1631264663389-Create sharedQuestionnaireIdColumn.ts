import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateSharedQuestionnaireIdColumn1631264663389 implements MigrationInterface {
    name = 'CreateSharedQuestionnaireIdColumn1631264663389'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" ADD "shared_questionnaire_id" integer`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_bc6a22a2860827764270aad78c0"`);
        await queryRunner.query(`ALTER TABLE "transaction" ALTER COLUMN "shared_asset_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_bc6a22a2860827764270aad78c0" FOREIGN KEY ("shared_asset_id") REFERENCES "shared_asset"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_bc6a22a2860827764270aad78c0"`);
        await queryRunner.query(`ALTER TABLE "transaction" ALTER COLUMN "shared_asset_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_bc6a22a2860827764270aad78c0" FOREIGN KEY ("shared_asset_id") REFERENCES "shared_asset"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "shared_questionnaire_id"`);
    }

}
