import {MigrationInterface, QueryRunner} from "typeorm";

export class AccessPolicyTemplate1647353516826 implements MigrationInterface {
    name = 'AccessPolicyTemplate1647353516826'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "access_policy_template" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying, "description" character varying, "configuration" jsonb, "user_id" integer NOT NULL, CONSTRAINT "PK_c8a7f4583341fb72ccd8f043468" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "access_policy_template" ADD CONSTRAINT "FK_5ec0873bc9e724219b549afbcb4" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "access_policy_template" DROP CONSTRAINT "FK_5ec0873bc9e724219b549afbcb4"`);
        await queryRunner.query(`DROP TABLE "access_policy_template"`);
    }

}
