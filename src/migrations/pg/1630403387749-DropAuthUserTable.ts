import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropAuthUserTable1630403387749 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "auth_user"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "auth_user" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "email" character varying NOT NULL, "password" character varying NOT NULL, "is_verified" boolean NOT NULL, "is_enabled" boolean NOT NULL , CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab8" PRIMARY KEY ("id"))`,
    );
  }
}
