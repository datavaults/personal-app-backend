import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeNameOfTermsInUser1657034485857 implements MigrationInterface {
    name = 'ChangeNameOfTermsInUser1657034485857'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "last_terms_acceptance" TIMESTAMP`);
        await queryRunner.query(`ALTER TYPE "public"."user_demonstrator_enum" RENAME TO "user_demonstrator_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_demonstrator_enum" AS ENUM('prato', 'olympiacos', 'piraeus', 'miwenergia', 'andaman7')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "demonstrator" TYPE "public"."user_demonstrator_enum" USING "demonstrator"::"text"::"public"."user_demonstrator_enum"`);
        await queryRunner.query(`DROP TYPE "public"."user_demonstrator_enum_old"`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_demonstrator_enum_old" AS ENUM('prato', 'olympiakos', 'piraeus', 'miwenergia', 'andaman7')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "demonstrator" TYPE "public"."user_demonstrator_enum_old" USING "demonstrator"::"text"::"public"."user_demonstrator_enum_old"`);
        await queryRunner.query(`DROP TYPE "public"."user_demonstrator_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_demonstrator_enum_old" RENAME TO "user_demonstrator_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "last_terms_acceptance"`);
    }

}
