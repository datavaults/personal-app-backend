import {MigrationInterface, QueryRunner} from "typeorm";

export class userProfile1621399608498 implements MigrationInterface {
    name = 'userProfile1621399608498'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_qualification_skill" ("user_id" integer NOT NULL, "skill_id" integer NOT NULL, CONSTRAINT "PK_e46d38f2bb10575329e7ad9a6f6" PRIMARY KEY ("user_id", "skill_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_b626b483159d50d5423b013ebe" ON "user_qualification_skill" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_99373968e835ba718d4d8518ba" ON "user_qualification_skill" ("skill_id") `);
        await queryRunner.query(`ALTER TABLE "user" ADD "share_profile" boolean`);
        await queryRunner.query(`ALTER TABLE "user" ADD "request_resolver_settings" boolean`);
        await queryRunner.query(`ALTER TABLE "user" ADD "nationality" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "profile_picture" jsonb`);
        await queryRunner.query(`CREATE TYPE "user_cultural_interest_enum" AS ENUM('Visual Arts', 'Performing Arts', 'Plastic Arts', 'Music', 'Fashion', 'Architecture', 'History')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "cultural_interest" "user_cultural_interest_enum" array NOT NULL DEFAULT '{}'`);
        await queryRunner.query(`CREATE TYPE "user_transportation_means_enum" AS ENUM('On foot', 'Bicycle', 'Motorcycle', 'Car', 'Taxi', 'Public Bus', 'Tram', 'Subway')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "transportation_means" "user_transportation_means_enum" array NOT NULL DEFAULT '{}'`);
        await queryRunner.query(`CREATE TYPE "user_civil_status_enum" AS ENUM('Married', 'Widowed', 'Separated', 'Divorced', 'Single')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "civil_status" "user_civil_status_enum" NOT NULL DEFAULT 'Single'`);
        await queryRunner.query(`CREATE TYPE "user_disabilities_enum" AS ENUM('Acquired brain injury', 'Anxiety disorders and stress', 'Autism spectrum disorder', 'Bipolar disorder', 'Cancer', 'Chronic pain', 'Dementia', 'Depression', 'Diabetes', 'Disability etiquette', 'Dyslexia', 'Dyspraxia', 'Epilepsy', 'Hearing Impairment (deafness)', 'Learning disability', 'Limb loss', 'Mental health', 'Migraine', 'Multiple sclerosis', 'Musculoskeletal disorders', 'Schizophrenia', 'Visual impairment (blindness).')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "disabilities" "user_disabilities_enum" array NOT NULL DEFAULT '{}'`);
        await queryRunner.query(`ALTER TABLE "user_qualification_skill" ADD CONSTRAINT "FK_b626b483159d50d5423b013ebeb" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_qualification_skill" ADD CONSTRAINT "FK_99373968e835ba718d4d8518ba6" FOREIGN KEY ("skill_id") REFERENCES "skill"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_qualification_skill" DROP CONSTRAINT "FK_99373968e835ba718d4d8518ba6"`);
        await queryRunner.query(`ALTER TABLE "user_qualification_skill" DROP CONSTRAINT "FK_b626b483159d50d5423b013ebeb"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "disabilities"`);
        await queryRunner.query(`DROP TYPE "user_disabilities_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "civil_status"`);
        await queryRunner.query(`DROP TYPE "user_civil_status_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "transportation_means"`);
        await queryRunner.query(`DROP TYPE "user_transportation_means_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "cultural_interest"`);
        await queryRunner.query(`DROP TYPE "user_cultural_interest_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "profile_picture"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "nationality"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "request_resolver_settings"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "share_profile"`);
        await queryRunner.query(`DROP INDEX "IDX_99373968e835ba718d4d8518ba"`);
        await queryRunner.query(`DROP INDEX "IDX_b626b483159d50d5423b013ebe"`);
        await queryRunner.query(`DROP TABLE "user_qualification_skill"`);
    }

}
