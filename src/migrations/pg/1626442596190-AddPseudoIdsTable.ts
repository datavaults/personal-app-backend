import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPseudoIdsTable1626442596190 implements MigrationInterface {
  name = 'AddPseudoIdsTable1626442596190';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "pseudo_id" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "pseudo_id" character varying NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_3dd5487493fdd834f3e915b4646" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "pseudo_id" ADD CONSTRAINT "FK_6e6a9833b02af15a38a91255d94" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "pseudo_id" DROP CONSTRAINT "FK_6e6a9833b02af15a38a91255d94"`,
    );
    await queryRunner.query(`DROP TABLE "pseudo_id"`);
  }
}
