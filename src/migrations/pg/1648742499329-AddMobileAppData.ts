import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddMobileAppData1648742499329 implements MigrationInterface {
  name = 'AddMobileAppData1648742499329';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "mobile_app" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "sub_id" character varying NOT NULL, "record_date" TIMESTAMP NOT NULL, "data_source" character varying NOT NULL, "data" json NOT NULL, CONSTRAINT "PK_f9a18f6a5b6c50d35e0ef6744a7" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "mobile_app"`);
  }
}
