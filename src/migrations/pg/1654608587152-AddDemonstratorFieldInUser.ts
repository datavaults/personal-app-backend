import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDemonstratorFieldInUser1654608587152 implements MigrationInterface {
    name = 'AddDemonstratorFieldInUser1654608587152'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_demonstrator_enum" AS ENUM('prato', 'olympiakos', 'piraeus', 'miwenergia', 'andaman7')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "demonstrator" "public"."user_demonstrator_enum"`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "demonstrator"`);
        await queryRunner.query(`DROP TYPE "public"."user_demonstrator_enum"`);
    }

}
