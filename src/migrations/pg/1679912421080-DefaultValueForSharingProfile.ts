import {MigrationInterface, QueryRunner} from "typeorm";

export class DefaultValueForSharingProfile1679912421080 implements MigrationInterface {
    name = 'DefaultValueForSharingProfile1679912421080'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "share_profile" SET DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "share_profile" DROP DEFAULT`);
    }

}
