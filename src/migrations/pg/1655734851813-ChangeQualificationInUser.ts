import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeQualificationInUser1655734851813
  implements MigrationInterface
{
  name = 'ChangeQualificationInUser1655734851813';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."user_qualifications_enum" AS ENUM('High school', 'Bachelor Degree', 'Master\`s degree', 'Doctor of Philosophy')`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "qualifications" "public"."user_qualifications_enum" array NOT NULL DEFAULT '{}'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TYPE "public"."user_qualifications_enum"`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD "qualifications" "public"."user_qualifications_enum"`,
    );
  }
}
