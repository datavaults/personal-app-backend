import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddLanguageToUserEntity1653565775024
  implements MigrationInterface
{
  name = 'AddLanguageToUserEntity1653565775024';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "language" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "language"`);
  }
}
