import {MigrationInterface, QueryRunner} from "typeorm";

export class SharedAssetFK1626350556136 implements MigrationInterface {
    name = 'SharedAssetFK1626350556136'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD CONSTRAINT "FK_58a3dfb61e4615e06bc2ea8b04a" FOREIGN KEY ("asset_id") REFERENCES "asset"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP CONSTRAINT "FK_58a3dfb61e4615e06bc2ea8b04a"`);
    }

}
