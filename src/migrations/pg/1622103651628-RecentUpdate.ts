import { MigrationInterface, QueryRunner } from 'typeorm';

export class RecentUpdate1622103651628 implements MigrationInterface {
  name = 'RecentUpdate1622103651628';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "recent_update" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "last_date" TIMESTAMP NOT NULL, "successful" boolean NOT NULL, "source_id" integer NOT NULL, CONSTRAINT "PK_64e740b6c3bc2eb8e14fb1894b7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "recent_update" ADD CONSTRAINT "FK_7cafda5f7f05035a6cc47f5c064" FOREIGN KEY ("source_id") REFERENCES "source"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "recent_update" DROP CONSTRAINT "FK_7cafda5f7f05035a6cc47f5c064"`,
    );
    await queryRunner.query(`DROP TABLE "recent_update"`);
  }
}
