import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingDefaultValueInRequestResolveSettings1680704761662 implements MigrationInterface {
    name = 'AddingDefaultValueInRequestResolveSettings1680704761662'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "request_resolver_settings" SET DEFAULT true`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "request_resolver_settings" DROP DEFAULT`);
    }

}
