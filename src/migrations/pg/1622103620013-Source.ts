import { MigrationInterface, QueryRunner } from 'typeorm';

export class Source1622103620013 implements MigrationInterface {
  name = 'Source1622103620013';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "source" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "source_id" character varying NOT NULL, "name" character varying NOT NULL, "keywords" text, "url" character varying, "source_type" character varying NOT NULL, "created_by_id" integer NOT NULL, "credentials" json, "schedule" json, "errors" json, CONSTRAINT "PK_018c433f8264b58c86363eaadde" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "source" ADD CONSTRAINT "FK_ee14f9d3e9cfa09e2cb12963141" FOREIGN KEY ("created_by_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "source" DROP CONSTRAINT "FK_ee14f9d3e9cfa09e2cb12963141"`,
    );
    await queryRunner.query(`DROP TABLE "source"`);
  }
}
