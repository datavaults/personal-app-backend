import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddAssetDescription1634905151696 implements MigrationInterface {
  name = 'AddAssetDescription1634905151696';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "asset" ADD "description" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "description"`);
  }
}
