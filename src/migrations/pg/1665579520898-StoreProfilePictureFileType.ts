import { MigrationInterface, QueryRunner } from 'typeorm';

export class StoreProfilePictureFileType1665579520898
  implements MigrationInterface
{
  name = 'StoreProfilePictureFileType1665579520898';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "profile_picture_type" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "profile_picture_type"`,
    );
  }
}
