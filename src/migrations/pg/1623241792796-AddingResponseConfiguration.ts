import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingResponseConfiguration1623241792796 implements MigrationInterface {
    name = 'AddingResponseConfiguration1623241792796'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" ADD "response_configuration" json`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "response_configuration"`);
    }

}
