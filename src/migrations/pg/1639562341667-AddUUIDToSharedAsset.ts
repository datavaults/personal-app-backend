import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUUIDToSharedAsset1639562341667 implements MigrationInterface {
    name = 'AddUUIDToSharedAsset1639562341667'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" ADD "uid" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "shared_asset" DROP COLUMN "uid"`);
    }

}
