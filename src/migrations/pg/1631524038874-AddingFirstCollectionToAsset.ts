import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingFirstCollectionToAsset1631524038874 implements MigrationInterface {
    name = 'AddingFirstCollectionToAsset1631524038874'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" ADD "first_collection" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "first_collection"`);
    }

}
