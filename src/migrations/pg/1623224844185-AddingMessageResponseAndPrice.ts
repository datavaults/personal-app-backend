import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingMessageResponseAndPrice1623224844185 implements MigrationInterface {
    name = 'AddingMessageResponseAndPrice1623224844185'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" ADD "price" integer`);
        await queryRunner.query(`ALTER TABLE "message" ADD "response" character varying NOT NULL DEFAULT 'Pending'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "response"`);
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "price"`);
    }

}
