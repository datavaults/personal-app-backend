import { MigrationInterface, QueryRunner } from 'typeorm';

export class SharedAsset1622204934628 implements MigrationInterface {
  name = 'SharedAsset1622204934628';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "shared_asset" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "asset_id" integer NOT NULL, "name" character varying NOT NULL, "description" character varying, "keywords" text, "configuration" json, "user_id" integer NOT NULL, CONSTRAINT "PK_b71134fd8e9744b54d99a4e4e04" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" ADD CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "shared_asset" DROP CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e"`,
    );
    await queryRunner.query(`DROP TABLE "shared_asset"`);
  }
}
