import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCityInUsers1643035783597 implements MigrationInterface {
    name = 'AddCityInUsers1643035783597'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "city" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "city"`);
    }

}
