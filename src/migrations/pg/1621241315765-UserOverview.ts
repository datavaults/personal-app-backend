import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserOverview1621241315765 implements MigrationInterface {
  name = 'UserOverview1621241315765';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "street" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "postcode" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "city" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "country" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "phone" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "birth_date" TIMESTAMP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "place_of_birth" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "national_insurance_number" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "member_number" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "member_number"`);
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "national_insurance_number"`,
    );
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "place_of_birth"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "birth_date"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "phone"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "country"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "city"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "postcode"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "street"`);
  }
}
