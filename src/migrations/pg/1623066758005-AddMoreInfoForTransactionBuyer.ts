import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddMoreInfoForTransactionBuyer1623066758005
  implements MigrationInterface
{
  name = 'AddMoreInfoForTransactionBuyer1623066758005';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "buyer"`);
    await queryRunner.query(
      `ALTER TABLE "transaction" ADD "buyer_name" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" ADD "buyer_uuid" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" ADD "buyer_description" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" ADD "buyer_website" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transaction" DROP COLUMN "buyer_website"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" DROP COLUMN "buyer_description"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" DROP COLUMN "buyer_uuid"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" DROP COLUMN "buyer_name"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction" ADD "buyer" character varying NOT NULL`,
    );
  }
}
