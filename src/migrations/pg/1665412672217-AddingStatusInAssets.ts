import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingStatusInAssets1665412672217 implements MigrationInterface {
    name = 'AddingStatusInAssets1665412672217'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" ADD "status" boolean DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "status"`);
    }

}
