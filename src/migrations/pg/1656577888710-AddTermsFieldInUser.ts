import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTermsFieldInUser1656577888710 implements MigrationInterface {
    name = 'AddTermsFieldInUser1656577888710'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "lastTermsAcceptance" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "lastTermsAcceptance"`);
    }

}
