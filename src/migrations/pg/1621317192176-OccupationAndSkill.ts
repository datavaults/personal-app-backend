import {MigrationInterface, QueryRunner} from "typeorm";

export class OccupationAndSkill1621317192176 implements MigrationInterface {
    name = 'OccupationAndSkill1621317192176'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "occupation" ("id" SERIAL NOT NULL, "concept_type" character varying, "concept_uri" character varying, "isco_group" character varying, "skill_type" character varying, "reuse_level" character varying, "preferred_label" character varying, "alt_labels" character varying, "hidden_labels" character varying, "status" character varying, "modified_date" TIMESTAMP, "regulated_profession_note" character varying, "scope_note" character varying, "definition" character varying, "in_scheme" character varying, "description" character varying, CONSTRAINT "PK_07cfcefef555693d96dce8805c5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "skill" ("id" SERIAL NOT NULL, "concept_type" character varying, "concept_uri" character varying, "skill_type" character varying, "reuse_level" character varying, "preferred_label" character varying, "alt_labels" character varying, "hidden_labels" character varying, "status" character varying, "modified_date" TIMESTAMP, "scope_note" character varying, "definition" character varying, "in_scheme" character varying, "description" character varying, CONSTRAINT "PK_a0d33334424e64fb78dc3ce7196" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_occupations_occupation" ("user_id" integer NOT NULL, "occupation_id" integer NOT NULL, CONSTRAINT "PK_cb7fabaa2939647cdba71848c9a" PRIMARY KEY ("user_id", "occupation_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_30191f4b4099cd2b3e22c4cfae" ON "user_occupations_occupation" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_5d31a237ede96c0e8107ac85eb" ON "user_occupations_occupation" ("occupation_id") `);
        await queryRunner.query(`CREATE TABLE "user_skills_skill" ("user_id" integer NOT NULL, "skill_id" integer NOT NULL, CONSTRAINT "PK_68002f532a7848141e0fa32711d" PRIMARY KEY ("user_id", "skill_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_78b253ae60c0ac087a7f82780f" ON "user_skills_skill" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_b447b1a2dc7552a716532e2216" ON "user_skills_skill" ("skill_id") `);
        await queryRunner.query(`ALTER TABLE "user_occupations_occupation" ADD CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_occupations_occupation" ADD CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1" FOREIGN KEY ("occupation_id") REFERENCES "occupation"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_skills_skill" ADD CONSTRAINT "FK_78b253ae60c0ac087a7f82780f7" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_skills_skill" ADD CONSTRAINT "FK_b447b1a2dc7552a716532e22166" FOREIGN KEY ("skill_id") REFERENCES "skill"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_skills_skill" DROP CONSTRAINT "FK_b447b1a2dc7552a716532e22166"`);
        await queryRunner.query(`ALTER TABLE "user_skills_skill" DROP CONSTRAINT "FK_78b253ae60c0ac087a7f82780f7"`);
        await queryRunner.query(`ALTER TABLE "user_occupations_occupation" DROP CONSTRAINT "FK_5d31a237ede96c0e8107ac85eb1"`);
        await queryRunner.query(`ALTER TABLE "user_occupations_occupation" DROP CONSTRAINT "FK_30191f4b4099cd2b3e22c4cfaea"`);
        await queryRunner.query(`DROP INDEX "IDX_b447b1a2dc7552a716532e2216"`);
        await queryRunner.query(`DROP INDEX "IDX_78b253ae60c0ac087a7f82780f"`);
        await queryRunner.query(`DROP TABLE "user_skills_skill"`);
        await queryRunner.query(`DROP INDEX "IDX_5d31a237ede96c0e8107ac85eb"`);
        await queryRunner.query(`DROP INDEX "IDX_30191f4b4099cd2b3e22c4cfae"`);
        await queryRunner.query(`DROP TABLE "user_occupations_occupation"`);
        await queryRunner.query(`DROP TABLE "skill"`);
        await queryRunner.query(`DROP TABLE "occupation"`);
    }

}
