import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingOptionsInDisabilitiesSkillsOccupations1658235394878 implements MigrationInterface {
    name = 'AddingOptionsInDisabilitiesSkillsOccupations1658235394878'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."user_cultural_interest_enum" RENAME TO "user_cultural_interest_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_cultural_interest_enum" AS ENUM('Visual Arts', 'Performing Arts', 'Plastic Arts', 'Music', 'Fashion', 'Architecture', 'History', 'I do not want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" TYPE "public"."user_cultural_interest_enum"[] USING "cultural_interest"::"text"::"public"."user_cultural_interest_enum"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_cultural_interest_enum_old"`);
        await queryRunner.query(`ALTER TYPE "public"."user_transportation_means_enum" RENAME TO "user_transportation_means_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_transportation_means_enum" AS ENUM('On foot', 'Bicycle', 'Motorcycle', 'Car', 'Taxi', 'Public Bus', 'Tram', 'Subway', 'I do not want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" TYPE "public"."user_transportation_means_enum"[] USING "transportation_means"::"text"::"public"."user_transportation_means_enum"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_transportation_means_enum_old"`);
        await queryRunner.query(`ALTER TYPE "public"."user_civil_status_enum" RENAME TO "user_civil_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_civil_status_enum" AS ENUM('Married', 'Widowed', 'Separated', 'Divorced', 'Single', 'I do not want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "civil_status" TYPE "public"."user_civil_status_enum" USING "civil_status"::"text"::"public"."user_civil_status_enum"`);
        await queryRunner.query(`DROP TYPE "public"."user_civil_status_enum_old"`);
        await queryRunner.query(`ALTER TYPE "public"."user_disabilities_enum" RENAME TO "user_disabilities_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."user_disabilities_enum" AS ENUM('Acquired brain injury', 'Anxiety disorders and stress', 'Autism spectrum disorder', 'Bipolar disorder', 'Cancer', 'Chronic pain', 'Dementia', 'Depression', 'Diabetes', 'Disability etiquette', 'Dyslexia', 'Dyspraxia', 'Epilepsy', 'Hearing Impairment (deafness)', 'Learning disability', 'Limb loss', 'Mental health', 'Migraine', 'Multiple sclerosis', 'Musculoskeletal disorders', 'Non Disabled', 'I do not want to answer', 'Schizophrenia', 'Visual impairment (blindness).')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" TYPE "public"."user_disabilities_enum"[] USING "disabilities"::"text"::"public"."user_disabilities_enum"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_disabilities_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_disabilities_enum_old" AS ENUM('Acquired brain injury', 'Anxiety disorders and stress', 'Autism spectrum disorder', 'Bipolar disorder', 'Cancer', 'Chronic pain', 'Dementia', 'Depression', 'Diabetes', 'Disability etiquette', 'Dyslexia', 'Dyspraxia', 'Epilepsy', 'Hearing Impairment (deafness)', 'Learning disability', 'Limb loss', 'Mental health', 'Migraine', 'Multiple sclerosis', 'Musculoskeletal disorders', 'Non Disabled', 'I don''t want to answer', 'Schizophrenia', 'Visual impairment (blindness).')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" TYPE "public"."user_disabilities_enum_old"[] USING "disabilities"::"text"::"public"."user_disabilities_enum_old"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "disabilities" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_disabilities_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_disabilities_enum_old" RENAME TO "user_disabilities_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."user_civil_status_enum_old" AS ENUM('Married', 'Widowed', 'Separated', 'Divorced', 'Single', 'I don''t want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "civil_status" TYPE "public"."user_civil_status_enum_old" USING "civil_status"::"text"::"public"."user_civil_status_enum_old"`);
        await queryRunner.query(`DROP TYPE "public"."user_civil_status_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_civil_status_enum_old" RENAME TO "user_civil_status_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."user_transportation_means_enum_old" AS ENUM('On foot', 'Bicycle', 'Motorcycle', 'Car', 'Taxi', 'Public Bus', 'Tram', 'Subway', 'I don''t want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" TYPE "public"."user_transportation_means_enum_old"[] USING "transportation_means"::"text"::"public"."user_transportation_means_enum_old"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "transportation_means" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_transportation_means_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_transportation_means_enum_old" RENAME TO "user_transportation_means_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."user_cultural_interest_enum_old" AS ENUM('Visual Arts', 'Performing Arts', 'Plastic Arts', 'Music', 'Fashion', 'Architecture', 'History', 'I don''t want to answer')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" TYPE "public"."user_cultural_interest_enum_old"[] USING "cultural_interest"::"text"::"public"."user_cultural_interest_enum_old"[]`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "cultural_interest" SET DEFAULT '{}'`);
        await queryRunner.query(`DROP TYPE "public"."user_cultural_interest_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."user_cultural_interest_enum_old" RENAME TO "user_cultural_interest_enum"`);
    }

}
