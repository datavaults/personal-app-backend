import { MigrationInterface, QueryRunner } from 'typeorm';

export class SetOnDeleteAndFixCivilStatus1622444307685
  implements MigrationInterface
{
  name = 'SetOnDeleteAndFixCivilStatus1622444307685';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "sharing_configuration" DROP CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" DROP CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "civil_status" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "civil_status" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "sharing_configuration" ADD CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" ADD CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "shared_asset" DROP CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "sharing_configuration" DROP CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "civil_status" SET DEFAULT 'Single'`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "civil_status" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_asset" ADD CONSTRAINT "FK_dc3b39a14e2cccaa244620afa4e" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "sharing_configuration" ADD CONSTRAINT "FK_82e7b4e2a02f6ea54a839c00cc7" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
