import {MigrationInterface, QueryRunner} from "typeorm";

export class keycloakSUB1621953334159 implements MigrationInterface {
    name = 'keycloakSUB1621953334159'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "sub" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "sub"`);
    }

}
