import {MigrationInterface, QueryRunner} from "typeorm";

export class socialSecurityNumber1621405216299 implements MigrationInterface {
    name = 'socialSecurityNumber1621405216299'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "social_security_number" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "social_security_number"`);
    }

}
