import { MigrationInterface, QueryRunner } from 'typeorm';

export class Blacklist1621497908166 implements MigrationInterface {
  name = 'Blacklist1621497908166';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "blacklist" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "data_seeker_uuid" character varying NOT NULL, "data_seeker_name" character varying NOT NULL, "data_seeker_public_url" character varying NOT NULL, "blacklisted_by_id" integer NOT NULL, CONSTRAINT "PK_04dc42a96bf0914cda31b579702" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "blacklist" ADD CONSTRAINT "FK_88007aa2318bc77eeb17013e20e" FOREIGN KEY ("blacklisted_by_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "blacklist" DROP CONSTRAINT "FK_88007aa2318bc77eeb17013e20e"`,
    );
    await queryRunner.query(`DROP TABLE "blacklist"`);
  }
}
