import {MigrationInterface, QueryRunner} from "typeorm";

export class AddingTimestampToMessages1623157662894 implements MigrationInterface {
    name = 'AddingTimestampToMessages1623157662894'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" ADD "read_at" TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "message" ADD "timestamp" TIMESTAMP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "timestamp"`);
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "read_at"`);
    }

}
