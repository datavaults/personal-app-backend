import parse from 'csv-parse/lib/sync';
// import parse from 'csv-parse';
import fs from 'fs';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class seedSkill1621317242275 implements MigrationInterface {
  camelToSnakeCase = (element) => {
    for (const [key, prop] of Object.entries(element)) {
      const replace = (str) =>
        str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);
      element[replace(key)] = prop;
      delete element[key];
    }
  };
  public async up(queryRunner: QueryRunner): Promise<void> {
    const csvParseOptions = {
      auto_parse: true, // Ensures that numeric values remain numeric
      columns: true,
      delimiter: ',',
      quote: '"',
      relax: true,
      rowDelimiter: '', // This is an issue, I had to set the \n here as 'auto' wasn't working, nor was 'windows'.  Maybe look at auto-detecting line endings?
      skip_empty_lines: true,
      escape: '"',
    };
    let csv;
    try {
      csv = fs.readFileSync('./src/seeds/skills_en.csv', 'utf8');
    } catch (error) {
      csv = fs.readFileSync('./dist/seeds/skills_en.csv', 'utf8');
    }
    const records = parse(csv, csvParseOptions);
    for (let index = 0; index < records.length; index++) {
      const element = records[index];
      this.camelToSnakeCase(element);
      await queryRunner.manager
        .createQueryBuilder()
        .insert()
        .into('skill')
        .values(element)
        .execute();
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
