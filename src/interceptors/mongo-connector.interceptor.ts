import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserData } from '@suite5/core/authentication/decorators';
import { VaultService } from '@suite5/vault';
import { Request } from 'express';
import mongoose from 'mongoose';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class MongoInterceptor implements NestInterceptor {
  constructor(
    private readonly configService: ConfigService,
    private readonly vaultService: VaultService,
  ) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const request: Request = context.switchToHttp().getRequest();

    let mongoUri: string = this.configService.get<string>('mongo.uri');
    if (request.url !== '/api/auth/keycloakLogin') {
      const userId: string = `${(request.user as UserData).id}`;
      const credentials = await this.vaultService.getSecret(
        'user',
        userId,
        'mongo',
        this.configService.get<string>('vault.vaultRootToken'),
      );
      mongoUri = `mongodb://${credentials.name}:${
        credentials.password
      }@${this.configService.get<string>(
        'mongo.dbHost',
      )}:${this.configService.get<string>('mongo.dbPort')}/${credentials.name}`;
    }

    let connection = null;
    try {
      connection = await mongoose.createConnection(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    } catch (e) {
      console.error(JSON.stringify(e));
      throw e;
    }
    request['mongoConnection'] = connection;
    return next.handle().pipe(
      tap(async () => {
        if (connection) {
          await connection.close();
        }
      }),
    );
  }
}
