import { registerAs } from '@nestjs/config';

export default registerAs('analytics', () => ({
  url: process.env.ANALYTICS_URL,
}));
