import { registerAs } from '@nestjs/config';
import { hostname } from 'os';

export default registerAs('app', () => ({
  name: process.env.APP_NAME,
  hostname: process.env.APP_HOSTNAME || hostname(),
  description: process.env.APP_DESCRIPTION,
  port: parseInt(process.env.APP_PORT, 10),
  get isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
  },
  get isDevelopment(): boolean {
    return process.env.NODE_ENV !== 'production';
  },
}));
