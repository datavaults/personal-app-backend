import appConfig from './app.config';
import emailConfig from './email.config';
import mongoConfig from './mongo.config';
import postgresConfig from './postgres.config';
import analyticsConfig from './analytics.config';

export default [
  appConfig,
  postgresConfig,
  mongoConfig,
  emailConfig,
  analyticsConfig,
];
