import { registerAs } from '@nestjs/config';
import { hostname } from 'os';

export default registerAs('email', () => ({
  mock: process.env.EMAIL_MOCK || false,
  host: process.env.EMAIL_HOST,
  secure: process.env.EMAIL_SECURE || hostname(),
  user: process.env.EMAIL_USER,
  pass: process.env.EMAIL_PASS,
  port: parseInt(process.env.EMAIL_PORT, 465),
  supportAddress: process.env.EMAIL_SUPPORT_ADDRESS,
}));
