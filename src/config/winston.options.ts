import { format, level, transports } from 'winston';

const colorizer = format.colorize();

// const production = {
//   level: process.env.APP_LOG_LEVEL || 'debug',
//   transports: [
//     new transports.File({
//       filename: 'logs/app.log',
//       format: format.combine(
//         format.timestamp(),
//         format.errors({ stack: true }),
//         format.splat(),
//         format.json(),
//       ),
//     }),
//     new transports.File({
//       filename: 'logs/error.log',
//       level: 'error',
//       format: format.combine(
//         format.timestamp(),
//         format.errors({ stack: true }),
//         format.splat(),
//         format.json(),
//       ),
//     }),
//     new transports.Console({
//       format: format.combine(
//         format((info) => {
//           info.level = info.level.toUpperCase();
//           return info;
//         })(),
//         format.timestamp(),
//         format.colorize(),
//         format.align(),
//         format.printf((info) => {
//           const { timestamp, level, message, context, trace } = info;
//           return `${timestamp} [${level}] [${colorizer.colorize(
//             'info',
//             context,
//           )}] ${message} ${trace ? trace : ''}`;
//         }),
//       ),
//     }),
//   ],
// };

const development = {
  level: 'debug',
  transports: [
    new transports.Console({
      format: format.combine(
        format((info) => {
          info.level = info.level.toUpperCase();
          return info;
        })(),
        format.timestamp(),
        format.colorize(),
        format.align(),
        format.printf((info) => {
          const { timestamp, level, message, context, trace } = info;
          return `${timestamp} [${level}] [${colorizer.colorize(
            'verbose',
            context,
          )}] ${message} ${trace ? trace : ''}`;
        }),
      ),
    }),
  ],
};

const accessLog = {
  transports: [
    new transports.File({ filename: 'logs/access.log' }),
    new transports.Console({
      format: format.combine(
        format((info) => {
          info.level = info.level.toUpperCase();
          return info;
        })(),
        format.colorize(),
        format.timestamp(),
        format.align(),
        format.printf((info) => {
          const { timestamp, level, message } = info;
          return `${timestamp} [${level}] [${format
            .colorize()
            .colorize('warn', 'AccessLog')}] ${message}`;
        }),
      ),
    }),
  ],
  format: format.combine(
    format((info) => {
      info.level = info.level.toUpperCase();
      return info;
    })(),
    format.timestamp(),
    format.align(),
    format.printf((info) => {
      const { timestamp, level, message } = info;
      return `${timestamp} [${level}] [AccessLog] ${message}`;
    }),
  ),
  statusLevels: {},
  meta: true,
  msg: 'HTTP {{req.method}} {{req.statusCode}} {{res.responseTime}}ms {{req.url}}',
  expressFormat: true,
  colorize: false,
};

// export { accessLog, development, production };
export { accessLog, development };
