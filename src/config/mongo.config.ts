import { registerAs } from '@nestjs/config';

export default registerAs('mongo', () => ({
  uri: process.env.MONGO_URI || 'mongodb://localhost:27017/datavaults',
  dbName: process.env.MONGO_NAME || 'datavault',
  dbHost: process.env.MONGO_HOST || 'mongodb',
  dbPort: process.env.MONGO_PORT || 27017,
}));
