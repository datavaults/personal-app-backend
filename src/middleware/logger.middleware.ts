import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as expressWinston from 'express-winston';
import { accessLog } from '../config/winston.options';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private readonly handler;

  constructor() {
    this.handler = expressWinston.logger(accessLog);
  }

  use(req: Request, res: Response, next: NextFunction): void {
    return this.handler(req, res, next);
  }
}
