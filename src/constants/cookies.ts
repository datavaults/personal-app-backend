export enum Cookies {
  Token = 'token',
  RefreshToken = 'refresh_token',
}
