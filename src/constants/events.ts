export enum Events {
  UpdateUserProfile = 'updateUserProfile',
  UserCreated = 'userCreated',
  UpdateSharedAsset = 'updateSharedAsset',
  DeleteUser = 'deleteUser',
}
