import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccessPoliciesModule } from '@suite5/access-policies';
import { AssetModule } from '@suite5/asset';
import { CommonModule } from '@suite5/common/common.module';
import { CoreModule } from '@suite5/core';
import { EncryptionModule } from '@suite5/encryption/src';
import { MessageModule } from '@suite5/message';
import { MobileAppModule } from '@suite5/mobile-app';
import { VaultModule } from '@suite5/vault';
import { WalletModule } from '@suite5/wallet';
import { RiskManagementModule } from 'modules/risk-management/risk-management.module';
import { resolve } from 'path';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import configuration from './config';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: resolve(process.cwd(), '.env'),
      load: [...configuration],
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('postgres.host'),
        port: config.get<number>('postgres.port'),
        username: config.get<string>('postgres.username'),
        password: config.get<string>('postgres.password'),
        database: config.get<string>('postgres.name'),
        synchronize: false,
        autoLoadEntities: true,
        namingStrategy: new SnakeNamingStrategy(),
      }),
    }),
    GraphQLModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        debug: config.get<boolean>('app.isDevelopment'),
        playground: config.get<boolean>('app.isDevelopment'),
        autoSchemaFile: resolve(__dirname, '__snapshots__/schema.gql'),
        context: ({ req, res }) => ({ req, res }),
      }),
      inject: [ConfigService],
    }),
    CommonModule,
    CoreModule,
    AccessPoliciesModule,
    AssetModule,
    MessageModule,
    VaultModule,
    MobileAppModule,
    RiskManagementModule,
    EncryptionModule,
    WalletModule,
  ],
  controllers: [],
})
export class AppModule {}
