/* eslint-disable @typescript-eslint/no-var-requires */
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { WinstonModule } from 'nest-winston';
import { AppModule } from './app.module';
import { development } from './config/winston.options';
import { EntityNotFoundFilter } from './filters';
import cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';

require('dotenv').config({ path: `../${process.env.NODE_ENV}.env` });

async function bootstrap() {
  const loggerOptions: any = development;
  // if (process.env.NODE_ENV === 'production') {
  //   loggerOptions = production;
  //   require('dotenv').config(); // eslint-disable-line
  //   Sentry.init({
  //     dsn: process.env.APP_SENTRY_DSN,
  //     release: process.env.APP_VERSION,
  //   });
  // }
  // const app = await NestFactory.create(AppModule);
  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: WinstonModule.createLogger(loggerOptions),
  });
  // the next two lines did the trick
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  const config = app.get<ConfigService>(ConfigService);
  app.setGlobalPrefix('api');

  // Setup OpenAPI/Swagger
  if (config.get('app.isDevelopment')) {
    const options = new DocumentBuilder()
      .setTitle(config.get('app.name'))
      .setDescription(config.get('app.description'))
      .setVersion('1.0')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document, {
      swaggerOptions: {
        docExpansion: false,
        defaultModelsExpandDepth: -1,
      },
    });
  }

  // Add global validation pipe
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  // Add global interceptors
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  // Add global filters
  app.useGlobalFilters(new EntityNotFoundFilter());

  app.use(cookieParser());

  await app.listen(config.get('app.port'));
}
bootstrap();
