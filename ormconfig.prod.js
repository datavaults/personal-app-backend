/* eslint-disable @typescript-eslint/no-var-requires */
const SnakeNamingStrategy =
  require('typeorm-naming-strategies').SnakeNamingStrategy;

module.exports = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  database: process.env.POSTGRES_NAME,
  username: process.env.POSTGRES_USERNAME,
  password: process.env.POSTGRES_PASSWORD,
  syncrhonize: false,
  namingStrategy: new SnakeNamingStrategy(),
  migrations: ['/app/dist/migrations/*.js'],
};
