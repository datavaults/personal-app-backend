import { resolve } from 'path';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({ path: resolve(__dirname, '.env.template') });
}

// Calculate migrations path, relative to current directory
const migrationsDir = resolve(__dirname, 'src', 'seeds').replace(
  `${process.cwd()}/`,
  '',
);

module.exports = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  database: process.env.POSTGRES_NAME,
  username: process.env.POSTGRES_USERNAME,
  password: process.env.POSTGRES_PASSWORD,
  syncrhonize: false,
  namingStrategy: new SnakeNamingStrategy(),
  entities: [resolve(__dirname, 'dist', 'src', 'pg.entities.js')],
  migrations: [resolve(__dirname, 'src', 'seeds/**.ts')],
  cli: {
    migrationsDir,
  },
};
