const Axios = require('axios');
const { resolve } = require('path');
const mongoose = require('mongoose');

const getAdminCredentials = async () => {
    require('dotenv').config({ path: resolve(__dirname, '..', '.env') });
    var options = {
        method: 'GET',
        url: `${process.env.VAULT_URL}/secret/user/admin/mongo`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.VAULT_ROOT_TOKEN}`,
        },
    };

    try {
        const { data: response } = await Axios.request(options);
        return response.data;
    } catch {
        return false;
    }
};

const deleteSecret = async (userId) => {
    require('dotenv').config({ path: resolve(__dirname, '..', '.env') });
    var options = {
        method: 'DELETE',
        url: `${process.env.VAULT_URL}/secret/user/${userId}/mongo`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.VAULT_ROOT_TOKEN}`,
        }
    };

    try {
        const response = await Axios.request(options);
        return response.status === 204;
    } catch {
        console.log('user not created');
        return false;
    }
};

const deleteExistingUser = async (userId) => {
    const admin = await getAdminCredentials();
    const username = `user${userId}`
    console.log(`Deleting secret of ${username}...`)
    await deleteSecret(userId)

    uri = `mongodb://${admin.username}:${admin.password}@${admin.host}/${admin.name}`;
    conn = await mongoose.createConnection(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    const userDb = conn.useDb(username);

    try {
        console.log(`Deleting user${userId}...`)
        var userDropped = await userDb.db.command({
            dropUser: username
        });
        if (userDropped.ok === 1) {
            console.log('User dropped: ' + username);
        }
    } catch (err) {
        if (err.codeName === "UserNotFound") {
            console.log("User not found")
        } else {

            console.log('Error deleting user');
        }
    }

    console.log("Dropping database...")
    await userDb.dropDatabase()
    console.log("Database dropped")
    await conn.close();
}

deleteExistingUser(1);
