#!/bin/bash
pwd
echo -ne '#                   (5%)\r'
docker-compose up -d 
echo -ne '##                   (10%)\r'
sleep 1
echo -ne '####                 (20%)\r'
sleep 4
echo -ne '########             (40%)\r'
sleep 4
docker-compose exec vault sh -c  'export VAULT_ADDR=http://localhost:8200; unset VAULT_TOKEN; vault operator init -n 1 -t 1' > .init.txt
unset VAULT_ROOT_TOKEN;
unset VAULT_KEY;
export VAULT_ROOT_TOKEN=`cat .init.txt | tr -dc '[:print:]' | sed 's/\[0m/\n/g' | sed -n 's/Initial Root Token: \(.*\)/\1/p'`
export VAULT_KEY=`cat .init.txt | tr -dc '[:print:]' | sed 's/\[0m/\n/g' | sed -n 's/Unseal Key 1: \(.*\)/\1/p'`
rm .init.txt
echo $VAULT_ROOT_TOKEN;
echo $VAULT_KEY;
if [ ! -z "$VAULT_KEY" ]
then
    sed -i -e 's|VAULT_ROOT_TOKEN=.*|VAULT_ROOT_TOKEN='${VAULT_ROOT_TOKEN}'|g' .env
    sed -i -e 's|VAULT_KEY=.*|VAULT_KEY='${VAULT_KEY}'|g' .env
fi  
cat .env
export $(egrep  'VAULT_' .env | xargs)
echo "";
echo $VAULT_ROOT_TOKEN;
echo $VAULT_KEY;
echo "";
docker-compose exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault operator unseal ${VAULT_KEY}"
docker-compose exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault login ${VAULT_ROOT_TOKEN}"
docker-compose exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault secrets enable -path=secret -version=1 kv"


echo -ne '############         (60%)\r'
export $(egrep  'MONGO_' .env | xargs)
docker-compose exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault kv put secret/user/admin/mongo host='${MONGO_HOST}:${MONGO_PORT}' name='${MONGO_NAME}' username='${MONGO_USERNAME}' password='${MONGO_PASSWORD}' authMechanism='SCRAM-SHA-1'"
docker-compose exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault kv get secret/user/admin/mongo"
docker-compose exec mongodb sh -c "mongo ${MONGO_NAME} --host ${MONGO_HOST} --authenticationDatabase admin -u root -p ${MONGO_INIT_ROOT_PASSWORD} --eval \"db.createUser( {user: '${MONGO_USERNAME}', pwd: '${MONGO_PASSWORD}', roles: [{role: 'userAdminAnyDatabase' , db:'admin'},{role: 'readWriteAnyDatabase' , db:'admin'},{role: 'clusterAdmin' , db:'admin'}, 'readWrite'], mechanisms:[ 'SCRAM-SHA-1'] });\""
docker-compose exec mongodb sh -c "mongo ${MONGO_NAME} --host ${MONGO_HOST} --authenticationDatabase admin -u root -p ${MONGO_INIT_ROOT_PASSWORD}  --eval \"db.getUsers();\""


echo -ne '##############       (70%)\r'
export $(egrep  'RABBITMQ_' .env | xargs)
docker-compose exec rabbitmq sh -c "rabbitmqctl delete_user $RABBITMQ_DEFAULT_USER"
docker-compose exec rabbitmq sh -c "rabbitmqctl add_user ${RABBITMQ_DEFAULT_USER} ${RABBITMQ_DEFAULT_PASS}"
docker-compose exec rabbitmq sh -c "rabbitmqctl set_user_tags ${RABBITMQ_DEFAULT_USER} administrator"
docker-compose exec rabbitmq sh -c 'rabbitmqctl set_permissions -p / '${RABBITMQ_DEFAULT_USER}'  ".*" ".*" ".*"'


echo -ne '################     (80%)\r'
yarn install
yarn pg:migration:run

echo -ne '##################   (90%)\r'
echo "please run the below command if you running it for the first time:"
echo ""
echo "                                                                  yarn pg:migration:run:seed"
echo ""

echo -ne '#################### (100%)\r'
echo -ne '\n'