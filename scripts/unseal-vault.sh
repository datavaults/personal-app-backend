export $(egrep  'VAULT_' .env | xargs)
echo "";
echo $VAULT_ROOT_TOKEN;
echo $VAULT_KEY;
echo "";
docker-compose  exec vault sh -c "export VAULT_ADDR=http://localhost:8200; vault operator unseal ${VAULT_KEY}"